<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
include_once 'bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
$date = filter_input(INPUT_GET, 'date');
$res = new response();
if (isset($_GET['date'])) {
    $res = $appm->GetMyScheduleByDate($date);
    //echo "test";
} else {
    header("Location: ../");
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    รายละเอียดตารางนัดหมาย
                    <a href="Planner/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปตารางนัดหมาย</div></a>
                </div>
                <div class="date-title"><?= thaidate_withouttime_short($_GET['date']) ?></div>
                <div class="btn-right"><a href="Planner/Select/<?= $_GET['date'] ?>/" data-toggle="tooltip" title="เพิ่มการนัดหมาย" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มการนัดหมาย</a></div>
                <div class="table-responsive" id="tableCustomer">
                    <table id="plannerTable" class="table table-vcenter table-condensed table-striped table-borderless" style="margin-top: 50px !important;">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">ลำดับที่</th>
                                <th class="text-center" style="width: 80px;">เวลา</th>
                                <th style="width: 150px;">ชื่อร้าน</th>
                                <th class="text-center" style="width: 100px;">ชื่อผู้ติดต่อ</th>
                                <th>ข้อมูลร้านค้า</th>
                                <th class="no-sort" style="width: 400px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            if ($res->MSGID === SERV_COMPLETE) {
                                //service::printr($res);
                                foreach ($res->MSGDATA1 as $data) {
                                    $plan = new plan();
                                    $plan = $data;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $i ?></td>
                                        <td class="text-center"><?= $plan->PLANTIME; ?></td>
                                        <td><?= $plan->CUSTOMER_NAME; ?></td>
                                        <td class="text-center"><?= $plan->CUSTOMER_CONTACTNAME; ?></td>
                                        <td>
                                            ที่อยู่: <?= $plan->CUSTOMER_ADDRESS; ?><br>
                                            เบอร์โทรศัพท์: <?= $plan->CUSTOMER_CONTACTPHONE; ?><br>
                                            อีเมล์: <?= $plan->CUSTOMER_MAIL; ?>
                                        </td>
                                        <td class="text-right">
                                            <?php
                                            $option = '';
                                            if ($plan->STATUS != PLAN_STATUS_NEW) {
                                                $option = 'disabled';
                                            }
                                            ?>
                                            <a href="Planner/Record/<?= $_GET['date'] ?>/<?= $plan->PLANID; ?>/" data-toggle="tooltip" title="บันทึกผลการเข้าพบลูกค้า" class="btn btn-effect-ripple btn-xs btn-info <?= $option; ?>" style="margin: 0"><i class="fa fa-file-text-o"></i> บันทึกผลการเข้าพบ</a>
                                            <a href="Planner/History/<?= $_GET['date'] ?>/<?= $plan->CUSTOMER_ID; ?>/" data-toggle="tooltip" title="ประวัติการนัดหมาย" class="btn btn-effect-ripple btn-xs btn-primary" style="margin: 0"><i class="gi gi-eye_open"></i> ประวัติการนัดหมาย</a>
                                            <a href="Planner/Edit/<?= $_GET['date'] ?>/<?= $plan->PLANID; ?>/" data-toggle="tooltip" title="แก้ไขข้อมูลการนัดหมาย" class="btn btn-effect-ripple btn-xs btn-warning <?= $option; ?>" style="margin: 0" ><i class="gi gi-pencil"></i> แก้ไข</a>
                                            <a href="#"  data-key="<?= $plan->PLANID; ?>" data-toggle="tooltip" title="ลบข้อมูลลูกค้า" class="btn btn-effect-ripple btn-xs btn-danger <?= $option; ?> btnDeactive" style="margin: 0"><i class="gi gi-bin"></i> ลบ</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td>Nodata</td>
                                </tr>
                                <?php
                            }
                            ?>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
        UiTables.init();
    });</script>
<script>
    $('#plannerTable').dataTable({
        ordering: false,
        info: false,
        searching: false,
        lengthChange: false
    })
    $(document).ready(function () {
        $('.btnDeactive').on('click', function (e) {
            e.preventDefault();
            var plan = $(this).data('key').toString();
            bootbox.confirm({
                size: 'small',
                title: "ยืนยันการลบนัดหมาย",
                message: "คุณต้องการยืนยันลบการนัดหมายใช่หรือไม่ หากกดตกลงจะไม่สามารถย้อนกลับมาดำเนินการได้อีก",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> ยกเลิก',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> ยืนยันการลบ',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    var loading = "";
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "AppHttpRequest.php",
                            data: {req: '<?= REQ_DEACTIVE_PLAN; ?>', planid: plan},
                            dataType: "json",
                            beforeSend: function (xhr) {
                                loading = bootbox.dialog({
                                    size: 'small',
                                    message: '<p class="text-center">เรากำลังดำเนินการตามคำขอของท่าน กรุณารอสักครู่...</p>',
                                    closeButton: false
                                });
                            },
                            error: function (transport, status, errorThrown) {
                                console.log(transport.responseText);
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: transport.responseText,
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            },
                            success: function (data) {
                                console.log(data);
                                //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                if (data.MSGID === 100) {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "บันทึกข้อมูลการนัดหมายสำเร็จ",
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Planner/<?= $date; ?>/";
                                            }
                                        });
                                    }, 3000);
                                    location.reload();
                                } else {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: data.MSGMESSAGE1,
                                            title: "การแจ้งเตือน"
                                        });
                                    }, 3000);
                                }
                            }
                        });

                    }

                }
            });
        });
    });
</script>

<?php include 'inc/template_end.php'; ?>