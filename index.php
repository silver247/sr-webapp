<?php
session_start();
session_destroy();
?>
<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php require_once './bundle.php'; ?>
<!-- Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
<img src="img/bg_index.png" alt="Full Background" class="full-bg animation-pulseSlow">
<!-- END Full Background -->

<!-- Login Container -->
<div id="login-container">
    <!-- Login Header -->
    <h1 class="text-light text-center push-top-bottom animation-pullDown">
        <img src="img/logo_index.png">
    </h1>
    <!-- END Login Header -->

    <!-- Login Block -->
    <div class="block block-login animation-fadeInQuick">
        <!-- Login Form -->
        <form id="tform1" method="post" class="form-horizontal">
            <input type="hidden" name="req" value="<?= REQ_LOGIN; ?>">
            <div class="form-group">
                <label for="login-email" class="col-xs-12">ชื่อผู้ใช้งาน</label>
                <div class="col-xs-12">
                    <input type="text" id="usr" name="usr" class="form-control" placeholder="ชื่อผู้ใช้งานของคุณ" required>
                </div>
            </div>
            <div class="form-group">
                <label for="login-password" class="col-xs-12">รหัสผ่าน</label>
                <div class="col-xs-12">
                    <input type="password" id="pwd" name="pwd" class="form-control" placeholder="รหัสผ่านของคุณ" required>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-effect-ripple btn-sm btn-info btn-login">เข้าสู่ระบบ</button>
                </div>
            </div>
        </form>
        <!-- END Login Form -->
    </div>
    <!-- END Login Block -->

    <!-- Footer -->
    <footer class="text-muted text-center animation-pullUp">
        Click!! เข้าสู่ระบบได้เลย
    </footer>
    <!-- END Footer -->
</div>
<!-- END Login Container -->

<?php include 'inc/template_scripts.php'; ?>
<script>
    $(document).ready(function () {
        $('#tform1').on('submit', function (e) {
            e.preventDefault();
            if ($('#usr').val().length <= 0 || $('#pwd').val().length <= 0) {
                bootbox.alert({
                    size: 'small',
                    message: "กรุณากรอกข้อมูลให้ครบถ้วน",
                    title: "การแจ้งเตือน"
                });
                return;
            }
            $.ajax({
                method: "POST",
                url: "AppHttpRequest.php",
                data: $("#tform1").serialize(),
                dataType: "json",
                beforeSend: function (xhr) {
                    loading = bootbox.dialog({
                        size: 'small',
                        message: '<p class="text-center">เรากำลังลงชื่อเข้าใช้ในระบบ กรุณารอสักครู่...</p>',
                        closeButton: false
                    });
                },
                error: function (transport, status, errorThrown) {
                    setTimeout(function () {
                        loading.modal('hide');
                        bootbox.alert({
                            size: 'small',
                            message: "ไม่สามารถเข้าสู่ระบบได้ กรุณาติดต่อผู้ดูแลระบบ",
                            title: "การแจ้งเตือน"
                        });
                        return;
                    }, 3000);
                    console.log(transport.responseText);
                },
                success: function (data) {
                    console.log(data);

                    if (data.MSGID != '<?= SERV_COMPLETE; ?>') {
                        if (data.MSGID == '<?= SERV_FORCECHANGE; ?>') {
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: data.MSGMESSAGE1,
                                    title: "การแจ้งเตือน",
                                    callback: function () {
                                        window.location = 'ChangePassword/';
                                    }
                                });
                            }, 3000);
                        } else {
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: data.MSGMESSAGE1,
                                    title: "การแจ้งเตือน"
                                });
                            }, 3000);
                        }

                    } else {
                        setTimeout(function () {
                            loading.modal('hide');
                            bootbox.alert({
                                size: 'small',
                                message: data.MSGMESSAGE1,
                                title: "การแจ้งเตือน",
                                callback: function () {
                                    window.location = data.MSGMESSAGE2;
                                }
                            });
                        }, 3000);
                    }

                }
            });
        });
    });
</script>
<?php include 'inc/template_end.php'; ?>