<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of customer
 *
 * @author puwakitk
 */
class customer {
    //put your code here
    public $CUSTOMER_ID = "";
    public $CUSTOMER_NAME = "";
    public $CUSTOMER_ADDRESS = "";
    public $CUSTOMER_HOMEID = "";
    public $CUSTOMER_SOI = "";
    public $CUSTOMER_ROAD = "";
    public $CUSTOMER_SUBDIST = "";
    public $CUSTOMER_DIST = "";
    public $CUSTOMER_PROVINCE = "";
    public $CUSTOMER_PROVINCENAME = "";
    public $CUSTOMER_ZIPCODE = "";
    public $CUSTOMER_CONTACTNAME = "";
    public $CUSTOMER_CONTACTPHONE = "";
    public $CUSTOMER_MAIL = "";
    public $CUSTOMER_GRPID = "";
    public $CUSTOMER_GRPDESC = "";
    public $CUSTOMER_GRPCREDIT = 0;
    public $CUSTOMER_GRPDISCOUNT = 0;
    public $CUSTOMER_ACTIVE = 1;
    public $REGION_ID = "";
    public $REGION_DESC = "";
}
