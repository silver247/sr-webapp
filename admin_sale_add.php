<?php $pagename = "Sale" ?>
<?php
include 'inc/config_admin.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$action = filter_input(INPUT_GET, 'action');
$action = trim($action);
$appm = new AppManager();
$req = REQ_ADD_USER;
$user = new user();
$date = date('Y-m-d');
$readonly = "";
$userid = '';
if ($action == ACTION_EDIT) {
    $userid = filter_input(INPUT_GET, 'id');
    $userlist = $appm->GetUserDataList($userid);
    $customerlist = $appm->GetCustomerByUserID($userid);
    $user = $userlist->MSGDATA1[0];
    $req = REQ_EDIT_USER;
    $date = $user->STARTDATE;
    $readonly = " readonly";
    //service::printr($customerlist);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <form name="tform1" method="post" id="tform1" onsubmit="return false;">
                <input type="hidden" name="req" value="<?= $req; ?>">
                <input type="hidden" name="userid" value="<?= $userid; ?>">
                <div class="block full">
                    <div class="block-title">
                        <?= $_GET["action"] == "add" ? "เพิ่มข้อมูลพนักงานขาย" : "แก้ไขข้อมูลพนักงานขาย" ?> 
                        <a href="Admin/Sale/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าพนักงานขาย</div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ชื่อ <span style="color:red;">*</span> </label>
                                    <input class="form-control" type="text" name="tname" id="tname" value="<?= $user->TNAME; ?>" placeholder="ชื่อ" required="true"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>นามสกุล <span style="color:red;">*</span> </label>
                                    <input class="form-control" type="text" name="tsurname" id="tsurname" value="<?= $user->TLNAME; ?>" placeholder="นามสกุล" required="true"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ชื่อ(ภาษาอังกฤษ) <span style="color:red;">*</span> </label>
                                    <input class="form-control" type="text" name="ename" id="ename" value="<?= $user->ENAME; ?>" placeholder="Name" required="true"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>นามสกุล(ภาษาอังกฤษ) <span style="color:red;">*</span> </label>
                                    <input class="form-control" type="text" name="esurname" id="esurname" value="<?= $user->ELNAME; ?>" placeholder="Surname" required="true"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>username <span style="color:red;">*</span> </label>
                                    <input class="form-control" type="text" name="username" value="<?= $user->USERNAME; ?>" id="username" <?= $readonly; ?> required="true"/>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>วันที่เริ่มงาน <span style="color:red;">*</span> </label>
                                    <input type="text" id="start_date" name="start_date" readonly="true" class="form-control input-datepicker" value="" placeholder="วันที่เริ่มงาน" <?= $readonly; ?> required="true"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ภูมิภาคที่ดูแล <span style="color:red;">*</span> </label>
                                    <?php
                                    $readonly = "";
                                    if ($action == ACTION_EDIT) {
                                        $readonly = "disabled";
                                    }
                                    ?>
                                    <select id="region" name="region" class="select-select2" style="width: 100%;" data-placeholder="ภูมิภาคที่ดูแล" data-live-search="true" <?= $readonly; ?> required="true">
                                        <option></option>
                                        <?= $appm->GetDropdownRegion($user->REGIONID); ?>
                                    </select>
                                    <input type="hidden" id="region_val" name="region_val" value="<?= $user->REGIONID; ?>">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="customerTable">
                                    <table class="table table-vcenter table-condensed table-striped table-borderless">
                                        <thead>
                                            <tr>
                                                <th>ร้านค้า <span style="color:red;">*</span> </th>

                                                <th width="30px"></th>
                                            </tr>										
                                        </thead>
                                        <tbody id="customerTable">
                                            <?php
                                            if ($action == ACTION_EDIT) {
                                                $rowcnt = 0;
                                                $str = '';
                                                foreach ($customerlist->MSGDATA1 as $customer) {
                                                    $str = '';
                                                    $rowcnt++;
                                                    //service::printr($customerlist);
                                                    $str .= '<tr id="row' . $rowcnt . '" class="tableRow">';
                                                    $str .= '<td>';
                                                    $str .= '<select id="ddlCustomer' . $rowcnt . '" name="ddlCustomer[]" data-row="' . $rowcnt . '" class="select-select2" style="width: 100%;" data-placeholder="ค้นหารายชื่อลูกค้า" data-live-search="true" required="true">';
                                                    $str .= $appm->GetCustomerDropdownlistForCreateSale($customer->CUSTOMER_ID, $user->REGIONID);
                                                    $str .= '</select>';
                                                    $str .= '</td>';
                                                    $str .= '<td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>';
                                                    $str .= '</tr>';
                                                    echo $str;
                                                }
                                                echo '<input type="hidden" name="rowCustomerCnt" id="rowCustomerCnt" value="' . $rowcnt . '">';
                                            } else {
                                                ?>
                                            <input type="hidden" name="rowCustomerCnt" id="rowCustomerCnt" value="1">
                                            <tr id="row1" class="tableRow">
                                                <td>
                                                    <select id="ddlCustomer1" name="ddlCustomer[]" data-row="1" class="select-select2" style="width: 100%;" data-placeholder="ค้นหารายชื่อลูกค้า" data-live-search="true">
                                                        <option ></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->

                                                    </select>
                                                </td>

                                                <td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"><button type="button" class="btn btn-xs btn-primary" onClick="addField()"><i class="gi gi-circle_plus"></i> เพิ่มร้านค้า</button></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;">
                        <button type="submit"  id="btnSubmit" data-toggle="tooltip" title="บันทึกข้อมูลพนักงาน" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึกข้อมูลพนักงาน" aria-describedby="tooltip441840" ><i class="gi gi-circle_plus"></i> บันทึกข้อมูลพนักงาน</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
                                                        UiTables.init();
                                                    });</script>

<script>
    $(document).ready(function () {
        $('#region').on('change', function (e) {
            $('#region_val').val(this.value);
            $('#customerTable > tr').remove();
            $("#rowCustomerCnt").val(0);
            addField();
        });
        $('#btnSubmit').on('click', function (e) {
            e.preventDefault();
            if (!validateform.validate('tform1')) {
                bootbox.alert({
                    size: 'small',
                    message: "กรุณากรอกข้อมูลให้ครบถ้วน",
                    title: "การแจ้งเตือน"
                });
                return false;
            }
            bootbox.confirm({
                size: 'small',
                title: "ยืนยันการบันทึกข้อมูลพนักงาน",
                message: "คุณต้องการยืนยันการบันทึกข้อมูลพนักงานใช่หรือไม่",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> ยกเลิก',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> ยืนยันการสร้างบันทึกข้อมูลพนักงาน',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    var loading = "";
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "AppHttpRequest.php",
                            data: $("#tform1").serialize(),
                            dataType: "json",
                            beforeSend: function (xhr) {
                                loading = bootbox.dialog({
                                    size: 'small',
                                    message: '<p class="text-center">เรากำลังบันทึกข้อมูลพนักงานของท่าน กรุณารอสักครู่...</p>',
                                    closeButton: false
                                });
                            },
                            error: function (transport, status, errorThrown) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลพนักงานไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                    //loading.find('.bootbox-body').html("error : " + errorThrown + "detail : " + transport.responseText);
                                }, 3000);
                                console.log(transport.responseText);
                            },
                            success: function (data) {
                                console.log(data);
                                loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                if (data.MSGID === 100) {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "บันทึกข้อมูลพนักงานสำเร็จ",
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Admin/Sale/";
                                            }
                                        });
                                    }, 3000);
                                } else {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "บันทึกข้อมูลพนักงานไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                            title: "การแจ้งเตือน"
                                        });
                                    }, 3000);
                                }
                            }
                        });

                    }

                }
            });
        });
        moment.locale();
        $('#start_date').datepicker('update', new Date(moment('<?= $date; ?>').add(1, 'days')));
    });
    /*function ValidateInput() {
        var chk = true;
        $('#tform1').find(':input').each(function (key, value) {
            if (this.type != 'hidden' && this.required && !this.disabled) {
                if (!this.value.length) {
                    chk = false;
                    return chk;
                }
            }

        });
        return chk;
    }*/
    $('#orderdate').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 0,
        language: "th-th"
    });
    $('#start_date').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        language: "th-th"
    });

</script>

<script type="text/javascript" >
    function addField() {
        var cnt = $("#rowCustomerCnt").val();
        cnt++;
        $("#rowCustomerCnt").val(cnt);
        var opt = "";
        //var opt = "<?= $appm->GetCustomerDropdownlist(); ?>";
        $.ajax({
            type: "GET",
            url: "admin_sale_add_table.php",
            //data: {search: search, month: month, year: year},
            data: {region: $('#region_val').val()},
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                opt = data;
                $('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlCustomer' + cnt + '" name="ddlCustomer[]" data-row="' + cnt + '"  class="select-select2" style="width: 100%;" data-placeholder="ค้นหาชื่อลูกค้า"  data-live-search="true" required="true"><option></option>' + opt + '</select></td><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#customerTable'));
                $.getScript('js/app.js');
            }
        });



    }
    $(document).on('click', '.remove-btn', function (events) {
        $(this).parents('tr').remove();
        if ($('#customerTable').children('tr').length <= 0) {
            $("#rowCustomerCnt").val(0);
        }
    });
</script>

<?php include 'inc/template_end.php'; ?>