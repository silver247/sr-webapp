<?php $pagename = "Summary" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
$productid = filter_input(INPUT_GET, 'productid');
$month = filter_input(INPUT_GET, 'month');
$year = filter_input(INPUT_GET, 'year');
$dataobject = $appm->GetMyProductBuyers($productid,$year,$month);
$productobject = $appm->GetMyProductDetail($productid);
$product =  $productobject->MSGDATA1[0];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    รายละเอียดการสั่งซื้อ
                    <a href="Summary/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าสรุปยอดขาย</div></a>
                </div>
                <div class="block-option">
                    <table>
                        <tr>
                            <th width="70px">สินค้ารุ่น</th>
                            <td width="150px"><?=$product->PRODUCT_NAME;?></td>
                            <th width="50px">เดือน</th>
                            <td width="150px"><?= GetThaiMonthDesc($month) ?> <?= ConvertToThaiYear($year) ?></td>
                        </tr>
                    </table>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableSum">
                        <table id="sumTable" class="table table-vcenter table-condensed table-striped table-borderless">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">ลำดับที่</th>
                                    <th class="text-center" style="width: 100px;">วันที่สั่งซื้อ</th>
                                    <th class="text-left">ร้าน</th>
                                    <th class="text-center" style="width: 70px;">จำนวน</th>
                                    <th class="text-center">หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($dataobject->MSGID == SERV_COMPLETE){
                                    $i = 1;
                                    foreach($dataobject->MSGDATA1 as $row){
                                        $order = new order();
                                        $order = $row;
                                        $str = '';
                                        $str .= '<tr>';
                                        $str .= '<td class="text-center">'.$i.'</td>';
                                        $str .= '<td class="text-center">'.$order->TIMESTAMP.'</td>';
                                        $str .= '<td>'.$order->CUSTOMER_NAME.'</td>';
                                        $str .= '<td  class="text-center">'.$order->AMOUNT.'</td>';
                                        $str .= '<td class="text-center">'.$order->STATUS_DESC.'</td>';
                                        $str .= '</tr>';
                                        echo $str;
                                        $i++;
                                    }
                                }
                                ?>
                                
                                
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
        UiTables.init();
    });</script>
<script>
    $('#sumTable').dataTable({
        ordering: false,
        info: false,
        searching: false,
        lengthChange: false
    })
</script>

<?php include 'inc/template_end.php'; ?>