<?php
include_once './bundle.php';
$appm = new AppManager();
$total = filter_input(INPUT_GET, 'total');
$query = filter_input(INPUT_GET, 'query');
$sortby = filter_input(INPUT_GET, 'sortby');
$month = filter_input(INPUT_GET, 'month');
$year = filter_input(INPUT_GET, 'year');
if ($sortby == "Product") {
    $descriptionobject = $appm->GetMyProductDetail($query);
    $description = $descriptionobject->MSGDATA1[0]->PRODUCT_NAME;
    $dataobject = $appm->GetMyProductBuyers($query, $year, $month);
    $datalist = $dataobject->MSGDATA1;
} else if ($sortby == "Shop") {
    $descriptionobject = $appm->GetMyCustomer($query);
    $description = $descriptionobject->MSGDATA1[0]->CUSTOMER_NAME;
    $dataobject = $appm->GetMyOrderHistory($query, $month, $year,$_SESSION['usrid']);
    $datalist = $dataobject->MSGDATA1;
    //service::printr($datalist);
} else {
    
}
?>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <form class="push">
            <div class="modal-header">
                <div class="block-title">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <i class="gi gi-eye_open"></i> <?= $_GET['sortby'] == "Product" ? "ร้านค้าที่สั่งซื้อ" : "รายการสั่งซื้อ" ?>
                </div>
            </div>
            <div class="block modal-body">
                <div class="block-option">
                    <table width="100%">
                        <tr>
                            <td width="150px">อันดับที่ <?= $_GET['range'] ?></td>
                            <td><?= $description; ?></td>
                            <?php if ($_GET['sortby'] == "Product") { ?><td class="text-right">จำนวนทั้งหมด <?= $total; ?> ชิ้น</td><?php } ?>
                            <?php if ($_GET['sortby'] == "Shop") { ?><td class="text-right">จำนวนเงินทั้งหมด <?= $total; ?> บาท</td><?php } ?>
                        </tr>
                    </table>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive">
                        <table id="ratingTable" class="table table-vcenter table-condensed table-striped table-borderless">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 100px;">ลำดับที่</th>
                                    <th><?= $_GET['sortby'] == "Product" ? "ร้านค้า" : "รายการสั่งซื้อ" ?></th>
                                    <?php if ($_GET['sortby'] == "Product") { ?><th class="text-center" style="width: 100px;">จำนวน</th><?php } ?>
                                    <th class="text-right" style="width: 100px;">จำนวนเงิน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($_GET['sortby'] == "Shop") {
                                    $i = 1;
                                    foreach ($datalist as $data) {
                                        //service::printr($data);
                                        ?>
                                        <tr <?= $i < 4 ? "class='strong'" : "" ?> <?= $i == 1 ? "style='color:#03B734'" : ($i == 2 || $i == 3 ? "style='color:#5ccdde'" : "") ?>>
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td><?= $data->TIMESTAMP; ?></td>
                                            <td class="text-right"><?= $data->TOTAL; ?>.-</td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else if ($_GET['sortby'] == "Product") {
                                    $i = 1;
                                    foreach ($datalist as $data) {
                                        //service::printr($data);
                                        ?>
                                        <tr <?= $i < 4 ? "class='strong'" : "" ?> <?= $i == 1 ? "style='color:#03B734'" : ($i == 2 || $i == 3 ? "style='color:#5ccdde'" : "") ?>>
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td><?= $data->CUSTOMER_NAME; ?></td>
                                            <td class="text-center"><?= $data->AMOUNT; ?></td>
                                            <td class="text-right"><?= $data->TOTAL; ?>.-</td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                } else {
                                    
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>