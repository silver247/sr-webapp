<?php $pagename = "Customer" ?>
<?php
include 'inc/config.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    รายชื่อลูกค้า
                </div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:35px;"></label>
                    <div class="inlineDiv">
                        <!--<input class="form-control" type="text" id="search" name="search" style="width:200px" placeholder="ค้นหาข้อมูล" />-->
                    </div>
                </div>

                <div class="block full">
                    <div class="btn-right"><a href="Customer/Add/" data-toggle="tooltip" title="เพิ่มข้อมูลลูกค้า" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มข้อมูลลูกค้า</a></div>
                    <div class="table-responsive" id="tableCustomer"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
    $(document).ready(function () {
        tableCustomer($('#search').val());

        $(document).on('click', '#btnCancel', function (e) {
            e.preventDefault();
            var order = $(this).data('key').toString();
            bootbox.confirm({
                size: 'small',
                title: "ยืนยันการลบลูกค้า",
                message: "คุณต้องการยืนยันการลบลูกค้าใช่หรือไม่",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> ยกเลิก',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> ยืนยันการลบลูกค้า',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    var loading = "";
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "AppHttpRequest.php",
                            data: {req: '<?= REQ_DEL_CUSTOMER; ?>', customerid: order},
                            dataType: "json",
                            beforeSend: function (xhr) {
                                loading = bootbox.dialog({
                                    size: 'small',
                                    message: '<p class="text-center">เรากำลังบันทึกข้อมูลการลบเอกสารขายของท่าน กรุณารอสักครู่...</p>',
                                    closeButton: false
                                });
                            },
                            error: function (transport, status, errorThrown) {
                                console.log(transport.responseText);
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: transport.responseText,
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            },
                            success: function (data) {
                                console.log(data);
                                //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                if (data.MSGID === 100) {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "บันทึกข้อมูลการนัดหมายสำเร็จ",
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Customer/";
                                            }
                                        });
                                    }, 3000);
                                } else {
                                    setTimeout(function () {
                                        bootbox.alert({
                                            size: 'small',
                                            message: data.MSGMESSAGE1,
                                            title: "การแจ้งเตือน"
                                        });
                                    }, 3000);
                                }
                            }
                        });

                    }

                }
            });
        });
    });
</script>
<script>
    function tableCustomer(search) {
        //alert(shop+month+year);

        $.ajax({
            type: "GET",
            url: "sale_customer_table.php",
            //data: {search: search},
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var show = "";
                show += '<table id="customerTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>'
                show += '<th style="width: 150px;">ชื่อร้าน</th>'
                show += '<th class="text-center" style="width: 100px;">ชื่อผู้ติดต่อ</th>'
                show += '<th>ข้อมูลร้านค้า</th>'
                show += '<th class="text-center" style="width: 100px;">กลุ่มลุกค้า</th>'
                show += '<th class="text-center" style="width: 100px;">Credit Term</th>'
                show += '<th class="no-sort" style="width: 150px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'
                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>';
                        show += '<td class="text-center">' + i + '</td>';
                        show += '<td>' + value.CUSTOMER_NAME + '</td>';
                        show += '<td>' + value.CUSTOMER_CONTACTNAME + '</td>';
                        show += '<td>';
                        show += 'ที่อยู่: ' + value.CUSTOMER_ADDRESS + '<br>';
                        show += '<table><tr><td width="180px">เบอร์โทรศัพท์: ' + value.CUSTOMER_CONTACTPHONE + '</td><td>อีเมล์: ' + value.CUSTOMER_MAIL + '</td></tr></table>';
                        show += '</td>';
                        show += '<td class="text-center">' + value.CUSTOMER_GRPDESC + '</td>';
                        show += '<td class="text-center">' + value.CUSTOMER_GRPCREDIT + '</td>';
                        show += '<td class="text-right">';

                        var disabled = '';
                        if (value.CUSTOMER_ACTIVE == '<?= CUSTOMER_STATUS_INACTIVE; ?>') {
                            disabled = 'disabled';
                        }
                        show += '<a href="Customer/Edit/' + value.CUSTOMER_ID + '/" data-toggle="tooltip" title="แก้ไขข้อมูลลูกค้า" class="btn btn-effect-ripple btn-xs btn-warning ' + disabled + '"><i class="gi gi-pencil "></i> แก้ไข</a>';
                        show += '<a href="#" id="btnCancel" data-key="' + value.CUSTOMER_ID + '" data-toggle="tooltip" title="ลบข้อมูลลูกค้า" class="btn btn-effect-ripple btn-xs btn-danger ' + disabled + '"><i class="gi gi-bin"></i> ลบ</a>';
                        show += '</td>';
                        show += '</tr>';
                        i++;
                    });

                }

                show += '</tbody>'
                show += '</table>'

                $('#tableCustomer').html(show);
                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });
                $('#customerTable').dataTable({
                    ordering: true,
                    info: false,
                    searching: true,
                    lengthChange: false,
                    columnDefs: [
                        {targets: 6, orderable: false}
                    ]
                });
            }
        });
    }
</script>

<?php include 'inc/template_end.php'; ?>