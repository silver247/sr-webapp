<?php

session_start();
require_once './bundle.php';
;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$req = filter_input(INPUT_POST, 'req');
//echo "hello req = " . $req;
if ($req === REQ_SALE_HOME_TOP5) {
    GetMyTop5();
} else if ($req === REQ_MYSCHEDULE_Month) {
    GetMyScheduleMonth();
} else if ($req === REQ_ADD_PLAN) {
    //echo "infunction";
    CreateNewPlan();
} else if ($req === REQ_ADD_PLAN_NEWC) {
    //echo "infunction";
    CreateNewPlanWithCustomer();
} else if ($req === REQ_DEACTIVE_PLAN) {
    DeActivePlan();
} else if ($req === REQ_ADD_ORDER_REFPLAN) {
    CreateNewOrderRefPlan();
} else if ($req === REQ_ADD_ORDER) {
    CreateNewOrder();
} else if ($req === REQ_DEL_ORDER) {
    DeleteOrder();
} else if ($req === REQ_ADD_CUSTOMER) {
    CreateNewCustomer();
} else if ($req === REQ_DEL_CUSTOMER) {
    InActiveCustomerStatus();
} else if ($req === REQ_EDIT_PLANHEADER) {
    EditPlanHeader();
} else if ($req === REQ_EDIT_ORDER) {
    EditOrder();
} else if ($req === REQ_EDIT_CUSTOMER) {
    EditCustomer();
} else if ($req == REQ_APPROVEREJECT_ORDER) {
    ApproveRejectOrder();
} else if ($req == REQ_DEL_PRODUCT) {
    InActiveProductStatus();
} else if ($req == REQ_ADD_USER) {
    CreateUser();
} else if ($req == REQ_EDIT_USER) {
    EditUser();
} else if ($req == REQ_EDIT_PLAN) {
    $res = ClearYearPlan();
    if ($res->MSGID === SERV_COMPLETE) {
        $yplanid = $res->MSGMESSAGE2;
        CreateYearPlan($yplanid);
    } else {
        echo json_encode($res);
    }
} else if ($req == REQ_ADD_PROMOTION) {
    CreatePromotion();
} else if ($req == REQ_EDIT_PROMOTION) {
    EditPromotion();
} else if ($req == REQ_DEL_PROMOTION) {
    InActivePromotion();
} else if ($req == REQ_ADD_PRODUCT) {
    CreateProduct();
} else if ($req == REQ_EDIT_PRODUCT) {
    UpdateProduct();
} else if ($req == REQ_LOGIN) {
    Login();
} else if ($req == REQ_CHANGE_PASSWORD) {
    ChangePassword();
} else if ($req == REQ_CHANGE_CREDIT) {
    UpdateCredit();
}

function GetMyTop5() {
    $appm = new AppManager();
    $month = filter_input(INPUT_POST, 'month');
    $year = filter_input(INPUT_POST, 'year');
    $res = $appm->GetMyTop5($month, $year);
    echo json_encode($res);
}

function GetMyScheduleMonth() {
    $appm = new AppManager();
    $start = filter_input(INPUT_POST, 'start');
    $end = filter_input(INPUT_POST, 'end');
    $res = $appm->GeyMyScheduleByMonth($start, $end);
    echo json_encode($res);
}

function CreateNewPlan() {
    $app = new AppManager();
    $q1 = filter_input(INPUT_POST, 'rdappoint'); //มีการนัดหมายหรือไม่
    $q2 = filter_input(INPUT_POST, 'rdorderProduct'); //สั่งซื้อสินค้าหรือไม่
    $plan = new plan();
    $plan->CUSTOMER_ID = filter_input(INPUT_POST, 'shop');
    if ($q1 == '1') {
        $plan->Q1 = 1;
        $plan->Q1_DATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
        $plan->Q1_TIME = filter_input(INPUT_POST, 'plantime');
        $plan->PLANDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
        $plan->PLANTIME = filter_input(INPUT_POST, 'plantime');
    } else {
        $plan->Q1 = 0;
        $plan->Q1_DATE = date('Y-m-d');
        $plan->Q1_TIME = date('H:i:s');
        $plan->PLANDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
        $plan->PLANTIME = filter_input(INPUT_POST, 'plantime');
    }
    if ($q2 == '1') {
        $plan->Q2 = 1;
        $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
        $arr = array();
        // For Check Duplicate Entry
        for ($i = 0; $i < $cnt; $i++) {
            if (array_key_exists($_POST['ddlproductOrder'][$i], $arr)) {
                $arr[$_POST['ddlproductOrder'][$i]][0] += $_POST['txtQty'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][2] += $_POST['txtTotal'][$i];
            } else {
                $arr[$_POST['ddlproductOrder'][$i]][0] = $_POST['txtQty'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1] = $_POST['txtPrice'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][2] = $_POST['txtTotal'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][3] = $_POST['promotionid'][$i];
            }
        }
        foreach ($arr as $key => $value) {
            $product = new order();
            $product->PRODUCT_ID = $key;
            $product->AMOUNT = $value[0];
            $product->PRICE = $value[1];
            $product->TOTAL = $value[2];
            $product->REF_PROMOTION = $value[3];
            $plan->PRODUCT_ORDER[] = $product;
        }
        $plan->Q3 = 0;
        //print_r($arr);
        //print_r($plan);
    } else {
        $plan->Q2 = 0;
        $plan->Q2_R1 = filter_input(INPUT_POST, 'txtnobuy'); //ทำไมไม่สั่งสินค้า


        $plan->Q3_R1 = filter_input(INPUT_POST, 'txtq2r1'); //ผลตอบรับ
        $plan->Q3_R2 = filter_input(INPUT_POST, 'txtq2r2'); //หมายเหตุ
        $plan->Q3 = 0;
        $cnt = filter_input(INPUT_POST, 'rowSuggest');
        /*
         * Check ว่ามีการแนะนำสินค้าหรือไม่ ถ้ามี Q3 = 1
         */
        if ($cnt > 0 && $_POST['txtproductSuggest'][1] != '' && ($_POST['ddlproductSuggest'][1] != '' || $_POST['ddlproductSuggest'][1] != null || $_POST['ddlproductSuggest'][1] != '0')) {
            $arr = array();
            for ($i = 0; $i < $cnt; $i++) {
                if (array_key_exists($_POST['ddlproductSuggest'][$i], $arr)) {
                    $res = new response();
                    $res->MSGID = SERV_ERROR;
                    $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากข้อมูลมีการซ้ำกัน กรุณาตรวจสอบใหม่อีกครั้ง";
                    echo json_encode($res);
                    die();
                } else {
                    $arr[$_POST['ddlproductSuggest'][$i]] = $_POST['txtproductSuggest'][$i];
                }
            }
            foreach ($arr as $key => $value) {
                $product = new order();
                $product->PRODUCT_ID = $key;
                $product->PRODUCT_REASON = $value;
                $plan->PRODUCT_SUGGEST[] = $product;
            }
            $plan->Q3 = 1;
        }

        //print_r($arr);
        //print_r($plan);
    }
    $response = $app->CreateNewPlan($plan, DOCTYPE_PLAN_CREATE_OLD_CUSTOMER);
    if ($response->MSGID === SERV_COMPLETE) {
        $response->MSGMESSAGE1 = "ยินดีด้วย <br> กระบวนการนัดหมายเสร็จสมบูรณ์";
    } else {
        $response->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($response);
}

function CreateNewPlanWithCustomer() {
    $customer = new customer();
    $customer->CUSTOMER_NAME = filter_input(INPUT_POST, 'txtShop');
    $customer->CUSTOMER_ADDRESS = filter_input(INPUT_POST, 'txtHomeID');
    $customer->CUSTOMER_SOI = filter_input(INPUT_POST, 'txtSoi');
    $customer->CUSTOMER_ROAD = filter_input(INPUT_POST, 'txtRoad');
    $customer->CUSTOMER_PROVINCE = filter_input(INPUT_POST, 'ddlProvince');
    $customer->CUSTOMER_DIST = filter_input(INPUT_POST, 'amphoe'); //แขวง
    $customer->CUSTOMER_SUBDIST = filter_input(INPUT_POST, 'district'); //เขต
    $customer->CUSTOMER_MAIL = filter_input(INPUT_POST, 'txtEmail');
    $customer->CUSTOMER_CONTACTNAME = filter_input(INPUT_POST, 'txtContactName');
    $customer->CUSTOMER_CONTACTPHONE = filter_input(INPUT_POST, 'txtContactPhone');
    $app = new AppManager();
    $plan = PrepareCreatePlanData();
    $res = $app->CreateNewPlanWithCustomer($customer, $plan);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "ยินดีด้วย <br> กระบวนการนัดหมายเสร็จสมบูรณ์";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function PrepareCreatePlanData() {
    $app = new AppManager();
    $q1 = filter_input(INPUT_POST, 'appoint'); //มีการนัดหมายหรือไม่
    $q2 = filter_input(INPUT_POST, 'orderProduct'); //สั่งซื้อสินค้าหรือไม่
    $q3 = filter_input(INPUT_POST, 'adProduct'); //มีการแนะนำสินค้าใหม่หรือไม่
    $plan = new plan();
    if (isset($_POST['shop'])) {
        $plan->CUSTOMER_ID = filter_input(INPUT_POST, 'shop');
    }

    if ($q1 == '1') {
        $plan->Q1 = 1;
        $plan->Q1_DATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
        $plan->Q1_TIME = filter_input(INPUT_POST, 'plantime');
        $plan->PLANDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
        $plan->PLANTIME = filter_input(INPUT_POST, 'plantime');
    } else {
        $plan->Q1 = 0;
        $plan->Q1_DATE = date('Y-m-d');
        $plan->Q1_TIME = date('H:i:s');
        $plan->PLANDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
        $plan->PLANTIME = filter_input(INPUT_POST, 'plantime');
    }
    if ($q2 == '1') {
        $plan->Q2 = 1;
        $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
        $arr = array();
        // For Check Duplicate Entry
        for ($i = 0; $i < $cnt; $i++) {
            if (array_key_exists($_POST['ddlproductOrder'][$i], $arr)) {
                $arr[$_POST['ddlproductOrder'][$i]][0] += $_POST['txtQty'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][2] += $_POST['txtTotal'][$i];
            } else {
                $arr[$_POST['ddlproductOrder'][$i]][0] = $_POST['txtQty'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1] = $_POST['txtPrice'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][2] = $_POST['txtTotal'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][3] = $_POST['promotionid'][$i];
            }
        }
        foreach ($arr as $key => $value) {
            $product = new order();
            $product->PRODUCT_ID = $key;
            $product->AMOUNT = $value[0];
            $product->PRICE = $value[1];
            $product->TOTAL = $value[2];
            $product->REF_PROMOTION = $value[3];
            $plan->PRODUCT_ORDER[] = $product;
        }
    } else {
        $plan->Q2 = 0;
        $plan->Q2_R1 = filter_input(INPUT_POST, 'txtnobuy');
    }

    $cnt = filter_input(INPUT_POST, 'rowSuggest');

    if ($q3 == '1') {
        //Check ว่าใน product suggest มีเว้นว่างไว้หรือไม่
        if ($cnt > 0 && $_POST['txtproductSuggest'][0] != '' && ($_POST['ddlproductSuggest'][0] != '' || $_POST['ddlproductSuggest'][0] != null || $_POST['ddlproductSuggest'][0] != '0')) {
            $cnt = filter_input(INPUT_POST, 'rowSuggest');
            $arr = array();
            for ($i = 0; $i < $cnt; $i++) {
                if (array_key_exists($_POST['ddlproductSuggest'][$i], $arr)) {
                    $res = new response();
                    $res->MSGID = SERV_ERROR;
                    $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากข้อมูลมีการซ้ำกัน กรุณาตรวจสอบใหม่อีกครั้ง";
                    echo json_encode($res);
                    die();
                } else {
                    $arr[$_POST['ddlproductSuggest'][$i]] = $_POST['txtproductSuggest'][$i];
                }
            }
            foreach ($arr as $key => $value) {
                $product = new order();
                $product->PRODUCT_ID = $key;
                $product->PRODUCT_REASON = $value;
                $plan->PRODUCT_SUGGEST[] = $product;
            }
            $plan->Q3 = 1;
        } else {
            $plan->Q3 = 0;
        }
    } else {
        $plan->Q3 = 0;
        $plan->Q3_R3 = filter_input(INPUT_POST, 'txtq2r3'); //เหตุผลที่ไม่แนะนำสินค้า
        $plan->Q3_R2 = filter_input(INPUT_POST, 'txtq2r2'); //หมายเหตุ
    }
    return $plan;
}

function DeActivePlan() {
    $app = new AppManager();
    $res = $app->DeActivePlan(filter_input(INPUT_POST, 'planid'));
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถวางแผนการเข้าพบลูกค้าได้ใหม่";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function CreateNewOrderRefPlan() {
    $app = new AppManager();
    $order = new order();
    $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
    $arr = array();
    $i = 0;
    // For Check Duplicate Entry
    /* for ($i = 0; $i < $cnt; $i++) {
      if (array_key_exists($_POST['productOrder'][$i], $arr)) {
      if ($arr[$_POST['productOrder'][$i]][3] == $_POST['pricetype'][$i]) { //check ถ้าเป็นราคาประเภทเดียวกัน
      $arr[$_POST['productOrder'][$i]][0] += $_POST['txtQty'][$i];
      } else {
      $arr[$_POST['productOrder'][$i]][0] = $_POST['txtQty'][$i];
      $arr[$_POST['productOrder'][$i]][1] = $_POST['txtPrice'][$i];
      $arr[$_POST['productOrder'][$i]][2] = $_POST['promotionid'][$i];
      $arr[$_POST['productOrder'][$i]][3] = $_POST['pricetype'][$i];
      }
      } else {
      $arr[$_POST['productOrder'][$i]][0] = $_POST['txtQty'][$i];
      $arr[$_POST['productOrder'][$i]][1] = $_POST['txtPrice'][$i];
      $arr[$_POST['productOrder'][$i]][2] = $_POST['promotionid'][$i];
      $arr[$_POST['productOrder'][$i]][3] = $_POST['pricetype'][$i];
      }
      } */
    foreach ($_POST['productOrder'] as $key => $value) {
        if (array_key_exists($_POST['productOrder'][$i], $arr)) {
            if ($arr[$_POST['productOrder'][$i]][0][3] == $_POST['pricetype'][$i]) { //check ถ้าเป็นราคาประเภทเดียวกัน
                $arr[$_POST['productOrder'][$i]][0][0] += $_POST['txtQty'][$i];
            } else {
                $arr[$_POST['productOrder'][$i]][1][0] = $_POST['txtQty'][$i];
                $arr[$_POST['productOrder'][$i]][1][1] = $_POST['txtPrice'][$i];
                $arr[$_POST['productOrder'][$i]][1][2] = $_POST['promotionid'][$i];
                $arr[$_POST['productOrder'][$i]][1][3] = $_POST['pricetype'][$i];
            }
        } else {
            $arr[$_POST['productOrder'][$i]][0][0] = $_POST['txtQty'][$i];
            $arr[$_POST['productOrder'][$i]][0][1] = $_POST['txtPrice'][$i];
            $arr[$_POST['productOrder'][$i]][0][2] = $_POST['promotionid'][$i];
            $arr[$_POST['productOrder'][$i]][0][3] = $_POST['pricetype'][$i];
        }
        //echo "loop " . $i . service::printr($arr);
        $i++;
    }
    foreach ($arr as $datakey => $datavalue) {
        foreach ($datavalue as $key => $value) {
            $product = new orderdetail();
            $product->PRODUCT_ID = $datakey;
            $product->AMOUNT = $value[0];
            $product->PRICE = $value[1];
            $product->REF_PROMOTION = $value[2];
            $product->PRICETYPE = $value[3];
            $order->ORDER_PRODUCTS[] = $product;
        }
    }
    $order->CUSTOMER_ID = filter_input(INPUT_POST, 'customer');
    $order->TOTAL = filter_input(INPUT_POST, 'txtSubtotal');
    $order->REFPLAN = filter_input(INPUT_POST, 'planid');
    $order->TIMESTAMP = date('Y-m-d');
    //service::printr($order);
    $res = $app->CreateNewOrder($order, DOCTYPE_CREATE_SALE_ORDER_REFPLAN);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การบันทึกผลการเข้าพบลูกค้าได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถติดตามผลได้ที่....";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function CreateNewOrder() {
    $app = new AppManager();
    $order = new order();
    $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
    $arr = array();
    $i = 0;
    // For Check Duplicate Entry
    foreach ($_POST['ddlproductOrder'] as $key => $value) {
        $j = 0;
        if (array_key_exists($_POST['ddlproductOrder'][$i], $arr)) {
            if ($arr[$_POST['ddlproductOrder'][$i]][0][3] == $_POST['pricetype'][$i]) { //check ถ้าเป็นราคาประเภทเดียวกัน
                $arr[$_POST['ddlproductOrder'][$i]][0][0] += $_POST['txtQty'][$i];
            } else {
                $arr[$_POST['ddlproductOrder'][$i]][1][0] = $_POST['txtQty'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1][1] = $_POST['txtPrice'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1][2] = $_POST['promotionid'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1][3] = $_POST['pricetype'][$i];
            }
        } else {
            $arr[$_POST['ddlproductOrder'][$i]][0][0] = $_POST['txtQty'][$i];
            $arr[$_POST['ddlproductOrder'][$i]][0][1] = $_POST['txtPrice'][$i];
            $arr[$_POST['ddlproductOrder'][$i]][0][2] = $_POST['promotionid'][$i];
            $arr[$_POST['ddlproductOrder'][$i]][0][3] = $_POST['pricetype'][$i];
        }
        //echo "loop " . $i . service::printr($arr);
        $i++;
    }

    foreach ($arr as $datakey => $datavalue) {
        foreach ($datavalue as $key => $value) {
            $product = new orderdetail();
            $product->PRODUCT_ID = $datakey;
            $product->AMOUNT = $value[0];
            $product->PRICE = $value[1];
            $product->REF_PROMOTION = $value[2];
            $product->PRICETYPE = $value[3];
            $order->ORDER_PRODUCTS[] = $product;
        }
    }
    //service::printr($order);
    $order->CUSTOMER_ID = filter_input(INPUT_POST, 'shop');
    $order->TOTAL = filter_input(INPUT_POST, 'ordertotal');
    $order->TIMESTAMP = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'orderdate'));
    $res = $app->CreateNewOrder($order, DOCTYPE_CREATE_SALE_ORDER);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การบันทึกผลการเข้าพบลูกค้าได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถติดตามผลได้ที่....";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function DeleteOrder() {
    $app = new AppManager();
    $res = $app->UpdateOrderStatus(filter_input(INPUT_POST, 'orderid'), ORDER_STATUS_CANC, '');
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function InActiveCustomerStatus() {
    $app = new AppManager();
    $res = $app->UpdateCustomerStatus(filter_input(INPUT_POST, 'customerid'), CUSTOMER_STATUS_INACTIVE);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function CreateNewCustomer() {
    $app = new AppManager();
    $customer = new customer();
    $customer->CUSTOMER_NAME = filter_input(INPUT_POST, 'txtShop');
    $customer->CUSTOMER_ADDRESS = filter_input(INPUT_POST, 'txtHomeID');
    $customer->CUSTOMER_SOI = filter_input(INPUT_POST, 'txtSoi');
    $customer->CUSTOMER_ROAD = filter_input(INPUT_POST, 'txtRoad');
    $customer->CUSTOMER_PROVINCE = filter_input(INPUT_POST, 'ddlProvince');
    $customer->CUSTOMER_DIST = filter_input(INPUT_POST, 'amphoe'); //แขวง
    $customer->CUSTOMER_SUBDIST = filter_input(INPUT_POST, 'district'); //เขต
    $customer->CUSTOMER_MAIL = filter_input(INPUT_POST, 'txtEmail');
    $customer->CUSTOMER_CONTACTNAME = filter_input(INPUT_POST, 'txtContactName');
    $customer->CUSTOMER_CONTACTPHONE = filter_input(INPUT_POST, 'txtContactPhone');
    $customer->CUSTOMER_ZIPCODE = filter_input(INPUT_POST, 'zipcode');
    $customer->CUSTOMER_GRPID = filter_input(INPUT_POST, 'ddlGrp');
    $res = $app->CreateNewCustomer($customer);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function EditPlanHeader() {
    $app = new AppManager();
    $plan = new plan();
    $plan->PLANID = filter_input(INPUT_POST, 'planid');
    $plan->CUSTOMER_ID = filter_input(INPUT_POST, 'shop');
    $plan->PLANDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'plandate'));
    $plan->PLANTIME = filter_input(INPUT_POST, 'plantime');
    $plan->Q3_R2 = filter_input(INPUT_POST, 'txtq3r2');
    $res = $app->UpdatePlanHeader($plan);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function EditOrder() {
    $app = new AppManager();
    $order = new order();
    $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
    $arr = array();
    // For Check Duplicate Entry
    /* for ($i = 0; $i < $cnt; $i++) {
      if (array_key_exists($_POST['ddlproductOrder'][$i], $arr)) {
      $arr[$_POST['ddlproductOrder'][$i]][0] += $_POST['txtQty'][$i];
      } else {
      $arr[$_POST['ddlproductOrder'][$i]][0] = $_POST['txtQty'][$i];
      $arr[$_POST['ddlproductOrder'][$i]][1] = $_POST['txtPrice'][$i];
      }
      } */
    $i = 0;
    foreach ($_POST['ddlproductOrder'] as $key => $value) {
        if (array_key_exists($_POST['ddlproductOrder'][$i], $arr)) {
            if ($arr[$_POST['ddlproductOrder'][$i]][0][3] == $_POST['pricetype'][$i]) { //check ถ้าเป็นราคาประเภทเดียวกัน
                $arr[$_POST['ddlproductOrder'][$i]][0][0] += $_POST['txtQty'][$i];
            } else {
                $arr[$_POST['ddlproductOrder'][$i]][1][0] = $_POST['txtQty'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1][1] = $_POST['txtPrice'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1][2] = $_POST['promotionid'][$i];
                $arr[$_POST['ddlproductOrder'][$i]][1][3] = $_POST['pricetype'][$i];
            }
        } else {
            $arr[$_POST['ddlproductOrder'][$i]][0][0] = $_POST['txtQty'][$i];
            $arr[$_POST['ddlproductOrder'][$i]][0][1] = $_POST['txtPrice'][$i];
            $arr[$_POST['ddlproductOrder'][$i]][0][2] = $_POST['promotionid'][$i];
            $arr[$_POST['ddlproductOrder'][$i]][0][3] = $_POST['pricetype'][$i];
        }
        //echo "loop " . $i . service::printr($arr);
        $i++;
    }

    foreach ($arr as $datakey => $datavalue) {
        foreach ($datavalue as $key => $value) {
            $product = new orderdetail();
            $product->PRODUCT_ID = $datakey;
            $product->AMOUNT = $value[0];
            $product->PRICE = $value[1];
            $product->REF_PROMOTION = $value[2];
            $product->PRICETYPE = $value[3];
            $order->ORDER_PRODUCTS[] = $product;
        }
    }
    $order->ORDERID = filter_input(INPUT_POST, 'orderid');
    $order->TIMESTAMP = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'orderdate'));
    $order->CUSTOMER_ID = filter_input(INPUT_POST, 'shop');
    $order->TOTAL = filter_input(INPUT_POST, 'ordertotal');
    $res = $app->UpdateMyOrder($order, DOCTYPE_UPDATE_ORDERDETAIL);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การบันทึกผลได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถติดตามผลได้ที่....";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function EditCustomer() {
    $customer = new customer();
    $customer->CUSTOMER_ID = filter_input(INPUT_POST, 'customerid');
    $customer->CUSTOMER_NAME = filter_input(INPUT_POST, 'txtShop');
    $customer->CUSTOMER_ADDRESS = filter_input(INPUT_POST, 'txtHomeID');
    $customer->CUSTOMER_SOI = filter_input(INPUT_POST, 'txtSoi');
    $customer->CUSTOMER_ROAD = filter_input(INPUT_POST, 'txtRoad');
    $customer->CUSTOMER_PROVINCE = filter_input(INPUT_POST, 'ddlProvince');
    $customer->CUSTOMER_DIST = filter_input(INPUT_POST, 'amphoe'); //แขวง
    $customer->CUSTOMER_SUBDIST = filter_input(INPUT_POST, 'district'); //เขต
    $customer->CUSTOMER_MAIL = filter_input(INPUT_POST, 'txtEmail');
    $customer->CUSTOMER_ZIPCODE = filter_input(INPUT_POST, 'zipcode');
    $customer->CUSTOMER_CONTACTNAME = filter_input(INPUT_POST, 'txtContactName');
    $customer->CUSTOMER_CONTACTPHONE = filter_input(INPUT_POST, 'txtContactPhone');
    $customer->CUSTOMER_ZIPCODE = filter_input(INPUT_POST, 'zipcode');
    $customer->CUSTOMER_GRPID = filter_input(INPUT_POST, 'ddlGrp');
    $appm = new AppManager();
    $res = $appm->UpdateCustomer($customer, DOCTYPE_UPDATE_CUSTOMER);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การบันทึกผลได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถติดตามผลได้ที่....";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function ApproveRejectOrder() {
    $app = new AppManager();
    $command = filter_input(INPUT_POST, 'command');
    $orderid = filter_input(INPUT_POST, 'orderid');
    $comment = filter_input(INPUT_POST, 'comment');
    $res = $app->UpdateOrderStatus($orderid, $command, $comment);
    if ($res->MSGID != SERV_COMPLETE) {
        $res->MSGMESSAGE1 = " เอกสารขายหมายเลข " . $orderid . " ไม่สามารถอนุมัติได้ ";
        return $res;
    }
    if ($command == ORDER_STATUS_DONE) {
        $type = DOCTYPE_APPROVE_ORDER;
    } else {
        $type = DOCTYPE_REJECT_ORDER;
    }
    $res = $app->CreateDocument($type, $orderid);
    if ($res->MSGID === SERV_COMPLETE) {
        if ($command == ORDER_STATUS_DONE) {
            $res->MSGMESSAGE1 = " เอกสารขายหมายเลข " . $orderid . " ได้ถูกอนุมัติแล้ว ";
        } else {
            $res->MSGMESSAGE1 = " เอกสารขายหมายเลข " . $orderid . " ได้ส่งกลับไปหาพนักงานขายแล้ว ";
        }
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function InActiveProductStatus() {
    $app = new AppManager();
    $productid = filter_input(INPUT_POST, 'productid');
    $res = $app->UpdateProductStatus($productid, PRODUCT_STATUS_INACTIVE);
    if ($res->MSGID != SERV_COMPLETE) {
        return $res;
    }
    $res = $app->CreateDocument(DOCTYPE_DELETE_PRODUCT, $productid);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function CreateUser() {
    $app = new AppManager();
    $user = new user();
    $cnt = filter_input(INPUT_POST, 'rowCustomerCnt');
    $customerlist = array();
    $user->USERNAME = filter_input(INPUT_POST, 'username');
    $user->PWD = md5(filter_input(INPUT_POST, 'username'));
    $user->TNAME = filter_input(INPUT_POST, 'tname');
    $user->TLNAME = filter_input(INPUT_POST, 'tsurname');
    $user->ENAME = filter_input(INPUT_POST, 'ename');
    $user->ELNAME = filter_input(INPUT_POST, 'esurname');
    $user->REGIONID = filter_input(INPUT_POST, 'region_val');
    $user->STARTDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'start_date'));
    // For Check Duplicate Entry
    for ($i = 0; $i < $cnt; $i++) {
        if (!array_key_exists($_POST['ddlCustomer'][$i], $customerlist)) {
            $customerlist[] = $_POST['ddlCustomer'][$i];
        }
    }
    foreach ($customerlist as $customerid) {
        $user->CUSTOMERS[] = $customerid;
    }

    $res = $app->CreateUser($user, DOCTYPE_CREATE_USER);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การบันทึกข้อมูลพนักงานได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถติดตามผลได้ที่....";
    } else if ($res->MSGID == SERV_DUPLICATE_KEY) {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจาก username นี้มีผู้อื่นใช้แล้ว กรุณาตรวจสอบใหม่อีกครั้ง";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function EditUser() {
    $app = new AppManager();
    $user = new user();
    $cnt = filter_input(INPUT_POST, 'rowCustomerCnt');
    $customerlist = array();
    $user->USERID = filter_input(INPUT_POST, 'userid');
    $user->TNAME = filter_input(INPUT_POST, 'tname');
    $user->TLNAME = filter_input(INPUT_POST, 'tsurname');
    $user->ENAME = filter_input(INPUT_POST, 'ename');
    $user->ELNAME = filter_input(INPUT_POST, 'esurname');
    $user->REGIONID = filter_input(INPUT_POST, 'region_val');
    // For Check Duplicate Entry
    foreach ($_POST['ddlCustomer'] as $customerid) {
        if (!array_key_exists($customerid, $customerlist)) {
            $customerlist[] = $customerid;
        }
    }
    foreach ($customerlist as $customerid) {
        $user->CUSTOMERS[] = $customerid;
    }
    //service::printr($customerlist);
    $res = $app->UpdateUser($user, DOCTYPE_UPDATE_USER);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การบันทึกข้อมูลพนักงานได้เสร็จสิ้นสมบูรณ์แล้ว ท่านสามารถติดตามผลได้ที่....";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function ClearYearPlan() {
    $app = new AppManager();
    $res = $app->ClearYearPlan();
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดกรุณาดำเนินการใหม่อีกครั้ง";
    }
    $app->CreateDocument(DOCTYPE_CLEAR_YEARPLAN, $res->MSGMESSAGE2);
    return $res;
    //echo json_encode($res);
}

function CreateYearPlan($yplanid) {
    $app = new AppManager();
    $i = 0;
    $regionAll = filter_input(INPUT_POST, 'regionAll');
    $res = new response();
    foreach ($_POST['region_price'] as $key => $value) {
        //echo "NAM3$yplanid";
        $target_value = $_POST['region_price'][$i];
        $target_percent = $_POST['region_percent'][$i];
        $regionid = $_POST['region'][$i];
        $res = $app->CreateYearPlan($regionAll, $yplanid, $regionid, $target_percent, $target_value);
        $i++;
    }

    $res = $app->CreateDocument(DOCTYPE_CREATE_YEARPLAN, $yplanid);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "สำเร็จ <br> การดำเนินตามคำขอได้เสร็จสิ้นสมบูรณ์แล้ว ";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดกรุณาดำเนินการใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function EditPromotion() {
    $app = new AppManager();
    $promotion = new promotion();
    $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
    $promotion->PROMOTION_ID = filter_input(INPUT_POST, 'promotionid');
    $promotion->PROMOTION_NAME = filter_input(INPUT_POST, 'name');
    $promotion->PROMOTION_DESC = filter_input(INPUT_POST, 'promotion_desc');
    $promotion->PROMOTION_STARTDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'startdate'));
    $promotion->PROMOTION_ENDDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'enddate'));
    $arr = array();
    $i = 0;
    foreach ($_POST['product'] as $key => $value) {
        $arr[$_POST['product'][$i]][0] = $_POST['retail_discount'][$i] / 100;
        $arr[$_POST['product'][$i]][1] = $_POST['whole_discount'][$i] / 100;
        $i++;
    }
    foreach ($arr as $key => $value) {
        $product = new product();
        $product->PRODUCT_ID = $key;
        $product->PRODUCT_RETAIL_DISCOUNT = $value[0];
        $product->PRODUCT_WHOLE_DISCOUNT = $value[1];
        //$product->TOTAL = $value[2];
        $promotion->PROMOTION_PRODUCT[] = $product;
    }
    //service::printr($promotion);
    $res = $app->UpdatePromotion($promotion, DOCTYPE_UPDATE_PROMOTION);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "รายการส่งเสริมการขาย : " . $promotion->PROMOTION_NAME . " ได้ถูกสร้างขึ้นเสร็จสิ้นแล้ว";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function CreatePromotion() {
    $app = new AppManager();
    $promotion = new promotion();
    $promotion->PROMOTION_NAME = filter_input(INPUT_POST, 'name');
    $promotion->PROMOTION_DESC = filter_input(INPUT_POST, 'promotion_desc');
    $promotion->PROMOTION_STARTDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'startdate'));
    $promotion->PROMOTION_ENDDATE = service::ConvertYearThaiToGlobal(filter_input(INPUT_POST, 'enddate'));
    $arr = array();
    $i = 0;
    foreach ($_POST['product'] as $key => $value) {
        $arr[$_POST['product'][$i]][0] = $_POST['retail_discount'][$i] / 100;
        $arr[$_POST['product'][$i]][1] = $_POST['whole_discount'][$i] / 100;
        $i++;
    }
    foreach ($arr as $key => $value) {
        $product = new product();
        $product->PRODUCT_ID = $key;
        $product->PRODUCT_RETAIL_DISCOUNT = $value[0];
        $product->PRODUCT_WHOLE_DISCOUNT = $value[1];
        //$product->TOTAL = $value[2];
        $promotion->PROMOTION_PRODUCT[] = $product;
    }
    //service::printr($promotion);
    $res = $app->CreatePromotion($promotion, DOCTYPE_CREATE_PROMOTION);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "รายการส่งเสริมการขาย : " . $promotion->PROMOTION_NAME . " ได้ถูกสร้างขึ้นเสร็จสิ้นแล้ว";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function InActivePromotion() {
    $app = new AppManager();
    $promotion = new promotion();
    $promotion->PROMOTION_ID = filter_input(INPUT_POST, 'promotionid');
    $res = $app->InActivePromotion($promotion, DOCTYPE_DELETE_PROMOTION);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "รายการส่งเสริมการขายนี้ ได้ถูกยกเลิกแล้ว";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function CreateProduct() {
    $app = new AppManager();
    $product = new product();
    $product->PRODUCTTYPE_ID = filter_input(INPUT_POST, 'ddlprdtype');
    $product->MODELID = filter_input(INPUT_POST, 'modelid');
    $product->PRODUCT_NAME = filter_input(INPUT_POST, 'productname');
    $product->PRODUCT_RETAIL_COST = floatval(str_replace(',', '', filter_input(INPUT_POST, 'retail_cost'))) ;
    $product->PRODUCT_RETAIL_PROFIT = filter_input(INPUT_POST, 'retail_profit') / 100;
    $product->PRODUCT_WHOLE_COST = floatval(str_replace(',', '', filter_input(INPUT_POST, 'whole_cost')));
    $product->PRODUCT_WHOLE_PROFIT = filter_input(INPUT_POST, 'whole_profit') / 100;
    $product->PRODUCT_STATUS = PRODUCT_STATUS_ACTIVE;
    //service::printr($promotion);
    $res = $app->CreateProduct($product, DOCTYPE_CREATE_PRODUCT);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "รายการส่งเสริมการขาย : " . $product->PRODUCT_NAME . " ได้ถูกสร้างขึ้นเสร็จสิ้นแล้ว";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function UpdateProduct() {
    $app = new AppManager();
    $product = new product();
    $product->PRODUCT_ID = filter_input(INPUT_POST, 'productid');
    $product->MODELID = filter_input(INPUT_POST, 'modelid');
    $product->PRODUCTTYPE_ID = filter_input(INPUT_POST, 'ddlprdtype');
    $product->PRODUCT_NAME = filter_input(INPUT_POST, 'productname');
    $product->PRODUCT_RETAIL_COST = floatval(str_replace(',', '', filter_input(INPUT_POST, 'retail_cost'))) ;
    $product->PRODUCT_RETAIL_PROFIT = filter_input(INPUT_POST, 'retail_profit') / 100;
    $product->PRODUCT_WHOLE_COST = floatval(str_replace(',', '', filter_input(INPUT_POST, 'whole_cost')));
    $product->PRODUCT_WHOLE_PROFIT = filter_input(INPUT_POST, 'whole_profit') / 100;
    //service::printr($product);
    $res = $app->UpdateProduct($product, DOCTYPE_UPDATE_PRODUCT);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = "รายการส่งเสริมการขาย : " . $product->PRODUCT_NAME . " ได้ถูกแก้ไขเสร็จสิ้นแล้ว";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function Login() {
    $app = new AppManager();
    $user = new user();
    $user->USERNAME = filter_input(INPUT_POST, 'usr');
    $user->PWD = filter_input(INPUT_POST, 'pwd');
    $res = new response();
    $response = $app->Auth($user);
    if ($response->MSGMESSAGE1 == '0') {
        $res->MSGID = SERV_ERROR;
        $res->MSGMESSAGE1 = "username/password ผิด กรุณาลองอีกครั้ง";
        echo json_encode($res);
        return;
    }
    if ($response->MSGMESSAGE2 != '0') {
        $res->MSGID = SERV_FORCECHANGE;
        $res->MSGMESSAGE1 = "Need Change Password";
        echo json_encode($res);
        return;
    }
    $res->MSGID = SERV_COMPLETE;
    $res->MSGMESSAGE1 = "ยินดีต้อนรับ ขณะนี้ระบบกำลังนำทางท่านเข้าไปใข้งาน";
    $res->MSGMESSAGE2 = $response->MSGMESSAGE3; //URL
    echo json_encode($res);
    return;
}

function ChangePassword() {
    $app = new AppManager();
    $user = new user();
    $user->USERID = filter_input(INPUT_POST, 'userid');
    $user->PWD = filter_input(INPUT_POST, 'pwd');
    $res = $app->ChangePassword($user);
    if ($res->MSGID === SERV_COMPLETE) {
        $data = $app->GetUserDataList($user->USERID)->MSGDATA1[0];
        $res->MSGMESSAGE3 = "Home/";
        if ($data->ROLEID == '1') {
            $res->MSGMESSAGE3 = "Admin/Home/";
        }
        $res->MSGMESSAGE1 = "แก้ไขเสร็จสิ้น";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}

function UpdateCredit() {
    $credit = array();
    $app = new AppManager();
    $credit[1] = filter_input(INPUT_POST, 'premiumcredit');
    $credit[2] = filter_input(INPUT_POST, 'goodcredit');
    $credit[3] = filter_input(INPUT_POST, 'normalcredit');
    $discount[1] = filter_input(INPUT_POST, 'premiumdiscount') / 100;
    $discount[2] = filter_input(INPUT_POST, 'gooddiscount') / 100;
    $discount[3] = filter_input(INPUT_POST, 'normaldiscount') / 100;
    $res = $app->UpdateCreditTerm($credit,$discount, DOCTYPE_UPDATE_CREDIT);
    if ($res->MSGID === SERV_COMPLETE) {
        $res->MSGMESSAGE1 = " Credit การชำระเงินและส่วนลด ถูกแก้ไขเสร็จสิ้นแล้ว";
    } else {
        $res->MSGMESSAGE1 = "ไม่สามารถดำเนินการได้ เนื่องจากเกิดข้อผิดพลาดขณะสร้างเอกสาร กรุณาตรวจสอบใหม่อีกครั้ง";
    }
    echo json_encode($res);
}
