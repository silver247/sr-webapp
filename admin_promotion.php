<?php
$pagename = "Product";
$subpagename = "Promotion";
?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">สินค้าโปรโมชั่น</div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:40px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();tablePromotion($('#month').val(), $('#year').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();tablePromotion($('#month').val(), $('#year').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="btn-right"><a href="Admin/Promotion/Add/" data-toggle="tooltip" title="เพิ่มโปรโมชั่น" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มโปรโมชั่น</a></div>
                    <div class="table-responsive" id="tablePromotion"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->
<div id="lightBlock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-alert">
        <div class="modal-content">
            <form class="push">
                <div class="modal-header">
                    <div class="block-title">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <i class="gi gi-ok_2"></i> ยืนยันการยกเลิกรายการส่งเสริมการขาย
                    </div>
                </div>
                <div class="block modal-body">
                    <div class="form-group">
                        คุณต้องการยืนยันการยกเลิกรายการส่งเสริมการขายใช่หรือไม่
                    </div>
                    <div class="text-center hidden" id="loading_apr">
                        <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>
                    </div>
                    <span class="text-center hidden" id="ApproveResult"></span>
                </div>
                <div class="block modal-footer">
                    <div class="text-right btn-force">
                        <a href="javascript:void(0)" data-toggle="tooltip" title="ไม่อนุมัติการสั่งซื้อ" id="btnApprove" class="btn btn-effect-ripple btn-xs btn-success">ยืนยัน</a>
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-xs btn-danger" id="btnCancelApprove" data-dismiss="modal">ยกเลิก</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
                                    $('#monthYear').datepicker({
                                        format: "MM yyyy",
                                        weekStart: 0,
                                        viewMode: "months",
                                        minViewMode: "months",
                                        language: "th-th"
                                    });
</script>

<script>
    $(document).ready(function () {
        //set initial state.
        $("#allMonth").change(function () {
            if (this.checked) {
                $("#monthYear").prop('disabled', true);
                $("#preMonth").prop('disabled', true);
                $("#nextMonth").prop('disabled', true);
                tableOrder($('#shop').val(), "0", "0");
            } else {
                $("#monthYear").prop('disabled', false);
                $("#preMonth").prop('disabled', false);
                $("#nextMonth").prop('disabled', false);
                tableOrder($('#shop').val(), $("#month").val(), $("#year").val());
            }
        });

        tablePromotion($("#month").val(), $("#year").val());

        $(document).on('click', '#btnCancel', function (e) {
            e.preventDefault();
            var promotionid = $(this).data('key').toString();
            $('#btnApprove').attr('data-key', promotionid);
            lightBlock();
        });

        $(document).on('click', '#btnApprove', function (e) {
            e.preventDefault();
            $('#loading_apr').removeClass('hidden');
            $('#btnApprove').addClass('disabled');
            var promotionid = $(this).data('key').toString();
            SendData(promotionid);
        });

        $('#lightBlock').on('hidden.bs.modal', function () {
            $('#btnApprove').removeAttr('data-key');
            tablePromotion($("#month").val(), $("#year").val());
        })
    });
</script>

<script>
    function lightBlock() {
        RenderLightBlockModal(0);
        window.location.hash = '#';
        $('#lightBlock').modal();
    }

    function RenderLightBlockModal(trg) {
        if (trg == 1) {
            $('#btnCancelApprove').text('ปิดหน้าต่างนี้');
            $('#loading_apr').addClass('hidden');
            $('#ApproveResult').removeClass('hidden');
            $('#btnApprove').removeAttr('data-key');
        } else {
            $('#btnApprove').removeClass('disabled');
            $('#loading_apr').addClass('hidden');
            $('#ApproveResult').addClass('hidden');
            $('#btnCancelApprove').text('ยกเลิก');
        }
    }

    function SendData(promotionid) {
        $.ajax({
            method: "POST",
            url: "AppHttpRequest.php",
            data: {req: '<?= REQ_DEL_PROMOTION; ?>', promotionid: promotionid},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
                setTimeout(function () {
                    RenderLightBlockModal(1);
                    $('#ApproveResult').empty().html(errorThrown);
                }, 3000);
            },
            success: function (data) {
                if (data.MSGID === 100) {
                    setTimeout(function () {
                        RenderLightBlockModal(1);
                        $('#ApproveResult').empty().html(data.MSGMESSAGE1);
                    }, 3000);
                } else {
                    setTimeout(function () {
                        RenderLightBlockModal(1);
                        $('#ApproveResult').empty().html(data.MSGMESSAGE1);
                    }, 3000);
                }
            }
        });
    }

    function tablePromotion(month, year) {
        //alert(shop+month+year);

        $.ajax({
            type: "GET",
            url: "admin_promotion_table.php",
            data: {month: month, year: year},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="promotionTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th style="width: 120px;">วันที่เริ่ม</th>'
                show += '<th style="width: 120px;">วันที่สิ้นสุด</th>'
                show += '<th>ชื่อโปรโมชั่น</th>'
                show += '<th style="width: 50px;">สถานะ</th>'
                show += '<th style="width: 250px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'

                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        var status = "เปิดใช้งาน";
                        if (value.PROMOTION_STATUS == '<?= PROMOTION_STATUS_INACTIVE; ?>') {
                            status = "ไม่ถูกใช้งาน";
                        }
                        show += '<tr>'
                        show += '<td>' + value.PROMOTION_STARTDATE + '</td>'
                        show += '<td>' + value.PROMOTION_ENDDATE + '</td>'
                        show += '<td>' + value.PROMOTION_NAME + '</td>'
                        show += '<td>' + status + '</td>'
                        show += '<td class="text-right">'
                        show += '<a href="Admin/Promotion/' + value.PROMOTION_ID + '/" data-toggle="tooltip" title="รายละเอียดโปรโมชั่น" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> รายละเอียด</a>'
                        var disabled = "";
                        if (value.PROMOTION_STATUS == '<?= PROMOTION_STATUS_INACTIVE; ?>') {
                            disabled = "disabled";
                        }
    
                        show += '<a href="Admin/Promotion/Edit/' + value.PROMOTION_ID + '/" data-toggle="tooltip" title="แก้ไขโปรโมชั่น" class="btn btn-effect-ripple btn-xs btn-warning '+disabled+'"><i class="gi gi-pencil"></i> แก้ไข</a>'
                        show += '<a href="#" id="btnCancel" data-toggle="tooltip" data-key="' + value.PROMOTION_ID + '" title="ลบโปรโมชั่น" class="btn btn-effect-ripple btn-xs btn-danger '+disabled+'"><i class="gi gi-bin"></i> ลบ</a>'
                        show += '</td>'
                        show += '</tr>'
                    });
                }
                show += '</tbody>'
                show += '</table>'

                $('#tablePromotion').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });

                $('#promotionTable').dataTable({
                    "ordering": false,
                    "info": false,
                    "searching": false,
                    "lengthChange": false
                });
            }
        });
    }
</script>


<?php include 'inc/template_end.php'; ?>