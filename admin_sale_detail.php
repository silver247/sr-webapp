<?php $pagename = "Sale" ?>
<?php
include 'inc/config_admin.php';
include_once './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
$userid = filter_input(INPUT_GET, 'id');
$user = new user();
$user = $appm->GetUserDataList($userid)->MSGDATA1[0];
$curdate = new DateTime();
$start_date = new DateTime($user->STARTDATE);
$interval = $start_date->diff($curdate);
$years_exp = $interval->format('%Y');
$months_exp = $interval->format('%m');
$days_exp = $interval->format('%d');
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    ข้อมูลการขาย
                    <a href="Admin/Sale/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าพนักงานขาย</div></a>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:25px;">
                        <table>
                            <tr>
                                <th>ชื่อ-นามสกุล</th>
                                <td colspan="3"><?= $user->TNAME; ?> <?= $user->TLNAME; ?></td>
                            </tr>
                            <tr>
                                <th width="120px">ตำแหน่ง</th>
                                <td width="250px">-</td>
                                <th width="130px">ภูมิภาคที่ดูแล</th>
                                <td><?=$user->REGIONNAME;?></td>
                            </tr>
                            <tr>
                                <th>วันที่เริ่มงาน</th>
                                <td><?= thaidate_withouttime($user->STARTDATE); ?></td>
                                <th>อายุงาน</th>
                                <td><?=$years_exp;?> ปี <?=$months_exp;?> เดือน <?=$days_exp;?> วัน</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:40px;">ร้าน</label>
                    <select id="shop" name="shop" class="select-select2 inlineDiv" style="width:200px;margin-right:30px;" data-placeholder="ร้านค้า">
                        <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                        <?=$appm->GetCustomerDropdownlist();?>
                    </select>
                    <label class="control-label inlineDiv" style="width:40px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();tableSum($('#shop').val(), $('#month').val(), $('#year').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();tableSum($('#shop').val(), $('#month').val(), $('#year').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableSum" style="margin-top:-25px"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
                                    $(function () {
                                        UiTables.init();
                                    });
</script>

<script>
    $('#sumTable').dataTable({
        ordering: false,
        info: false,
        searching: false
    })

    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
</script>

<script>
    $(document).ready(function () {
        tableSum($('#shop').val(), $("#month").val(), $("#year").val());
    });
</script>

<script>
    function tableSum(shop, month, year) {
        $.ajax({
            type: "GET",
            url: "sale_summary_table.php",
            data: {shop: shop, month: month, year: year, uid: "<?=$userid;?>"},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="sumTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>'
                show += '<th>สินค้า</th>'
                show += '<th class="text-center" style="width: 100px;">จำนวน</th>'
                show += '<th class="text-right" style="width: 70px;">จำนวนเงิน</th>'
                show += '<th style="width: 150px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'
                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>'
                        show += '<td class="text-center">' + i + '</td>'
                        show += '<td>'+value.PRODUCT_NAME+'</td>'
                        show += '<td class="text-center">'+value.AMOUNT+'</td>'
                        show += '<td class="text-right">'+value.TOTAL+'.-</td>'
                        show += '<td class="text-right">'
                        show += '<a href="Admin/Sale/Detail/<?=$userid;?>/'+value.PRODUCT_ID+'/'+year+'/'+month+'/" data-toggle="tooltip" title="รายละเอียดการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> รายละเอียด</a>'
                        //show += '<a href="Summary/'+value.PRODUCT_ID+'/'+year+'/'+month+'/" data-toggle="tooltip" title="รายละเอียดการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> รายละเอียด</a>'
                        show += '</td>'
                        show += '</tr>'
                        i++;
                    });
                }
                for (i = 1; i < 31; i++) {
                    
                }
                show += '</tbody>'
                show += '</table>'

                $('#tableSum').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });

                $('#sumTable').dataTable({
                    "ordering": false,
                    "info": false,
                    "searching": false,
                    "lengthChange": false
                });
            }
        });
    }
</script>


<?php include 'inc/template_end.php'; ?>