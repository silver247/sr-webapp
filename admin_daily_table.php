<?PHP
include_once './bundle.php';
$appm = new AppManager();
$year = filter_input(INPUT_GET, 'year');
$month = filter_input(INPUT_GET, 'month');
$userid = filter_input(INPUT_GET, 'userid');
if($userid == '0'){
    $userid = null;
}
$yeartargets = $appm->queryUserPlan($userid,$month,$year);
$currentsalesobject = $appm->GetDailySalesInMonth($year, $month , $userid);
//service::printr($currentsalesobject);
$currentsales = $currentsalesobject->MSGDATA1[0];
$totalsales = 0;
foreach ($currentsales as $sales){
    $totalsales += $sales;
}
$status = 0;
$response = new response();
if($currentsalesobject->MSGID == SERV_COMPLETE){
    $status += 100;
    $response->MSGDATA1['current_sales'][0] = $currentsales;
    $response->MSGDATA1['current_sales'][1] = $totalsales;
}
if($yeartargets->MSGID == SERV_COMPLETE){
    $status += 100;
    $response->MSGDATA1['target_sales'][0] = $yeartargets->MSGDATA1;
    $response->MSGDATA1['target_sales'][1] = $yeartargets->MSGDATA2;
}
//service::printr($yeartargets);
//service::printr($response);
//echo $status;
/*header('HTTP/1.1 200 OK');
if($status != 200){
    header('HTTP/1.1 404 Not Found');
}*/
echo json_encode($response);
?>
