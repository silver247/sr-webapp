<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class database extends PDO {

    public $MSGID;
    public $ERR_CODE;
    public $ERR_DESC;
    public $QRESULT;
    public $AFFECT_ROW;
    private $sql;
    private $qmode;

    function __construct() {
        //return $this->Open();
    }

    private function Open() {
        static $connection;
        $config = parse_ini_file("config/connection.ini", true);
        $env = DEV; //Change here for enviornment
        $host = "";
        $usr = "";
        $pass = "";
        $db = "";
        $chk = NO;
        foreach ($config as $key => $value) {
            if ($key === $env) {
                $host = $config[$key][SERV_HOST];
                $usr = $config[$key][SERV_USR];
                $pass = $config[$key][SERV_PASW];
                $db = $config[$key][SERV_DBNAME];
                $chk = YES;
            }
        }
        //echo $host;
        try {
            $connection = new PDO('mysql:host=' . $host . ';dbname=' . $db . '', $usr, $pass);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $connection->query("SET NAMES 'utf8'");
            return $connection;
        } catch (PDOException $ex) {
            throw $ex;
        }
    }

    public function SQL($value) {
        $this->sql = $value;
        return $this;
    }

    public function WRITE() {
        $this->qmode = WRITE;
        return $this;
    }

    public function READ() {
        $this->qmode = READ;
        return $this;
    }

    public function EXECUTE() {
        $query = $this->sql;
        $con = new database();

        try {
            $mydb = $con->Open();
            $stmt = $mydb->prepare($query);
            if ($this->qmode === READ) {
                $stmt->execute();
                $con->QRESULT = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $con->QRESULT = $stmt->execute();
            }
            $con->AFFECT_ROW = $stmt->rowCount();
            $con->MSGID = SERV_COMPLETE;
            return $con;
        } catch (PDOException $ex) {
            throw $ex;
        }
    }

    public function WriteLog($xml) {
        $data = (string) $xml;
        $sql = " insert into logdata values (DEFAULT, '" . $data . "', DEFAULT) ";
        //echo $sql;
        try {
            $con = new database();
            $mydb = $con->Open();
            $stmt = $mydb->prepare($sql);
            $stmt->execute();
            $con->AFFECT_ROW = $stmt->rowCount();
            $con->MSGID = SERV_COMPLETE;
            return $con;
        } catch (PDOException $ex) {
            throw $ex;
        }
    }

    public function CallStoredProcedure($functionquery,$resultquery) {
        $query = $functionquery;
        $con = new database();
        try {
            $mydb = $con->Open();
            $mydb->query($query);
            $res = $mydb->query($resultquery, PDO::FETCH_ASSOC);
            foreach ($res as $row) {
                $resultset[] = $row;
            }
            return $resultset;
        } catch (PDOException $ex) {
            throw $ex;
        }
        return;
    }
    
    public function ExcuteStoredProcedure($query){
        $con = new database();
        try {
            $mydb = $con->Open();
            $mydb->query($query);
        } catch (PDOException $ex) {
            throw $ex;
        }
        return;
    }

}

?>