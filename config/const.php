<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define('SERV_HOST', "host");
define('SERV_USR', "username");
define('SERV_PASW', "password");
define('SERV_DBNAME', "dbname");
define('DEV', "dev");
define('UAT', "uat");
define('PRD', "prd");
define('YES', TRUE);
define('NO', FALSE);
define('SERV_COMPLETE', 100);
define('SERV_ERROR', 200);
define('SERV_NODATA',300);
define('SERV_DUPLICATE_KEY', 400);
define('SERV_FORCECHANGE', 500);

define('READ',0);
define('WRITE',1);

define('ACTION_ADD','add');
define('ACTION_EDIT','edit');

define('ROLE_ADMIN', md5(1));
define('ROLE_SALES',md5(2));

define('REQ_SALE_HOME_TOP5', sha1('REQ_SALE_HOME_TOP5'));
define('REQ_MYSCHEDULE_Month', sha1('MyScheduleMonth'));
define('REQ_ADD_PLAN',sha1('REQ_ADD_PLAN'));
define('REQ_ADD_PLAN_NEWC',sha1('REQ_ADD_PLAN_NEWC'));
define('REQ_DEACTIVE_PLAN',sha1('REQ_DEACTIVE_PLAN'));
define('REQ_ADD_ORDER', sha1('REQ_ADD_ORDER'));
define('REQ_ADD_ORDER_REFPLAN', sha1('REQ_ADD_ORDER_REFPLAN'));
define('REQ_DEL_ORDER',sha1('REQ_DEL_ORDER'));
define('REQ_ADD_CUSTOMER',sha1('REQ_ADD_CUSTOMER'));
define('REQ_DEL_CUSTOMER',sha1('REQ_DEL_CUSTOMER'));
define('REQ_EDIT_CUSTOMER', sha1('REQ_EDIT_CUSTOMER'));
define('REQ_EDIT_PLANHEADER',sha1('REQ_EDIT_PLANHEADER'));
define('REQ_EDIT_ORDER', sha1('REQ_EDIT_ORDER'));
define('REQ_APPROVEREJECT_ORDER', sha1('REQ_APPROVEREJECT_ORDER'));
define('REQ_ADD_PRODUCT', sha1('REQ_ADD_PRODUCT'));
define('REQ_DEL_PRODUCT', sha1('REQ_DEL_PRODUCT'));
define('REQ_EDIT_PRODUCT', sha1('REQ_EDIT_PRODUCT'));
define('REQ_ADD_USER', sha1('REQ_ADD_USER'));
define('REQ_EDIT_USER', sha1('REQ_EDIT_USER'));
define('REQ_EDIT_PLAN', sha1('REQ_EDIT_PLAN'));
define('REQ_ADD_PROMOTION', sha1('REQ_ADD_PROMOTION'));
define('REQ_EDIT_PROMOTION', sha1('REQ_EDIT_PROMOTION'));
define('REQ_DEL_PROMOTION', sha1('REQ_DEL_PROMOTION'));
define('REQ_LOGIN', sha1('REQ_LOGIN'));
define('REQ_CHANGE_PASSWORD', sha1('REQ_CHANGE_PASSWORD'));
define('REQ_CHANGE_CREDIT', sha1('REQ_CHANGE_CREDIT'));

define('REGION_N', '1');
define('REGION_M', '2');
define('REGION_NE', '3');
define('REGION_W', '4');
define('REGION_E', '5');
define('REGION_S', '6');

define('DOCTYPE_PLAN_CREATE_OLD_CUSTOMER',1);
define('DOCTYPE_PLAN_CREATE_NEW_CUSTOMER',2);
define('DOCTYPE_CREATE_SALE_ORDER',3);
define('DOCTYPE_CREATE_SALE_ORDER_REFPLAN',4);
define('DOCTYPE_CREATE_CUSTOMER',5);
define('DOCTYPE_DELETE_PLAN',6);
define('DOCTYPE_DELETE_ORDER',7);
define('DOCTYPE_DELETE_CUSTOMER',8);
define('DOCTYPE_UPDATE_ORDERDETAIL',9);
define('DOCTYPE_UPDATE_CUSTOMER',10);
define('DOCTYPE_APPROVE_ORDER',11);
define('DOCTYPE_REJECT_ORDER',12);
define('DOCTYPE_CREATE_PRODUCT',13);
define('DOCTYPE_UPDATE_PRODUCT',14);
define('DOCTYPE_DELETE_PRODUCT',15);
define('DOCTYPE_CREATE_USER',16);
define('DOCTYPE_UPDATE_USER',17);
define('DOCTYPE_DELETE_USER',18);
define('DOCTYPE_CLEAR_YEARPLAN',19);
define('DOCTYPE_CREATE_YEARPLAN',20);
define('DOCTYPE_CREATE_PROMOTION',21);
define('DOCTYPE_UPDATE_PROMOTION',22);
define('DOCTYPE_DELETE_PROMOTION',23);
define('DOCTYPE_UPDATE_CREDIT',24);

define('PLAN_STATUS_NEW','NEW');//วาง plan
define('PLAN_STATUS_PED','PED');//รอ order ได้รับการจ่ายตัง
define('PLAN_STATUS_COM','COM');// เสร็จสิ้น
define('PLAN_STATUS_CAN','CAN');//ยกเลิก

define('ORDER_STATUS_OPEN','OPEN');//Create Order
define('ORDER_STATUS_WPAY','WPAY');//Waiting payment
define('ORDER_STATUS_PCOM','PCOM');//Payment Complete
define('ORDER_STATUS_SHIP','SHIP');//Shipping
define('ORDER_STATUS_DONE','DONE');//ปิดการขาย
define('ORDER_STATUS_CANC','CANC');//ยกเลิกการขาย
define('ORDER_STATUS_REJT','REJT');//ยกเลิกการขาย

define('CUSTOMER_STATUS_ACTIVE', 1);
define('CUSTOMER_STATUS_INACTIVE', 0);

define('PRODUCT_STATUS_ACTIVE',1);
define('PRODUCT_STATUS_INACTIVE', 0);

define('USER_STATUS_ACTIVE',1);
define('USER_STATUS_INACTIVE', 0);

define('PROMOTION_STATUS_ACTIVE',1);
define('PROMOTION_STATUS_INACTIVE', 0);
?>
