<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once './bundle.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppManager
 *
 * @author puwakitk
 */
class AppManager {

    //put your code here
    private $appbm;
    private $service;
    private $blogic;
    private $ddl;
    private $usermodule;
    private $salesmodule;
    private $planingmodule;
    private $warehousemodule;

    function __construct() {
        $this->appbm = new AppBaseManager();
        $this->service = new service();
        $this->blogic = new BusinessLogic();
        $this->ddl = new DropdownlistManager();
        $this->usermodule = new UserModule();
        $this->salesmodule = new SalesModule();
        $this->planingmodule = new PlaningModule();
        $this->warehousemodule = new WarehouseModule();
    }

    function Auth(user $userlogin) {
        $response = new response();
        $loginflag = $this->usermodule->Login($userlogin);
        $response->MSGMESSAGE1 = $loginflag;
        if ($loginflag != '0') {
            //service::printr($loginflag);
            $forcechange = $this->usermodule->CheckIsNeedForceChange($loginflag);
            $response->MSGMESSAGE2 = $forcechange;
            $response->MSGMESSAGE3 = "ChangePassword/";
            $userdata = $this->GetUserDataList($loginflag);
            if ($forcechange != '1') {
                $response->MSGMESSAGE3 = "Home/";
                if ($userdata->MSGDATA1[0]->ROLEID == ROLE_ADMIN) {
                    $response->MSGMESSAGE3 = "Admin/Home/";
                }
            }
            //service::printr($userdata);
            $this->InitializeSystem($userdata->MSGDATA1[0]);
        }

        return $response;
    }

    function InitializeSystem(user $user) {
        //service::printr($user);
        $this->InActiveExpirePromotion();
        $_SESSION["usrid"] = $user->USERID;
        $_SESSION['role'] = $user->ROLEID;
        $_SESSION['thname'] = $user->TNAME . " " . $user->TLNAME;
        $_SESSION['roledesc'] = $user->ROLENAME;
        /*$chk = $this->usermodule->CheckNeedUpdateUserEval();
        if ($chk->MSGMESSAGE1 == TRUE) {
            $this->usermodule->UpdateUserEvaluation();
        }*/
    }

    function GenerateID($docType) {
        return $this->GetDocTypeCode($docType) . round(microtime(true) * 1000);
    }

    function GetMyTop5($month, $year) {
        $res = new response();
        $chk1 = FALSE;
        $chk2 = FALSE;
        $res1 = $this->GetSaleTop(5, $month, $year);
        $res2 = $this->GetCustomerTop(5, $month, $year);
        $res->MSGID = SERV_COMPLETE;
        if ($res1->MSGID != SERV_COMPLETE) {
            $res->MSGID = $res1->MSGID;
        }
        if ($res2->MSGID != SERV_COMPLETE) {
            $res->MSGID = $res2->MSGID;
        }
        $res->MSGDATA1 = $res1;
        $res->MSGDATA2 = $res2;
        $res->MSGMESSAGE1 = $res1->MSGMESSAGE1;
        $res->MSGMESSAGE2 = $res2->MSGMESSAGE1;
        //service::printr($res);
        return $res;
    }

    function GetSaleTop($top, $month, $year) {
        $condition = " WHERE userid = '" . $_SESSION["usrid"] . "' and YEAR(order_date) = '" . $year . "' and MONTH(order_date) = '" . $month . "' ";
        $limit = "";
        if ($top != 0) {
            $limit = " LIMIT 0," . $top;
        }
        return $this->appbm->GetMyTopProducts($condition, $limit);
    }

    function GetCustomerTop($top, $month, $year) {
        $condition = " WHERE ordering.userid = '" . $_SESSION["usrid"] . "' and YEAR(order_date) = '" . $year . "' and MONTH(order_date) = '" . $month . "' ";
        $limit = "";
        if ($top != 0) {
            $limit = " LIMIT 0," . $top;
        }
        return $this->appbm->GetMyTopCustomers($condition, $limit);
    }

    function GeyMyScheduleByMonth($start, $end) {
        $condition = " WHERE USERID = '" . $_SESSION["usrid"] . "' AND (plan_date BETWEEN '" . $start . "' AND '" . $end . "')";
        return $this->appbm->GetMyMonthSchedule($condition);
    }

    function GetMyScheduleByDate($date) {
        $condition = " WHERE USERID = '" . $_SESSION["usrid"] . "' AND plan_date = '" . $date . "' ";
        return $this->planingmodule->GetMyDateSchedule($condition);
    }

    function GetCustomerDropdownlist() {
        $condition = "";
        if ($_SESSION['role'] != ROLE_ADMIN) {
            $condition = " WHERE USERID = '" . $_SESSION["usrid"] . "' ";
        }
        return $this->ddl->DropdownCustomer($condition);
    }

    function GetProductDropdownlist() {
        $condition = " WHERE ";
        return $this->ddl->DropdownProduct($condition, '');
    }

    function GetProductDropdownlistWithValue($val) {
        $condition = " WHERE ";
        return $this->ddl->DropdownProduct($condition, $val);
    }

    function GetProvinceDropdownlist($id) {
        $condition = "";
        if ($_SESSION['role'] != ROLE_ADMIN) {
            $condition = " join user_region on province.regionid = user_region.regionid where user_region.userid = '".$_SESSION['usrid']."' ";
        }
        return $this->ddl->DropdownProvince($id,$condition);
    }

    function CreateNewPlanWithCustomer(customer $customer, plan $plan) {
        $customer_result = $this->CreateNewCustomer($customer);
        if ($customer_result->MSGID == SERV_COMPLETE) {
            $plan->CUSTOMER_ID = $customer_result->MSGMESSAGE2;
            return $this->CreateNewPlan($plan, DOCTYPE_PLAN_CREATE_NEW_CUSTOMER);
        } else {
            return $customer_result;
        }
    }

    function CreateNewCustomer(customer $customer) {
        $customer->CUSTOMER_ID = $this->MapCustomerGrpIDToCode(3) . round(microtime(true) * 1000);
        //$customer->CUSTOMER_GRPID = 3; //DEFAULT = 1
        $cust_res = $this->salesmodule->CreateCustomer($customer);
        if ($cust_res->MSGID == SERV_COMPLETE) {
            $relation = $this->salesmodule->CreateRelationUserCustomer($customer->CUSTOMER_ID); //Create User Customer Relation
            if ($relation->MSGID == SERV_COMPLETE) {
                return $this->CreateDocument(DOCTYPE_CREATE_CUSTOMER, $customer->CUSTOMER_ID); //Create User Customer Relation
            } else {
                return $relation;
            }
        } else {
            return $cust_res;
        }
    }

    function CreateNewPlan(plan $plan, $docType) {
        $plan->USERID = $_SESSION['usrid'];
        $plan->PLANID = $this->GetDocTypeCode($docType) . round(microtime(true) * 1000);
        $plan_result = $this->planingmodule->CreatePlanHeader($plan);

        if ($plan_result->MSGID == SERV_COMPLETE) {
            if ($plan->Q2 == 1) {
                /*
                 * Create Plan Ordering
                 */
                $sql = "";
                $plan_order_result = new response();
                $plan_order_result->MSGID = SERV_COMPLETE;
                foreach ($plan->PRODUCT_ORDER as $order) {
                    if ($plan_order_result->MSGID === SERV_COMPLETE) {
                        if ($order->REF_PROMOTION == "" || $order->REF_PROMOTION == null) {
                            $ref_pro = " DEFAULT) ";
                        } else {
                            $ref_pro = " '" . $order->REF_PROMOTION . "') ";
                        }
                        $sql = " INSERT INTO plan_ordering values ("
                                . " '" . $plan->PLANID . "', "
                                . " '" . $order->PRODUCT_ID . "', "
                                . " '" . $order->PRICE . "', "
                                . " '" . $order->AMOUNT . "', "
                                . $ref_pro;
                        //echo $sql;
                        $plan_order_result = $this->planingmodule->CreatePlanDetail($sql);
                    } else {
                        return $plan_order_result;
                    }
                }
            } else {
                
            }

            /*
             * Create Plan Suggestion
             */
            if ($plan->Q3 == 1) {

                $sql = "";
                $plan_suggest_result = new response();
                $plan_suggest_result->MSGID = SERV_COMPLETE;
                foreach ($plan->PRODUCT_SUGGEST as $order) {
                    if ($plan_suggest_result->MSGID === SERV_COMPLETE) {
                        $sql = " INSERT INTO plan_suggestion values ("
                                . " '" . $plan->PLANID . "', "
                                . " '" . $order->PRODUCT_ID . "', "
                                . " '" . $order->PRODUCT_REASON . "') ";
                        $plan_suggest_result = $this->planingmodule->CreatePlanDetail($sql);
                        //echo $sql;
                    } else {
                        return $plan_suggest_result;
                    }
                }
            }

            /*
             * Create Document
             */

            return $this->CreateDocument($docType, $plan->PLANID);
        } else {
            /*
             * to do Error Handler
             */
            return $plan_result;
        }
    }

    function GetDocTypeCode($docid) {
        $condition = " WHERE doctypeid = '" . $docid . "' ";
        $res = $this->appbm->GetDocType($condition);
        return $res->MSGMESSAGE1;
    }

    function CreateDocument($docType, $refID) {
        //$doctypecode = $this->GetDocTypeCode($docType);
        $docid = round(microtime(true) * 1000);
        return $this->appbm->CreateDocument($docid, $docType, $_SESSION['usrid'], $refID);
    }

    function MapCustomerGrpIDToCode($id) {
        if ($id == 1) {
            return 'CP';
        } elseif ($id == 2) {
            return 'CH';
        } else {
            return 'CN';
        }
    }

    function DeActivePlan($planid) {
        return $this->planingmodule->DeActivePlan($planid);
    }

    function GetMyPlanHistoryByShop($customerid) {
        return $this->appbm->GetMyPlanHistoryByShop($customerid);
    }

    function CreateNewOrder(order $order, $doctypeid) {
        $order->ORDERID = $this->GetDocTypeCode($doctypeid) . round(microtime(true) * 1000);
        $order->USERID = $_SESSION['usrid'];
        //$order->TIMESTAMP = date('Y-m-d');
        $orderheader = $this->salesmodule->CreateOrderHeader($order);
        if ($orderheader->MSGID == SERV_COMPLETE) {
            /*
             * Create Order Detail
             */
            $orderdetail_result = new response();
            $orderdetail_result->MSGID = SERV_COMPLETE;
            //print_r($order);
            foreach ($order->ORDER_PRODUCTS as $row) {
                if ($orderdetail_result->MSGID === SERV_COMPLETE) {
                    $orderdetail = new order();
                    $orderdetail = $row;
                    if ($orderdetail->REF_PROMOTION == '' || $orderdetail->REF_PROMOTION == null || $orderdetail->REF_PROMOTION == '-') {
                        $refpromo = " DEFAULT, ";
                    } else {
                        $refpromo = " '" . $orderdetail->REF_PROMOTION . "', ";
                    }
                    $sql = "INSERT INTO ordering_detail VALUES ( "
                            . " '" . $order->ORDERID . "', "
                            . " '" . $orderdetail->PRODUCT_ID . "', "
                            . " '" . $orderdetail->PRICE . "', "
                            . " '" . $orderdetail->AMOUNT . "', "
                            . $refpromo
                            . " '" . $orderdetail->PRICETYPE . "' "
                            . " )";
                    //echo $sql."<br>";
                    $orderdetail_result = $this->salesmodule->CreateOrderDetail($sql);
                } else {
                    return $orderdetail_result;
                }
            }

            if ($doctypeid == DOCTYPE_CREATE_SALE_ORDER_REFPLAN) {
                $plan_status = $this->UpdatePlanStatus($order->REFPLAN, PLAN_STATUS_PED); //เปลี่ยน status plan
                if ($plan_status->MSGID == SERV_COMPLETE) {
                    return $this->CreateDocument($doctypeid, $order->ORDERID);
                } else {
                    return $plan_status;
                }
            }
            return $this->CreateDocument($doctypeid, $order->ORDERID);
        } else {
            return $orderheader;
        }
    }

    function GetMyPlanOrderDetail($planid) {
        return $this->planingmodule->GetMyPlanOrderDetail($planid);
    }

    function GetMyPlanHeaderByPlanID($planid) {
        $condition = " where planid = '" . $planid . "' ";
        return $this->appbm->GetMyPlanHeader($condition);
    }

    function GetProductDescription($productid) {
        return $this->appbm->GetProductDescription($productid);
    }

    function UpdatePlanStatus($planid, $status) {
        return $this->salesmodule->UpdatePlanStatus($planid, $status);
    }

    function GetMyOrderHistory($shopid, $month, $year, $userid) {
        $condition = "";
        //echo "shopid = ".$shopid;
        if ($shopid == '0') {
            $condition = "";
        } else {
            $condition .= " WHERE customer.customerid = '" . $shopid . "' ";
        }
        if ($month != 0 && $year != 0) {
            if ($condition == "") {
                $condition .= " WHERE MONTH(order_date) = '" . $month . "' and YEAR(order_date) = '" . $year . "' ";
            } else {
                $condition .= " and MONTH(order_date) = '" . $month . "' and YEAR(order_date) = '" . $year . "' ";
            }
        }

        if ($userid != '0' && $userid != '') {
            if ($condition == "") {
                $condition .= " WHERE ordering.userid = '" . $userid . "'  ";
            } else {
                $condition .= " and ordering.userid = '" . $userid . "' ";
            }
        }

        //echo $condition;
        return $this->appbm->GetMyOrderHistory($condition);
    }

    function GetMyOrderDetail($orderid) {
        $condition = " where ordering_detail.orderid = '" . $orderid . "' ";
        return $this->salesmodule->GetMyOrderDetail($condition);
    }

    function UpdateOrderStatus($orderid, $staus, $comment) {
        return $this->appbm->UpdateOrderStatus($orderid, $staus, $comment);
    }

    function GetMyCustomer($customerid) {
        $condition = " where c.customerid = '" . $customerid . "' ";
        if ($customerid == '' || $customerid == NULL) {
            $condition = "";
            if ($_SESSION['role'] != ROLE_ADMIN) {
                $condition = " join user_customer on c.customerid = user_customer.customerid where user_customer.userid = '" . $_SESSION['usrid'] . "' ";
            }
        }
        return $this->salesmodule->GetMyCustomer($condition);
    }

    function UpdateCustomerStatus($customerid, $staus) {
        return $this->appbm->UpdateCustomerStatus($customerid, $staus);
    }

    function GetMyProductHistory($shopid, $month, $year, $userid) {
        $condition = "";
        if ($shopid == '0' || $shopid == '') {
            $condition = "";
        } else {
            $condition .= " WHERE ordering.customerid = '" . $shopid . "' ";
        }

        if ($year != 0) {
            if ($condition == "") {
                $condition .= " WHERE YEAR(ordering.order_date) = '" . $year . "' ";
            } else {
                $condition .= " and YEAR(ordering.order_date) = '" . $year . "' ";
            }
        }

        if ($month != 0) {
            if ($condition == "") {
                $condition .= " WHERE MONTH(ordering.order_date) = '" . $month . "' ";
            } else {
                $condition .= " and MONTH(ordering.order_date) = '" . $month . "' ";
            }
        }

        if ($month != 0) {
            if ($condition == "") {
                $condition .= " WHERE MONTH(ordering.order_date) = '" . $month . "' ";
            } else {
                $condition .= " and MONTH(ordering.order_date) = '" . $month . "' ";
            }
        }

        if ($userid != '' && $userid != NULL) {
            if ($condition == "") {
                $condition .= " WHERE ordering.userid = '" . $userid . "' ";
            } else {
                $condition .= " and ordering.userid = '" . $userid . "' ";
            }
        }

        //echo $condition;
        return $this->salesmodule->GetMyProductHistory($condition);
    }

    function GetMyProductBuyers($productid, $year, $month) {
        return $this->salesmodule->GetMyProductBuyers($productid, $year, $month);
    }

    function GetMyProductDetail($productid) {
        //$condition = " where p.Active = 1 ";
        $condition = "";
        if ($productid != NULL || $productid != '') {
            $condition = " where p.productid = '" . $productid . "' ";
            $condition .= " and p.Active = 1 ";
        }
        return $this->salesmodule->GetMyProductDetail($condition);
    }

    function GetCustomerDropdownlistWithValue($val) {
        $condition = "";
        if ($_SESSION['role'] != ROLE_ADMIN) {
            $condition = " WHERE USERID = '" . $_SESSION["usrid"] . "' ";
        }
        return $this->ddl->DropdownCustomerWithValue($condition, $val);
    }

    function UpdatePlanHeader(plan $plan) {
        return $this->appbm->UpdatePlanHeader($plan);
    }

    function GetMyOrderHeader($orderid) {
        $condition = " where orderid = '" . $orderid . "' ";
        return $this->salesmodule->GetMyOrderHeader($condition);
    }

    function DeleteOrderDetail($orderid) {
        return $this->appbm->DeleteOrderDetail($orderid);
    }

    function UpdateOrderHeader(order $order) {
        return $this->appbm->UpdateMyOrderHeader($order);
    }

    function UpdateMyOrder(order $order, $doctypeid) {
        $deleteorder = $this->appbm->DeleteOrderDetail($order->ORDERID);
        $updateheader = $this->UpdateOrderHeader($order);
        if ($updateheader->MSGID != SERV_COMPLETE) {
            return $updateheader;
        }
        if ($deleteorder->MSGID == SERV_COMPLETE) {
            /*
             * Create Order Detail
             */
            $orderdetail_result = new response();
            $orderdetail_result->MSGID = SERV_COMPLETE;
            //print_r($order);
            foreach ($order->ORDER_PRODUCTS as $row) {
                if ($orderdetail_result->MSGID === SERV_COMPLETE) {
                    $orderdetail = new order();
                    $orderdetail = $row;
                    if ($orderdetail->REF_PROMOTION == '' || $orderdetail->REF_PROMOTION == null) {
                        $refpromo = 'DEFAULT, ';
                    } else {
                        $refpromo = "'" . $orderdetail->REF_PROMOTION . "', ";
                    }
                    $sql = "INSERT INTO ordering_detail VALUES ( "
                            . " '" . $order->ORDERID . "', "
                            . " '" . $orderdetail->PRODUCT_ID . "', "
                            . " '" . $orderdetail->PRICE . "', "
                            . " '" . $orderdetail->AMOUNT . "', "
                            . $refpromo
                            . " '" . $orderdetail->PRICETYPE . "')";
                    //echo $sql."<br>";
                    $orderdetail_result = $this->appbm->CreateOrderDetail($sql);
                } else {
                    return $orderdetail_result;
                }
            }

            return $this->CreateDocument($doctypeid, $order->ORDERID);
        } else {
            return $deleteorder;
        }
    }

    function UpdateCustomer(customer $customer, $doctypeid) {
        $customerresult = $this->appbm->UpdateCustomer($customer);
        if ($customerresult->MSGID != SERV_COMPLETE) {
            return $customerresult;
        }
        return $this->CreateDocument($doctypeid, $customer->CUSTOMER_ID);
    }

    /*
     * Admin
     */

    function GetUnApproveOrderList($month, $year) {
        $condition = " where YEAR(order_date) = '" . $year . "' and MONTH(order_date) = '" . $month . "' and ordering.status = '" . ORDER_STATUS_OPEN . "' ";
        if ($month == 0 || $year == 0) {
            $condition = " where ordering.status = '" . ORDER_STATUS_OPEN . "' ";
        }
        return $this->salesmodule->GetMyOrderHeader($condition);
    }

    function GetAllCustomer($customerid) {
        $condition = " where c.customerid = '" . $customerid . "' ";
        if ($customerid == '' || $customerid == NULL) {
            $condition = " ";
        }
        return $this->salesmodule->GetMyCustomer($condition);
    }

    function GetOrderDetailByCustomerID($customerid) {
        $condition = " where customerid = '" . $customerid . "' ";
        return $this->salesmodule->GetMyOrderDetail($condition);
    }

    function UpdateProductStatus($productid, $staus) {
        return $this->appbm->UpdateProductStatus($productid, $staus);
    }

    function GetUserDataList($userid) {
        $condition = "";
        if ($userid != "" && $userid != NULL && $userid != "0") {
            $condition = " where user.userid = '" . $userid . "' ";
        }
        return $this->usermodule->GetUserDataList($condition);
    }

    function GetCustomerByUserID($userid) {
        $condition = " join user_customer on c.customerid = user_customer.customerid where user_customer.userid = '" . $userid . "' ";
        return $this->salesmodule->GetMyCustomer($condition);
    }

    function GetCustomerByCustomerID($customerid) {
        $condition = " where customer.customerid = '" . $customerid . "' ";
        return $this->salesmodule->GetMyCustomer($condition);
    }

    function CreateUser(user $user, $doctype) {
        $user->USERID = $this->GenerateID($doctype);
        $checkduplicate = $this->usermodule->CheckDuplicateUser($user);
        if ($checkduplicate->MSGID != SERV_COMPLETE) {
            return $checkduplicate;
        }
        $createuser = $this->usermodule->CreateUser($user);
        if ($createuser->MSGID != SERV_COMPLETE) {
            return $createuser;
        }
        $createuser_customer = $this->CreateUserCustomer($user);
        if ($createuser_customer->MSGID != SERV_COMPLETE) {
            return $createuser_customer;
        }
        $user_region = $this->usermodule->CreateUserRegion($user);
        if ($user_region->MSGID != SERV_COMPLETE) {
            return $user_region;
        }
        /*
         * To do Set Sales Target
         */
        $yp = $this->planingmodule->CallStoreUpdateUserYearPlan();
        if($yp->MSGID != SERV_COMPLETE){
            return $yp;
        }
        $uev = $this->planingmodule->CallStoreUpdateUserEval();
        if($uev->MSGID != SERV_COMPLETE){
            return $uev; 
        }
        
        $document = $this->CreateDocument($doctype, $user->USERID);

        return $document;
    }

    function UpdateUser(user $user, $doctype) {
        $deleteUserRelation = $this->usermodule->DeleteUserCustomerByUserID($user->USERID);
        if ($deleteUserRelation->MSGID != SERV_COMPLETE) {
            return $deleteUserRelation;
        }
        $createuser_customer = $this->CreateUserCustomer($user);
        if ($createuser_customer->MSGID != SERV_COMPLETE) {
            return $createuser_customer;
        }
        $deleteUserRegion = $this->usermodule->DeleteUserRegionByUserID($user->USERID);
        if ($deleteUserRegion->MSGID != SERV_COMPLETE) {
            return $deleteUserRegion;
        }
        $user_region = $this->usermodule->CreateUserRegion($user);
        if ($user_region->MSGID != SERV_COMPLETE) {
            return $user_region;
        }
        $yp = $this->planingmodule->CallStoreUpdateUserYearPlan();
        if($yp->MSGID != SERV_COMPLETE){
            return $yp;
        }
        $uev = $this->planingmodule->CallStoreUpdateUserEval();
        if($uev->MSGID != SERV_COMPLETE){
            return $uev; 
        }
        $document = $this->CreateDocument($doctype, $user->USERID);
        return $document;
    }

    function InActiveUser($userid) {
        return $this->usermodule->InActiveUser($userid);
    }

    function CreateUserCustomer(user $user) {
        $response = new response();
        $response->MSGID = SERV_COMPLETE;
        foreach ($user->CUSTOMERS as $customerid) {
            if ($response->MSGID == SERV_COMPLETE) {
                $response = $this->usermodule->CreateUserCustomer($user->USERID, $customerid);
            } else {
                return $response;
            }
        }
        return $response;
    }

    function GetSalesSummary($userid, $date, $month, $year) {
        $condition = "";
        if ($userid != null && $userid != '') {
            $condition .= " and userid = '" . $userid . "' ";
        }
        if ($date != '' && $date != '0') {
            $condition .= " and order_date = '" . $date . "' ";
        } else {
            if ($month != null && $month != '' && $month != '0') {
                $condition .= " and MONTH(order_date) = '" . $month . "' ";
            }
            if ($year != null && $year != '' && $year != '0') {
                $condition .= " and YEAR(order_date) = '" . $year . "' ";
            }
        }
        $rset = $this->salesmodule->GetSalesSummary($condition);
        //service::printr($rset);
        return $rset;
    }

    function ClearYearPlan() {
        $res = $this->planingmodule->ClearYearPlanAcion();
        //service::printr($res);
        $this->CreateDocument(DOCTYPE_CLEAR_YEARPLAN, $res->MSGMESSAGE2);
        return $res;
    }

    function CreateYearPlan($regionAll, $yplanid, $regionid, $target_percent, $target_value) {
        $res = $this->planingmodule->CreateYearPlanAcion($regionAll, $yplanid, $regionid, $target_percent, $target_value);
        $this->CreateDocument(DOCTYPE_CREATE_YEARPLAN, $res->MSGMESSAGE2);
        return $res;
    }

    function GetPromotionHeader($promotionid, $year, $month) {
        $condition = "";
        if ($month != null && $month != '' && $month != '0') {
            $condition .= " Where '" . $year . "' between YEAR(start_date) and YEAR(end_date) and '" . $month . "' between MONTH(start_date) and MONTH(end_date)";
            //. "(YEAR(start_date) <= '" . $year . "' and MONTH(start_date) <= '" . $month . "') and (YEAR(end_date) >= '" . $year . "' and MONTH(end_date) >= '" . $month . "')";
        }
        /* if($condition != ""){
          $condition .= " and active = '" . PROMOTION_STATUS_ACTIVE . "' ";
          }
          else{
          $condition .= " where active = '" . PROMOTION_STATUS_ACTIVE . "' ";
          } */

        if ($promotionid != "" && $promotionid != null) {
            $condition = " where promotionid = '" . $promotionid . "' ";
        }
        return $this->salesmodule->GetPromotionHeader($condition);
    }

    function GetPromotionDetail($promotionid) {
        return $this->salesmodule->GetPromotionDetail($promotionid);
    }

    function UpdatePromotion(promotion $promotion, $doctype) {
        $promotionheader = $this->salesmodule->UpdatePromotionHeader($promotion);
        if ($promotionheader->MSGID != SERV_COMPLETE) {
            return $promotionheader;
        }
        $cleardata = $this->salesmodule->DeletePromotionProduct($promotion);
        if ($cleardata->MSGID != SERV_COMPLETE) {
            return $cleardata;
        }
        $promotiondetail = $this->salesmodule->InsertPromotionDetail($promotion);
        if ($promotiondetail->MSGID != SERV_COMPLETE) {
            return $promotiondetail;
        }

        return $this->CreateDocument($doctype, $promotion->PROMOTION_ID);
    }

    function CreatePromotion(promotion $promotion, $doctype) {
        $promotion->PROMOTION_ID = $this->GenerateID($doctype);
        $promotionheader = $this->salesmodule->InsertPromotionHeader($promotion);
        if ($promotionheader->MSGID != SERV_COMPLETE) {
            return $promotionheader;
        }
        $promotiondetail = $this->salesmodule->InsertPromotionDetail($promotion);
        if ($promotiondetail->MSGID != SERV_COMPLETE) {
            return $promotiondetail;
        }

        return $this->CreateDocument($doctype, $promotion->PROMOTION_ID);
    }

    function InActivePromotion(promotion $promotion, $doctype) {
        $promotionheader = $this->salesmodule->InActivePromotion($promotion);
        if ($promotionheader->MSGID != SERV_COMPLETE) {
            return $promotionheader;
        }

        return $this->CreateDocument($doctype, $promotion->PROMOTION_ID);
    }

    function queryYearPlan() {
        return $this->planingmodule->queryYearPlanAcion();
    }

    function GetCustomerDiscount($customerid) {
        return $this->salesmodule->GetCustomerDiscount($customerid);
    }

    function GetProductDropdownlistForPromotion($val) {
        $condition = " WHERE p.productid not in (select productid from promotion join promotion_product on promotion.promotionid = promotion_product.promotionid where promotion.active = 1) and ";
        return $this->ddl->DropdownProduct($condition, $val);
    }

    function InActiveExpirePromotion() {
        return $this->salesmodule->InActiveExpirePromotion();
    }

    function GetProductTypeDropdownlist($val) {
        return $this->ddl->DropdownProductType($val);
    }

    function CreateProduct(product $product, $doctype) {
        $product->PRODUCT_ID = $this->GenerateID($doctype);
        $productresult = $this->warehousemodule->CreateProduct($product);
        if ($productresult->MSGID != SERV_COMPLETE) {
            return $productresult;
        }
        $priceresult = $this->salesmodule->CreateProductPrice($product);
        if ($priceresult->MSGID != SERV_COMPLETE) {
            return $priceresult;
        }

        return $this->CreateDocument($doctype, $product->PRODUCT_ID);
    }

    function UpdateProduct(product $product, $doctype) {
        $productresult = $this->warehousemodule->UpdateProduct($product);
        if ($productresult->MSGID != SERV_COMPLETE) {
            return $productresult;
        }
        $priceresult = $this->salesmodule->UpdateProductPrice($product);
        if ($priceresult->MSGID != SERV_COMPLETE) {
            return $priceresult;
        }

        return $this->CreateDocument($doctype, $product->PRODUCT_ID);
    }

    function GetProductDetail($productid) {
        $product = new product();
        $product_detail_object = $this->warehousemodule->GetProductDetail($productid);
        $product_price_object = $this->salesmodule->GetProductPrice($productid);

        if ($product_detail_object->MSGID != SERV_COMPLETE) {
            return $product_detail_object;
        }
        if ($product_price_object->MSGID != SERV_COMPLETE) {
            return $product_price_object;
        }

        $product_detail = $product_detail_object->MSGDATA1[0];
        $product_price = $product_price_object->MSGDATA1[0];
        $product->MODELID = $product_detail->MODELID;
        $product->PRODUCT_ID = $productid;
        $product->PRODUCT_NAME = $product_detail->PRODUCT_NAME;
        $product->PRODUCTTYPE_ID = $product_detail->PRODUCTTYPE_ID;
        $product->PRODUCTTYPE_NAME = $product_detail->PRODUCTTYPE_NAME;
        $product->PRODUCT_CREATEDATE = $product_detail->PRODUCT_CREATEDATE;
        $product->PRODUCT_STATUS = $product_detail->PRODUCT_STATUS;
        $product->PRODUCT_RETAIL_COST = $product_price->PRODUCT_RETAIL_COST;
        $product->PRODUCT_WHOLE_COST = $product_price->PRODUCT_WHOLE_COST;
        $product->PRODUCT_RETAIL_PROFIT = $product_price->PRODUCT_RETAIL_PROFIT;
        $product->PRODUCT_WHOLE_PROFIT = $product_price->PRODUCT_WHOLE_PROFIT;
        $product->PRODUCT_PRICEDATE = $product_price->PRODUCT_PRICEDATE;

        return $product;
    }

    function queryUserPlan($userid, $month, $year) {
        if (!isset($month)) {
            $month = date("m");
        }
        if (!isset($year)) {
            $year = date("Y");
        }
        return $this->planingmodule->queryUserPlanAcion($userid, $month, $year);
    }

    function GetSalesRegionSummary($year, $month, $userid) {
        $condition = "";
        if ($year != '0' && $year != '' && $year != null) {
            $condition .= " AND YEAR(ordering.order_date) = '" . $year . "' ";
        }
        if ($month != '0' && $month != '' && $month != null) {
            $condition .= " AND MONTH(ordering.order_date) = '" . $month . "' ";
        }
        if ($userid != '0' && $userid != '' && $userid != null) {
            $condition .= " AND ordering.userid = '" . $userid . "' ";
        }
        return $this->salesmodule->GetSalesRegionSummary($condition);
    }

    function queryConditionSale() {
        return $this->planingmodule->queryConditionSaleAcion();
    }

    function GetDailySalesInMonth($year, $month, $userid) {
        $condition = "";
        if ($year != '0' && $year != '' && $year != null) {
            $condition .= " AND YEAR(ordering.order_date) = '" . $year . "' ";
        }
        if ($month != '0' && $month != '' && $month != null) {
            $condition .= " AND MONTH(ordering.order_date) = '" . $month . "' ";
        }
        if ($userid != '0' && $userid != '' && $userid != null) {
            $condition .= " AND ordering.userid = '" . $userid . "' ";
        }
        return $this->salesmodule->GetDailySalesInMonth($condition);
    }

    function ChangePassword(user $user) {
        return $this->usermodule->UpdatePassword($user);
    }

    function GetUserDropdownlist($val) {
        return $this->ddl->DropdownUserList($val);
    }

    function GetDropdownPriceType($val) {
        return $this->ddl->DropdownPriceType($val);
    }
    
    function GetDropdownRegion($val){
        return $this->ddl->DropdownRegion($val);
    }
    
    function GetCustomerDropdownlistForCreateSale($val,$regionid) { //Admin use only
        $condition = " join province on c.provinceid = province.provinceid and province.regionid = '".$regionid."' ";
        /*if ($_SESSION['role'] != ROLE_ADMIN) {
            $condition = " WHERE USERID = '" . $_SESSION["usrid"] . "' ";
        }*/
        return $this->ddl->DropdownCustomerWithValue($condition, $val);
    }
    
    function GetCustomerGroupDetail(){
        return $this->salesmodule->GetCustomerGroupDetail();
    }
    
    function UpdateCreditTerm(array $credit, array $discount, $doctype){
        $creditterm =  $this->salesmodule->UpdateCreditTerm($credit,$discount);
        if($creditterm->MSGID != SERV_COMPLETE){
            return $creditterm;
        }
        return $this->CreateDocument($doctype, 'CreditTerm');
    }
    
    function GetDropdownCustomerGroup($val){
        return $this->ddl->DropdownCustomerGroup($val);
    }
}
