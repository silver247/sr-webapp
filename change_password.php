<?php
session_start();
?>
<?php include 'inc/config.php'; ?>
<?php include 'inc/template_start.php'; ?>
<?php session_start();
require_once './bundle.php';
?>
<!-- Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
<img src="img/bg_index.png" alt="Full Background" class="full-bg animation-pulseSlow">
<!-- END Full Background -->

<!-- Login Container -->
<div id="login-container">
    <!-- Login Header -->
    <h1 class="text-light text-center push-top-bottom animation-pullDown">
        <img src="img/logo_index.png">
    </h1>
    <!-- END Login Header -->

    <!-- Login Block -->
    <div class="block block-login animation-fadeInQuick">
        <!-- Login Form -->
        <form id="tform1" method="post" class="form-horizontal">
            <input type="hidden" value="<?= REQ_CHANGE_PASSWORD; ?>" name="req">
            <input type="hidden" value="<?= $_SESSION['usrid']; ?>" name="userid">
            <div class="form-group">
                <label class="col-xs-12 headChange"><i class="fa fa-refresh"></i> กรุณาเปลี่ยนรหัสผ่าน</label>
            </div>
            <div class="form-group">
                <label for="login-email" class="col-xs-12">ระบุรหัสผ่านใหม่</label>
                <div class="col-xs-12">
                    <input type="password" id="pwd" name="pwd" class="form-control" placeholder="ระบุรหัสผ่านใหม่">
                </div>
            </div>
            <div class="form-group">
                <label for="login-password" class="col-xs-12">ระบุรหัสผ่านใหม่อีกครั้ง</label>
                <div class="col-xs-12">
                    <input type="password" id="rpwd" name="rpwd" class="form-control" placeholder="ระบุรหัสผ่านใหม่อีกครั้ง">
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-effect-ripple btn-sm btn-info btn-login">ยืนยัน</button>
                </div>
            </div>
        </form>
        <!-- END Login Form -->
    </div>
    <!-- END Login Block -->
</div>
<!-- END Login Container -->

<?php include 'inc/template_scripts.php'; ?>
<script>
    $(document).ready(function () {
        $('#tform1').on('submit', function (e) {
            e.preventDefault();
            if ($('#rpwd').val() !== $('#pwd').val()) {
                bootbox.alert({
                    size: 'small',
                    message: "รหัสผ่านต้องเหมือนกัน",
                    title: "การแจ้งเตือน"
                });
                return;
            }
            $.ajax({
                method: "POST",
                url: "AppHttpRequest.php",
                data: $("#tform1").serialize(),
                dataType: "json",
                beforeSend: function (xhr) {
                    loading = bootbox.dialog({
                        size: 'small',
                        message: '<p class="text-center">เรากำลังดำเนินการ กรุณารอสักครู่...</p>',
                        closeButton: false
                    });
                },
                error: function (transport, status, errorThrown) {
                    setTimeout(function () {
                        loading.modal('hide');
                        bootbox.alert({
                            size: 'small',
                            message: "ไม่เปลี่ยนรหัสผ่านได้ กรุณาติดต่อผู้ดูแลระบบ",
                            title: "การแจ้งเตือน"
                        });
                        return;
                    }, 3000);
                    console.log(transport.responseText);
                },
                success: function (data) {
                    console.log(data);
                    if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                        setTimeout(function () {
                            loading.modal('hide');
                            bootbox.alert({
                                size: 'small',
                                message: data.MSGMESSAGE1,
                                title: "การแจ้งเตือน",
                                callback: function () {
                                    window.location = data.MSGMESSAGE3;
                                }
                            });
                        }, 3000);
                    } else {
                        setTimeout(function () {
                            loading.modal('hide');
                            bootbox.alert({
                                size: 'small',
                                message: data.MSGMESSAGE1,
                                title: "การแจ้งเตือน"
                            });
                        }, 3000);
                    }

                }


            });
        });
    });
</script>
<?php include 'inc/template_end.php'; ?>