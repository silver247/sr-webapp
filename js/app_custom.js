// JavaScript Document
var monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
var dayNames = ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"];
var dateNow = new Date();
var dateMonthNow = dateNow.getMonth();
var dateYearNow = dateNow.getFullYear();
var page = $(location).attr('pathname');

$(document).ready(function () {
    $("#month").val(dateMonthNow + 1);
    $("#year").val(dateYearNow);
    $("#yearBD").val(dateYearNow + 543);
    var dateYearNowBD = dateYearNow + 543;
    $("#monthYear").val(monthNames[dateMonthNow] + " " + dateYearNowBD);
    $("#monthYear").change(function () {
        putVal();
    });
});

function putVal() {
    var date = $("#monthYear").val();

    date = date.split(" ");
    var dateM = parseInt(monthNames.indexOf(date[0])) + 1;
    var dateY = parseInt(date[1]) - 543;
    var dateYTh = parseInt(date[1]);

    $("#monthYear").val(date[0] + " " + dateYTh);
    $("#month").val(dateM);
    $("#year").val(dateY);
}

function nextMonth() {
    var dateMonthInput = $("#month").val();
    var dateYearInput = $("#year").val();
    var dateInput = dateYearInput + "-" + dateMonthInput + "-1";
    dateInput = new Date(dateInput);
    var dateNext = new Date(dateInput.getFullYear(), dateInput.getMonth() + 1, 1);

    var dateMonthNext = dateNext.getMonth();
    var dateYearNext = dateNext.getFullYear();
    var dateYearNextBD = dateYearNext + 543;

    $("#monthYear").val(monthNames[dateMonthNext] + " " + dateYearNextBD);
    $("#month").val(dateMonthNext + 1);
    $("#year").val(dateYearNext);

}
function preMonth() {
    var dateMonthInput = $("#month").val();
    var dateYearInput = $("#year").val();
    var dateInput = dateYearInput + "-" + dateMonthInput + "-1";
    dateInput = new Date(dateInput);
    var datePre = new Date(dateInput.getFullYear(), dateInput.getMonth() - 1, 1);

    var dateMonthPre = datePre.getMonth();
    var dateYearPre = datePre.getFullYear();
    var dateYearPreBD = dateYearPre + 543;

    $("#monthYear").val(monthNames[dateMonthPre] + " " + dateYearPreBD);
    $("#month").val(dateMonthPre + 1);
    $("#year").val(dateYearPre);
}
function nextYear() {
    var dateYearInput = $("#yearBD").val();
    var dateYearNextBD = parseInt(dateYearInput) + 1;

    $("#yearBD").val(dateYearNextBD);
}

function preYear() {
    var dateYearInput = $("#yearBD").val();
    var dateYearPreBD = parseInt(dateYearInput) - 1;

    $("#yearBD").val(dateYearPreBD);
}



function dokeyup(obj) {
    var key = event.keyCode;
    if (key != 37 & key != 39 & key != 110) {
        var value = obj.value;
        var svals = value.split("."); //แยกทศนิยมออก
        var sval = svals[0]; //ตัวเลขจำนวนเต็ม

        var n = 0;
        var result = "";
        var c = "";
        for (a = sval.length - 1; a >= 0; a--) {
            c = sval.charAt(a);
            if (c != ',') {
                n++;
                if (n == 4) {
                    result = "," + result;
                    n = 1;
                }
                ;
                result = c + result;
            }
            ;
        }
        ;

        if (svals[1]) {
            result = result + '.' + svals[1];
        }
        ;

        obj.value = result;
    }
    ;
}
;

//ให้ text รับค่าเป็นตัวเลขอย่างเดียว
function checknumber() {
    key = event.keyCode;
    if (key != 45 && key != 46 && (key < 48 || key > 57)) {
        event.returnValue = false;
    }
    ;
}
;

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

var validateform = function() {
    return {
        validate: function (formname = '') {
            if(formname === ''){
                return false;
            }
            var chk = true;
            $('#'+formname).find(':input').each(function (key, value) {
                if (this.type != 'hidden' && this.required && !this.disabled) {
                    if (!this.value.length) {
                        chk = false;
                        return chk;
                    }
                }

            });
            return chk;
        }
    };
}();
