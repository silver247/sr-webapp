<?php $pagename = "Home" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appmng = new AppManager();
$userplan = new response();
$userid = null;
if ($_SESSION['role'] != ROLE_ADMIN) {
    $userid = $_SESSION["usrid"];
}
$year = date("Y");
$month = date("n");
$userplan = $appmng->queryUserPlan($userid, $month, $year);
$salessummary = new response();
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-sm-12 col-lg-5">
            <div class="block full" style="padding:40px 40px 20px 40px;margin-bottom:30px;">
                <div class="row">
                    <div class="col-xs-12">
                        <strong>สรุปยอดขายรายวัน (<?= $nowDMYTHSH ?>)</strong>
                        <?php
                        $tragetvalue = $userplan->MSGDATA1["target_sales_value"];
                        $value = floatval($tragetvalue);
                        $year = date("Y");
                        $month = date("n");
                        $date = date('Y-m-d');
                        $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                        $viewvalue = number_format($value / 12 / $day + 1, 2);
                        $day = date("j"); //\]
                        $summaryvalue = new response();
                        $summaryvalue = $appmng->GetSalesSummary($userid, $date, $month, $year);
                        //service::printr($summaryvalue);
                        $current_sales = 0;
                        if (!empty($summaryvalue->MSGDATA1[0][$month])) {
                            $current_sales = $summaryvalue->MSGDATA1[0][$month];
                        }
                        $percenvalue = 0;
                        if ($value != 0) {
                            $percenvalue = ($current_sales  * 100) / floatval(str_replace(',', '', $viewvalue)) ;//($value / 12 / $day + 1);
                        }

                        $viewpercenvalue = number_format($percenvalue, 0);
                        $viewsummaryvalue = number_format($current_sales, 0);
                        ?>
                        <div class="progress progressWithBT">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percenvalue > 100 ? 100 : $percenvalue ?>%"><?= $viewpercenvalue ?>%</div>
                        </div>
                        <div class="progressBT"><?= $viewsummaryvalue ?>/<?= $viewvalue ?> บาท</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <strong>สรุปยอดขายรายเดือน (<?= $nowMTH ?>)</strong>
                        <?php
                        $viewvalue = number_format($value / 12, 2);
                        $summaryvalue = new response();
                        $summaryvalue = $appmng->GetSalesSummary($userid, 0, $month, $year);
                        //service::printr($summaryvalue);
                        $current_sales = 0;
                        if (!empty($summaryvalue->MSGDATA1[0][$month])) {
                            $current_sales = $summaryvalue->MSGDATA1[0][$month];
                        }
                        $percenvalue = 0;
                        if ($value != 0) {
                            $percenvalue = ($current_sales / ($value / 12)) * 100;
                        }

                        $viewpercenvalue = number_format($percenvalue, 0);
                        $viewsummaryvalue = number_format($current_sales, 0);
                        ?>
                        <div class="progress progressWithBT">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="83.30" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percenvalue > 100 ? 100 : $percenvalue ?>%"><?= $viewpercenvalue ?>%</div>
                        </div>
                        <div class="progressBT"><?= $viewsummaryvalue ?>/<?= $viewvalue ?> บาท</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <strong>สรุปยอดขายรายปี (<?= $nowYTH ?>)</strong>
                        <?php
                        $viewvalue = number_format($value, 2);
                        $tmpsummaryvalue = new response();
                        $tmpsummaryvalue = $appmng->GetSalesSummary($userid, 0, 0, $year);
                        $summaryvalue = 0;
                        if (isset($tmpsummaryvalue->MSGDATA1[0])) {
                            foreach ($tmpsummaryvalue->MSGDATA1[0] as $aSummary) {
                                $summaryvalue += floatval($aSummary);
                            }
                        }
                        $percenvalue = 0;
                        if ($value != 0) {
                            $percenvalue = ($summaryvalue / ($value)) * 100;
                        }

                        $viewpercenvalue = number_format($percenvalue, 0);
                        $viewsummaryvalue = number_format($summaryvalue, 0);
                        ?>
                        <div class="progress progressWithBT">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="12.50" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percenvalue > 100 ? 100 : $percenvalue ?>%"><?= $viewpercenvalue ?>%</div>
                        </div>
                        <div class="progressBT"><?= $viewsummaryvalue ?>/<?= $viewvalue ?> บาท</div>
                    </div>
                </div>
            </div>
            <div class="block full">
                <div class="block-title">ผลการประเมิน</div>
                <div class="row">
                    <?php
                    $summaryvalue = new response();
                    $summaryvalue = $appmng->GetSalesSummary($userid, 0, $month, $year);
                    //service::printr($summaryvalue);
                    $sales = 0;
                    if (!empty($summaryvalue->MSGDATA1[0][$month-2])) {
                        $sales = $summaryvalue->MSGDATA1[0][$month-2];
                    }
                    $percenvalue = 0;
                    if ($value != 0) {
                        $percenvalue = $sales / ($value / 12);
                    }

                    $conditon = new response();
                    $conditon = $appmng->queryConditionSale();
                    $viewgrade = "-";
                    foreach ($conditon->MSGDATA1 as $aConditon) {
                        if (($percenvalue >= $aConditon["condition_min"] && $percenvalue <= $aConditon["condition_max"]) || $percenvalue > $aConditon["condition_max"]) {
                            $viewgrade = $aConditon["eval_grade"];
                            break;
                        }
                    }
                    ?>
                    <div class="col-xs-4">
                        <div class="monthRate"><?= $last2MYTH ?></div>
                        <div class="rate"><?= $viewgrade ?></div>
                    </div>
                    <?php
                    $summaryvalue = new response();
                    $summaryvalue = $appmng->GetSalesSummary($userid, 0, $month, $year);
                    $percenvalue = 0;
                    $sales = 0;
                    if (!empty($summaryvalue->MSGDATA1[0][$month-1])) {
                        $sales = $summaryvalue->MSGDATA1[0][$month-1];
                    }
                    if ($value != 0) {
                        $percenvalue = $sales / ($value / 12);
                    }

                    $viewgrade = "-";
                    foreach ($conditon->MSGDATA1 as $aConditon) {
                        if (($percenvalue >= $aConditon["condition_min"] && $percenvalue <= $aConditon["condition_max"]) || $percenvalue > $aConditon["condition_max"]) {
                            $viewgrade = $aConditon["eval_grade"];
                            break;
                        }
                    }
                    ?>
                    <div class="col-xs-4 rateLine">
                        <div class="monthRate"><?= $last1MYTH ?></div>
                        <div class="rate"><?= $viewgrade ?></div>
                    </div>
                    <?php
                    $summaryvalue = new response();
                    $summaryvalue = $appmng->GetSalesSummary($userid, 0, $month, $year);
                    $percenvalue = 0;
                    $sales = 0;
                    if (!empty($summaryvalue->MSGDATA1[0][$month])) {
                        $sales = $summaryvalue->MSGDATA1[0][$month];
                    }
                    if ($value != 0) {
                        $percenvalue = ($sales / ($value / 12)) * 100;
                    }

                    $viewgrade = "-";
                    foreach ($conditon->MSGDATA1 as $aConditon) {
                        if (($percenvalue >= $aConditon["condition_min"] && $percenvalue <= $aConditon["condition_max"]) || $percenvalue > $aConditon["condition_max"]) {
                            $viewgrade = $aConditon["eval_grade"];
                            break;
                        }
                    }
                    ?>
                    <div class="col-xs-4 rateLine">
                        <div class="monthRate"><?= $nowMYTH ?></div>
                        <div class="rate"><?= $viewgrade ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-7">
            <div class="block full">
                <div class="block-title">5 อันดับยอดขาย</div>
                <label class="control-label inlineDiv" style="width:40px;">เดือน</label>
                <div class="inlineDiv">
                    <div class="input-group" style="width:272px;">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();ReqforTop5();"><i class="fa fa-chevron-left"></i></button>
                        </span>
                        <input type="text" id="monthYear" name="monthYear" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();ReqforTop5();"><i class="fa fa-chevron-right"></i></button>
                        </span>
                    </div>
                </div>
                <input id="month" type="hidden"><input id="year" type="hidden">
                <div class="row">
                    <h3 class="col-xs-12" style="color: #ff8c04">สินค้าขายดี</h3>
                    <div class="col-xs-12">
                        <div class="col-xs-4 rangeBlock">
                            <h1 class="numRange1">1</h1>
                            <div class="rangeDetail1">
                                <span id="best_product_1"></span><br/>
                                <strong><span id="best_product_1_vol"></span></strong>
                            </div>
                        </div>
                        <div class="col-xs-4 rangeBlock">
                            <h1 class="numRange">2</h1>
                            <div class="rangeDetail">
                                <span id="best_product_2"></span><br/>
                                <strong><span id="best_product_2_vol"></span></strong>
                            </div>
                        </div>
                        <div class="col-xs-4 rangeBlock">
                            <h1 class="numRange">3</h1>
                            <div class="rangeDetail">
                                <span id="best_product_3"></span><br/>
                                <strong><span id="best_product_3_vol"></span></strong>
                            </div>
                        </div>
                        <div class="col-xs-12 rangeBlock2">
                            <div class="rangeDetail2">
                                <table width="100%">
                                    <tr>
                                        <td width="20px">4</td>
                                        <td><span id="best_product_4"></span></td>
                                        <td align="right"><strong><span id="best_product_4_vol"></span></strong></td>
                                        <td rowspan="2" width="110px" align="right"><a href="Rating/Product/" class="btnRating"><img src="img/icon_menu/rangeAll.png"/></a></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td><span id="best_product_5"></span></td>
                                        <td align="right"><strong><span id="best_product_5_vol"></span></strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h3 class="col-xs-12" style="color: #ff8c04">สุดยอดร้านค้า</h3>
                    <div class="col-xs-12">
                        <div class="col-xs-4 rangeBlock">
                            <h1 class="numRange1">1</h1>
                            <div class="rangeDetail1">
                                <span id="best_customer_1"></span><br/>
                                <strong><span id="best_customer_1_vol"></span></strong>
                            </div>
                        </div>
                        <div class="col-xs-4 rangeBlock">
                            <h1 class="numRange">2</h1>
                            <div class="rangeDetail">
                                <span id="best_customer_2"></span><br/>
                                <strong><span id="best_customer_2_vol"></span></strong>
                            </div>
                        </div>
                        <div class="col-xs-4 rangeBlock">
                            <h1 class="numRange">3</h1>
                            <div class="rangeDetail">
                                <span id="best_customer_3"></span><br/>
                                <strong><span id="best_customer_3_vol"></span></strong>
                            </div>
                        </div>
                        <div class="col-xs-12 rangeBlock2">
                            <div class="rangeDetail2">
                                <table width="100%">
                                    <tr>
                                        <td width="20px">4</td>
                                        <td><span id="best_customer_4"></span></td>
                                        <td align="right"><strong><span id="best_customer_4_vol"></span></strong></td>
                                        <td rowspan="2" width="110px" align="right"><a href="Rating/Shop/" class="btnRating"><img src="img/icon_menu/rangeAll.png"/></a></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td><span id="best_customer_5"></span></td>
                                        <td align="right"><strong><span id="best_customer_5_vol"></span></strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script>
    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
    $(document).ready(function () {
        ReqforTop5();
    });
    $('.btnRating').on('click', function (e) {
        e.preventDefault();
        var addr = $(this).attr('href');
        window.location = addr + $('#year').val() + '/' + $('#month').val() + '/';
    });

    function ReqforTop5() {
        var m = $('#month').val();
        var y = $('#year').val();
        var request = '<?= REQ_SALE_HOME_TOP5; ?>';
        $.ajax({
            url: "AppHttpRequest.php",
            type: "POST",
            data: {month: m, year: y, req: request},
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            dataType: 'JSON',
            error: function (transport, status, errorThrown) {
                bootbox.alert("error : " + errorThrown.stack + "detail : " + transport.responseText);
            },
            success: function (data) {
                if (data.MSGID == req_success) {
                    RenderSaleHomeTop5(data.MSGDATA1.MSGDATA1, data.MSGDATA2.MSGDATA1);
                } else {
                    bootbox.alert("error : " + data + "detail : " + data.MSGMESSAGE1);
                }
            }
        });
    }

    function RenderSaleHomeTop5(data1, data2) {
        var i = 1;
        ClearData();
        $.each(data1, function (key, value) {
            $("#best_product_" + i).empty().html(value.PRODUCT_NAME);
            $("#best_product_" + i + "_vol").empty().html(value.AMOUNT);
            i++;
        });
        var i = 1;
        $.each(data2, function (key, value) {
            $("#best_customer_" + i).empty().html(value.CUSTOMER_NAME);
            $("#best_customer_" + i + "_vol").empty().html(numberWithCommas(value.TOTAL));
            i++;
        });
        return;
    }

    function ClearData() {
        for (var i = 1; i <= 5; i++) {
            $("#best_product_" + i).empty();
            $("#best_product_" + i + "_vol").empty();
            $("#best_customer_" + i).empty();
            $("#best_customer_" + i + "_vol").empty();
        }
        return;
    }
</script>	

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/readyDashboard.js"></script>
<script>$(function () {
        ReadyDashboard.init();
    });</script>

<?php include 'inc/template_end.php'; ?>