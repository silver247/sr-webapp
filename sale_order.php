<?php $pagename = "Order" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">รายการสั่งซื้อ</div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:35px;">ร้าน</label>
                    <select id="shop" name="shop" class="form-control inlineDiv" style="width:200px;margin-right:30px;" onChange="tableOrder(this.value, $('#month').val(), $('#year').val())">
                        <option value="0">แสดงทั้งหมด</option>
                        <?= $appm->GetCustomerDropdownlist(); ?>
                    </select>
                    <label class="control-label inlineDiv" style="width:40px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();tableOrder($('#shop').val(), $('#month').val(), $('#year').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();tableOrder($('#shop').val(), $('#month').val(), $('#year').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                    <label class="csscheckbox csscheckbox-primary"><input id="allMonth" type="checkbox"><span style="background: #FFF"></span></label>
                    <label class="control-label inlineDiv">แสดงทุกเดือน</label>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="btn-right"><a href="Order/Add/" data-toggle="tooltip" title="เพิ่มรายการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มรายการสั่งซื้อ</a></div>
                    <div class="table-responsive" id="tableOrder"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
                                    $('#monthYear').datepicker({
                                        format: "MM yyyy",
                                        weekStart: 0,
                                        viewMode: "months",
                                        minViewMode: "months",
                                        language: "th-th"
                                    });
</script>

<!-- Add Entry Form-->
<div id="lightBlock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>
<!-- END Add Entry Form -->

<script>
    $(document).ready(function () {
        //set initial state.
        $("#allMonth").change(function () {
            if (this.checked) {
                $("#monthYear").prop('disabled', true);
                $("#preMonth").prop('disabled', true);
                $("#nextMonth").prop('disabled', true);
                tableOrder($('#shop').val(), "0", "0");
            } else {
                $("#monthYear").prop('disabled', false);
                $("#preMonth").prop('disabled', false);
                $("#nextMonth").prop('disabled', false);
                tableOrder($('#shop').val(), $("#month").val(), $("#year").val());
            }
        });

        tableOrder($('#shop').val(), $("#month").val(), $("#year").val());

        $(document).on('click', '#btnCancel', function (e) {
            e.preventDefault();
            var order = $(this).data('key').toString();
            bootbox.confirm({
                size: 'small',
                title: "ยืนยันการลบเอกสารขาย",
                message: "คุณต้องการยืนยันการลบเอกสารขายใช่หรือไม่",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> ยกเลิก',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> ยืนยันการลบเอกสารขาย',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    var loading = "";
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "AppHttpRequest.php",
                            data: {req: '<?= REQ_DEL_ORDER; ?>', orderid: order},
                            dataType: "json",
                            beforeSend: function (xhr) {
                                loading = bootbox.dialog({
                                    size: 'small',
                                    message: '<p class="text-center">เรากำลังบันทึกข้อมูลการลบเอกสารขายของท่าน กรุณารอสักครู่...</p>',
                                    closeButton: false
                                });
                            },
                            error: function (transport, status, errorThrown) {
                                console.log(transport.reponseText);
                                setTimeout(function () {
                                    loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "เกิดข้อผิดพลาดขึ้น กรุณาติดต่อผู้ดูแลระบบ",
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Order/";
                                            }
                                        });
                                }, 3000);
                            },
                            success: function (data) {
                                console.log(data);
                                //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                if (data.MSGID === 100) {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: data.MSGMESSAGE1,
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Order/";
                                            }
                                        });
                                    }, 3000);
                                } else {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: data.MSGMESSAGE1,
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Order/";
                                            }
                                        });
                                    }, 3000);
                                }
                            }
                        });

                    }

                }
            });
        });
    });
</script>

<script>
    function RemoveOrder(orderid) {
        alert(orderid.trim());
    }

    function lightBlock(range, sortby) {
        //alert(sortby);

        window.location.hash = '#lightBlock';
        $('#lightBlock').modal();

        $.ajax({
            type: "GET",
            url: "rating_block.php",
            data: {range: range, sortby: sortby},
            success: function (data) {
                $('#lightBlock').html(data);
            }
        });
    }
</script>

<script>
    function tableOrder(shop, month, year) {
        //alert(shop+month+year);

        $.ajax({
            type: "GET",
            url: "sale_order_table.php",
            data: {shop: shop, month: month, year: year},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);
                var show = "";
                show += '<table id="orderTable" class="table table-vcenter table-condensed table-striped table-borderless">';
                show += '<thead>';
                show += '<tr>';
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>';
                show += '<th class="text-center" style="width: 100px;">วันที่สั่งซื้อ</th>';
                show += '<th>ชื่อร้าน</th>';
                show += '<th class="text-right" style="width: 100px;">ยอดขายรวม</th>';
                show += '<th class="text-center" style="width: 200px;">สถานะการสั่งซื้อ</th>';
                show += '<th style="width: 250px;"></th>';
                show += '</tr>';
                show += '</thead>';
                show += '<tbody>';
                var status_desc = new Map([
                    ['<?= ORDER_STATUS_REJT; ?>', 'เอกสารขายไม่ได้รับการอนุมัติ']
                    ,['<?= ORDER_STATUS_OPEN; ?>', 'เอกสารขายรอการอนุมัติ']
                    ,['<?= ORDER_STATUS_WPAY; ?>', 'เอกสารขายได้รับการอนุมัติแล้ว']
                    ,['<?= ORDER_STATUS_PCOM; ?>', 'เอกสารขายได้รับการชำระเงินเรียบร้อยแล้ว']
                    ,['<?= ORDER_STATUS_SHIP; ?>', 'สินค้าภายในเอกสารขาย อยู่ระหว่างการขนส่ง']
                    ,['<?= ORDER_STATUS_DONE; ?>', 'เอกสารขายเสร็จสมบูรณ์']
                    ,['<?= ORDER_STATUS_CANC; ?>', 'เอกสารขายถูกยกเลิกโดยผู้ใช้งาน']
                ]);
                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>'
                        show += '<td class="text-center">' + i + '</td>';
                        show += '<td class="text-center">' + value.TIMESTAMP + '</td>';
                        show += '<td>' + value.CUSTOMER_NAME + '</td>';
                        show += '<td class="text-right">' + numberWithCommas(value.TOTAL) + '.-</td>';
                        show += '<td class="text-center">' + status_desc.get(value.STATUS) + '</td>';
                        show += '<td class="text-right">';
                        show += '<a href="Order/' + value.ORDERID + '/" data-toggle="tooltip" title="ดูรายละเอียดการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> ดูการสั่งซื้อ</a>';
                        var disabled = '';
                        if (value.STATUS != '<?= ORDER_STATUS_WPAY; ?>' && value.STATUS != '<?= ORDER_STATUS_OPEN; ?>') {
                            disabled = 'disabled';
                        }
                        show += '<a href="Order/Edit/' + value.ORDERID + '/" data-toggle="tooltip" title="แก้ไขรายการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-warning ' + disabled + '"><i class="gi gi-pencil"></i> แก้ไข</a>';
                        show += '<a href="#" id="btnCancel" data-key="' + value.ORDERID + '" data-toggle="tooltip" title="ลบรายการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-danger ' + disabled + '"><i class="gi gi-bin"></i> ลบ</a>';
                        show += '</td>';
                        show += '</tr>';
                        i++;
                    });
                }


                /*			obj = jQuery.parseJSON(data);
                 
                 if(obj['entry'] != ''){
                 $.each(obj['entry'],function(key,val){
                 show += '<tr>'
                 show += '<td>'+val["shipdate"]+'</td>'
                 show += '<td>'+val["ticketdate"]+'</td>'
                 show += '<td>'+val["outdate"]+'</td>'
                 show += '<td>'+val["companyname_en"]+'</td>'
                 show += '</tr>'
                 })
                 }
                 
                 for (i = 1; i < 31; i++) {
                 show += '<tr>'
                 show += '<td class="text-center">' + i + '</td>'
                 show += '<td class="text-center">??/??/????</td>'
                 show += '<td>?????????????</td>'
                 show += '<td class="text-right">xx,xxx.-</td>'
                 show += '<td class="text-center">??????</td>'
                 show += '<td class="text-right">'
                 show += '<a href="Order/ID/" data-toggle="tooltip" title="ดูรายละเอียดการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> ดูการสั่งซื้อ</a>'
                 show += '<a href="Order/Edit/ID/" data-toggle="tooltip" title="แก้ไขรายการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-warning"><i class="gi gi-pencil"></i> แก้ไข</a>'
                 show += '<a href="javascript:void(0)" data-toggle="tooltip" title="ลบรายการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-danger"><i class="gi gi-bin"></i> ลบ</a>'
                 show += '</td>'
                 show += '</tr>'
                 }*/
                show += '</tbody>'
                show += '</table>'

                $('#tableOrder').empty().html(show);
                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });
                $('#orderTable').dataTable({
                    "ordering": false,
                    "info": false,
                    "searching": false,
                    "lengthChange": false
                });
            }
        });
    }
</script>


<?php include 'inc/template_end.php'; ?>