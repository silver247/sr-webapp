<?php require_once('functions/convertdate.php'); ?>
<?php
include_once './bundle.php';
$appm = new AppManager();
$planid = filter_input(INPUT_POST, 'planid');
//echo 'planid = '.$planid;
$planheader_response = $appm->GetMyPlanHeaderByPlanID($planid);
//print_r($planheader_response);
$planheader = $planheader_response->MSGDATA1[0];
?>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <form name="recordform" method="post" id="recordform" class="push" onsubmit="CreateNewOrderRefPlan();return false;">
            <input type="hidden" name="planid" id="planid" value="<?= $planid; ?>">
            <input type="hidden" name="req" id="req" value="<?= REQ_ADD_ORDER_REFPLAN; ?>">
            <input type="hidden" name="customer" id="customer" value="<?= $planheader->CUSTOMER_ID; ?>">
            <div class="modal-header">
                <div class="block-title">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <i class="gi gi-floppy_disk"></i> บันทึกผลการเข้าพบลูกค้า
                </div>
            </div>
            <div class="block modal-body">
                <table width="100%">
                    <tr>
                    <table>
                        <tr>
                            <td width="40px"><strong>ร้าน</strong></td>
                            <td><?= $planheader->CUSTOMER_NAME; ?></td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td width="80px"><strong>วันที่เข้าพบ</strong></td>
                            <td width="180px"><?= thaidate_withouttime_short($planheader->PLANDATE) ?></td>
                            <td width="45px"><strong>เวลา</strong></td>
                            <td><?= $planheader->PLANTIME; ?></td>
                        </tr>
                    </table>
                    </tr>
                </table>
                <!--<div class="topicLine"></div>-->

                <label><strong>การสั่งซื้อ</strong></label>
                <div class="orderTable">
                    <div class="table-responsive">
                        <table id="ratingTable" class="table table-vcenter table-condensed table-striped table-borderless">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 100px;">ลำดับที่</th>
                                    <th>รุ่นสินค้า</th>
                                    <th class="text-right" style="width: 100px;">ชนิดราคา</th>
                                    <th class="text-right" style="width: 100px;">ราคาขาย</th>
                                    <th class="text-center" style="width: 100px;">จำนวน</th>
                                    <th class="text-right" style="width: 100px;">จำนวนเงิน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cnt = filter_input(INPUT_POST, 'rowOrdercnt');
                                $pricetype = ['ราคาปลีก','ราคาส่ง'];
                                //echo $cnt;
                                for ($i = 0; $i < $cnt; $i++) {
                                    $x = 1;
                                    if (isset($_POST['txtPrice'][$i]) && isset($_POST['txtPrice'][$i]) && isset($_POST['txtPrice'][$i])) {
                                        $str = '';
                                        $str .= '<tr>';
                                        $str .= '<td class="text-center">' . $x . '</td>';
                                        $str .= '<td>' . $appm->GetProductDescription($_POST['ddlproductOrder'][$i]) . ' <input type="hidden" name="productOrder[]" value = "' . $_POST['ddlproductOrder'][$i] . '"></td>';
                                        $str .= '<td class="text-right">' . $pricetype[$_POST['pricetype'][$i]] . '.- <input type="hidden" name="pricetype[]" value = "' . $_POST['pricetype'][$i] . '"></td>';
                                        $str .= '<td class="text-right">' . number_format($_POST['txtPrice'][$i],2) . '.- <input type="hidden" name="txtPrice[]" value = "' . $_POST['txtPrice'][$i] . '"></td>';
                                        $str .= '<td class="text-center">' . $_POST['txtQty'][$i] . ' <input type="hidden" name="txtQty[]" value = "' . $_POST['txtQty'][$i] . '"></td>';
                                        $str .= '<td class="text-right">' . $_POST['txtTotal'][$i] . '.- <input type="hidden" name="txtTotal[]" value = "' . $_POST['txtTotal'][$i] . '"></td>';
                                        $str .= '<input type="hidden" name="promotionid[]">';
                                        $str .= '</tr>';
                                        echo $str;
                                        $x++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="<?= $cnt; ?>">
                    <?= '<div align="right">ส่วนลดพิเศษ <input class="form-control disabledText inlineDiv" value = "' . $_POST['sd_cal'] . '" name="sd_cal" style="width:130px;" disabled/> บาท </div>'; ?>
                    <?= '<div align="right">ยอดขายรวมทั้งหมด <input class="form-control disabledText inlineDiv" value = "' . $_POST['txtSubtotal'] . '" name="txtOrderSubtotal" style="width:130px;" disabled/> บาท <input type="hidden" name="txtSubtotal" value = "' . $_POST['txtSubtotal'] . '"></div>'; ?>

                </div>
            </div>
            <div class="block modal-footer">
                <div class="btn-right" style="position: relative;">
                    <button type="submit" id="btnconfirm" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="ยืนยันบันทึกผลการเข้าพบลูกค้า" aria-describedby="tooltip441840"><i class="gi gi-floppy_disk"></i> ยืนยันการบันทึกผล</button>

                </div>
            </div>
        </form>
    </div>
</div>

<!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>
<script>
            function CreateNewOrderRefPlan() {
                bootbox.confirm({
                    size: 'small',
                    title: "ยืนยันการนัดหมาย",
                    message: "คุณต้องการยืนยันการนัดหมายใช่หรือไม่",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> ยกเลิก',
                            className: 'btn-danger'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> ยืนยันการทำรายการ',
                            className: 'btn-success'
                        }
                    },
                    callback: function (result) {
                        var loading = "";
                        if (result) {
                            $.ajax({
                                method: "POST",
                                url: "AppHttpRequest.php",
                                data: $("#recordform").serialize(),
                                dataType: "json",
                                beforeSend: function (xhr) {
                                    loading = bootbox.dialog({
                                        size: 'small',
                                        message: '<p class="text-center">เรากำลังบันทึกข้อมูลการนัดหมายของท่าน กรุณารอสักครู่...</p>',
                                        closeButton: false
                                    });
                                },
                                error: function (transport, status, errorThrown) {
                                    console.log("error : " + errorThrown + "detail : " + transport.responseText);
                                    setTimeout(function () {
                                        loading.find('.bootbox-body').html("error : " + errorThrown + "detail : " + transport.responseText);
                                    }, 3000);
                                    setTimeout(function () {
                                        loading.modal('hide');
                                    }, 5000);
                                    //loading.modal('hide');
                                    //bootbox.alert("error : " + errorThrown + "detail : " + transport.responseText);
                                    //$this.attr('disabled', false);
                                },
                                success: function (data) {
                                    loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                    if (data.MSGID === 100) {
                                        setTimeout(function () {
                                            loading.modal('hide');
                                            window.location = "Planner/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>";
                                        }, 3000);
                                    } else {
                                        setTimeout(function () {
                                            loading.modal('hide');
                                        }, 3000);
                                    }
                                }
                            });

                        }

                    }
                });
            }
</script>