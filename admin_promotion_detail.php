<?php
$pagename = "Product";
$subpagename = "Promotion";
?>
<?php
include './bundle.php';
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
$promotionid = filter_input(INPUT_GET, 'id');
$appm = new AppManager();
$promotionobject = $appm->GetPromotionDetail($promotionid);
$promotionheader = $appm->GetPromotionHeader($promotionid, '', '')->MSGDATA1[0];
$promotion = $promotionobject->MSGDATA1[0];
//service::printr($promotion);
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    รายละเอียดโปรโมชั่น 
                    <a href="Admin/Promotion/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าโปรโมชั่น</div></a>
                </div>

                <div class="block-option">
                    <table>
                        <tr>
                            <th colspan="2">โปรโมชั่น <?= $promotionheader->PROMOTION_NAME; ?></th>
                        </tr>
                        <tr>
                            <th width="100px">รายละเอียด</th>
                            <td><?= $promotionheader->PROMOTION_DESC; ?></td>
                            <th>ระยะเวลา</th>
                            <td>&nbsp;<?= $promotionheader->PROMOTION_STARTDATE; ?> - <?= $promotionheader->PROMOTION_ENDDATE; ?></td>
                        </tr>
                        <tr>
                            <th>ระยะเวลา</th>
                            <td>&nbsp;<?= $promotionheader->PROMOTION_STARTDATE; ?> - <?= $promotionheader->PROMOTION_ENDDATE; ?></td>
                        </tr>
                    </table>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableOrder">
                        <table id="orderTable" class="table table-vcenter table-condensed table-striped table-borderless">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 100px;">ลำดับที่</th>
                                    <th>รุ่นสินค้า</th>
                                    <th class="text-right" style="width: 150px;">ราคาปลีก</th>
                                    <th class="text-right" style="width: 150px;">ส่วนลดราคาปลีก</th>
                                    <th class="text-right" style="width: 150px;">ราคาปลีกสุทธิ</th>
                                    <th class="text-right" style="width: 150px;">ราคาส่ง</th>
                                    <th class="text-right" style="width: 150px;">ส่วนลดราคาส่ง</th>
                                    <th class="text-right" style="width: 150px;">ราคาส่งสุทธิ</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($promotion->PROMOTION_PRODUCT as $product) {
                                    $i = 1;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $i ?></td>
                                        <td><?= $product->PRODUCT_NAME; ?></td>
                                        <td class="text-right"><?= $product->PRODUCT_RETAIL_PRICE; ?>.-</td>
                                        <td class="text-right"><?= $product->PRODUCT_RETAIL_DISCOUNT * 100; ?> %</td>
                                        <td class="text-right"><?= $product->PRODUCT_RETAIL_DISCOUNT_PRICE; ?>.-</td>
                                        <td class="text-right"><?= $product->PRODUCT_WHOLE_PRICE; ?>.-</td>
                                        <td class="text-right"><?= $product->PRODUCT_WHOLE_DISCOUNT * 100; ?> %</td>
                                        <td class="text-right"><?= $product->PRODUCT_WHOLE_DISCOUNT_PRICE; ?>.-</td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
        UiTables.init();
    });</script>
<script>
    $('#ratingTable').dataTable({
        ordering: false,
        info: false,
        searching: false,
        lengthChange: false
    })

</script>

<?php include 'inc/template_end.php'; ?>