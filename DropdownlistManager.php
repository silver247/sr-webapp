<?php

require_once ('./framework/database.php');
require_once './bundle.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DropdownlistManager
 *
 * @author puwakitk
 */
class DropdownlistManager {

    //put your code here

    private $database;
    private $service;

    function __construct() {
        $this->database = new database();
        $this->service = new service();
    }

    function DropdownCustomer($condition) {
        $sql = "select distinct c.name, c.customerid, c.address, c.soi, c.road, "
                . " c.sub_district, c.district, c.provinceid, c.contactname, "
                . " c.contactphone, c.contactemail "
                . " ,customer_grp.desc as grpname, credit, discount "
                . " from customer c "
                . " join user_customer d "
                . " on c.customerid = d.customerid "
                . " join customer_grp on c.custgrpid = customer_grp.custgrpid "
                . $condition;
        //echo $sql;
        $ddl = "";
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                foreach ($res->QRESULT as $row) {
                    $ddl .= "<option value='" . $row['customerid'] . "' "
                            . " data-tokens='" . $row['customerid'] . "' "
                            . " data-grp='" . $row['grpname'] . "' "
                            . " data-credit='" . $row['credit'] . "' "
                            . " data-discount='" . $row['discount'] . "' "
                            . " data-subtext='" . $row['customerid'] . "'>" . $row['name'] . "</option> ";
                }
            } else {
                $ddl .= "<option value='0'>ไม่พบข้อมูล</option>";
            }
        } catch (PDOException $ex) {
            $ddl .= "<option value='0'>เกิดข้อผิดพลาด</option>";
        }
        return $ddl;
    }

    function DropdownProduct($condition, $val) {
        $salesmodule = new SalesModule();
        $sql = "select pt.name, p.productid, p.productname, "
                . " pp.price_individual_cost, pp.price_mass_cost, "
                . " pp.individual_margin_profit, pp.mass_margin_profit, "
                . " pp.price_date"
                . " from product p "
                . " join product_type pt "
                . " on p.prdtypeid = pt.prdtypeid "
                . " join product_price pp "
                . " on pp.productid = p.productid and pp.active = 1 "
                . $condition
                . "  p.Active = 1 ";
        //echo $sql;
        $ddl = "";
        $chk = '';
        if ($val != null || $val != '') {
            $chk = 'selected';
        }
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                foreach ($res->QRESULT as $row) {
                    $chk = '';
                    if ($val != null || $val != '') {
                        if ($row['productid'] == $val) {
                            $chk = 'selected';
                        }
                    }
                    $promotion = new promotion();
                    $product = new product();
                    $promotion = $salesmodule->GetPromotionDiscountByProductID($row['productid'])->MSGDATA1[0];
                    $product = $promotion->PROMOTION_PRODUCT[0];
                    $ddl .= "<option value='" . $row['productid'] . "' "
                            . "data-tokens='" . $row['productname'] . "' "
                            . "data-rprice='" . $product->PRODUCT_RETAIL_COST . "' "
                            . "data-wprice='" . $product->PRODUCT_WHOLE_COST . "' "
                            . "data-rdiscount='" . $product->PRODUCT_RETAIL_DISCOUNT . "' "
                            . "data-wdiscount='" . $product->PRODUCT_WHOLE_DISCOUNT . "' "
                            . "data-rdprice='" . $product->PRODUCT_RETAIL_DISCOUNT_PRICE . "' "
                            . "data-wdprice='" . $product->PRODUCT_WHOLE_DISCOUNT_PRICE . "' "
                            . "data-promotion='" . $promotion->PROMOTION_ID . "' "
                            /* . "data-icost='" . $row['price_individual_cost'] . "' "
                              . "data-mcost='" . $row['price_mass_cost'] . "' "
                              . "data-iprofit='" . $row['individual_margin_profit'] . "'"
                              . "data-mprofit='" . $row['mass_margin_profit'] . "' " */
                            . "data-pdate='" . $row['price_date'] . "' " . $chk . ">" . $row['productname'] . "</option> ";
                }
            } else {
                $ddl .= "<option value='0'>ไม่พบข้อมูล</option>";
            }
        } catch (PDOException $ex) {
            $ddl .= "<option value='0'>เกิดข้อผิดพลาด</option>";
        }
        return $ddl;
    }

    function DropdownProvince($id,$condition) {
        $sql = "select provinceid, name from province "
                . $condition;
        //echo $sql;
        $ddl = "";
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                foreach ($res->QRESULT as $row) {
                    $option = '';
                    if ($row['provinceid'] == $id) {
                        $option = 'selected';
                    }
                    $ddl .= "<option value='" . $row['provinceid'] . "' data-tokens='" . $row['name'] . "' " . $option . ">" . $row['name'] . "</option> ";
                }
            } else {
                $ddl .= "<option value='0'>ไม่พบข้อมูล</option>";
            }
        } catch (PDOException $ex) {
            $ddl .= "<option value='0'>เกิดข้อผิดพลาด</option>";
        }
        return $ddl;
    }

    function DropdownCustomerWithValue($condition, $val) {
        $sql = "select distinct c.name, c.customerid, c.address, c.soi, c.road, "
                . " c.sub_district, c.district, c.provinceid, c.contactname, "
                . " c.contactphone, c.contactemail "
                . " ,customer_grp.desc as grpname, credit, discount "
                . " from customer c "
                . " join user_customer d "
                . " on c.customerid = d.customerid "
                . " join customer_grp on c.custgrpid = customer_grp.custgrpid "
                . $condition;
        //echo $sql;
        $ddl = "";
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                foreach ($res->QRESULT as $row) {
                    $option = '';
                    if ($val == $row['customerid']) {
                        $option = 'selected';
                    }
                    $ddl .= "<option value='" . $row['customerid'] . "' "
                            . " data-tokens='" . $row['customerid'] . "' "
                            . " data-grp='" . $row['grpname'] . "' "
                            . " data-credit='" . $row['credit'] . "' "
                            . " data-discount='" . $row['discount'] . "' "
                            . " data-subtext='" . $row['customerid'] . "' " . $option . ">" . $row['name'] . "</option> ";
                }
            } else {
                $ddl .= "<option value='0'>ไม่พบข้อมูล</option>";
            }
        } catch (PDOException $ex) {
            $ddl .= "<option value='0'>เกิดข้อผิดพลาด</option>";
        }
        return $ddl;
    }

    function DropdownProductType($val) {
        $warehouse = new WarehouseModule();
        $prdtypelist = $warehouse->GetProductType()->MSGDATA1[0];
        //service::printr($prdtypelist);
        $ddl = "";
        foreach ($prdtypelist as $prdtype) {
            $option = '';
            if ($val != '' && $val == $prdtype['prdtypeid']) {
                $option = 'selected';
            }
            $ddl .= "<option value='" . $prdtype['prdtypeid'] . "' "
                    . " data-tokens='" . $prdtype['prdtypeid'] . "' "
                    . " data-subtext='" . $prdtype['prdtypeid'] . "' " . $option . ">" . $prdtype['name'] . "</option> ";
        }
         return $ddl;
    }
    
    function DropdownUserList($val){
        $usermodule = new UserModule();
        $condition = " where user.active = '".USER_STATUS_ACTIVE."' ";
        $condition .= " and user.roleid != '1' ";
        $userlist = $usermodule->GetUserDataList($condition);
        $ddl = "";
        foreach ($userlist->MSGDATA1 as $user) {
            $option = '';
            if ($val != '' && $val == $user->USERID) {
                $option = 'selected';
            }
            $ddl .= "<option value='" . $user->USERID . "' "
                    . " data-tokens='" . $user->TNAME . " ".$user->TLNAME."' "
                    //. " data-subtext='" . $prdtype['prdtypeid'] . "' "
                    . " " . $option . ">" . $user->TNAME . " ".$user->TLNAME."</option> ";
        }
         return $ddl;
    }
    
    function DropdownPriceType($val){
        $pricetype = ['ราคาปลีก','ราคาส่ง'];
        $ddl = "";
        for($i = 0; $i<= 1;$i++){
            $option = '';
            if ($val != '' && $val == $i) {
                $option = 'selected';
            }
            $ddl .= "<option value='" . $i . "' "
                    . " " . $option . ">" . $pricetype[$i] ."</option> ";
        }
         return $ddl;
    }
    
    function DropdownRegion($val){
        $sql = " select regionid, region.desc as regiondesc from region where active = '1' ";
        $ddl = "";
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                foreach ($res->QRESULT as $row) {
                    $option = '';
                    if ($val == $row['regionid']) {
                        $option = 'selected';
                    }
                    $ddl .= "<option value='" . $row['regionid'] . "' "
                            . " data-tokens='" . $row['regiondesc'] . "' " . $option . ">" . $row['regiondesc'] . "</option> ";
                }
            } else {
                $ddl .= "<option value='0'>ไม่พบข้อมูล</option>";
            }
        } catch (PDOException $ex) {
            $ddl .= "<option value='0'>เกิดข้อผิดพลาด</option>";
        }
        return $ddl;
    }
    
    function DropdownCustomerGroup($val){
        $sql = " select custgrpid,customer_grp.desc as grpname, credit, discount "
                . " from customer_grp ";
        $ddl = "";
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                foreach ($res->QRESULT as $row) {
                    $option = '';
                    if ($val == $row['custgrpid']) {
                        $option = 'selected';
                    }
                    $ddl .= "<option value='" . $row['custgrpid'] . "' "
                            . " data-tokens='" . $row['grpname'] . "' " . $option . ">" . $row['grpname'] . "</option> ";
                }
            } else {
                $ddl .= "<option value='0'>ไม่พบข้อมูล</option>";
            }
        } catch (PDOException $ex) {
            $ddl .= "<option value='0'>เกิดข้อผิดพลาด</option>";
        }
        return $ddl;
    }

}
