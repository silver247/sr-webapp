<?PHP
include_once './bundle.php';
$appm = new AppManager();
$yeartargets = $appm->queryYearPlan();
$year = filter_input(INPUT_GET, 'year');
$currentsalesobject = $appm->GetSalesRegionSummary($year, '' , '');
//service::printr($currentsalesobject);
$currentsales = $currentsalesobject->MSGDATA1[0];
$totalsales = 0;
foreach ($currentsales as $sales){
    $totalsales += $sales;
}
$status = 0;
$response = new response();
if($currentsalesobject->MSGID == SERV_COMPLETE){
    $status += 100;
    $response->MSGDATA1['current_sales'][0] = $currentsales;
    $response->MSGDATA1['current_sales'][1] = $totalsales;
}
if($yeartargets->MSGID == SERV_COMPLETE){
    $status += 100;
    $response->MSGDATA1['target_sales'][0] = $yeartargets->MSGDATA1;
    $response->MSGDATA1['target_sales'][1] = $yeartargets->MSGDATA2;
}
//service::printr($yeartargets);
//service::printr($response);
//echo $status;
header('HTTP/1.1 200 OK');
if($status != 200){
    header('HTTP/1.1 404 Not Found');
}
echo json_encode($response);

?>
