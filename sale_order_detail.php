<?php $pagename = "Order" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$orderid = filter_input(INPUT_GET, 'id');
$appm = new AppManager();
$orderdetail_response = $appm->GetMyOrderDetail($orderid);
$buyerobject = $appm->GetMyOrderHeader($orderid);
$orderheader = $buyerobject->MSGDATA1[0];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    รายละเอียดการสั่งซื้อ 
                    <a href="Order/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้ารายการสั่งซื้อ</div></a>
                </div>

                <div class="block-option">
                    <strong class="inlineDiv" style="width:200px;">ร้าน <?=$orderheader->CUSTOMER_NAME;?></strong>
                    <strong class="inlineDiv">วันที่สั่งซื้อ <?=thaidate_withouttime($orderheader->TIMESTAMP);?></strong>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableOrder">
                        <table id="orderTable" class="table table-vcenter table-condensed table-striped table-borderless">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 70px;">ลำดับที่</th>
                                    <th>รุ่นสินค้า</th>
                                    <th class="text-right" style="width: 130px;">ชนิดราคา</th>
                                    <th class="text-right" style="width: 130px;">ราคาขาย</th>
                                    <th class="text-right" style="width: 110px;">จำนวน</th>
                                    <th class="text-right" style="width: 130px;">จำนวนเงิน</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($orderdetail_response->MSGID == SERV_COMPLETE) {
                                    $pricetype = ['ราคาปลีก','ราคาส่ง'];
                                    $i = 1;
                                    $subtotal = 0;
                                    $sumprice = 0;
                                    $sumamount = 0;
                                    foreach ($orderdetail_response->MSGDATA1 as $row) {
                                        $order = new order();
                                        $order = $row;
                                        $total = $order->PRICE * $order->AMOUNT;
                                        $subtotal += $total;
                                        $sumamount += $order->AMOUNT;
                                        $sumprice += $order->PRICE;
                                        $str = '';
                                        $str .= '<tr>';
                                        $str .= '<td class="text-center">' . $i . '</td>';
                                        $str .= '<td>' . $order->PRODUCT_NAME . '</td>';
                                        $str .= '<td class="text-right">' . $pricetype[$order->PRICETYPE] . '.-</td>';
                                        $str .= '<td class="text-right">' . $order->PRICE . '.-</td>';
                                        $str .= '<td class="text-right">' . $order->AMOUNT . '</td>';
                                        $str .= '<td class="text-right">' . $total . '.-</td>';
                                        $str .= '</tr>';

                                        $str .= '';
                                        $str .= '';
                                        echo $str;
                                        $i++;
                                    }
                                    $customer_discount = $subtotal * $orderheader->CUSTOMER_DISCOUNT;
                                    $str = '<tr class="strong" style="background-color:#5ccdde;color:white;">';
                                    $str .= '<td colspan="3" class="text-right">รวมทั้งหมด</td>';
                                    $str .= '<td class="text-right">' . $sumprice . '.-</td>';
                                    $str .= '<td class="text-right">' . $sumamount . '</td>';
                                    $str .= '<td class="text-right">' . $subtotal . '.-</td>';
                                    $str .= '</tr>';
                                    $str .= '<tr class="strong" style="background-color:#5ccdde;color:white;">';
                                    $str .= '<td colspan="3" class="text-right">ส่วนลดพิเศษสำหรับลูกค้า</td>';
                                    $str .= '<td class="text-right">-</td>';
                                    $str .= '<td class="text-right">-</td>';
                                    $str .= '<td class="text-right">' . $customer_discount . '.-</td>';
                                    $subtotal = $subtotal - $customer_discount;
                                    $str .= '</tr>';
                                    $str .= '<tr class="strong" style="background-color:#5ccdde;color:white;">';
                                    $str .= '<td colspan="3" class="text-right">ยอดสุทธิ</td>';
                                    $str .= '<td class="text-right">' . $sumprice . '.-</td>';
                                    $str .= '<td class="text-right">' . $sumamount . '</td>';
                                    $str .= '<td class="text-right">' . $subtotal . '.-</td>';
                                    $str .= '</tr>';
                                    echo $str;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
        UiTables.init();
    });</script>
<script>
    $('#ratingTable').dataTable({
        ordering: false,
        info: false,
        searching: false,
        lengthChange: false
    })

</script>

<?php include 'inc/template_end.php'; ?>