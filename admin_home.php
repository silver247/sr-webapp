<?php $pagename = "Home" ?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
include './bundle.php';
$appm = new AppManager();
$yeartargets = $appm->queryYearPlan();
$currentsalesobject = $appm->GetSalesRegionSummary(date('Y'), '', '');
//service::printr($currentsalesobject);
$currentsales = $currentsalesobject->MSGDATA1[0];
$totalsales = 0;
foreach ($currentsales as $sales) {
    $totalsales += $sales;
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">ยอดขายทั้งหมด</div>
                <label class="control-label inlineDiv" style="width:80px;">ยอดขายปี</label>
                <div class="inlineDiv">
                    <div class="input-group" style="width:180px;">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preYear();loadpagedata($('#yearBD').val() - 543);"><i class="fa fa-chevron-left"></i></button>
                        </span>
                        <input type="text" id="yearBD" name="yearBD" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextYear();loadpagedata($('#yearBD').val() - 543);"><i class="fa fa-chevron-right"></i></button>
                        </span>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-12">
                        <div class="col-md-4 salesBlockAll">
                            <h2 class="salesHeader" style="padding: 20px 0 30px">ยอดขายทั้งหมดในปี 2560</h2>
                            
                            <div class="pie-chart easyPieChart" id="overall" data-percent="" data-size="300" data-line-width="13" data-bar-color="#ff8c04" data-track-color="#f9f9f9">
                                <span style="font-size:80px"><strong><span id="overall_percent"></span>%</strong></span>
                            </div>
                            <div class="salesDetail" style="font-size:30px"><span id="overall_sales"></span>/<span id="overall_target"></span> บาท</div>
                        </div>
                        <div class="col-md-8" style="padding:0">
                            <div class="salesBlock top right left">

                                <h3 class="salesHeader">ภาคเหนือ</h3>
                                <div class="pie-chart easyPieChart" id="north" data-percent="" data-size="150" data-line-width="7" data-bar-color="#ffc955" data-track-color="#fefefe">
                                    <span style="font-size:40px"><strong><span id="north_percent"></span>%</strong></span>
                                </div>
                                <div class="salesDetail"><span id="north_sales"></span>/<span id="north_target"></span> บาท</div>
                            </div>
                            <div class="salesBlock top right">
                                <h3 class="salesHeader">ภาคใต้</h3>
                                <div class="pie-chart easyPieChart" id="south" data-percent="" data-size="150" data-line-width="7" data-bar-color="#ffc955" data-track-color="#fefefe">
                                    <span style="font-size:40px"><strong><span id="south_percent"></span>%</strong></span>
                                </div>
                                <div class="salesDetail"><span id="south_sales"></span>/<span id="south_target"></span> บาท</div>
                            </div>
                            <div class="salesBlock top right">

                                <h3 class="salesHeader">ภาคตะวันออก</h3>
                                <div class="pie-chart easyPieChart" id="east" data-percent="" data-size="150" data-line-width="7" data-bar-color="#ffc955" data-track-color="#fefefe">
                                    <span style="font-size:40px"><strong><span id="east_percent"></span>%</strong></span>
                                </div>
                                <div class="salesDetail"><span id="east_sales"></span>/<span id="east_target"></span> บาท</div>
                            </div>
                            <div class="salesBlock top bottom right left">

                                <h3 class="salesHeader">ภาคตะวันตก</h3>
                                <div class="pie-chart easyPieChart" id="west" data-percent="" data-size="150" data-line-width="7" data-bar-color="#ffc955" data-track-color="#fefefe">
                                    <span style="font-size:40px"><strong><span id="west_percent"></span>%</strong></span>
                                </div>
                                <div class="salesDetail"><span id="west_sales"></span>/<span id="west_target"></span> บาท</div>
                            </div>
                            <div class="salesBlock top bottom right">

                                <h3 class="salesHeader">ภาคอีสานตอนบน</h3>
                                <div class="pie-chart easyPieChart" id="northeast" data-percent="" data-size="150" data-line-width="7" data-bar-color="#ffc955" data-track-color="#fefefe">
                                    <span style="font-size:40px"><strong><span id="northeast_percent"></span>%</strong></span>
                                </div>
                                <div class="salesDetail"><span id="northeast_sales"></span>/<span id="northeast_target"></span> บาท</div>
                            </div>
                            <div class="salesBlock top bottom right">

                                <h3 class="salesHeader">ภาคกลาง</h3>
                                <div class="pie-chart easyPieChart" id="middle" data-percent="" data-size="150" data-line-width="7" data-bar-color="#ffc955" data-track-color="#fefefe">
                                    <span style="font-size:40px"><strong><span id="middle_percent"></span>%</strong></span>
                                </div>
                                <div class="salesDetail"><span id="middle_sales"></span>/<span id="middle_target"></span> บาท</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script>
    $(document).ready(function () {
        loadpagedata($('#yearBD').val() - 543);
    });
    var loadpagedata = function (year){
        $.ajax({
            type: "GET",
            url: "admin_home_feed.php",
            data: {year: year},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log(transport.responseText);
            },
            success: function (data, status, transport) {
                console.log(data);

                renderpage(data);
            }
        });
    }
    var renderpage = function (dataobject) {
        var regions = ['north', 'south', 'east', 'west', 'northeast', 'middle'];
        var regionskey = [1, 6, 5, 4, 3, 2];
        var [north, south, east, west, northeast, middle] = regionskey;
        var percent = dataobject.MSGDATA1.current_sales[1] * 100 / dataobject.MSGDATA1.target_sales[1];
        //$('#overall').attr('data-percent', dataobject.MSGDATA1.current_sales[1]);
        $('#overall').data('easyPieChart').update(percent.toFixed(2));
        $('#overall_percent').html(percent.toFixed(2));
        $('#overall_sales').html(numberWithCommas(dataobject.MSGDATA1.current_sales[1]));
        $('#overall_target').html(numberWithCommas(dataobject.MSGDATA1.target_sales[1]));
        for (let region of regions) {
            console.log(region);
            var percent = dataobject.MSGDATA1.current_sales[0][eval(region)] * 100 / dataobject.MSGDATA1.target_sales[0][eval(region)].target_value;
            //$('#' + region).attr('data-percent', dataobject.MSGDATA1.current_sales[0][eval(region)]);
            $('#' + region).data('easyPieChart').update(percent.toFixed(2));
            $('#' + region + '_percent').html(percent.toFixed(2));
            $('#' + region + '_sales').html(numberWithCommas(dataobject.MSGDATA1.current_sales[0][eval(region)]));
            $('#' + region + '_target').html(numberWithCommas(dataobject.MSGDATA1.target_sales[0][eval(region)].target_value));
        }
    };
    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
</script>	

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/readyDashboard.js"></script>
<script>$(function () {
        ReadyDashboard.init();
    });</script>

<?php include 'inc/template_end.php'; ?>