<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    เพิ่มตารางนัดหมาย 
                    <a href="Planner/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปตารางนัดหมาย</div></a>
                </div>
                <div class="row select-btn-group">
                    <a href="Planner/Add/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>"class="select-btn old">
                        <div class="select-btn-icon"><i class="fa fa-angle-double-left"></i></div>
                        <div class="select-btn-text">
                            <div class="select-btn-top">ลูกค้า</div>
                            <div class="select-btn-bottom">เก่า</div>
                        </div>
                    </a>
                    <a href="Planner/AddNew/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>"class="select-btn new">
                        <div class="select-btn-text">
                            <div class="select-btn-top">ลูกค้า</div>
                            <div class="select-btn-bottom">ใหม่</div>
                        </div>
                        <div class="select-btn-icon"><i class="fa fa-angle-double-right"></i></div>
                    </a>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>