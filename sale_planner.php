<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    ตารางนัดหมาย
                </div>

                <div class="btn-right"><a href="Planner/Select/" data-toggle="tooltip" title="เพิ่มตารางนัดหมาย" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มตารางนัดหมาย</a></div>

                <div class="row">
                    <div class="col-xs-12">
                        <!-- FullCalendar (initialized in js/pages/compCalendar.js), for more info and examples you can check out http://arshaw.com/fullcalendar/ -->
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/compCalendar.js"></script>
<script>$(function () {
        CompCalendar.init();
    });</script>

<?php include 'inc/template_end.php'; ?>