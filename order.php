<?php
require_once './orderdetail.php';
require_once './user.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of order
 *
 * @author puwakitk
 */
class order extends orderdetail{
    //put your code here
    public $ORDERID = "";
    public $USERID = "";
    public $USER_NAMETH = "";
    public $DOCID = "";
    public $REFPLAN = "";
    public $TIMESTAMP = "";
    public $STATUS = "";
    public $STATUS_DESC = "";
    public $CUSTOMER_ID = "";
    public $CUSTOMER_NAME = "";
    public $CUSTOMER_DISCOUNT = "";
    public $TOTAL = "";
    public $ORDER_PRODUCTS = array();
}
