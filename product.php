<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of product
 *
 * @author puwakitk
 */
class product {
    //put your code here
    public $PRODUCT_ID = "";
    public $MODELID = "";
    public $PRODUCTTYPE_ID = "";
    public $PRODUCTTYPE_NAME = "";
    public $PRODUCT_NAME = "";
    public $PRODUCT_CREATEDATE;
    public $PRODUCT_RETAIL_COST = 0;
    public $PRODUCT_WHOLE_COST = 0;
    public $PRODUCT_RETAIL_PROFIT = 0;
    public $PRODUCT_WHOLE_PROFIT = 0;
    public $PRODUCT_REASON = "";
    public $PRODUCT_PRICEDATE = "";
    public $PRODUCT_RETAIL_DISCOUNT = 0;
    public $PRODUCT_WHOLE_DISCOUNT = 0;
    public $PRODUCT_STATUS = "";
    public $PRODUCT_RETAIL_PRICE = 0;
    public $PRODUCT_WHOLE_PRICE = 0;
    public $PRODUCT_RETAIL_DISCOUNT_PRICE = 0;
    public $PRODUCT_WHOLE_DISCOUNT_PRICE = 0;
}
