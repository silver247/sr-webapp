<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once './framework/service.php';
require_once './config/config.php';
require_once './config/const.php';
require_once './customer.php';
require_once './plan.php';
require_once './response.php';
require_once './order.php';
require_once './orderdetail.php';
require_once './product.php';
require_once './promotion.php';
require_once './AppManager.php';
require_once './AppBaseManager.php';
require_once './BusinessLogic.php';
require_once './DropdownlistManager.php';
require_once './modules/UserModule.php';
require_once './modules/SalesModule.php';
require_once './modules/PlanningModule.php';
require_once './modules/WarehouseModule.php';