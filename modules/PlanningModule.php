<?php

require_once ('./framework/database.php');
require_once './bundle.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlaningModule
 *
 * @author Chonlasak_Sak
 */
class PlaningModule {

    //put your code here
    private $database;
    private $service;

    function __construct() {
        $this->database = new database();
        $this->service = new service();
    }

    function WriteExceptionLog(response $response, $sql, $payload, $fuctionname) {
        $response->MSGMESSAGE2 = $sql;
        $response->MSGMESSAGE3 = $fuctionname;
        $response->REQDATA[] = $payload;
        $xml = service::generateValidXmlFromObj($response, 'Header', 'Payload');
        $this->database->WriteLog($xml);
    }

    function ClearYearPlanAcion() {
        date_default_timezone_set('Asia/Bangkok');
        $year = date("Ym");
        $sql = "SELECT yplanid FROM year_plan WHERE yplanid='y$year'";
        $response = new response();
        try {

            $sql = "UPDATE region_yearplan SET active = '0',enddate = now() WHERE yplanid IN ";
            $sql .= "(SELECT yplanid FROM year_plan WHERE active = '1')";
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            $sql = "UPDATE year_plan SET active = '0' AND update_date = now() WHERE active = '1'";
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        $sql = "DELETE FROM region_yearplan WHERE yplanid='y$year'";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        $response->MSGMESSAGE2 = 'y' . $year;
        return $response;
    }

    function CreateYearPlanAcion($regionAll, $yplanid, $regionid, $target_percent, $target_value) {
        date_default_timezone_set('Asia/Bangkok');
        $year = date("Y");
        $sql = "SELECT yplanid FROM year_plan WHERE yplanid='$yplanid' AND active = '1'";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW <= 0) {
                $sql = "INSERT INTO year_plan (`yplanid`, `year`, `create_date`, `update_date`, `expectcirculation`, `active`) VALUES('$yplanid','$year',now(),now()," . str_replace(',', '', $regionAll) . ",1)";
                //echo $sql;
                $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
                if ($res->QRESULT) {
                    $response->MSGID = SERV_COMPLETE;
                    $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                }
            } else {
                $sql = "UPDATE year_plan SET expectcirculation = " . str_replace(',', '', $regionAll) . ",update_date = now() WHERE yplanid='$yplanid' AND active = '1'";
                $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
                if ($res->QRESULT) {
                    $response->MSGID = SERV_COMPLETE;
                    $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                }
            }
            $sql = "INSERT INTO region_yearplan (`yplanid`, `regionid`, `target_percent`, `target_value`, `active`, `effdate`, `enddate`) VALUES('$yplanid',$regionid," . str_replace(',', '', $target_percent) . "," . str_replace(',', '', $target_value) . ",1,now(),null)";
            //echo $sql;
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            }
            /* else {
              $sql = "UPDATE year_plan SET expectcirculation = " . str_replace(',', '', $regionAll) . ",update_date = now() WHERE yplanid='$yplanid'";
              $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
              if ($res->QRESULT) {
              $response->MSGID = SERV_COMPLETE;
              $response->MSGMESSAGE1 = $res->AFFECT_ROW;
              }
              $sql = "INSERT INTO region_yearplan (`yplanid`, `regionid`, `target_percent`, `target_value`, `active`, `effdate`, `enddate`) VALUES('$yplanid',$regionid," . str_replace(',', '', $target_percent) . "," . str_replace(',', '', $target_value) . ",1,now(),null)";
              //echo $sql;
              $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
              if ($res->QRESULT) {
              $response->MSGID = SERV_COMPLETE;
              $response->MSGMESSAGE1 = $res->AFFECT_ROW;
              }
              } */
            $sql = "CALL UpdateUserYearPlan()";
            $this->database->ExcuteStoredProcedure($sql);
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('regionAll' => $regionAll, 'regionAll' => $yplanid, 'regionid' => $regionid, 'target_percent' => $target_percent, 'target_value' => $target_value);
            $this->WriteExceptionLog($response, $sql, "", __FUNCTION__);
        }
        return $response;
    }

    function queryYearPlanAcion() {
        date_default_timezone_set('Asia/Bangkok');
        $year = date("Y");
        $sql = "SELECT * FROM region_yearplan WHERE yplanid IN ";
        $sql .= "(SELECT yplanid FROM year_plan WHERE active = '1')";
        $response = new response();
        try {
            $response->MSGDATA2 = null;
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            $response->MSGDATA1 = NULL;
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $output = array();
                foreach ($res->QRESULT as $row) {
                    $plan = array();
                    $plan["target_percent"] = $row['target_percent'];
                    $plan["target_value"] = $row['target_value'];
                    $output[$row['regionid']] = $plan;
                }
                $response->MSGDATA1 = $output;
            }
            $sql = "SELECT expectcirculation FROM year_plan WHERE active = '1'";
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            $response->MSGDATA2 = NULL;
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $response->MSGDATA2 = $row['expectcirculation'];
                }
            }
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, "", __FUNCTION__);
        }
        return $response;
    }

    function queryUserPlanAcion($userid, $month, $year) {
        $response = new response();
        try {
            $sql = "";
            if ($userid != NULL) {
                $sql = "SELECT regionid,target_sales_percent,target_sales_value FROM user_region_yearplan WHERE userid = '$userid'";
                if ($month != NULL && $year != NULL) {
                    $sql .= " AND CONVERT(CONCAT('$year-$month-01'),DATETIME) < IFNULL(enddate,now())";
                    $sql .= " AND effdate < now() AND active = '1'";
                } else if ($month != NULL) {
                    $sql .= " AND CONVERT(CONCAT(DATE_FORMAT(now(),'%Y'),-$month-01'),DATETIME) < IFNULL(enddate,now())";
                    $sql .= " AND effdate BETWEEN CONVERT(CONCAT(DATE_FORMAT(now(),'%Y'),-$month-01'),DATETIME) AND now()";
                } else if ($year != NULL) {
                    $sql .= " AND CONVERT('$year-01-01',DATETIME) < IFNULL(enddate,now())";
                    $sql .= " AND effdate BETWEEN CONVERT('$year-01-01',DATETIME) AND now()";
                }
            } else {
                $sql = "SELECT SUM(target_sales_value) target_sales_value FROM user_region_yearplan ";
                if ($month != NULL && $year != NULL) {
                    $sql .= " WHERE CONVERT(CONCAT('$year-$month-01'),DATETIME) < IFNULL(enddate,now())";
                    $sql .= " AND effdate < now() AND active = '1'";
                } else if ($month != NULL) {
                    $sql .= " WHERE CONVERT(CONCAT(DATE_FORMAT(now(),'%Y'),-$month-01'),DATETIME) < IFNULL(enddate,now())";
                    $sql .= " AND effdate BETWEEN CONVERT(CONCAT(DATE_FORMAT(now(),'%Y'),-$month-01'),DATETIME) AND now()";
                } else if ($year != NULL) {
                    $sql .= " WHERE CONVERT('$year-01-01',DATETIME) < IFNULL(enddate,now())";
                    $sql .= " AND effdate BETWEEN CONVERT('$year-01-01',DATETIME) AND now()";
                }
            }
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $response->MSGDATA1 = $row;
                }
            }
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $userid, __FUNCTION__);
        }
        return $response;
    }

    function queryConditionSaleAcion() {
        $response = new response();
        try {
            $sql = "SELECT * FROM eval_master order by condition_max desc ";
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $response->MSGDATA1[] = $row;
                }
            }
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $userid, __FUNCTION__);
        }
        return $response;
    }

    function GetMyPlanOrderDetail($planid) {
        $salesmodule = new SalesModule();
        $sql = "select plan_ordering.productid, price, amount, promotionid,  sum(price * amount) as summary"
                . " , price_individual_cost + (price_individual_cost * individual_margin_profit) as retail_price"
                . " from plan_ordering "
                . " join product p on p.productid = plan_ordering.productid"
                . " join product_price on product_price.productid = p.productid "
                . " where planid = '" . $planid . "' "
                . " GROUP BY plan_ordering.productid ";
        //echo $sql;
        $cnt = 0;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRICE = $row['price'];
                    $order->AMOUNT = $row['amount'];
                    $retail_price = 0;
                    if ($row['retail_price'] != null && $row['retail_price'] != "") {
                        $retail_price = $row['retail_price'];
                    }
                    $order->PRODUCT_RETAIL_PRICE = $retail_price;
                    $order->PRODUCT_RETAIL_DISCOUNT = 0;
                    $order->REF_PROMOTION = $row['promotionid'];
                    if ($row['promotionid'] != null && $row['promotionid'] != "") {
                        $promotion = new promotion();
                        $promotion = $salesmodule->GetPromotionDetail($row['promotionid'])->MSGDATA1[0];
                        $product = new product();
                        $product = $promotion->PROMOTION_PRODUCT[0];
                        $order->PRODUCT_RETAIL_DISCOUNT = $product->PRODUCT_RETAIL_DISCOUNT;
                    }
                    $response->MSGDATA1[] = $order;
                    $cnt++;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $planid, __FUNCTION__);
        }
        $response->MSGMESSAGE2 = $cnt;

        return $response;
    }

    function CallStoreUpdateUserYearPlan() {
        $response = new response();
        $sql = "CALL UpdateUserYearPlan()";
        try {
            $this->database->ExcuteStoredProcedure($sql);
            $response->MSGID = SERV_COMPLETE;
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        return $response;
    }

    function CallStoreUpdateUserEval() {
        $response = new response();
        $sql = "CALL UpdateUserEval()";
        try {
            $this->database->ExcuteStoredProcedure($sql);
            $response->MSGID = SERV_COMPLETE;
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        return $response;
    }

    function CreatePlanHeader(plan $plan) {
        // Q2 R1 == No buy
        if ($plan->Q2_R1 == "" || $plan->Q2_R1 == null) {
            $planq2r1 = " DEFAULT, ";
        } else {
            $planq2r1 = " '" . $plan->Q2_R1 . "', ";
        }
        // Q3 R1 //ผลตอบรับ
        if ($plan->Q3_R1 == "" || $plan->Q3_R1 == null) {
            $planq3r1 = " DEFAULT, ";
        } else {
            $planq3r1 = " '" . $plan->Q3_R1 . "', ";
        }
        // Q3 R2 //หมายเหตุ
        if ($plan->Q3_R2 == "" || $plan->Q3_R2 == null) {
            $planq3r2 = " DEFAULT, ";
        } else {
            $planq3r2 = " '" . $plan->Q3_R2 . "', ";
        }
        // Q3 R3 //เหตุผลที่ไม่แนะนำ
        if ($plan->Q3_R3 == "" || $plan->Q3_R3 == null) {
            $planq3r3 = " DEFAULT, ";
        } else {
            $planq3r3 = " '" . $plan->Q3_R3 . "', ";
        }

        $sql = "INSERT INTO plan values ("
                . " '" . $plan->PLANID . "', "
                . " '" . $plan->USERID . "', "
                . " '" . $plan->CUSTOMER_ID . "', "
                . " '" . $plan->PLANDATE . "', "
                . " '" . $plan->PLANTIME . "', "
                . " 'NEW', "
                . " '" . $plan->Q1 . "', "
                . " '" . $plan->Q1_DATE . "', "
                . " '" . $plan->Q1_TIME . "', "
                . " '" . $plan->Q2 . "', "
                . $planq2r1
                . " '" . $plan->Q3 . "', "
                . $planq3r1
                . $planq3r2
                . $planq3r3
                . " DEFAULT )";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $plan, __FUNCTION__);
        }
        return $response;
    }

    function CreatePlanDetail($sql) {
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        return $response;
    }

    function GetMyDateSchedule($condition) {
        $sql = "select planid, plan_date, plan_time, status, "
                . " c.name, c.customerid, c.address, c.soi, c.road, "
                . " c.sub_district, c.district, d.name as provname, c.contactname, "
                . " c.contactphone, c.contactemail "
                . " from plan join customer c "
                . " on plan.customerid = c.customerid and plan.status != 'CAN' "
                . " join province d "
                . " on c.provinceid = d.provinceid "
                . $condition;
        //echo $sql;
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            $response = new response();
            if ($res->MSGID === SERV_COMPLETE) {
                foreach ($res->QRESULT as $row) {
                    $plan = new plan();
                    $plan->PLANID = $row['planid'];
                    $plan->PLANDATE = $row['plan_date'];
                    $plan->PLANTIME = $row['plan_time'];
                    $plan->STATUS = $row['status'];
                    $plan->CUSTOMER_ID = $row['customerid'];
                    $plan->CUSTOMER_NAME = $row['name'];
                    $plan->CUSTOMER_CONTACTNAME = $row['contactname'];
                    $plan->CUSTOMER_ADDRESS = $row['address'] . " " . $row['soi'] . " " . $row['road'] . " " . $row['sub_district'] . " " . $row['district'] . " " . $row['provname'];
                    $plan->CUSTOMER_MAIL = $row['contactemail'];
                    $plan->CUSTOMER_CONTACTPHONE = $row['contactphone'];
                    $response->MSGDATA1[] = $plan;
                }
                $response->MSGID = SERV_COMPLETE;
            }
            return $response;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }
    }

    function DeActivePlan($planid) {
        $sql = "UPDATE `plan` SET `active`= 0, status = '" . PLAN_STATUS_CAN . "' WHERE planid = '" . $planid . "'";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('planid' => $planid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

}
