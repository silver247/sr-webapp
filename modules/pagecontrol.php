<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once './config/const.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Init($mainpage){
    //echo $_SESSION['usrid']." ".$_SESSION['role']." ".ROLE_ADMIN;
    if(!isset($_SESSION['usrid'])){
        header('Location: /');
    }
    if($mainpage == "Admin" && $_SESSION['role'] != ROLE_ADMIN){
        //echo "in codition";
        header('Location: /Home/');
    }
}
