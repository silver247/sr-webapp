<?php

require_once ('./framework/database.php');
require_once './bundle.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SalesModule
 *
 * @author puwakitk
 */
class SalesModule {

    //put your code here
    private $database;
    private $service;

    function __construct() {
        $this->database = new database();
        $this->service = new service();
    }

    function WriteExceptionLog(response $response, $sql, $payload, $fuctionname) {
        $response->MSGMESSAGE2 = $sql;
        $response->MSGMESSAGE3 = $fuctionname;
        if ($payload != null || $payload != '') {
            $response->REQDATA[] = $payload;
        }
        $xml = service::generateValidXmlFromObj($response, 'Header', 'Payload');
        $this->database->WriteLog($xml);
    }

    function GetSalesSummary($condition) {
        $sql = " select MONTH(order_date) as orderdate, sum(summary) as total "
                . " from ordering "
                . " where status = '" . ORDER_STATUS_DONE . "' "
                . $condition
                . " group by MONTH(order_date) ";
        //echo $sql;
        $response = new response();
        $saleslist = array();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $saleslist[$row['orderdate']] = $row['total'];
                }
                for($i=1;$i<=12;$i++){
                    if(!isset($saleslist[$i])){
                        $saleslist[$i] = 0;
                    }
                }
                $response->MSGDATA1[] = $saleslist;
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }
        //service::printr($response);
        return $response;
    }

    function GetMyProductHistory($condition) {
        $sql = "select ordering.orderid, product.productid, product.productname, sum(amount) as totalamount, sum(amount*price) as total "
                . " from ordering "
                . " join ordering_detail on ordering.orderid = ordering_detail.orderid "
                . " join product on ordering_detail.productid = product.productid "
                . $condition
                . " and ordering.status = '" . ORDER_STATUS_DONE . "' "
                . " group by product.productid "
                . " order by total DESC";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRODUCT_NAME = $row['productname'];
                    $order->AMOUNT = $row['totalamount'];
                    $order->TOTAL = $row['total'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }

        return $response;
    }

    function GetPromotionHeader($condition) {
        $sql = " select promotionid, promotion_name, promotion_desc, start_date, end_date, active  "
                . " from promotion "
                //. " where active = '" . PROMOTION_STATUS_ACTIVE . "' "
                . $condition;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $promotion = new promotion();
                    $promotion->PROMOTION_ID = $row['promotionid'];
                    $promotion->PROMOTION_NAME = $row['promotion_name'];
                    $promotion->PROMOTION_DESC = $row['promotion_desc'];
                    $promotion->PROMOTION_STARTDATE = $row['start_date'];
                    $promotion->PROMOTION_ENDDATE = $row['end_date'];
                    $promotion->PROMOTION_STATUS = $row['active'];
                    $response->MSGDATA1[] = $promotion;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }

        return $response;
    }

    function GetPromotionDetail($promotionid) {
        $sql = " select promotionid, productid, percent_retail, percent_whole "
                . " from promotion_product "
                . " where promotionid = '" . $promotionid . "' ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $promotion = new promotion();
                foreach ($res->QRESULT as $row) {
                    $product = new product();
                    $condition = " where p.productid = '" . $row['productid'] . "' and p.Active = 1 ";
                    $product = $this->GetMyProductDetail($condition)->MSGDATA1[0];
                    $product->PRODUCT_RETAIL_DISCOUNT = $row['percent_retail'];
                    $product->PRODUCT_WHOLE_DISCOUNT = $row['percent_whole'];
                    $retail_cost = $product->PRODUCT_RETAIL_COST;
                    $whole_cost = $product->PRODUCT_RETAIL_COST;
                    $retail_price = $retail_cost + ($retail_cost * $product->PRODUCT_RETAIL_PROFIT);
                    $whole_price = $whole_cost + ($whole_cost * $product->PRODUCT_WHOLE_PROFIT);
                    $retail_price_discount = $retail_price - ($retail_price * $row['percent_retail']);
                    $whole_price_discount = $whole_price - ($whole_price * $row['percent_whole']);
                    $product->PRODUCT_RETAIL_PRICE = $retail_price;
                    $product->PRODUCT_WHOLE_PRICE = $whole_price;
                    $product->PRODUCT_RETAIL_DISCOUNT_PRICE = $retail_price_discount;
                    $product->PRODUCT_WHOLE_DISCOUNT_PRICE = $whole_price_discount;
                    $promotion->PROMOTION_PRODUCT[] = $product;
                }
                $response->MSGDATA1[] = $promotion;
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('promotionid' => $promotionid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }

        return $response;
    }

    function GetPromotionDiscountByProductID($productid) {
        //echo $sql;
        $response = new response();
        try {
            $sql = " CALL GetPromotionDiscount('" . $productid . "',@retail_price,@whole_price,@retail_discount,@whole_discount,@retail_price_discount,@whole_price_discount,@promotionid,@promotionname); ";
            $sql2 = "SELECT @retail_price,@whole_price,@retail_discount,@whole_discount,@retail_price_discount,@whole_price_discount,@promotionid,@promotionname;";
            $dataset = $this->database->CallStoredProcedure($sql, $sql2);
            $promotion = new promotion();
            foreach ($dataset as $row) {
                $product = new product();
                $product->PRODUCT_ID = $productid;
                $product->PRODUCT_RETAIL_COST = $row['@retail_price'];
                $product->PRODUCT_WHOLE_COST = $row['@whole_price'];
                $product->PRODUCT_RETAIL_DISCOUNT = $row['@retail_discount'];
                $product->PRODUCT_WHOLE_DISCOUNT = $row['@whole_discount'];
                $product->PRODUCT_RETAIL_DISCOUNT_PRICE = $row['@retail_price_discount'];
                $product->PRODUCT_WHOLE_DISCOUNT_PRICE = $row['@whole_price_discount'];
                $promotion->PROMOTION_ID = "";
                if ($row['@promotionid'] != NULL && $row['@promotionid'] != "") {
                    $promotion->PROMOTION_ID = $row['@promotionid'];
                }
                $promotion->PROMOTION_NAME = "";
                if ($row['@promotionname'] != NULL && $row['@promotionname'] != "") {
                    $promotion->PROMOTION_NAME = $row['@promotionname'];
                }
                $promotion->PROMOTION_PRODUCT[] = $product;
            }
            $response->MSGDATA1[] = $promotion;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('productid' => $productid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        //service::printr($response);
        return $response;
    }

    function GetMyProductDetail($condition) {
        $sql = "select pt.name, p.productid, p.productname, "
                . " pp.price_individual_cost, pp.price_mass_cost, "
                . " pp.individual_margin_profit, pp.mass_margin_profit, "
                . " pp.price_date, p.active "
                . " from product p "
                . " join product_type pt "
                . " on p.prdtypeid = pt.prdtypeid "
                . " join product_price pp "
                . " on pp.productid = p.productid and pp.active = 1 "
                . $condition;
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $product = new product();
                    $product->PRODUCT_NAME = $row['productname'];
                    $product->PRODUCT_ID = $row['productid'];
                    $product->PRODUCTTYPE_NAME = $row['name'];
                    $product->PRODUCT_RETAIL_COST = $row['price_individual_cost'];
                    $product->PRODUCT_WHOLE_COST = $row['price_mass_cost'];
                    $product->PRODUCT_RETAIL_PROFIT = $row['individual_margin_profit'];
                    $product->PRODUCT_WHOLE_PROFIT = $row['mass_margin_profit'];
                    $product->PRODUCT_PRICEDATE = $row['price_date'];
                    $product->PRODUCT_STATUS = $row['active'];
                    $response->MSGDATA1[] = $product;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }

        return $response;
    }

    function UpdatePromotionHeader(promotion $promotion) {
        $sql = " update promotion set promotion_name = '" . $promotion->PROMOTION_NAME . "' "
                . " , promotion_desc = '" . $promotion->PROMOTION_DESC . "' "
                . " , start_date = '" . $promotion->PROMOTION_STARTDATE . "' "
                . " , end_date = '" . $promotion->PROMOTION_ENDDATE . "' "
                . " where promotionid = '" . $promotion->PROMOTION_ID . "' ";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                $response->MSGMESSAGE2 = $promotion->PROMOTION_ID;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $promotion, __FUNCTION__);
        }
        return $response;
    }

    function DeletePromotionProduct(promotion $promotion) {
        $sql = " delete from promotion_product where promotionid = '" . $promotion->PROMOTION_ID . "' ";
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $promotion, __FUNCTION__);
        }
        return $response;
    }

    function InsertPromotionHeader(promotion $promotion) {
        $sql = " insert into promotion values ( "
                . " '" . $promotion->PROMOTION_ID . "' "
                . " ,'" . $promotion->PROMOTION_NAME . "' "
                . " ,'" . $promotion->PROMOTION_DESC . "' "
                . " ,'" . $promotion->PROMOTION_STARTDATE . "' "
                . " ,'" . $promotion->PROMOTION_ENDDATE . "' "
                . " ,'" . PROMOTION_STATUS_ACTIVE . "' "
                . " )";
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $promotion, __FUNCTION__);
        }
        return $response;
    }

    function InsertPromotionDetail(promotion $promotion) {
        $response = new response();
        $response->MSGID = SERV_COMPLETE;
        foreach ($promotion->PROMOTION_PRODUCT as $productdata) {
            $product = new product();
            $product = $productdata;
            $sql = " insert into promotion_product values ( "
                    . " '" . $promotion->PROMOTION_ID . "' "
                    . " ,'" . $product->PRODUCT_ID . "' "
                    . " ,'" . $product->PRODUCT_RETAIL_DISCOUNT . "' "
                    . " ,'" . $product->PRODUCT_WHOLE_DISCOUNT . "' "
                    . " )";
            try {
                $this->database->WRITE()->SQL($sql)->EXECUTE();
                $response->MSGID = SERV_COMPLETE;
            } catch (PDOException $ex) {
                $response->MSGID = SERV_ERROR;
                $response->MSGMESSAGE1 = $ex;
                $this->WriteExceptionLog($response, $sql, $promotion, __FUNCTION__);
                break;
            }
        }
        return $response;
    }

    function InActivePromotion(promotion $promotion) {
        $sql = " update promotion set active = '" . PROMOTION_STATUS_INACTIVE . "' "
                . " where promotionid = '" . $promotion->PROMOTION_ID . "' ";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                $response->MSGMESSAGE2 = $promotion->PROMOTION_ID;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $promotion, __FUNCTION__);
        }
        return $response;
    }

    function InActiveExpirePromotion() {
        $sql = " update promotion set active = '" . PROMOTION_STATUS_INACTIVE . "' "
                . " where end_date < CURDATE() ";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        return $response;
    }

    function GetMyOrderDetail($condition) {
        $sql = "select ordering_detail.orderid, p.productname , p.productid, p.modelid, price, amount, sum(price * amount) as summary"
                . " , ordering_detail.promotionid "
                . " , price_individual_cost + (price_individual_cost * individual_margin_profit) as retail_price "
                . " , ordering_detail.pricetype "
                . " from ordering_detail "
                . " join ordering on ordering_detail.orderid = ordering.orderid "
                . " join product p on p.productid = ordering_detail.productid"
                . " join product_price on product_price.productid = p.productid "
                //. " join promotion_product on ordering_detail.promotionid = promotion_product.promotionid and promotion_product.productid = ordering_detail.productid "
                //. " join promotion on promotion.promotionid = promotion_product.promotionid "
                . $condition
                . " GROUP BY ordering_detail.productid, pricetype ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->ORDERID = $row['orderid'];
                    $order->PRODUCT_NAME = $row['productname'];
                    $order->MODELID = $row['modelid'];
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRICE = $row['price'];
                    $order->AMOUNT = $row['amount'];
                    $retail_price = 0;
                    if ($row['retail_price'] != null && $row['retail_price'] != "") {
                        $retail_price = $row['retail_price'];
                    }
                    $order->PRODUCT_RETAIL_PRICE = $retail_price;
                    $order->REF_PROMOTION = $row['promotionid'];
                    $order->PRODUCT_RETAIL_DISCOUNT = 0;
                    if ($row['promotionid'] != null && $row['promotionid'] != "") {
                        $promotion = new promotion();
                        $promotion = $this->GetPromotionDetail($row['promotionid'])->MSGDATA1[0];
                        $product = new product();
                        $product = $promotion->PROMOTION_PRODUCT[0];
                        $order->PRODUCT_RETAIL_DISCOUNT = $product->PRODUCT_RETAIL_DISCOUNT;
                    }
                    //$order->STATUS = $row['status'];
                    $order->PRICETYPE = $row['pricetype'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }
        return $response;
    }

    function CreateOrderHeader(order $order) {
        $response = new response();
        $refplan = "";
        if ($order->REFPLAN == '' || $order->REFPLAN == null) {
            $refplan = " DEFAULT ";
        } else {
            $refplan = "'" . $order->REFPLAN . "'";
        }
        $sql = "INSERT INTO ordering VALUES ( "
                . " '" . $order->ORDERID . "', "
                . " '" . $order->USERID . "', "
                . " '" . $order->CUSTOMER_ID . "', "
                . $refplan . ", "
                . " '" . $order->TOTAL . "', "
                . " '" . $order->TIMESTAMP . "', "
                . " DEFAULT ,"
                . " DEFAULT )";
        //echo $sql;
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $order, __FUNCTION__);
        }
        return $response;
    }

    function CreateOrderDetail($sql) {
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array();
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function UpdatePlanStatus($planid, $status) {
        $sql = "update plan set status = '" . $status . "' where planid = '" . $planid . "'";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('planid' => $planid, 'status' => $status);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function GetMyProductBuyers($productid, $year, $month) {
        $sql = "select ordering.orderid, product.productid, product.productname, "
                . " amount, amount*price as total ,order_date, customer.name as cname, "
                . " ordering.status, discount, credit "
                . " from ordering "
                . " join ordering_detail on ordering.orderid = ordering_detail.orderid "
                . " join customer on customer.customerid = ordering.customerid "
                . " join customer_grp on customer.custgrpid = customer_grp.custgrpid "
                . " join product on product.productid = ordering_detail.productid "
                . " where ordering_detail.productid = '" . $productid . "' "
                . " and YEAR(ordering.order_date) = '" . $year . "' "
                . " and MONTH(ordering.order_date) = '" . $month . "' "
                . " and ordering.status = '" . ORDER_STATUS_DONE . "' ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRODUCT_NAME = $row['productname'];
                    $order->TIMESTAMP = $row['order_date'];
                    $order->CUSTOMER_NAME = $row['cname'];
                    $order->STATUS_DESC = BusinessLogic::OrderStatusDescription($row['status']);
                    $order->AMOUNT = $row['amount'];
                    $order->TOTAL = $row['total'];
                    $order->CUSTOMER_DISCOUNT = $row['discount'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('productid' => $productid, 'year' => $year, 'month' => $month);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }

        return $response;
    }

    function GetMyOrderHeader($condition) {
        $usermodule = new UserModule();
        $sql = "select orderid, c.customerid , c.name, summary, order_date, status, userid  "
                . " ,customer_grp.desc as grpname, credit, discount "
                . " from ordering "
                . " join customer c on ordering.customerid = c.customerid "
                . " join customer_grp on c.custgrpid = customer_grp.custgrpid "
                . $condition;
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->ORDERID = $row['orderid'];
                    $order->CUSTOMER_ID = $row['customerid'];
                    $order->CUSTOMER_NAME = $row['name'];
                    $order->TOTAL = $row['summary'];
                    $order->STATUS = $row['status'];
                    $order->TIMESTAMP = $row['order_date'];
                    $condition = " where user.userid = '" . $row['userid'] . "' ";
                    $user = new user();
                    $user = $usermodule->GetUserDataList($condition)->MSGDATA1[0];
                    $order->USER_NAMETH = $user->TNAME . " " . $user->TLNAME;
                    $order->CUSTOMER_DISCOUNT = $row['discount'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }
        return $response;
    }

    function GetCustomerDiscount($customerid) {
        $sql = " select customer_grp.desc as grpname, credit, discount "
                . " from customer "
                . " join customer_grp on customer.custgrpid = customer_grp.custgrpid "
                . " where customer.customerid = '" . $customerid . "' ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $customer = new customer();
                foreach ($res->QRESULT as $row) {
                    $customer->CUSTOMER_GRPCREDIT = $row['credit'];
                    $customer->CUSTOMER_GRPDESC = $row['grpname'];
                    $customer->CUSTOMER_GRPDISCOUNT = $row['discount'];
                    $response->MSGDATA1[] = $customer;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('customerid' => $customerid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function CreateProductPrice(product $product) {
        $sql = " insert into product_price values ("
                . " DEFAULT "
                . " ,'" . $product->PRODUCT_ID . "' "
                . " ,'" . $product->PRODUCT_RETAIL_COST . "' "
                . " ,'" . $product->PRODUCT_WHOLE_COST . "' "
                . " ,'" . $product->PRODUCT_RETAIL_PROFIT . "' "
                . " ,'" . $product->PRODUCT_WHOLE_PROFIT . "' "
                . " , CURDATE() "
                . " , CURRENT_TIMESTAMP"
                . " ,'" . PRODUCT_STATUS_ACTIVE . "' "
                . " );";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $product, __FUNCTION__);
        }
        return $response;
    }

    function GetProductPrice($productid) {
        $sql = " select price_individual_cost, price_mass_cost, individual_margin_profit, mass_margin_profit, price_date, active "
                . " from product_price "
                . " where productid = '" . $productid . "' "
                . " and active = '" . PRODUCT_STATUS_ACTIVE . "' ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $product = new product();
                foreach ($res->QRESULT as $row) {
                    $product->PRODUCT_RETAIL_COST = $row['price_individual_cost'];
                    $product->PRODUCT_WHOLE_COST = $row['price_mass_cost'];
                    $product->PRODUCT_RETAIL_PROFIT = $row['individual_margin_profit'];
                    $product->PRODUCT_WHOLE_PROFIT = $row['mass_margin_profit'];
                    $product->PRODUCT_PRICEDATE = $row['price_date'];
                    $response->MSGDATA1[] = $product;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('productid' => $productid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function UpdateProductPrice(product $product) {
        /* $sqlupdate_arr = array();
          if($product->PRODUCT_RETAIL_COST != null || $product->PRODUCT_RETAIL_COST != ''){
          $sqlupdate_arr[] = " price_individual_cost = '".$product->PRODUCT_RETAIL_COST."' ";
          }
          if($product->PRODUCT_RETAIL_PROFIT != null || $product->PRODUCT_RETAIL_PROFIT != ''){
          $sqlupdate_arr[] = " individual_margin_profit = '".$product->PRODUCT_RETAIL_PROFIT."' ";
          }
          if($product->PRODUCT_WHOLE_COST != null || $product->PRODUCT_WHOLE_COST != ''){
          $sqlupdate_arr[] = " price_mass_cost = '".$product->PRODUCT_WHOLE_COST."' ";
          }
          if($product->PRODUCT_WHOLE_PROFIT != null || $product->PRODUCT_WHOLE_PROFIT != ''){
          $sqlupdate_arr[] = " mass_margin_profit = '".$product->PRODUCT_WHOLE_PROFIT."' ";
          }
          if(count($sqlupdate_arr) > 1){
          $sqlupdate = join(' , ', $sqlupdate_arr);
          }
          else{
          $sqlupdate = $sqlupdate_arr[0];
          }
          $sql = " update product_price set  "
          . $sqlupdate
          . " ,log_date = CURRENT_TIMESTAMP "
          . " where productid = '".$product->PRODUCT_ID."' "; */
        $response = new response();
        try {
            $spd = " CALL InActiveCurrentProductPrice('" . $product->PRODUCT_ID . "'); ";
            $this->database->ExcuteStoredProcedure($spd);
            return $res = $this->CreateProductPrice($product);
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $spd, $product, __FUNCTION__);
        }
        return $response;
    }

    function GetSalesRegionSummary($condition) {
        $sql = " SELECT province.regionid, SUM( ordering_detail.amount * ordering_detail.price ) AS total "
                . " FROM ordering "
                . " JOIN ordering_detail ON ordering.orderid = ordering_detail.orderid "
                . " JOIN customer ON ordering.customerid = customer.customerid "
                . " JOIN province ON customer.provinceid = province.provinceid "
                . " WHERE ordering.status = '" . ORDER_STATUS_DONE . "' "
                . $condition
                . " GROUP BY province.regionid ";
        //echo $sql;
        $response = new response();
        $saleslist = array();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $saleslist[$row['regionid']] = $row['total'];
                }
                for ($i = 1; $i <= 6; $i++) {
                    if (!isset($saleslist[$i])) {
                        $saleslist[$i] = 0;
                    }
                }
                $response->MSGDATA1[] = $saleslist;
            } else {
                /*
                 * No data
                 */
                $response->MSGID = SERV_COMPLETE;
                for ($i = 1; $i <= 6; $i++) {
                    if (!isset($saleslist[$i])) {
                        $saleslist[$i] = 0;
                    }
                }
                $response->MSGDATA1[] = $saleslist;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }

        return $response;
    }

    function GetDailySalesInMonth($condition) {
        //$sql = " select order_date, ordering_detail.amount * ordering_detail.price as total "
        $sql = " select order_date, sum(summary) as total "
                . " from ordering"
                //. " join ordering_detail on ordering.orderid = ordering_detail.orderid "
                . " where "
                . " status = '" . ORDER_STATUS_DONE . "' "
                . $condition
                . " group by order_date "
                . " order by order_date asc ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $salesdata = array();
                foreach ($res->QRESULT as $row) {
                    $date = new DateTime($row['order_date']);
                    $salesdata[$date->format('j')] = $row['total'];
                }
                for ($i = 1; $i <= 31; $i++) {
                    if (!isset($salesdata[$i])) {
                        $salesdata[$i] = 0;
                    }
                }
            } else {
                $response->MSGID = SERV_COMPLETE;
                for ($i = 1; $i <= 31; $i++) {
                    if (!isset($salesdata[$i])) {
                        $salesdata[$i] = 0;
                    }
                }
            }
            ksort($salesdata);
            $response->MSGDATA1[] = $salesdata;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }
        return $response;
    }

    function GetMyCustomer($condition) {
        $sql = "select c.name as custname, c.customerid, c.address, c.soi, c.road, "
                . "c.sub_district, c.district, c.provinceid, c.zipcode, c.contactname, "
                . "c.contactphone, c.contactemail, c.custgrpid, "
                . "customer_grp.desc, customer_grp.credit, customer_grp.discount, "
                . "province.name as provincename, c.active, region.desc as regioname "
                . "from customer c "
                . "join customer_grp on c.custgrpid = customer_grp.custgrpid "
                . "join province on province.provinceid = c.provinceid "
                . "join region on province.regionid = region.regionid "
                //. "join user_customer on c.customerid = user_customer.customerid "
                . $condition;
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $customer = new customer();
                    $customer->CUSTOMER_ID = $row['customerid'];
                    $customer->CUSTOMER_NAME = $row['custname'];
                    $soi = "";
                    $road = "";
                    $sdist = "";
                    $dist = "";

                    if ($row['soi'] != '' || $row['soi'] != null) {
                        $soi = " ซอย " . $row['soi'];
                    }
                    if ($row['road'] != '' || $row['road'] != null) {
                        $road = " ถนน " . $row['road'];
                    }
                    if ($row['sub_district'] != '' || $row['sub_district'] != null) {
                        $sdist = " ตำบล " . $row['soi'];
                    }
                    if ($row['district'] != '' || $row['district'] != null) {
                        $dist = " อำเภอ " . $row['district'];
                    }
                    $customer->CUSTOMER_ADDRESS = $row['address'] . $soi . $road . $sdist . $dist . ' ' . $row['provincename'];
                    $customer->CUSTOMER_HOMEID = $row['address'];
                    $customer->CUSTOMER_SOI = $row['soi'];
                    $customer->CUSTOMER_ROAD = $row['road'];
                    $customer->CUSTOMER_SUBDIST = $row['sub_district'];
                    $customer->CUSTOMER_DIST = $row['district'];
                    $customer->CUSTOMER_PROVINCE = $row['provinceid'];
                    $customer->CUSTOMER_PROVINCENAME = $row['provincename'];
                    $customer->CUSTOMER_ZIPCODE = $row['zipcode'];
                    $customer->CUSTOMER_CONTACTNAME = $row['contactname'];
                    $customer->CUSTOMER_CONTACTPHONE = $row['contactphone'];
                    $email = $row['contactemail'];
                    if ($row['contactemail'] == null || $row['contactemail'] == "") {
                        $email = "-";
                    }
                    $customer->CUSTOMER_MAIL = $email;
                    $customer->CUSTOMER_GRPID = $row['custgrpid'];
                    $customer->CUSTOMER_GRPDESC = $row['desc'];
                    $customer->CUSTOMER_GRPCREDIT = $row['credit'];
                    $customer->CUSTOMER_GRPDISCOUNT = $row['discount'];
                    $customer->CUSTOMER_ACTIVE = $row['active'];
                    $customer->REGION_DESC = $row['regioname'];
                    $response->MSGDATA1[] = $customer;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $response->MSGMESSAGE2 = $sql;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }
        return $response;
    }

    function GetCustomerGroupDetail() {
        $sql = " select custgrpid,customer_grp.desc as grpname, credit, discount "
                . " from customer_grp ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $customer = new customer();
                    $customer->CUSTOMER_GRPID = $row['custgrpid'];
                    $customer->CUSTOMER_GRPCREDIT = $row['credit'];
                    $customer->CUSTOMER_GRPDISCOUNT = $row['discount'];
                    $response->MSGDATA1[] = $customer;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        return $response;
    }

    function UpdateCreditTerm(array $credit,array $discount) {
        $sql = "";
        $response = new response();
        for ($i = 1; $i <= 3; $i++) {
            $sql .= " update customer_grp set credit = '" . $credit[$i] . "' "
                    . ", discount = '" . $discount[$i] . "'"
                    . " where custgrpid = '" . $i . "'; ";
        }
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array();
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function CreateCustomer(customer $customer) {
        // Q2 R1

        if ($customer->CUSTOMER_ADDRESS == "" || $customer->CUSTOMER_ADDRESS == null) {
            $address = " DEFAULT, ";
        } else {
            $address = " '" . $customer->CUSTOMER_ADDRESS . "', ";
        }

        if ($customer->CUSTOMER_SOI == "" || $customer->CUSTOMER_SOI == null) {
            $soi = " DEFAULT, ";
        } else {
            $soi = " '" . $customer->CUSTOMER_SOI . "', ";
        }

        if ($customer->CUSTOMER_ROAD == "" || $customer->CUSTOMER_ROAD == null) {
            $road = " DEFAULT, ";
        } else {
            $road = " '" . $customer->CUSTOMER_ROAD . "', ";
        }

        if ($customer->CUSTOMER_SUBDIST == "" || $customer->CUSTOMER_SUBDIST == null) {
            $subdistrict = " DEFAULT, ";
        } else {
            $subdistrict = " '" . $customer->CUSTOMER_SUBDIST . "', ";
        }

        if ($customer->CUSTOMER_DIST == "" || $customer->CUSTOMER_DIST == null) {
            $district = " DEFAULT, ";
        } else {
            $district = " '" . $customer->CUSTOMER_DIST . "', ";
        }

        if ($customer->CUSTOMER_MAIL == "" || $customer->CUSTOMER_MAIL == null) {
            $contactemail = " DEFAULT, ";
        } else {
            $contactemail = " '" . $customer->CUSTOMER_MAIL . "', ";
        }


        $sql = "INSERT INTO customer values ("
                . " '" . $customer->CUSTOMER_ID . "', "
                . " '" . $customer->CUSTOMER_GRPID . "', "
                . " '" . $customer->CUSTOMER_NAME . "', "
                . $address
                . $soi
                . $road
                . $subdistrict
                . $district
                . " '" . $customer->CUSTOMER_PROVINCE . "', "
                . " '" . $customer->CUSTOMER_ZIPCODE . "', "
                . " '" . $customer->CUSTOMER_CONTACTNAME . "', "
                . " '" . $customer->CUSTOMER_CONTACTPHONE . "', "
                . $contactemail
                . " DEFAULT )";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                $response->MSGMESSAGE2 = $customer->CUSTOMER_ID;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $customer, __FUNCTION__);
        }
        return $response;
    }

    function CreateRelationUserCustomer($customerid) {
        $sql = " INSERT IGNORE INTO user_customer values ('" . $_SESSION['usrid'] . "','" . $customerid . "') ";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('customerid' => $customerid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

}
