<?php

require_once ('./framework/database.php');
require_once './bundle.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserModule
 *
 * @author puwakitk
 */
class UserModule {

    //put your code here

    private $database;
    private $service;

    function __construct() {
        $this->database = new database();
        $this->service = new service();
    }

    function WriteExceptionLog(response $response, $sql, $payload, $fuctionname) {
        $response->MSGMESSAGE2 = $sql;
        $response->MSGMESSAGE3 = $fuctionname;
        $response->REQDATA[] = $payload;
        $xml = service::generateValidXmlFromObj($response, 'Header', 'Payload');
        $this->database->WriteLog($xml);
    }

    function Login(user $user) {
        $sql = " select userid "
                . " from user "
                . " where username = '" . $user->USERNAME . "'"
                . " and password = '" . md5($user->PWD) . "'"
                . " and active = '" . USER_STATUS_ACTIVE . "' ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            $userid = 0;
            foreach ($res->QRESULT as $row) {
                $userid = $row['userid'];
            }
            return $userid;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $user, __FUNCTION__);
        }
        return $response;
    }

    function CheckIsNeedForceChange($userid) {
        $sql = " select forcechange "
                . " from user "
                . " where userid = '" . $userid . "' ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            $forcechange = 0;
            foreach ($res->QRESULT as $row) {
                $forcechange = $row['forcechange'];
            }
            return $forcechange;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('userid' => $userid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function UpdatePassword(user $user) {
        $sql = " update user set password = '" . md5($user->PWD) . "', forcechange = 0 where userid = '" . $user->USERID . "' ";
        //echo $sql;
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $user, __FUNCTION__);
        }
        return $response;
    }

    function GetUserDataList($condition) {
        $sql = " select user.userid, username, password, tname, tsurname, ename, esurname "
                . " ,user_role.roleid, user_role.desc as rolename, start_date, user.active"
                . " ,user_region.regionid, region.desc as regionname "
                . " from user "
                . " left join user_role on user.roleid = user_role.roleid "
                . " left join user_region on user.userid = user_region.userid "
                . " left join region on user_region.regionid = region.regionid "
                . $condition;
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $user = new user();
                    $user->USERID = $row['userid'];
                    $user->USERNAME = $row['username'];
                    $user->TNAME = $row['tname'];
                    $user->TLNAME = $row['tsurname'];
                    $user->ENAME = $row['ename'];
                    $user->ELNAME = $row['esurname'];
                    $user->ROLEID = md5($row['roleid']);
                    $user->ROLENAME = $row['rolename'];
                    $user->STARTDATE = $row['start_date'];
                    $user->REGIONID = $row['regionid'];
                    $user->REGIONNAME = $row['regionname'];
                    $user->STATUS = $row['active'];
                    $response->MSGDATA1[] = $user;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $condition, __FUNCTION__);
        }

        return $response;
    }

    function CheckDuplicateUser(user $user) {
        $sql = " select count(username) as total"
                . " from user "
                . " where username = '" . $user->USERNAME . "' ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            foreach ($res->QRESULT as $row) {
                $res->MSGID = SERV_COMPLETE;
                if ($row['total'] > 0) {
                    $response->MSGID = SERV_DUPLICATE_KEY;
                }
                else{
                    $response->MSGID = SERV_COMPLETE;
                }
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $user, __FUNCTION__);
        }
        return $response;
    }

    function CreateUser(user $user) {
        $sql = " insert into user values ('" . $user->USERID . "',"
                . " '" . $user->USERNAME . "', "
                . " '" . $user->PWD . "', "
                . " '" . $user->TNAME . "', "
                . " '" . $user->TLNAME . "', "
                . " '" . $user->ENAME . "', "
                . " '" . $user->ELNAME . "', "
                . " 2, " //Sales = 2, Admin = 1
                . " '" . $user->STARTDATE . "', "
                . " " . USER_STATUS_ACTIVE . ","
                . " DEFAULT )";
        //echo $sql;
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $user, __FUNCTION__);
        }
        return $response;
    }

    function CreateUserCustomer($userid, $customerid) {
        $sql = " insert into user_customer value ('" . $userid . "', '" . $customerid . "')";
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('userid' => $userid, 'customerid' => $customerid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function UpdateUserData(user $user) {
        $sql = " update user set "
                . " tname = '" . $user->TNAME . "', "
                . " tsurname = '" . $user->TLNAME . "', "
                . " ename = '" . $user->ENAME . "', "
                . " esurname = '" . $user->ELNAME . "' "
                . " where userid = '" . $user->USERID . "' ";
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $user, __FUNCTION__);
        }
        return $response;
    }

    function InActiveUser($userid) {
        $sql = " update user set active = '" . USER_STATUS_INACTIVE . "' where userid = '" . $userid . "' ";
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('userid' => $userid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function DeleteUserCustomerByUserID($userid) {
        $sql = "delete from user_customer where userid = '" . $userid . "' ";
        //echo $sql;
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('userid' => $userid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function DeleteUserCustomerByCustomerID($customerid) {
        $sql = "delete from user_customer where customerid = '" . $customerid . "' ";
        $response = new response();

        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('customerid' => $customerid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

    function CheckNeedUpdateUserEval() {
        $response = new response();
        $year = date('Y');
        $month = date('m');
        if ($month == '1') {
            $year = $year - 1; //for last year
            $month = 12;
        } else {
            $month = $month - 1; //pervious month
        }
        $month = str_pad($month, 2, '0', STR_PAD_LEFT); 
        $sql = "select count(tran_evalid) as cnt "
                . " from user_eval"
                . " where eval_year = '" . $year . "' "
                . " and eval_month = '" . $month . "'"
                . " and active = 1 ";
        //echo $sql;
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            foreach ($res->QRESULT as $row) {
                $res->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = TRUE;
                if ($row['cnt'] > 0) {
                    $response->MSGMESSAGE1 = FALSE;
                }
                $response->MSGID = SERV_COMPLETE;
                return $response;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('customerid' => $customerid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
    }

    function UpdateUserEvaluation() {
        $sql = " CALL UpdateUserEval()";
        $response = new response();
        try {
            $this->database->ExcuteStoredProcedure($sql);
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
    }
    
    function CreateUserRegion(user $user) {
        $sql = " insert into user_region values ('" . $user->USERID . "',"
                . " '" . $user->REGIONID . "' )";
        //echo $sql;
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $user, __FUNCTION__);
        }
        return $response;
    }
    
    function DeleteUserRegionByUserID($userid) {
        $sql = "delete from user_region where userid = '" . $userid . "' ";
        //echo $sql;
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('userid' => $userid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }

}
