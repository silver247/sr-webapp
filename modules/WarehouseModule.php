<?php
require_once ('./framework/database.php');
require_once './bundle.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WarehouseModule
 *
 * @author EGAT
 */
class WarehouseModule {
    //put your code here
    function __construct() {
        $this->database = new database();
        $this->service = new service();
    }

    function WriteExceptionLog(response $response, $sql, $payload, $fuctionname) {
        $response->MSGMESSAGE2 = $sql;
        $response->MSGMESSAGE3 = $fuctionname;
        if($payload != null || $payload != ''){
            $response->REQDATA[] = $payload;
        }
        $xml = service::generateValidXmlFromObj($response, 'Header', 'Payload');
        $this->database->WriteLog($xml);
    }
    
    function GetProductType(){
        $sql = " select prdtypeid, name, active "
                . " from product_type "
                . " where active = '".PRODUCT_STATUS_ACTIVE."' ";
        //echo $sql;
        $response = new response();
        $producttypelist = array();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $producttypelist[] = $row;
                }
                $response->MSGDATA1[] = $producttypelist;
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, '', __FUNCTION__);
        }
        return $response;
    }
    
    function CreateProduct(product $product){
        $sql = " insert into product values ("
                . " '".$product->PRODUCT_ID."' "
                . " ,'".$product->MODELID."' "
                . " ,'".$product->PRODUCTTYPE_ID."' "
                . " ,'".$product->PRODUCT_NAME."' "
                . " ,'".$product->PRODUCT_STATUS."' "
                . " ,'".date('Y-m-d H:i:s')."' "
                . " ,'".date('Y-m-d H:i:s')."' "
                . " );";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $product, __FUNCTION__);
        }
        return $response;
    }
    
    function GetProductDetail($productid){
        $sql = " select product.productid, modelid, product.prdtypeid, productname, product.active, create_date, product.active "
                . " ,product_type.name as producttype_name "
                . " from product "
                . " join product_type on product.prdtypeid = product_type.prdtypeid "
                . " where product.productid = '".$productid."' ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $product = new product();
                foreach ($res->QRESULT as $row) {
                    $product->PRODUCT_ID = $row['productid'];
                    $product->MODELID = $row['modelid'];
                    $product->PRODUCTTYPE_ID = $row['prdtypeid'];
                    $product->PRODUCT_NAME = $row['productname'];
                    $product->PRODUCT_CREATEDATE = $row['create_date'];
                    $product->PRODUCTTYPE_NAME = $row['producttype_name'];
                    $product->PRODUCT_STATUS = $row['active'];
                    $response->MSGDATA1[] = $product;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $payload = array('productid' => $productid);
            $this->WriteExceptionLog($response, $sql, $payload, __FUNCTION__);
        }
        return $response;
    }
    
    function UpdateProduct(product $product){
        $sqlupdate_arr = array();
        if($product->MODELID != null || $product->MODELID != ''){
            $sqlupdate_arr[] = " modelid = '".$product->MODELID."' ";
        }
        if($product->PRODUCTTYPE_ID != null || $product->PRODUCTTYPE_ID != ''){
            $sqlupdate_arr[] = " prdtypeid = '".$product->PRODUCTTYPE_ID."' ";
        }
        if($product->PRODUCT_NAME != null || $product->PRODUCT_NAME != ''){
            $sqlupdate_arr[] = " productname = '".$product->PRODUCT_NAME."' ";
        }
        if($product->PRODUCT_STATUS != null || $product->PRODUCT_STATUS != ''){
            $sqlupdate_arr[] = " active = '".$product->PRODUCT_STATUS."' ";
        }
        if(count($sqlupdate_arr) > 1){
            $sqlupdate = join(' , ', $sqlupdate_arr);
        }
        else{
            $sqlupdate = $sqlupdate_arr[0];
        }
        $sql = " update product set  "
                . $sqlupdate
                . " ,update_date = CURRENT_TIMESTAMP "
                . " where productid = '".$product->PRODUCT_ID."' ";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
            $this->WriteExceptionLog($response, $sql, $product, __FUNCTION__);
        }
        return $response;
    }
}
