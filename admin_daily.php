<?php
$pagename = "Selling";
$subpagename = "Daily";
?>
<?php
include './bundle.php';
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    ยอดขายรายวัน
                </div>

                <div class="block-option" style="background-color: transparent;">
                    <label class="control-label inlineDiv text-right" style="width:45px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();getDaily($('#month').val(), $('#year').val(), $('#userid option:selected').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control" onChange="getDaily($('#month').val(), $('#year').val(), $('#userid option:selected').val());">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();getDaily($('#month').val(), $('#year').val(), $('#userid option:selected').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                    <label class="control-label inlineDiv" style="width:100px;">พนักงานขาย</label>
                    <select id="userid" name="userid" class="form-control inlineDiv" style="width:200px;margin-right:30px;" onChange="getDaily($('#month').val(), $('#year').val(), $('#userid option:selected').val())">
                        <option value="0">แสดงทั้งหมด</option>
                        <?= $appm->GetUserDropdownlist(''); ?>
                    </select>
                    <div class="inlineDiv headerMonth">
                        <h3 id="headerMonth"></h3> <h2><span id="current_sales"></span>/<span id="monthly_target"></span></h2>
                    </div>
                </div>

                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-12" id="daily">
                    </div>
                </div>

            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script>
    $(document).ready(function () {
        getDaily($("#month").val(), $("#year").val());
    });
</script>

<script>
    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
</script>

<script>
    function getDaily(month, year, userid = 0) {
        var countDate = daysInMonth(month, year);
        var yearTH = parseInt(year) + 543;
        var monthAR = parseInt(month) - 1;
        var workday = 0;
        var dayi = new Date(month + "/1/" + year);//วันเริ่มต้นของเดือน
        var dayNow = (dayi.getDay());//วันเริ่มต้นของเดือน(เลข array เริ่มที่ 0)
        var date = new Date();
        var dateNowM = date.getMonth() + 1//เดือนปัจจุบัน
        var dateNow = dateNowM + "/" + date.getDate() + "/" + date.getFullYear();
        dateNow = new Date(dateNow);
        //count workday
        for (i = 1; i <= countDate; i++) {
            if ((i + dayNow) % 7 > 1) {
                workday++;
            }
        }
        
        //var datemod = (parseInt(dayNow)+parseInt(countDate));
        var datemod = 7 - ((parseInt(dayNow) + parseInt(countDate)) % 7);
        var datemodL = ((parseInt(dayNow) + parseInt(countDate)) % 7);

        var header = "ยอดขายเดือน" + monthNames[month - 1] + " " + yearTH;

        $('#headerMonth').html(header);

        $.ajax({
            type: "GET",
            url: "admin_daily_table.php",
            data: {month: month, year: year, userid: userid},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log(transport.responseText);
            },
            success: function (data) {
                console.log(data);
                $('#userid select').val(userid);
                var workday_past = 0;
                var daily_sales_sum = 0;
                var workday_remain = workday;
                var target_val = 0;
                if(data.MSGDATA1.hasOwnProperty('target_sales')){
                    target_val = data.MSGDATA1.target_sales[0].target_sales_value;
                }
                var month_target = target_val / 12;
                //var month_target = data.MSGDATA1.target_sales[0][1].target_value / 12;
                //console.log(month_target);
                var show = "";

                show += '<div class="daily">'

                for (i = 0; i < 7; i++) {
                    show += '<div class="dailyDay">' + dayNames[i] + '</div>'
                }

                for (i = 0; i < dayNow; i++) {
                    show += '<div class="dailyBlockGroup top right"></div>'
                }

                for (i = 1; i <= countDate; i++) {
                    var dateUse = month + "/" + i + "/" + year;
                    dateUse = new Date(dateUse);
                    var timeDiff = dateUse.getTime() - dateNow.getTime();
                    var diffDays = timeDiff / (1000 * 3600 * 24);


                    if ((i + dayNow) % 7 == 0) {
                        if (diffDays < 0) {
                            show += '<div class="dailyBlockGroup top dateLast">'
                        } else if (diffDays > 0) {
                            show += '<div class="dailyBlockGroup top">'
                        } else {
                            show += '<div class="dailyBlockGroup top dateNow">'
                        }
                    } else {
                        if (diffDays < 0) {
                            show += '<div class="dailyBlockGroup top right dateLast">'
                        } else if (diffDays > 0) {
                            show += '<div class="dailyBlockGroup top right">'
                        } else {
                            show += '<div class="dailyBlockGroup top right dateNow">'
                        }
                    }
                    if ((i + dayNow) % 7 > 1) {
                        show += '<div class="dailyBlock header">'
                    } else {
                        show += '<div class="dailyBlock header" style="color:red;">'
                    }
                    show += i
                    show += '</div>'
                    show += '<div class="dailyBlock">'
                    show += '<div class="dailyHeader">'
                    if ((i + dayNow) % 7 > 1) {
                        var daily_sales = data.MSGDATA1.current_sales[0][i];
                        daily_sales_sum = parseFloat(daily_sales_sum) + parseFloat(daily_sales);
                        var temp_month_target = parseFloat(month_target) - parseFloat(daily_sales_sum);
                        if (workday_remain <= 0) {
                            workday_remain = 1;
                        }
                        //console.log(month_target+" "+daily_sales_sum);
                        var daily_target = temp_month_target / workday_remain;
                        if (temp_month_target <= 0) {
                            daily_target = 0;
                        }

                        workday_past++;
                        workday_remain--;
                        //>=100,80-100,'50-80','0-50'
                        var eval_daily = daily_sales * 100 / daily_target;
                        var color_code = colorcode(eval_daily);
                        show += '<strong style="color:' + color_code + '; ">' + numberWithCommas(daily_sales) + '/ <span style="color:#7dc402";>' + numberWithCommas(daily_target.toFixed(2)) + '</span></strong><br/>บาท'
                    }
                    show += '</div>'
                    show += '</div>'
                    show += '</div>'
                }

                for (i = 0; i < datemod; i++) {
                    if (datemod < 7) {
                        if ((i + datemodL + 1) % 7 == 0) {
                            show += '<div class="dailyBlockGroup top"></div>'
                        } else {
                            show += '<div class="dailyBlockGroup top right"></div>'
                        }
                    }
                }

                show += '</div>'
                $('#current_sales').empty().css('color',colorcode(data.MSGDATA1.current_sales[1]*100 / month_target)).html(numberWithCommas(data.MSGDATA1.current_sales[1]));
                $('#monthly_target').empty().html(numberWithCommas(month_target));
                $('#daily').html(show);
            }
        });
    }

    var colorcode = function (data) {
        var color = ['#7dc402', '#5ccdde', '#ebac04', '#de815c'];/* >=100,80-100,'50-80','0-50'*/
        var [excellent, great, good, bad] = color;
        var color_code = bad;
        if (data >= 100) {
            color_code = excellent;
        } else if (data <= 99 && data >= 80) {
            color_code = great;
        } else if (data <= 79 && data >= 50) {
            color_code = good;
        } else if (data <= 49 && data >= 0) {
            color_code = bad;
        } else {
            color_code = bad;
        }
        return color_code;
    };
</script>

<?php include 'inc/template_end.php'; ?>