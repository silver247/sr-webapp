<?php

require_once ('./framework/database.php');
require_once './bundle.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppBaseManager
 *
 * @author puwakitk
 */
class AppBaseManager {

    //put your code here

    private $database;
    private $service;

    function __construct() {
        $this->database = new database();
        $this->service = new service();
    }

    function Auth($usr, $pwd) {
        $sql = "select userid from user where username = '" . $usr . "' and password = '" . $pwd . "'";
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                return SERV_COMPLETE;
            }
        } catch (PDOException $ex) {
            throw $ex;
        }
    }

    function GetDocType($condition) {
        $sql = "select doc_code from document_type ";
        $sql .= $condition;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $response->MSGMESSAGE1 = $row['doc_code'];
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function CreateDocument($docid, $doctypeID, $owner, $refID) {
        $sql = "INSERT INTO document values ('" . $docid . "', '" . $doctypeID . "', NOW(), '" . $owner . "','" . $refID . "')";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                $response->MSGMESSAGE2 = $refID;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyPlanHeader($condition) {
        $sql = "select planid, plan_date, plan_time, q3_r2, "
                . " c.name, c.customerid, c.address, c.soi, c.road, "
                . " c.sub_district, c.district, c.provinceid, c.contactname, "
                . " c.contactphone, c.contactemail "
                . " from plan join customer c "
                . " on plan.customerid = c.customerid "
                . $condition;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $plan = new plan();
                    $plan->PLANID = $row['planid'];
                    $plan->PLANDATE = $row['plan_date'];
                    $plan->PLANTIME = $row['plan_time'];
                    $plan->CUSTOMER_ID = $row['customerid'];
                    $plan->CUSTOMER_NAME = $row['name'];
                    $plan->CUSTOMER_ADDRESS = $row['address'];
                    $plan->CUSTOMER_SOI = $row['soi'];
                    $plan->CUSTOMER_ROAD = $row['road'];
                    $plan->CUSTOMER_SUBDIST = $row['sub_district'];
                    $plan->CUSTOMER_DIST = $row['district'];
                    $plan->CUSTOMER_PROVINCE = $row['provinceid'];
                    $plan->CUSTOMER_CONTACTNAME = $row['contactname'];
                    $plan->CUSTOMER_CONTACTPHONE = $row['contactphone'];
                    $plan->CUSTOMER_MAIL = $row['contactemail'];
                    $plan->Q3_R2 = $row['q3_r2'];
                    $response->MSGDATA1[] = $plan;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }


    function GetMyOrderHeader($condition) {
        $sql = "select orderid, c.customerid , c.name, status, summary, order_date, status, u.tname, u.tsurname "
                . " from ordering "
                . " join customer c on ordering.customerid = c.customerid "
                . " join user_customer uc on ordering.customerid = uc.customerid "
                . " join user u on uc.userid = u.userid "
                . $condition;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->ORDERID = $row['orderid'];
                    $order->CUSTOMER_ID = $row['customerid'];
                    $order->CUSTOMER_NAME = $row['name'];
                    $order->TOTAL = $row['summary'];
                    $order->STATUS = $row['status'];
                    $order->TIMESTAMP = $row['order_date'];
                    $order->USER_NAMETH = $row['tname']." ".$row['tsurname'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyOrderDetail($condition) {
        $sql = "select ordering_detail.orderid, p.productname , p.productid, p.modelid, price, amount, sum(price * amount) as summary"
                . " from ordering_detail "
                . " join ordering on ordering_detail.orderid = ordering.orderid "
                . " join product p on p.productid = ordering_detail.productid "
                . $condition
                . " GROUP BY ordering_detail.productid ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->ORDERID = $row['orderid'];
                    $order->PRODUCT_NAME = $row['productname'];
                    $order->MODELID = $row['modelid'];
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRICE = $row['price'];
                    $order->AMOUNT = $row['amount'];
                    //$order->STATUS = $row['status'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyEvaluation($condition) {
        $sql = "select eval_grade, MONTH(eval_month) "
                . " from user_eval "
                . " join eval_master on user_eval.evalid = eval_master.evalid "
                . $condition
                . " ORDER BY eval_month DESC "
                . " LIMIT 0,3 ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $eval = array();
                $i = 0;
                foreach ($res->QRESULT as $row) {
                    $eval[$i] = $row['eval_grade'];
                    $i++;
                }
                $user = new user();
                $user->EVAL1 = $eval[0];
                $user->EVAL2 = $eval[1];
                $user->EVAL3 = $eval[2];
                $response->MSGDATA1[] = $user;
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMySales($condition) {
        $sql = "select sum(summary) as total"
                . " from ordering "
                . $condition;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $user = new user();
                    $user->TOTALSALES = $row['total'];
                    $response->MSGDATA1[] = $user;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyTopProducts($condition, $limit) {
        $sql = "select ordering_detail.productid, productname, sum(amount) as amt, sum(ordering_detail.amount * ordering_detail.price) as total "
                . " from ordering "
                . " join ordering_detail on ordering.orderid = ordering_detail.orderid "
                . " join product on ordering_detail.productid = product.productid "
                . $condition
                //. " and ordering.status = '".ORDER_STATUS_DONE."' "
                . " GROUP BY ordering_detail.productid "
                . " ORDER BY amt DESC "
                . $limit;
        //echo $sql;
        //. " LIMIT 0,5 ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRODUCT_NAME = $row['productname'];
                    $order->AMOUNT = $row['amt'];
                    $order->TOTAL = $row['total'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_COMPLETE;
                $order = new order();
                $order->PRODUCT_ID = '-';
                $order->PRODUCT_NAME = '-';
                $order->AMOUNT = ' ';
                $order->TOTAL = '-';
                $response->MSGDATA1[] = $order;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyTopCustomers($condition, $limit) {
        $sql = "select ordering.customerid, customer.name, sum(summary) as total"
                . " from ordering "
                . " join customer on ordering.customerid = customer.customerid "
                . $condition
                //. " and ordering.status = '".ORDER_STATUS_DONE."' "
                . " GROUP BY ordering.customerid "
                . " ORDER BY total DESC "
                . $limit;
        //. " LIMIT 0,5 ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->CUSTOMER_ID = $row['customerid'];
                    $order->CUSTOMER_NAME = $row['name'];
                    $order->TOTAL = $row['total'];
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_COMPLETE;
                $order = new order();
                $order->CUSTOMER_ID = '-';
                $order->CUSTOMER_NAME = '-';
                $order->TOTAL = ' ';
                $response->MSGDATA1[] = $order;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function SetSaleEvaluation(user $user) {
        $sql = "insert into user_eval values (DEFAULT,'" . $user->USERID . "','" . $user->EVAL_YEAR . "','" . $user->EVAL_MONTH . "','" . $user->SALES_MONTH . "','" . $user->EVALID . "',1)";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    /*
     * This function is only user for current month > Febuary 
     * If current month is January must use GetMyInitialSalesTarget
     */

    function GetMyTargetSalesCurrentMonth($user) {
        $sql = "select sell_target "
                . " from user_eval "
                . " join eval_master on user_eval.evalid = eval_master.evalid "
                . " where userid = '" . $user . "'"
                . " AND eval_year = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) "
                . " AND MONTH(eval_month) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $user = new user();
                    $user->TSALES_MONTH_VALUE = $row['sell_target'];
                    $response->MSGDATA1[] = $user;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    /*
     * This function is use only for First Month of the year
     */

    function GetMyInitialSalesTarget($user) {
        $sql = "select target_sales_allyear, target_sales_fmonth "
                . " from user_region_plan "
                . " where userid = '" . $user . "'"
                . " AND yplanid = (select yplanid from year_plan where year = YEAR(CURDATE())) ";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $user = new user();
                    $user->TSALES_MONTH_VALUE = $row['target_sales_fmonth'];
                    $response->MSGDATA1[] = $user;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyEvaluationFromSales($psales) {
        $sql = "select * from eval_master where " . $psales . " >= condition_min and " . $psales . " <= condition_max";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $eval = new evaluation();
                    $eval->EVALID = $row['evalid'];
                    $response->MSGDATA1[] = $eval;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    /*
     * $userid parameter means userid 
     */

    function GetMyYearTarget($userid) {
        $sql = "select target_sales_allyear "
                . " from user_region_yearplan "
                . " where yplanid = (select yplanid from year_plan where year = YEAR(CURDATE())) "
                . " and userid = '" . $userid . "'";
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $user = new user();
                    $user->TASLES_YEAR_VALUE = $row['target_sales_allyear'];
                    $response->MSGDATA1[] = $user;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    /*
     * Condition param is for month view
     */

    function GetMyMonthSchedule($condition) {
        $sql = "select planid, plan_date, plan_time, status, "
                . " c.name, c.customerid, c.address, c.soi, c.road, "
                . " c.sub_district, c.district, c.provinceid, c.contactname, "
                . " c.contactphone, c.contactemail "
                . " from plan join customer c "
                . " on plan.customerid = c.customerid and plan.status != 'CAN' and plan.status != 'COM'"
                . $condition;
        //echo $sql;
        $res = $this->database->READ()->SQL($sql)->EXECUTE();
        $events = array();
        if ($res->MSGID === SERV_COMPLETE) {
            foreach ($res->QRESULT as $row) {
                $event = array();
                $event['title'] = $row['name'];
                $event['start'] = $row['plan_date'] . "T" . $row['plan_time'];
                $event['end'] = $row['plan_date'] . "T" . $row['plan_time'];
                $event['description'] = $row['customerid'];
                if ($row['status'] == PLAN_STATUS_NEW) {
                    $event['backgroundColor'] = '#a9dded';
                    $event['borderColor'] = '#a9dded';
                }
                array_push($events, $event);
            }
        }

        return $events;
    }



    function GetMyPlanHistoryByShop($customerid) {
        $sql = "select customer.name, planid, plan_date, plan_time,status "
                . " from plan "
                . " join customer on plan.customerid = customer.customerid "
                . " where plan.customerid = '" . $customerid . "' ";
        $res = $this->database->READ()->SQL($sql)->EXECUTE();
        $response = new response();
        if ($res->MSGID === SERV_COMPLETE) {
            foreach ($res->QRESULT as $row) {
                $plan = new plan();
                $plan->PLANID = $row['planid'];
                $plan->PLANDATE = $row['plan_date'];
                $plan->PLANTIME = $row['plan_time'];
                if ($row['status'] == 'NEW') {
                    $plan->Q3_R2 = 'ยังไม่บันทึกผลการเข้าพบ';
                } else if ($row['status'] == 'PED') {
                    $plan->Q3_R2 = 'บันทึกผลแล้ว กำลังรอใบสั่งซื้อ';
                } else if ($row['status'] == 'COM') {
                    $plan->Q3_R2 = 'บันทึกผลแล้ว การชำระเงินเสร็จสมบูรณ์';
                } else if ($row['status'] == 'CAN') {
                    $plan->Q3_R2 = 'การนัดถูกยกเลิก';
                } else {
                    $plan->Q3_R2 = '-';
                }
                $plan->CUSTOMER_NAME = $row['name'];
                $response->MSGDATA1[] = $plan;
                $response->MSGMESSAGE1 = $row['name'];
            }
            $response->MSGID = SERV_COMPLETE;
        }
        return $response;
    }

    function CreateOrderHeader(order $order) {
        $response = new response();
        $refplan = "";
        if ($order->REFPLAN == '' || $order->REFPLAN == null) {
            $refplan = " DEFAULT ";
        } else {
            $refplan = $order->REFPLAN;
        }
        $sql = "INSERT INTO ordering VALUES ( "
                . " '" . $order->ORDERID . "', "
                . " '" . $order->USERID . "', "
                . " '" . $order->CUSTOMER_ID . "', "
                . $refplan . ", "
                . " '" . $order->TOTAL . "', "
                . " '" . $order->TIMESTAMP . "', "
                . " DEFAULT )";
        //echo $sql;
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function CreateOrderDetail($sql) {
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function UpdatePlanStatus($planid, $status) {
        $sql = "update plan set status = '" . $status . "' where planid = '" . $planid . "'";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function GetMyPlanOrderDetail($planid) {
        $salesmodule = new SalesModule();
        $sql = "select productid, price, amount, promotionid, , sum(price * amount) as summary"
                . " , price_individual_cost + (price_individual_cost * individual_margin_profit) as retail_price "
                . " from plan_ordering "
                . " join product p on p.productid = plan_ordering.productid"
                . " join product_price on product_price.productid = p.productid "
                . " where planid = '" . $planid . "' ";
        $cnt = 0;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->PRODUCT_ID = $row['productid'];
                    $order->PRICE = $row['price'];
                    $order->AMOUNT = $row['amount'];
                    $retail_price = 0;
                    if ($row['retail_price'] != null && $row['retail_price'] != "") {
                        $retail_price = $row['retail_price'];
                    }
                    $order->PRODUCT_RETAIL_PRICE = $retail_price;
                    $order->PRODUCT_RETAIL_DISCOUNT = 0;
                    $order->REF_PROMOTION = $row['promotionid'];
                    if ($row['promotionid'] != null && $row['promotionid'] != "") {
                        $promotion = new promotion();
                        $promotion = $salesmodule->GetPromotionDetail($row['promotionid'])->MSGDATA1[0];
                        $product = new product();
                        $product = $promotion->PROMOTION_PRODUCT[0];
                        $order->PRODUCT_RETAIL_DISCOUNT = $product->PRODUCT_RETAIL_DISCOUNT;
                    }
                    $response->MSGDATA1[] = $order;
                    $cnt++;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        $response->MSGMESSAGE2 = $cnt;

        return $response;
    }

    function GetProductDescription($productid) {
        $sql = "select productname from product where productid = '" . $productid . "' ";
        $res = $this->database->READ()->SQL($sql)->EXECUTE();
        $events = array();
        if ($res->MSGID === SERV_COMPLETE) {
            foreach ($res->QRESULT as $row) {
                return $row['productname'];
            }
        }

        return '-';
    }

    function GetMyOrderHistory($condition) {
        $sql = "select orderid, customer.name, order_date, summary, status, ordering.customerid "
                . " from ordering "
                . " join customer on ordering.customerid = customer.customerid "
                . $condition;
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->READ()->SQL($sql)->EXECUTE();
            if ($res->AFFECT_ROW > 0) {
                $response->MSGID = SERV_COMPLETE;
                foreach ($res->QRESULT as $row) {
                    $order = new order();
                    $order->ORDERID = $row['orderid'];
                    $order->CUSTOMER_ID = $row['customerid'];
                    $order->TIMESTAMP = $row['order_date'];
                    $order->CUSTOMER_NAME = $row['name'];
                    $order->TOTAL = $row['summary'];
                    $order->STATUS = $row['status'];
                    $order->STATUS_DESC = BusinessLogic::OrderStatusDescription($row['status']);
                    $response->MSGDATA1[] = $order;
                }
            } else {
                $response->MSGID = SERV_NODATA;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }

        return $response;
    }

    function UpdateOrderStatus($orderid, $status,$comment) {
        $sql = "UPDATE `ordering` SET `status`= '" . $status . "', comment = '".$comment."' WHERE orderid = '" . $orderid . "'";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function UpdateCustomerStatus($customerid, $status) {
        $sql = "UPDATE `customer` SET `active`= '" . $status . "' WHERE customerid = '" . $customerid . "'";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }


    function UpdatePlanHeader(plan $plan) {
        $sql = "update plan set customerid = '" . $plan->CUSTOMER_ID . "',"
                . " plan_date = '" . $plan->PLANDATE . "',"
                . " plan_time = '" . $plan->PLANTIME . "',"
                . " q3_r2 = '" . $plan->Q3_R2 . "' "
                . " where planid = '" . $plan->PLANID . "' ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function DeleteOrderDetail($orderid) {
        $sql = " delete from ordering_detail where orderid = '" . $orderid . "' ";
        $response = new response();
        try {
            $this->database->WRITE()->SQL($sql)->EXECUTE();
            $response->MSGID = SERV_COMPLETE;
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function UpdateMyOrderHeader(order $order) {
        $updateorder = "";
        if ($order->TOTAL != "") {
            $updateorder .= " summary = '" . $order->TOTAL . "' ";
        }
        if ($order->TIMESTAMP != "") {
            if ($updateorder != "") {
                $updateorder .= " ,order_date = '" . $order->TIMESTAMP . "' ";
            } else {
                $updateorder .= " order_date = '" . $order->TIMESTAMP . "' ";
            }
        }
        if ($order->STATUS != "") {
            if ($updateorder != "") {
                $updateorder .= " ,status = '" . $order->STATUS . "' ";
            } else {
                $updateorder .= " status = '" . $order->STATUS . "' ";
            }
        }
        $sql = " update ordering set " . $updateorder . " where orderid = '" . $order->ORDERID . "' ";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function UpdateCustomer(customer $customer) {
        // Q2 R1

        if ($customer->CUSTOMER_ADDRESS == "" || $customer->CUSTOMER_ADDRESS == null) {
            $address = " ' ' ";
        } else {
            $address = " '" . $customer->CUSTOMER_ADDRESS . "' ";
        }

        if ($customer->CUSTOMER_SOI == "" || $customer->CUSTOMER_SOI == null) {
            $soi = " ' ' ";
        } else {
            $soi = " '" . $customer->CUSTOMER_SOI . "' ";
        }

        if ($customer->CUSTOMER_ROAD == "" || $customer->CUSTOMER_ROAD == null) {
            $road = " ' ' ";
        } else {
            $road = " '" . $customer->CUSTOMER_ROAD . "' ";
        }

        if ($customer->CUSTOMER_SUBDIST == "" || $customer->CUSTOMER_SUBDIST == null) {
            $subdistrict = " ' ' ";
        } else {
            $subdistrict = " '" . $customer->CUSTOMER_SUBDIST . "' ";
        }

        if ($customer->CUSTOMER_DIST == "" || $customer->CUSTOMER_DIST == null) {
            $district = " ' ' ";
        } else {
            $district = " '" . $customer->CUSTOMER_DIST . "' ";
        }

        if ($customer->CUSTOMER_MAIL == "" || $customer->CUSTOMER_MAIL == null) {
            $contactemail = " ' ' ";
        } else {
            $contactemail = " '" . $customer->CUSTOMER_MAIL . "' ";
        }
        
        if ($customer->CUSTOMER_GRPID == "" || $customer->CUSTOMER_GRPID == null) {
            $customergrp = " ' ' ";
        } else {
            $customergrp = " '" . $customer->CUSTOMER_GRPID . "' ";
        }


        $sql = "UPDATE customer SET "
                //. " '" . $customer->CUSTOMER_ID . "', "
                . " custgrpid = '" . $customer->CUSTOMER_GRPID . "', "
                . " name = '" . $customer->CUSTOMER_NAME . "', "
                . " address = " . $address . ", "
                . " soi = " . $soi . ", "
                . " road = " . $road . ", "
                . " sub_district = " . $subdistrict . ", "
                . " district = " . $district . ", "
                . " provinceid = '" . $customer->CUSTOMER_PROVINCE . "', "
                . " zipcode = '" . $customer->CUSTOMER_ZIPCODE . "', "
                . " contactname = '" . $customer->CUSTOMER_CONTACTNAME . "', "
                . " contactphone = '" . $customer->CUSTOMER_CONTACTPHONE . "', "
                . " contactemail = " . $contactemail . " "
                . " WHERE customerid = '" . $customer->CUSTOMER_ID . "' ";
        //echo $sql;
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
                $response->MSGMESSAGE2 = $customer->CUSTOMER_ID;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }
    
    function UpdateProductStatus($productid, $status) {
        $sql = "UPDATE `product` SET `active`= '" . $status . "' WHERE productid = '" . $productid . "'";
        $response = new response();
        try {
            $res = $this->database->WRITE()->SQL($sql)->EXECUTE();
            if ($res->QRESULT == TRUE) {
                $response->MSGID = SERV_COMPLETE;
                $response->MSGMESSAGE1 = $res->AFFECT_ROW;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (PDOException $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }
    
}
