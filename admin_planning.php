<?php
$pagename = "Selling";
$subpagename = "Planning";
?>
<?php
//include 'inc/config.php';
include './bundle.php';
?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
$appmng = new AppManager();
$output = new response();
$output = $appmng->queryYearPlan();
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">ยอดขายทั้งหมด</div>

                <div class="btn-right"><a href="Admin/Planning/Edit/" data-toggle="tooltip" title="แก้ไขยอดขาย" class="btn btn-effect-ripple btn-xs btn-info"><i class="gi gi-pencil"></i> แก้ไขยอดขายทั้งหมด</a></div>

                <label class="control-label inlineDiv" style="width:80px;">ยอดขายปี</label>
                <div class="inlineDiv">
                    <div class="input-group" style="width:180px;">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preYear()"><i class="fa fa-chevron-left"></i></button>
                        </span>
                        <input type="text" id="yearBD" name="yearBD" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextYear()"><i class="fa fa-chevron-right"></i></button>
                        </span>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-12">
                        <div class="col-md-4 salesBlockAll">
                            <h2 class="salesHeader" style="padding: 20px 0 30px">ยอดขายทั้งหมดในปี 2560</h2>
                            <div class="regionImg"><img src="img/icon_menu/regionAll.png" /></div>
                            <?php
                            $value = $output->MSGDATA2;
                            if (isset($value)) {
                                $value = number_format($value, 0);
                            } else {
                                $value = number_format(0, 0);
                            }
                            ?>
                            <div class="salesDetail" style="font-size:30px"><?= $value ?> บาท</div>
                        </div>
                        <div class="col-md-8" style="padding:0">
                            <div class="salesBlock top right left">
                                <h3 class="salesHeader">ภาคเหนือ</h3>
                                <div class="regionImg"><img src="img/icon_menu/regionN.png" /></div>
                                <?php
                                $temp = $output->MSGDATA1;
                                $value = 0;
                                $region = REGION_N;
                                if (isset($temp[$region]["target_value"])) {
                                    $value = $temp[$region]["target_value"];
                                    $value = number_format($value, 0);
                                } else {
                                    $value = number_format(0, 0);
                                }
                                ?>
                                <div class="salesDetail"><?= $value ?> บาท</div>
                            </div>
                            <div class="salesBlock top right">
                                <h3 class="salesHeader">ภาคใต้</h3>
                                <div class="regionImg"><img src="img/icon_menu/regionS.png" /></div>
                                <?php
                                $temp = $output->MSGDATA1;
                                $value = 0;
                                $region = REGION_S;
                                if (isset($temp[$region]["target_value"])) {
                                    $value = $temp[$region]["target_value"];
                                    $value = number_format($value, 0);
                                } else {
                                    $value = number_format(0, 0);
                                }
                                ?>
                                <div class="salesDetail"><?= $value ?> บาท</div>
                            </div>
                            <div class="salesBlock top right">
                                <h3 class="salesHeader">ภาคตะวันออก</h3>
                                <div class="regionImg"><img src="img/icon_menu/regionE.png" /></div>
                                <?php
                                $temp = $output->MSGDATA1;
                                $value = 0;
                                $region = REGION_E;
                                if (isset($temp[$region]["target_value"])) {
                                    $value = $temp[$region]["target_value"];
                                    $value = number_format($value, 0);
                                } else {
                                    $value = number_format(0, 0);
                                }
                                ?>
                                <div class="salesDetail"><?= $value ?> บาท</div>
                            </div>
                            <div class="salesBlock top bottom right left">
                                <h3 class="salesHeader">ภาคตะวันตก</h3>
                                <div class="regionImg"><img src="img/icon_menu/regionW.png" /></div>
                                <?php
                                $temp = $output->MSGDATA1;
                                $value = 0;
                                $region = REGION_W;
                                if (isset($temp[$region]["target_value"])) {
                                    $value = $temp[$region]["target_value"];
                                    $value = number_format($value, 0);
                                } else {
                                    $value = number_format(0, 0);
                                }
                                ?>
                                <div class="salesDetail"><?= $value ?> บาท</div>
                            </div>
                            <div class="salesBlock top bottom right">
                                <h3 class="salesHeader">ภาคอีสานตอนบน</h3>
                                <div class="regionImg"><img src="img/icon_menu/regionNE.png" /></div>
                                <?php
                                $temp = $output->MSGDATA1;
                                $value = 0;
                                $region = REGION_NE;
                                if (isset($temp[$region]["target_value"])) {
                                    $value = $temp[$region]["target_value"];
                                    $value = number_format($value, 0);
                                } else {
                                    $value = number_format(0, 0);
                                }
                                ?>
                                <div class="salesDetail"><?= $value ?> บาท</div>
                            </div>
                            <div class="salesBlock top bottom right">
                                <h3 class="salesHeader">ภาคกลาง</h3>
                                <div class="regionImg"><img src="img/icon_menu/regionNEE.png" /></div>
                                <?php
                                $temp = $output->MSGDATA1;
                                $value = 0;
                                $region = REGION_M;
                                if (isset($temp[$region]["target_value"])) {
                                    $value = $temp[$region]["target_value"];
                                    $value = number_format($value, 0);
                                } else {
                                    $value = number_format(0, 0);
                                }
                                ?>
                                <div class="salesDetail"><?= $value ?> บาท</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script>
    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
</script>	

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/readyDashboard.js"></script>
<script>$(function () {
        ReadyDashboard.init();
    });</script>

<?php include 'inc/template_end.php'; ?>