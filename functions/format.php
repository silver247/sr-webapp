<?php
function addzero_2($number){
    $text = '';
    if($number < 10) $text = '0'.$number;
    else $text = $number;
    return $text;
}
function addzero_3($number){
    $text = '';
    if($number < 10) $text = '00'.$number;
    elseif($number < 100) $text = '0'.$number;
    else $text = $number;
    return $text;
}
function addzero_4($number){
    $text = '';
    if($number < 10) $text = '000'.$number;
    elseif($number < 100) $text = '00'.$number;
    elseif($number < 1000) $text = '0'.$number;
    else $text = $number;
    return $text;
}
function addzero_5($number){
    $text = '';
    if($number < 10) $text = '0000'.$number;
    elseif($number < 100) $text = '000'.$number;
    elseif($number < 1000) $text = '00'.$number;
    elseif($number < 10000) $text = '0'.$number;
    else $text = $number;
    return $text;
}
function number_id($number){
    $text = '';
	$number = $number+100000;
	$text = "#".$number;
    return $text;
}function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>