<?php
function DateSaveDB($date){
  $year = substr($date,6,4);
  $year = $year-543;
  $newdate = $year."-".substr($date,3,2)."-".substr($date,0,2);
  return $newdate;
}
function DateSaveDB2($date){
  $year = substr($date,6,4);
  $year = $year;
  $newdate = $year."-".substr($date,3,2)."-".substr($date,0,2);
  return $newdate;
}
function thaidate($date){  
  $year = substr($date,0,4);
  if($year != "0000") $year = $year+543;
  $newdate = substr($date,8,2)."-".substr($date,5,2)."-".$year;
  return $newdate;
}
function thaidate_split($date){  
  $year = substr($date,0,4);
  if($year != "0000") $year = $year+543;
  $newdate = substr($date,8,2).substr($date,5,2).$year;
  return $newdate;
}
$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");  
$thai_month_arr=array(  
    "0"=>"",  
    "1"=>"มกราคม",  
    "2"=>"กุมภาพันธ์",  
    "3"=>"มีนาคม",  
    "4"=>"เมษายน",  
    "5"=>"พฤษภาคม",  
    "6"=>"มิถุนายน",   
    "7"=>"กรกฎาคม",  
    "8"=>"สิงหาคม",  
    "9"=>"กันยายน",  
    "10"=>"ตุลาคม",  
    "11"=>"พฤศจิกายน",  
    "12"=>"ธันวาคม"                   
);  
function thaidate_withouttime($date){  
    global $thai_day_arr,$thai_month_arr;  
    $time = strtotime($date);
    $thai_date_return="วัน".$thai_day_arr[date("w",$time)];  
    $thai_date_return.= "ที่ ".date("j",$time);  
    $thai_date_return.=" ".$thai_month_arr[date("n",$time)];  
    $thai_date_return.= " พ.ศ.".(date("Y",$time)+543);   
    return $thai_date_return;  
}
function thaidate_withouttime_short($date){  
    global $thai_day_arr,$thai_month_arr;  
    $time = strtotime($date);  
    $thai_date_return= date("j",$time);  
    $thai_date_return.=" ".$thai_month_arr[date("n",$time)];  
    $thai_date_return.= " ".(date("Y",$time)+543);   
    return $thai_date_return;  
}
function thaidate_saveDB($date1){
  global $thai_month_arr;  

  $date = explode(" ",$date1);
  $year = $date[2]-543;
  for($i=0; $i<=12; $i++){
    if($date[1] == $thai_month_arr[$i]){
	  if($i < 10) $month = '0'.$i;
      else $month = $i;
	}
  }
  $day = $date[0];
  $date_saveDB = $year."-".$month."-".$day;
  return $date_saveDB;
}
function shipmonth($date){
    global $thai_month_arr;  
	$time = strtotime($date);
    $thai_date_return=$thai_month_arr[date("n",$time)];  
    $thai_date_return.= " ".(date("Y",$time)+543);   
    return $thai_date_return;  
}
function GetThaiMonthDesc($month){
    global $thai_month_arr;
    return $thai_month_arr[$month];
}
function ConvertToThaiYear($year){
    return $year + 543;
}
?>