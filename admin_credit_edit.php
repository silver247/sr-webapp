<?php
$pagename = "Selling";
$subpagename = "Credit";
?>

<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
include './bundle.php';
$appm = new AppManager();
$grplist = $appm->GetCustomerGroupDetail();
$credit = array();
//service::printr($grplist);
foreach ($grplist->MSGDATA1 as $grp) {
    $credit[$grp->CUSTOMER_GRPID] = $grp->CUSTOMER_GRPCREDIT;
    $discount[$grp->CUSTOMER_GRPID] = number_format($grp->CUSTOMER_GRPDISCOUNT * 100,0);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <form name="tform1" id="tform1" onsubmit="updateGrpDetail();return false;">
                <input type="hidden" name="req" value="<?=REQ_CHANGE_CREDIT;?>">
                 <div class="block full">
                    <div class="block-title">
                        แก้ไข Credit Term
                        <a href="Admin/Credit/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้า Credit Term</div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <h4>Credit Term</h4>
                        </div>
                        <div class="col-md-offset-3 col-md-6">
                            <div class="form-group block-3">
                                <label>ลูกค้าชั้นพรีเมี่ยม</label>
                                <div class="input-group text">
                                    <input type="text" id="premiumcredit" name="premiumcredit" class="form-control money" onkeypress="checknumber()" value="<?= $credit[1]; ?>">
                                    <span class="input-group-addon">วัน</span>
                                </div>
                            </div>
                            <div class="form-group block-3">
                                <label>ลูกค้าชั้นดี</label>
                                <div class="input-group text">
                                    <input type="text" id="goodcredit" name="goodcredit" class="form-control money" onkeypress="checknumber()" value="<?= $credit[2]; ?>">
                                    <span class="input-group-addon">วัน</span>
                                </div>
                            </div>
                            <div class="form-group block-3">
                                <label>ลูกค้าทั่วไป</label>
                                <div class="input-group text">
                                    <input type="text" id="normalcredit" name="normalcredit" class="form-control money" onkeypress="checknumber()" value="<?= $credit[3]; ?>">
                                    <span class="input-group-addon">วัน</span>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-md-offset-3 col-md-6">
                            <h4>ส่วนลด</h4>
                        </div>
                        <div class="col-md-offset-3 col-md-6">
                            <div class="form-group block-3">
                                <label>ลูกค้าชั้นพรีเมี่ยม</label>
                                <div class="input-group text">
                                    <input type="text" id="premiumdiscount" name="premiumdiscount" class="form-control money" onkeypress="checknumber()" value="<?= $discount[1]; ?>">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="form-group block-3">
                                <label>ลูกค้าชั้นดี</label>
                                <div class="input-group text">
                                    <input type="text" id="gooddiscount" name="gooddiscount" class="form-control money" onkeypress="checknumber()" value="<?= $discount[2]; ?>">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="form-group block-3">
                                <label>ลูกค้าทั่วไป</label>
                                <div class="input-group text">
                                    <input type="text" id="normaldiscount" name="normaldiscount" class="form-control money" onkeypress="checknumber()" value="<?= $discount[3]; ?>">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;">
                        <button type="submit" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึก" aria-describedby="tooltip441840"><i class="gi gi-floppy_disk"></i> บันทึก</button></div>
                </div>
            </form>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script>
    function updateGrpDetail() {
        bootbox.confirm({
            size: 'small',
            title: "ยืนยันยอดขาย",
            message: "คุณยืนยันการแก้ไขรายการนี้ใช่หรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ยกเลิก',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ยืนยันการแก้ไข',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                var loading = "";
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "AppHttpRequest.php",
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            loading = bootbox.dialog({
                                size: 'small',
                                message: '<p class="text-center">เรากำลังบันทึกข้อมูล  credit ของท่าน กรุณารอสักครู่...</p>',
                                closeButton: false
                            });
                        },
                        error: function (transport, status, errorThrown) {
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: "บันทึกข้อมูลการแก้ไข Credit ไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                    title: "การแจ้งเตือน"
                                });
                            }, 3000);
                            console.log(transport.responseText);
                        },
                        success: function (data) {
                            if (data.MSGID === 100) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลการแก้ไข Credit สำเร็จ",
                                        title: "การแจ้งเตือน",
                                        callback: function () {
                                            window.location = "Admin/Credit/";
                                        }
                                    });
                                }, 3000);
                            } else {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลการแก้ไข Credit ไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            }
                        }
                    });
                }

            }
        });

    }
</script>
<?php include 'inc/template_end.php'; ?>