<?php
$pagename = "Selling";
$subpagename = "Credit";
?>

<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
include './bundle.php';
$appm = new AppManager();
$grplist = $appm->GetCustomerGroupDetail();
$credit = array();
//service::printr($grplist);
foreach ($grplist->MSGDATA1 as $grp) {
    $credit[$grp->CUSTOMER_GRPID] = $grp->CUSTOMER_GRPCREDIT;
    $discount[$grp->CUSTOMER_GRPID] = number_format($grp->CUSTOMER_GRPDISCOUNT * 100,0);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">Credit Term</div>

                <div class="btn-right"><a href="Admin/Credit/Edit/" data-toggle="tooltip" title="แก้ไขยอดขาย" class="btn btn-effect-ripple btn-xs btn-info"><i class="gi gi-pencil"></i> แก้ไข Credit Term</a></div>

                <div class="row" style="margin-top: 80px;">
                    <div class="col-xs-12">
                        <div class="salesBlock all">
                            <h3 class="creditHeader">ลูกค้ากลุ่ม<br/><strong style="color:#22c726;">A</strong></h3>
                        </div>
                        <div class="salesBlock top bottom right">
                            <h3 class="creditHeader">ลูกค้ากลุ่ม<br/><strong style="color:#ffc955;">B</strong></h3>
                        </div>
                        <div class="salesBlock top bottom right">
                            <h3 class="creditHeader">ลูกค้ากลุ่ม<br/><strong style="color:#ff8c04;">C</strong></h3>
                        </div>
                        <div class="salesBlock bottom rightW left header">
                            <h3 class="creditDetail">Credit Term<br/><strong><?= $credit[1]; ?> วัน</strong></h3>
                            <h3 class="creditDetail">Discount<br/><strong><?= $discount[1]; ?> %</strong></h3>
                        </div>
                        <div class="salesBlock bottom rightW header">
                            <h3 class="creditDetail">Credit Term<br/><strong><?= $credit[2]; ?> วัน</strong></h3>
                            <h3 class="creditDetail">Discount<br/><strong><?= $discount[2]; ?> %</strong></h3>
                        </div>
                        <div class="salesBlock bottom right header">
                            <h3 class="creditDetail">Credit Term<br/><strong><?= $credit[3]; ?> วัน</strong></h3>
                            <h3 class="creditDetail">Discount<br/><strong><?= $discount[3]; ?> %</strong></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script>
    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
</script>	

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/readyDashboard.js"></script>
<script>$(function () {
        ReadyDashboard.init();
    });</script>

<?php include 'inc/template_end.php'; ?>