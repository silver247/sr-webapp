<?php $pagename = "Approve" ?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">อนุมัติรายการสั่งซื้อ</div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:35px;"></label>
                    <div class="inlineDiv" style="width:200px">
                        <input class="form-control" type="text" id="search" name="search" placeholder="ค้นหาข้อมูล" />
                    </div>
                    <label class="control-label inlineDiv text-right" style="width:45px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();tableApprove('', $('#month').val(), $('#year').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();tableApprove('', $('#month').val(), $('#year').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                    <label class="csscheckbox csscheckbox-primary"><input id="allMonth" type="checkbox"><span style="background: #FFF"></span></label>
                    <label class="control-label inlineDiv">แสดงทุกเดือน</label>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableApprove"></div>
                </div>
                <input type="hidden" id="orderid"><input type="hidden" id="req" value="<?= REQ_APPROVEREJECT_ORDER; ?>">
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
                                    $('#monthYear').datepicker({
                                        format: "MM yyyy",
                                        weekStart: 0,
                                        viewMode: "months",
                                        minViewMode: "months",
                                        language: "th-th"
                                    });
</script>

<!-- Add Entry Form-->
<div id="lightBlock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-alert">
        <div class="modal-content">
            <form class="push">
                <div class="modal-header">
                    <div class="block-title">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <i class="gi gi-ok_2"></i> อนุมัติรายการสั่งซื้อ
                    </div>
                </div>
                <div class="block modal-body">
                    <div class="form-group">
                        ยืนยันการอนุมัติรายการสั่งซื้อ ?
                    </div>
                    <div class="text-center hidden" id="loading_apr">
                        <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>
                    </div>
                    <span class="text-center hidden" id="ApproveResult"></span>
                </div>
                <div class="block modal-footer">
                    <div class="text-right btn-force">
                        <a href="javascript:void(0)" data-toggle="tooltip" title="ไม่อนุมัติการสั่งซื้อ" id="btnApprove" class="btn btn-effect-ripple btn-xs btn-success">ยืนยันการอนุมัติรายการสั่งซื้อ</a>
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-xs btn-danger" id="btnCancelApprove" data-dismiss="modal">ยกเลิก</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lightBlock2" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-alert">
        <div class="modal-content">
            <form class="push">
                <div class="modal-header">
                    <div class="block-title">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <i class="gi gi-remove_2"></i> ไม่อนุมัติรายการสั่งซื้อ
                    </div>
                </div>
                <div class="block modal-body">
                    <div class="form-group">
                        <label>หมายเหตุ</label>
                        <textarea class="form-control" id="comment" placeholder="หมายเหตุที่ไม่อนุมัติรายการสั่งซื้อ"></textarea>
                    </div>
                    <div class="text-center hidden" id="loading_rjt">
                        <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>
                    </div>
                    <span class="text-center hidden" id="RejectResult"></span>
                </div>
                <div class="block modal-footer">
                    <div class="text-right btn-force">
                        <a href="javascript:void(0)" data-toggle="tooltip" title="ไม่อนุมัติการสั่งซื้อ" id="btnReject" class="btn btn-effect-ripple btn-xs btn-success">ยืนยันการไม่อนุมัติรายการสั่งซื้อ</a>
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-xs btn-danger" id="btnCancelReject" data-dismiss="modal">ยกเลิก</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END Add Entry Form -->

<script>
    $(document).ready(function () {
        //set initial state.
        $("#allMonth").change(function () {
            if (this.checked) {
                $("#monthYear").prop('disabled', true);
                $("#preMonth").prop('disabled', true);
                $("#nextMonth").prop('disabled', true);
                tableApprove('', "0", "0");
            } else {
                $("#monthYear").prop('disabled', false);
                $("#preMonth").prop('disabled', false);
                $("#nextMonth").prop('disabled', false);
                tableApprove('', $("#month").val(), $("#year").val());
            }
        });

        tableApprove('', $("#month").val(), $("#year").val());

        $('#btnApprove').on('click', function (e) {
            e.preventDefault();
            $('#loading_apr').removeClass('hidden');
            $('#btnApprove').addClass('disabled');
            var data = {req: $('#req').val(), orderid: $('#orderid').val(), command: '<?= ORDER_STATUS_DONE; ?>'};
            SendData(data, 1);
        });
        $('#btnReject').on('click', function (e) {
            e.preventDefault();
            $('#loading_rjt').removeClass('hidden');
            $('#btnReject').addClass('disabled');
            $('#comment').attr('readonly',true);
            var data = {req: $('#req').val(), orderid: $('#orderid').val(), command: '<?= ORDER_STATUS_REJT; ?>', comment: $('#comment').val()};
            SendData(data, 2);
        });
    });
    $('#lightBlock').on('hidden.bs.modal', function () {
        tableApprove('', $("#month").val(), $("#year").val());
    })
    $('#lightBlock2').on('hidden.bs.modal', function () {
        tableApprove('', $("#month").val(), $("#year").val());
    })
</script>

<script>
    function SendData(dataobject, trg) {
        $.ajax({
            method: "POST",
            url: "AppHttpRequest.php",
            data: dataobject,
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
                setTimeout(function () {
                    if (trg == 1) {
                        RenderLightBlockModal(1);
                        $('#ApproveResult').empty().html(errorThrown);
                    } else {
                        RenderLightBlock2Modal(1);
                        $('#RejectResult').empty().html(errorThrown);
                    }

                    
                }, 3000);
            },
            success: function (data) {
                if (data.MSGID === 100) {
                    setTimeout(function () {
                        if (trg == 1) {
                            RenderLightBlockModal(1);
                            $('#ApproveResult').empty().html(data.MSGMESSAGE1);
                        } else {
                            RenderLightBlock2Modal(1);
                            $('#RejectResult').empty().html(data.MSGMESSAGE1);
                        }
                        

                    }, 3000);
                } else {
                    setTimeout(function () {
                        if (trg == 1) {
                            RenderLightBlockModal(1);
                             $('#ApproveResult').empty().html(data.MSGMESSAGE1);
                        } else {
                            RenderLightBlock2Modal(1);
                             $('#RejectResult').empty().html(data.MSGMESSAGE1);
                        }
                       
                    }, 3000);
                }
            }
        });
    }

    function RenderLightBlockModal(trg) {
        if (trg == 1) {
            $('#btnCancelApprove').text('ปิดหน้าต่างนี้');
            $('#loading_apr').addClass('hidden');
            $('#ApproveResult').removeClass('hidden');
        } else {
            $('#btnApprove').removeClass('disabled');
            $('#loading_apr').addClass('hidden');
            $('#ApproveResult').addClass('hidden');
            $('#btnCancelApprove').text('ยกเลิก');
        }
    }
    function RenderLightBlock2Modal(trg) {
        if (trg == 1) {
            $('#btnCancelReject').text('ปิดหน้าต่างนี้');
            $('#loading_rjt').addClass('hidden');
            $('#RejectResult').removeClass('hidden');
        } else {
            $('#btnReject').removeClass('disabled');
            $('#loading_rjt').addClass('hidden');
            $('#RejectResult').addClass('hidden');
            $('#btnCancelReject').text('ยกเลิก');
            $('#comment').attr('readonly',false);
        }
    }
    function lightBlock(orderid) {
        $('#orderid').val(orderid);
        RenderLightBlockModal(0);
        window.location.hash = '#lightBlock';
        $('#lightBlock').modal();
    }
    function lightBlock2(orderid) {
        $('#orderid').val(orderid);
        RenderLightBlock2Modal(0);
        window.location.hash = '#lightBlock2';
        $('#lightBlock2').modal();
    }
</script>

<script>
    function tableApprove(search, month, year) {
        $.ajax({
            type: "GET",
            url: "admin_approve_table.php",
            //data: {search: search, month: month, year: year},
            data: {month: month, year: year},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="approveTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>'
                show += '<th class="text-center" style="width: 100px;">วันที่สั่งซื้อ</th>'
                show += '<th>ร้านที่สั้งซื้อ</th>'
                show += '<th class="text-right" style="width: 100px;">ยอดขายรวม</th>'
                show += '<th class="text-center" style="width: 200px;">พนักงานขาย</th>'
                show += '<th style="width: 250px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'

                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>'
                        show += '<td class="text-center">' + i + '</td>'
                        show += '<td class="text-center">' + value.TIMESTAMP + '</td>'
                        show += '<td>' + value.CUSTOMER_NAME + '</td>'
                        show += '<td class="text-right">' + value.TOTAL + '.-</td>'
                        show += '<td class="text-center">' + value.USER_NAMETH + '</td>'
                        show += '<td class="text-right">'
                        show += '<a href="Admin/Approve/Detail/'+ value.ORDERID +'/" data-toggle="tooltip" title="ดูรายละเอียดการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> การสั่งซื้อ</a>'
                        show += '<a href="javascript:void(0)" data-toggle="tooltip" title="อนุมัติการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-success" onclick="lightBlock(\'' + value.ORDERID + '\');"><i class="gi gi-ok_2"></i> อนุมัติ</a>'
                        show += '<a href="javascript:void(0)" data-toggle="tooltip" title="ไม่อนุมัติการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-danger" onclick="lightBlock2(\'' + value.ORDERID + '\')"><i class="gi gi-remove_2"></i> ไม่อนุมัติ</a>'
                        show += '</td>'
                        show += '</tr>'
                        i++;
                    });
                }
                show += '</tbody>'
                show += '</table>'

                $('#tableApprove').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });

                $('#approveTable').dataTable({
                    ordering: true,
                    info: false,
                    searching: true,
                    lengthChange: false,
                    columnDefs: [
                        {targets: 5, orderable: false}
                    ]
                });
            }
        });
    }
</script>


<?php include 'inc/template_end.php'; ?>