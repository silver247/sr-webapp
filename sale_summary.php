<?php $pagename = "Summary" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    สรุปยอดขาย
                </div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:40px;">ร้าน</label>
                    <select id="shop" name="shop" class="select-select2 inlineDiv" style="width:200px;margin-right:30px;" data-placeholder="ร้านค้า" onChange="tableProductSummary(this.value, $('#month').val(), $('#year').val())">
                        <option value="0">แสดงทั้งหมด</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                        <?= $appm->GetCustomerDropdownlist(); ?>
                    </select>
                    <label class="control-label inlineDiv" style="width:40px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();tableProductSummary($('#shop').val(), $('#month').val(), $('#year').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();tableProductSummary($('#shop').val(), $('#month').val(), $('#year').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableSum"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
$(function () {
	UiTables.init();
});
</script>

<script>
$('#sumTable').dataTable({
	ordering: false,
	info: false,
	searching: false
})

$('#monthYear').datepicker({
	format: "MM yyyy",
	weekStart: 0,
	viewMode: "months", 
	minViewMode: "months",
	language: "th-th"
});
</script>

<script>
    $(document).ready(function () {
        tableProductSummary($('#shop').val(), $("#month").val(), $("#year").val());
    });
</script>

<script>
    function tableProductSummary(shop, month, year) {
        $.ajax({
            type: "GET",
            url: "sale_summary_table.php",
            data: {shop: shop, month: month, year: year, uid: '<?=$_SESSION['usrid'];?>'},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="sumTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>'
                show += '<th>สินค้า</th>'
                show += '<th class="text-center" style="width: 100px;">จำนวน</th>'
                show += '<th class="text-right" style="width: 70px;">จำนวนเงิน</th>'
                show += '<th style="width: 150px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'
                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>'
                        show += '<td class="text-center">' + i + '</td>'
                        show += '<td>'+value.PRODUCT_NAME+'</td>'
                        show += '<td class="text-center">'+value.AMOUNT+'</td>'
                        show += '<td class="text-right">'+value.TOTAL+'.-</td>'
                        show += '<td class="text-right">'
                        show += '<a href="Summary/'+value.PRODUCT_ID+'/'+year+'/'+month+'/" data-toggle="tooltip" title="รายละเอียดการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> รายละเอียด</a>'
                        show += '</td>'
                        show += '</tr>'
                        i++;
                    });
                }
                for (i = 1; i < 31; i++) {
                    
                }
                show += '</tbody>'
                show += '</table>'

                $('#tableSum').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });

                $('#sumTable').dataTable({
                    "ordering": false,
                    "info": false,
                    "searching": false,
                    "lengthChange": false
                });
            }
        });
    }
</script>


<?php include 'inc/template_end.php'; ?>