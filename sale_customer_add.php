<?php $pagename = "Customer" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$action = filter_input(INPUT_GET, 'action');
$appm = new AppManager();
$req = REQ_ADD_CUSTOMER;
$customer = new customer();
$customerid = "";
if ($action == ACTION_EDIT) {
    $customerid = filter_input(INPUT_GET, 'id');
    $customerlist = $appm->GetMyCustomer($customerid);

    $customer = $customerlist->MSGDATA1[0];
    $req = REQ_EDIT_CUSTOMER;
    //service::printr($orderdetaillist);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>
<?php
$dataSelect = '<option></option><option value="1">HTML</option><option value="2">CSS</option><option value="3">Javascript</option><option value="4">PHP</option><option value="5">MySQL</option>';
?>
<link rel="stylesheet" href="plugins/jquery.Thailand.js/dist/jquery.Thailand.min.css">
<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <form name="tform1" method="post" id="tform1" onsubmit="CreateNewCustomer(); return false;">
                <input type="hidden" name="req" id="hidDo" value="<?= $req; ?>">
                <input type="hidden" name="customerid" value="<?= $customerid; ?>">
                <div class="block full">
                    <div class="block-title">
                        <?= $_GET["action"] == "add" ? "เพิ่มข้อมูลลูกค้า" : "แก้ไขข้อมูลลูกค้า" ?> 
                        <a href="Customer/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปรายชื่อลูกค้า</div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="form-group">
                                <label>ชื่อร้าน <span style="color:red;">*</span> </label>
                                <input class="form-control" id="txtShop" name="txtShop" value="<?= $customer->CUSTOMER_NAME; ?>" placeholder="ร้านค้า" required="true"/>
                            </div>
                            <div class="form-group">
                                <label>กลุ่มลูกค้า <span style="color:red;">*</span> </label>
                                <select id="ddlGrp" name="ddlGrp" class="select-select2" style="width: 100%;" data-placeholder="กลุ่มลูกค้า" required="true">
                                    <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                    <?= $appm->GetDropdownCustomerGroup($customer->CUSTOMER_GRPID); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="orderTable">
                                    <div class="row" style="padding: 10px 0 20px;">
                                        <div class="col-xs-4">
                                            <label>เลขที่</label>
                                            <input class="form-control" id="txtHomeID" name="txtHomeID" value="<?= $customer->CUSTOMER_HOMEID; ?>" placeholder="เลขที่" />
                                        </div>
                                        <div class="col-xs-4">
                                            <label>ซอย</label>
                                            <input class="form-control" id="txtSoi" name="txtSoi" value="<?= $customer->CUSTOMER_SOI; ?>" placeholder="ซอย" />
                                        </div>
                                        <div class="col-xs-4">
                                            <label>ถนน</label>
                                            <input class="form-control" id="txtRoad" name="txtRoad" value="<?= $customer->CUSTOMER_ROAD; ?>" placeholder="ถนน" />
                                        </div>
                                        <div class="col-xs-6">
                                            <label>จังหวัด <span style="color:red;">*</span> </label>
                                            <select id="ddlProvince" name="ddlProvince" class="select-select2" style="width: 100%;" data-placeholder="จังหวัด" required="true">
                                                <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                <?= $appm->GetProvinceDropdownlist($customer->CUSTOMER_PROVINCE); ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>อำเภอ</label>
                                            <input type="text" id="amphoe" name="amphoe" class="form-control" value="<?= $customer->CUSTOMER_DIST; ?>" placeholder="อำเภอ">
                                        </div>
                                        <div class="col-xs-6">
                                            <label>ตำบล</label>
                                            <input type="text" id="district" name="district" class="form-control" value="<?= $customer->CUSTOMER_SUBDIST; ?>" placeholder="ตำบล">

                                        </div>
                                        <div class="col-xs-6">
                                            <label>รหัสไปรษณีย์</label>
                                            <input type="text" id="zipcode" name="zipcode" class="form-control" value="<?= $customer->CUSTOMER_ZIPCODE; ?>" placeholder="รหัสไปรษณีย์">
                                            <!--<input class="form-control disabledBlock" placeholder="รหัสไปรษณีย์" disabled />-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>เบอร์โทรศัพท์ <span style="color:red;">*</span> </label>
                                <input class="form-control" id="txtContactPhone" name="txtContactPhone" value="<?= $customer->CUSTOMER_CONTACTPHONE; ?>" placeholder="เบอร์โทรศัพท์" required="true"/>
                            </div>
                            <div class="form-group">
                                <label>อีเมล์</label>
                                <input class="form-control" id="txtEmail" name="txtEmail" placeholder="อีเมล์" value="<?= $customer->CUSTOMER_MAIL; ?>" />
                            </div>
                            <div class="form-group">
                                <label>ชื่อผู้ติดต่อ <span style="color:red;">*</span> </label>
                                <input class="form-control" id="txtContactName" name="txtContactName" placeholder="ชื่อผู้ติดต่อ" value="<?= $customer->CUSTOMER_CONTACTNAME; ?>" required="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;">
                        <button type="submit"  id="btnSubmit" data-toggle="tooltip" title="บันทึกลูกค้าใหม่" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึกลูกค้าใหม่" aria-describedby="tooltip441840" ><i class="gi gi-circle_plus"></i> บันทึก</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->
<?php include 'inc/template_scripts.php'; ?>
<script src="plugins/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script src="plugins/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<script type="text/javascript" src="plugins/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>
<script>
                $(document).ready(function () {
                    $.Thailand({
                        database: 'plugins/jquery.Thailand.js/database/db.json', // path หรือ url ไปยัง database
                        $district: $('#district'), // input ของตำบล
                        $amphoe: $('#amphoe'), // input ของอำเภอ
                        //$province: $('#province'), // input ของจังหวัด
                        $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
                    });
                });

                function CreateNewCustomer() {
                    var shopname = $('#txtShop').val();
                    if (shopname.trim().length <= 0) {
                        bootbox.alert({
                            size: 'small',
                            message: "กรุณาใส่ชื่อร้าน"
                        });
                        return;
                    }
                    if (!validateform.validate('tform1')) {
                        bootbox.alert({
                            size: 'small',
                            message: "กรุณากรอกข้อมูลให้ครบถ้วน",
                            title: "การแจ้งเตือน"
                        });
                        return false;
                    }
                    bootbox.confirm({
                        size: 'small',
                        title: "ยืนยันการสร้างลูกค้า",
                        message: "คุณต้องการยืนยันการสร้างลูกค้าใช่หรือไม่",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> ยกเลิก',
                                className: 'btn-danger'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> ยืนยันการสร้างลูกค้า',
                                className: 'btn-success'
                            }
                        },
                        callback: function (result) {
                            var loading = "";
                            if (result) {
                                $.ajax({
                                    method: "POST",
                                    url: "AppHttpRequest.php",
                                    data: $("#tform1").serialize(),
                                    dataType: "json",
                                    beforeSend: function (xhr) {
                                        loading = bootbox.dialog({
                                            size: 'small',
                                            message: '<p class="text-center">เรากำลังบันทึกข้อมูลลูกค้าของท่าน กรุณารอสักครู่...</p>',
                                            closeButton: false
                                        });
                                    },
                                    error: function (transport, status, errorThrown) {
                                        console.log(transport.responseText);
                                        setTimeout(function () {
                                            loading.modal('hide');
                                            bootbox.alert({
                                                size: 'small',
                                                message: transport.responseText,
                                                title: "การแจ้งเตือน"
                                            });
                                        }, 3000);
                                    },
                                    success: function (data) {
                                        loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                        if (data.MSGID === 100) {
                                            setTimeout(function () {
                                                loading.modal('hide');
                                                bootbox.alert({
                                                    size: 'small',
                                                    message: "บันทึกข้อมูลการสร้างลูกค้าสำเร็จ",
                                                    title: "การแจ้งเตือน",
                                                    callback: function () {
                                                        window.location = "Customer/";
                                                    }
                                                });
                                            }, 3000);
                                        } else {
                                            setTimeout(function () {
                                                loading.modal('hide');
                                                bootbox.alert({
                                                    size: 'small',
                                                    message: "บันทึกข้อมูลการสร้างลูกค้าไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                                    title: "การแจ้งเตือน"
                                                });
                                            }, 3000);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
</script>
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_end.php'; ?>