<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
if (!isset($_GET['shopid'])) {
    //echo 'test';
    echo '<script>window.history.go(-1);</script>';
}
$customerid = filter_input(INPUT_GET, 'shopid');
$date = filter_input(INPUT_GET, 'date');
//echo $customerid . " " . $date;
$res = $appm->GetMyPlanHistoryByShop($customerid);
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    ประวัติการนัดหมาย 
                    <a href="Planner/<?= $_GET['date'] ?>/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปตารางนัดหมาย</div></a>
                </div>

                <div class="block-option">
                    <strong class="inlineDiv" style="width:200px;">ร้าน <?=$res->MSGMESSAGE1;?></strong>
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableOrder">
                        <table id="historyTable" class="table table-vcenter table-condensed table-striped table-borderless">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 70px;">ลำดับที่</th>
                                    <th class="text-center" style="width: 130px;">วันที่นัดหมาย</th>
                                    <th class="text-center" style="width: 100px;">เวลา</th>
                                    <th>หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($res->MSGDATA1 as $row) {
                                    $plan = new plan();
                                    $plan = $row;
                                    ?>
                                <tr>
                                <td class="text-center"><?= $i ?></td>
                                <td class="text-center"><?= thaidate_withouttime_short($plan->PLANDATE);?></td>
                                <td class="text-center"><?=$plan->PLANTIME;?></td>
                                <td><?=$plan->Q3_R2;?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
        UiTables.init();
    });</script>
<script>
    $('#historyTable').dataTable({
        ordering: false,
        info: false,
        searching: false,
        lengthChange: false
    })
    
</script>

<?php include 'inc/template_end.php'; ?>