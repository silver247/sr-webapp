<?php

require_once './bundle.php';
require_once ('./AppBaseManager.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BusinessLogic
 *
 * @author puwakitk
 */
class BusinessLogic {

    //put your code here
    private $appbm;
    private $service;

    function __construct() {
        $this->appbm = new AppBaseManager();
        $this->service = new service();
    }

    /*
     * 
     */

    function CalculateSalesTarget($user) {
        $salestarget = 0;
        $curmonth = date('m');
        $response = new response();
        try {
            if ($curmonth == "01") {
                $queryobjects = $this->appbm->GetMyInitialSalesTarget($user);
            } else {
                $queryobjects = $this->appbm->GetMyTargetSalesCurrentMonth($user);
            }
            foreach ($queryobjects->MSGDATA1 as $data) {
                $user = new user();
                $user = $data;
                $response->MSGMESSAGE1 = $user->TSALES_MONTH_VALUE; //CurrentMonth Sales Target It's depend on previous month
            }
            $yearObject = $queryobjects = $this->appbm->GetMyYearTarget($user);
            foreach ($yearObject->MSGDATA1 as $data) {
                $user = new user();
                $user = $data;
                $response->MSGMESSAGE2 = $user->TASLES_YEAR_VALUE; //All Year Sales Target It's depend on previous month
            }
            $response->MSGMESSAGE3 = round($response->MSGMESSAGE1 / 24, 0, PHP_ROUND_HALF_UP); //Day Target
            $response->MSGID = SERV_COMPLETE;
        } catch (Exception $e) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    function CalculateSalesEvaluation($month, $user) {
        $salestarget = 0;
        $curmonth = date('m');
        $response = new response();
        try {
            if ($curmonth == "01") {
                $queryobjects = $this->appbm->GetMyInitialSalesTarget($user);
            } else {
                $queryobjects = $this->appbm->GetMyTargetSalesCurrentMonth($user);
            }
            foreach ($queryobjects->MSGDATA1 as $data) {
                $user = new user();
                $user = $data;
                $salestarget = $user->TSALES_MONTH_VALUE;
            }
            $cursales = 0;
            $cursalescond = " AND YEAR(order_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(order_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
            $cursalesobject = $this->appbm->GetMySales($cursalescond);
            foreach ($cursalesobject->MSGDATA1 as $data) {
                $user = new user();
                $user = $data;
                $cursales = $user->TOTALSALES;
            }
            $percent_cursales = ($cursales * 100) / $salestarget;
            $evalobject = $this->appbm->GetMyEvaluationFromSales($percent_cursales);
            $evalid = 0;
            foreach ($evalobject->MSGDATA1 as $data) {
                $eval = new evaluation();
                $eval = $data;
                $evalid = $eval->EVALID;
            }
            $userobj = new user();
            $userobj->USERID = "";
            $userobj->EVAL_YEAR = date('y');
            $userobj->EVAL_MONTH = $curmonth;
            $userobj->SALES_MONTH = $cursales;
            $userobj->EVALID = $evalid;
            $insertobj = $this->appbm->SetSaleEvaluation($userobj);
            if ($insertobj->MSGID === SERV_COMPLETE) {
                $response->MSGID = SERV_COMPLETE;
            } else {
                $response->MSGID = SERV_ERROR;
            }
        } catch (Exception $ex) {
            $response->MSGID = SERV_ERROR;
            $response->MSGMESSAGE1 = $ex;
        }
        return $response;
    }

    public static function PlanStatusDescription($status) {
        $str = "";
        if ($status == 'NEW') {
            $str = 'ยังไม่บันทึกผลการเข้าพบ';
        } else if ($status == 'PED') {
            $str = 'บันทึกผลแล้ว กำลังรอใบสั่งซื้อ';
        } else if ($status == 'COM') {
            $str = 'บันทึกผลแล้ว การชำระเงินเสร็จสมบูรณ์';
        } else if ($status == 'CAN') {
            $str = 'การนัดถูกยกเลิก';
        } else {
            $str = '-';
        }
        return $str;
    }
    
    public static function OrderStatusDescription($status) {
        $str = "";
        if ($status == ORDER_STATUS_OPEN) {
            $str = 'เปิดเอกสารขาย รอลูกค้าส่งใบ PO';
        } else if ($status == ORDER_STATUS_WPAY) {
            $str = 'ได้รับใบ PO แล้ว รอลูกค้าชำระเงิน';
        } else if ($status == ORDER_STATUS_PCOM) {
            $str = 'ได้รับการชำระเงินเรียบร้อยแล้ว';
        } else if ($status == ORDER_STATUS_SHIP) {
            $str = 'กำลังทำการขนส่ง';
        } else if ($status == ORDER_STATUS_DONE) {
            $str = 'เอกสารขายเสร็จสมบูรณ์';
        } else if ($status == ORDER_STATUS_CANC) {
            $str = 'เอกสารการขายถูกยกเลิก';
        } else {
            $str = '-';
        }
        return $str;
    }

}
