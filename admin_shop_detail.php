<?php $pagename = "Shop" ?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
include_once './bundle.php';
$appm = new AppManager();
$customerid = filter_input(INPUT_GET, 'id');
$customer = $appm->GetMyCustomer($customerid)->MSGDATA1[0];
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    ข้อมูลการสั่งซื้อ
                    <a href="Admin/Shop/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าข้อมูลร้านค้า</div></a>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:25px;">
                        <table>
                            <tr>
                                <th colspan="2">ร้าน <?= $customer->CUSTOMER_NAME; ?></th>
                                <th width="130px">กลุ่มลูกค้า </th>
                                <td><?= $customer->CUSTOMER_GRPDESC; ?> (Credit Term <?= $customer->CUSTOMER_GRPCREDIT; ?> วัน)</td>
                            </tr>
                            <tr>
                                <th width="120px">ที่อยู่</th>
                                <td width="250px" colspan="3"><?= $customer->CUSTOMER_ADDRESS; ?> (<?= $customer->REGION_DESC; ?>)</td>
                            </tr>
                            <tr>
                                <th>เบอร์โทรศัพท์</th>
                                <td><?= $customer->CUSTOMER_CONTACTPHONE; ?> </td>
                                <th>&nbsp;อีเมล์</th>
                                <td><?= $customer->CUSTOMER_MAIL; ?> </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:40px;">เดือน</label>
                    <div class="inlineDiv">
                        <div class="input-group" style="width:272px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="preMonth" onClick="preMonth();tableSum($('#month').val(), $('#year').val());"><i class="fa fa-chevron-left"></i></button>
                            </span>
                            <input type="text" id="monthYear" name="monthYear" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-effect-ripple btn-primary" id="nextMonth" onClick="nextMonth();tableSum($('#month').val(), $('#year').val());"><i class="fa fa-chevron-right"></i></button>
                            </span>
                        </div>
                    </div>
                    <input id="month" type="hidden"><input id="year" type="hidden">
                </div>
                <!--<div class="topicLine"></div>-->

                <div class="block full">
                    <div class="table-responsive" id="tableSum" style="margin-top:-25px"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
                                    $(function () {
                                        UiTables.init();
                                    });
</script>

<script>
    $('#sumTable').dataTable({
        ordering: false,
        info: false,
        searching: false
    })

    $('#monthYear').datepicker({
        format: "MM yyyy",
        weekStart: 0,
        viewMode: "months",
        minViewMode: "months",
        language: "th-th"
    });
</script>

<script>
    $(document).ready(function () {
        tableSum($("#month").val(), $("#year").val());
    });
</script>

<script>
    function tableSum(month, year) {
        $.ajax({
            type: "GET",
            url: "sale_summary_table.php",
            data: {shop: '<?= $customerid; ?>', month: month, year: year},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="sumTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>'
                show += '<th>สินค้า</th>'
                show += '<th class="text-center" style="width: 100px;">จำนวน</th>'
                show += '<th class="text-right" style="width: 70px;">จำนวนเงิน</th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'
                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>'
                        show += '<td class="text-center">' + i + '</td>'
                        show += '<td>' + value.PRODUCT_NAME + '</td>'
                        show += '<td class="text-center">' + value.AMOUNT + '</td>'
                        show += '<td class="text-right">' + value.TOTAL + '.-</td>'
                        show += '</tr>'
                        i++;
                    });
                }
                show += '</tbody>'
                show += '</table>'

                $('#tableSum').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });

                $('#sumTable').dataTable({
                    "ordering": false,
                    "info": false,
                    "searching": false,
                    "lengthChange": false
                });
            }
        });
    }
</script>


<?php include 'inc/template_end.php'; ?>