<?php $pagename = "Order" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$action = filter_input(INPUT_GET, 'action');
$appm = new AppManager();
$order_date = date('Y-m-d');
$req = REQ_ADD_ORDER;
$orderid = "";
$discount = 0;
if ($action == ACTION_EDIT) {
    $orderid = filter_input(INPUT_GET, 'id');
    $headerlist = $appm->GetMyOrderHeader($orderid);
    $orderheader = new order();
    $orderheader = $headerlist->MSGDATA1[0];
    $discount = $orderheader->CUSTOMER_DISCOUNT;
    $orderdetaillist = array();
    $orderdetaillist = $appm->GetMyOrderDetail($orderid)->MSGDATA1;
    $order_date = $orderheader->TIMESTAMP;
    $req = REQ_EDIT_ORDER;
    //service::printr($orderdetaillist);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <form name="tform1" method="post" id="tform1" onsubmit="CreateNewOrder(); return false;">
                    <div class="block-title">
                        <?= $_GET["action"] == "add" ? "เพิ่มรายการสั่งซื้อ" : "แก้ไขรายการสั่งซื้อ" ?> 
                        <a href="Order/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้ารายการสั่งซื้อ</div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">

                            <input type="hidden" name="req" value="<?= $req; ?>">
                            <input type="hidden" name="orderid" value="<?= $orderid; ?>">
                            <div class="form-group">
                                <label>ชื่อร้าน <span style="color:red;">*</span> </label>
                                <select id="shop" name="shop" class="select-select2" style="width: 100%;" data-placeholder="ร้านค้า" onchange="renderdiscount(this)" required="true">
                                    <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                    <?php
                                    if ($action == ACTION_EDIT) {
                                        echo $appm->GetCustomerDropdownlistWithValue($orderheader->CUSTOMER_ID);
                                    } else {
                                        echo $appm->GetCustomerDropdownlist();
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>วันที่สั่งซื้อ <span style="color:red;">*</span> </label>
                                <input type="text" id="orderdate" name="orderdate" class="form-control input-datepicker" placeholder="เลือกวันที่สั่งซื้อ" required="true">
                            </div>
                            <div class="form-group">
                                <label>การสั่งซื้อ</label>
                                <div class="orderTable">
                                    <table class="table table-vcenter table-condensed table-striped table-borderless">
                                        <thead>
                                            <tr>
                                                <th>รุ่นสินค้า</th>
                                                <th width="100px" class="text-right">ชนิดราคา</th>
                                                <th width="100px" class="text-right">ราคาขาย</th>
                                                <th width="100px" class="text-right">% ส่วนลด</th>
                                                <th width="100px" class="text-right">ราคาสุทธิ</th>
                                                <th width="80px" class="text-center">จำนวน <span style="color:red;">*</span> </th>
                                                <th width="100px" class="text-right">ยอดขายรวม</th>
                                                <th width="30px"></th>
                                            </tr>										
                                        </thead>
                                        <tbody id="orderProduct">
                                            <?php
                                            $subtotal = 0;
                                            if ($action == ACTION_EDIT) {
                                                $rowcnt = 0;
                                                $str = '';
                                                foreach ($orderdetaillist as $orderdetail) {
                                                    $str = '';
                                                    $rowcnt++;
                                                    $total = 0;
                                                    $total = $orderdetail->PRICE * $orderdetail->AMOUNT;
                                                    //service::printr($orderdetail);
                                                    //$str .= '<input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="' . $rowcnt . '">';
                                                    $str .= '<tr id="row' . $rowcnt . '" class="tableRow">';
                                                    $str .= '<td>';
                                                    $str .= '<select id="ddlproductOrder' . $rowcnt . '" name="ddlproductOrder[]" data-row="' . $rowcnt . '" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า" data-live-search="true">';
                                                    $str .= $appm->GetProductDropdownlistWithValue($orderdetail->PRODUCT_ID);
                                                    $str .= '</select>';
                                                    $str .= '</td>';
                                                    $str .= '<td class="text-right">'
                                                            . '<select name="pricetype[]" id="pricetype' . $rowcnt . '" class="select-select2" style="width: 100%;" data-row="' . $rowcnt . '"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true">'
                                                            . $appm->GetDropdownPriceType($orderdetail->PRICETYPE)
                                                            . '</select>'
                                                            . '</td>';
                                                    $str .= '<td class="text-right"><input id="sales_price' . $rowcnt . '" class="form-control disabledText" value="' . $orderdetail->PRODUCT_RETAIL_PRICE . '" disabled/></td>';
                                                    $str .= '<td class="text-right"><input id="sales_discount' . $rowcnt . '" class="form-control disabledText" value="' . number_format($orderdetail->PRODUCT_RETAIL_DISCOUNT * 100, 2) . '" disabled/></td>';
                                                    $str .= '<td class="text-right"><input id="txtPrice' . $rowcnt . '" name="txtPrice[]" class="form-control disabledText" value="' . $orderdetail->PRICE . '" readonly="true"/></td>';
                                                    $str .= '<td class="text-center"><input id="txtQty' . $rowcnt . '" name="txtQty[]" type="text" class="form-control" value="' . $orderdetail->AMOUNT . '" onkeyup="calculatePrice(this, ' . $rowcnt . ')" required="true"/></td>';
                                                    $str .= '<td class="text-right"><input id="txtTotal' . $rowcnt . '" name="txtTotal[]" class="form-control disabledText" value="' . $total . '"  readonly="true"/></td>';
                                                    $str .= '<td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>';
                                                    $str .= '<input type="hidden" id="promotion' . $rowcnt . '" name="promotionid[]" value="' . $orderdetail->REF_PROMOTION . '">';
                                                    $str .= '</tr>';

                                                    $subtotal += $total;
                                                    echo $str;
                                                }
                                                echo '<input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="' . $rowcnt . '">';
                                            } else {
                                                $subtotal = '';
                                                ?>
                                            <input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="1">
                                            <tr id="row1" class="tableRow">
                                                <td>
                                                    <select id="ddlproductOrder1" name="ddlproductOrder[]" data-row="1" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า" data-live-search="true">
                                                        <option ></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        <?= $appm->GetProductDropdownlist(); ?>
                                                    </select>
                                                                                    <!--<input type="text" id="example-typeahead" name="example-typeahead" class="form-control input-typeahead" autocomplete="off" placeholder="ค้นหาสินค้า">-->
                                                </td>
                                                <td class="text-right"><select name="pricetype[]" id="pricetype1" class="select-select2" style="width: 100%;" data-row="1"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true"><?= $appm->GetDropdownPriceType(0); ?></select></td>
                                                <td class="text-right"><input id="sales_price1" class="form-control disabledText" value="" disabled/></td>
                                                <td class="text-right"><input id="sales_discount1" class="form-control disabledText" value="" disabled/></td>
                                                <td class="text-right"><input id="txtPrice1" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td>
                                                <td class="text-center"><input id="txtQty1" name="txtQty[]" type="text" class="form-control" onkeyup="calculatePrice(this, 1)" readonly/></td>
                                                <td class="text-right"><input id="txtTotal1" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td>
                                            <input type="hidden" id="promotion1" name="promotionid[]">
                                            <td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"><button type="button" class="btn btn-xs btn-primary" onClick="addField()"><i class="gi gi-circle_plus"></i> เพิ่มสินค้า</button></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div align="right">ส่วนลดพิเศษ <span id="custgrp"></span> <input class="form-control disabledText inlineDiv" id="custgrp_discount" style="width:130px;" value="" disabled/> บาท <input type="hidden" id="special_discount" value="<?= $discount; ?>"></div>
                                <div align="right">ยอดขายรวมทั้งหมด <input class="form-control disabledText inlineDiv" id="txtOrderSubtotal" name="txtOrderSubtotal" style="width:130px;" value="<?= $subtotal; ?>" disabled/> บาท <input type="hidden" name="ordertotal" id="ordertotal" value="<?= $subtotal; ?>"></div>
                            </div>

                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;">
                        <button type="submit"  id="btnSubmit" data-toggle="tooltip" title="บันทึกรายการสั่งซื้อ" class="btn btn-effect-ripple btn-xs btn-success disabled" data-original-title="บันทึกรายการสั่งซื้อ" aria-describedby="tooltip441840" disabled><i class="gi gi-circle_plus"></i> บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
                                                        UiTables.init();
                                                    });</script>

<script>
    $('#orderdate').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        language: "th-th"
    });
    moment.locale();
    $('#orderdate').datepicker('update', new Date(moment('<?= $order_date; ?>').add(1, 'days')));
</script>

<script type="text/javascript" >
    function renderdiscount(data) {
        var grpdesc = $(data).find("option:selected").data('grp');
        var discount = $(data).find("option:selected").data('discount');
        grpdesc = 'ของ' + grpdesc;
        $('#custgrp').empty().html(grpdesc);
        $('#special_discount').val(discount);
        calculateSubtotal();
    }

    function renderprice(data) {
        //var selectedText = $(data).find("option:selected").text();
        var productid = data.value;
        var rowid = $(data).data('row');
        //var pricetype = 0;
        var selective = 'r';
        if ($('#pricetype' + rowid + ' option:selected').val() == '1') {
            //pricetype = 1;
            selective = 'w';
        }
        drawprice(data, rowid, selective);
    }

    var drawprice = function (data, rowid, selective) {

        var sales_price = $(data).find("option:selected").data(selective + 'price');
        var sales_discount = $(data).find("option:selected").data(selective + 'discount');
        var sales_discount_price = $(data).find("option:selected").data(selective + 'dprice');
        $('#sales_price' + rowid).val(sales_price);
        $('#sales_discount' + rowid).val(parseFloat(sales_discount * 100).toFixed(2));
        $('#txtPrice' + rowid).val(parseFloat(sales_discount_price).toFixed(2));
        $('#txtQty' + rowid).val('');
        $('#custgrp_discount').val('');
        $("#ordertotal").val('');
        $("#txtOrderSubtotal").val('');
        $('#txtTotal' + rowid).val('');
        $('#promotion' + rowid).val($(data).find("option:selected").data('promotion'));
        $('#txtQty' + rowid).attr('readOnly', false);
    };

    function calculatePrice(data, rowid) {
        var qty = data.value;
        var sellprice = $('#txtPrice' + rowid).val();
        $('#txtTotal' + rowid).val((qty * sellprice).toFixed(2));
        calculateSubtotal();
    }

    function calculateSubtotal() {
        var totalrow = $("#rowOrdercnt").val();
        var subt = 0;
        for (var i = 1; i <= totalrow; i++) {
            if ($('#txtTotal' + i).length) {
                var total = parseInt($('#txtTotal' + i).val());
                subt = subt + total;
            }
        }
        var customer_grp_discount = $('#special_discount').val() * subt;
        subt = subt - customer_grp_discount;
        $('#custgrp_discount').val(customer_grp_discount.toFixed(2));
        $("#txtOrderSubtotal").val(subt.toFixed(2));
        $("#ordertotal").val(subt.toFixed(2));
        if (subt != '0' && !isNaN(subt)) {
            $('#btnSubmit').removeClass('disabled');
            $('#btnSubmit').attr('disabled', false);
        } else {
            $('#btnSubmit').addClass('disabled');
            $('#btnSubmit').attr('disabled', true);
        }

    }

    function changeprice(data) {
        var selective = 'r';
        var rowid = $(data).data('row');
        if ($(data).find("option:selected").val() == '1') {
            //pricetype = 1;
            selective = 'w';
        }
        var productdata = $('#ddlproductOrder' + rowid);
        drawprice(productdata, rowid, selective);
    }

    function addField() {
        var cnt = $("#rowOrdercnt").val();
        cnt++;
        $("#rowOrdercnt").val(cnt);
        var opt = "<?= $appm->GetProductDropdownlist(); ?>";
        var priceopt = "<?= $appm->GetDropdownPriceType(0); ?>";
        $('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlproductOrder' + cnt + '" name="ddlproductOrder[]" data-row="' + cnt + '"  onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td>\n\
   <td class="text-right"><select name="pricetype[]" id="pricetype' + cnt + '" class="select-select2" style="width: 100%;" data-row="' + cnt + '"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true">' + priceopt + '</select></td> \n\
<td class="text-right"><input id="sales_price' + cnt + '" class="form-control disabledText" value="" disabled/></td>\n\
<td class="text-right"><input id="sales_discount' + cnt + '" class="form-control disabledText" value="" disabled/></td>\n\
</td><td align="right"><input id="txtPrice' + cnt + '" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td>\n\
<td><input type="text" id="txtQty' + cnt + '" name="txtQty[]" class="form-control" onkeyup="calculatePrice(this,' + cnt + ')" readonly required="true"/></td>\n\
<td align="right"><input id="txtTotal' + cnt + '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td>\n\
<input type="hidden" id="promotion' + cnt + '" name="promotionid[]"><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#orderProduct'));

        $.getScript('js/app.js');
    }
    $(document).on('click', '.remove-btn', function (events) {
        $(this).parents('tr').remove();
        if ($('#orderProduct').children('tr').length <= 0) {
            $("#rowOrdercnt").val(0);
        }
        calculateSubtotal();
    });

    function validate() {
        var valid = true;
        $('input[name^="txtQty"]').each(function () {
            if ($(this).val().trim().length <= 0) //or a more complex check here
                return valid = false;
        });
        if (!valid) {
            return valid;
        }
        if ($('#shop option:selected').val().trim().length <= 0) {
            return false;
        }
        if ($('#txtOrderSubtotal').val().trim().length <= 0) {
            return false;
        }
        if ($('#orderdate').val().trim().length <= 0) {
            return false;
        }
        return true;
    }
    function CreateNewOrder() {
        var isvalidate = validate();
        if (!isvalidate) {
            bootbox.alert({
                size: 'small',
                message: "กรุณากรอกข้อมูลให้ครบ"
            });
            return;
        }
        if (!validateform.validate('tform1')) {
            bootbox.alert({
                size: 'small',
                message: "กรุณากรอกข้อมูลให้ครบถ้วน",
                title: "การแจ้งเตือน"
            });
            return false;
        }
        /*var ddlshop = $('#shop option:selected').val();
         if (ddlshop.trim().length <= 0) {
         bootbox.alert({
         size: 'small',
         message: "กรุณาเลือกร้าน"
         });
         return;
         }
         
         var txtOrderSubtotal = $('#txtOrderSubtotal').val();
         if (txtOrderSubtotal.trim().length <= 0 || isNaN(parseFloat(txtOrderSubtotal))) {
         bootbox.alert({
         size: 'small',
         message: "กรุณากรอกข้อมูลให้ครบถ้วน"
         });
         return;
         }
         
         
         
         if ($('#orderdate').val().trim().length <= 0) {
         bootbox.alert({
         size: 'small',
         message: "กรุณากรอกข้อมูลให้ครบถ้วน"
         });
         return;
         }*/


        bootbox.confirm({
            size: 'small',
            title: "ยืนยันการสร้างเอกสารขาย",
            message: "คุณต้องการยืนยันการสร้างเอกสารขายใช่หรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ยกเลิก',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ยืนยันการสร้างเอกสารขาย',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                var loading = "";
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "AppHttpRequest.php",
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            loading = bootbox.dialog({
                                size: 'small',
                                message: '<p class="text-center">เรากำลังบันทึกข้อมูลเอกสารขายของท่าน กรุณารอสักครู่...</p>',
                                closeButton: false
                            });
                        },
                        error: function (transport, status, errorThrown) {
                            console.log(transport.responseText);
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: transport.responseText,
                                    title: "การแจ้งเตือน"
                                });
                            }, 3000);
                        },
                        success: function (data) {
                            console.log(data);
                            //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                            if (data.MSGID === 100) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลเอกสารขายสำเร็จ",
                                        title: "การแจ้งเตือน",
                                        callback: function () {
                                            window.location = "Order/";
                                        }
                                    });
                                }, 3000);
                            } else {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลยอดขายขายไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            }
                        }
                    });
                }

            }
        });
    }
</script>

<?php include 'inc/template_end.php'; ?>