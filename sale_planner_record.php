<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
if (!isset($_GET['id'])) {
    //echo 'test';
    echo '<script>window.history.go(-1);</script>';
}
$planid = filter_input(INPUT_GET, 'id');
$date = filter_input(INPUT_GET, 'date');
//echo $customerid . " " . $date;
$planheader_response = $appm->GetMyPlanHeaderByPlanID($planid);
//print_r($planheader_response);
$planheader = $planheader_response->MSGDATA1[0];
//echo $planheader->CUSTOMER_ID;
$customer = new customer();
$customer = $appm->GetCustomerDiscount($planheader->CUSTOMER_ID)->MSGDATA1[0];
$orderdetail = $appm->GetMyPlanOrderDetail($planid);
//service::printr($orderdetail);
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>
<link rel="stylesheet" href="plugins/bootstrapselect/bootstrap-select.min.css">
<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    บันทึกผลการเข้าพบลูกค้า 
                    <a href="Planner/<?= $_GET['date'] ?>/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปตารางนัดหมาย</div></a>
                </div>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <form name="tform1" id="tform1" onsubmit="return false;">
                            <input type="hidden" name="planid" id="planid" value="<?= $planid; ?>">
                            <div class="form-group">
                                <table>
                                    <tr>
                                        <td width="40px"><strong>ร้าน</strong></td>
                                        <td><?= $planheader->CUSTOMER_NAME; ?></td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td width="80px"><strong>วันที่เข้าพบ</strong></td>
                                        <td width="180px"><?= thaidate_withouttime_short($planheader->PLANDATE) ?></td>
                                        <td width="45px"><strong>เวลา</strong></td>
                                        <td><?= $planheader->PLANTIME; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-group" id="contentpanel">
                                <label><strong>การสั่งซื้อ</strong></label>
                                <div class="orderTable">
                                    <table class="table table-vcenter table-condensed table-striped table-borderless">
                                        <thead>
                                            <tr>
                                                <th>รุ่นสินค้า</th>
                                                <th width="100px" class="text-right">ชนิดราคา</th>
                                                <th width="100px" class="text-right">ราคาขาย</th>
                                                <th width="100px" class="text-right">% ส่วนลด</th>
                                                <th width="100px" class="text-right">ราคาสุทธิ</th>
                                                <th width="100px">จำนวน</th>
                                                <th width="130px" align="right">ยอดขายรวม</th>
                                                <th width="30px"></th>
                                            </tr>										
                                        </thead>
                                        <tbody id="orderProduct">

                                            <?php
                                            if ($orderdetail->MSGMESSAGE2 > 0 && $orderdetail->MSGID == SERV_COMPLETE) {
                                                $cnt = 1;
                                                foreach ($orderdetail->MSGDATA1 as $row) {
                                                    $order = new order();
                                                    $order = $row;
                                                    //service::printr($order);
                                                    $ddl = $appm->GetProductDropdownlistWithValue($order->PRODUCT_ID);
                                                    $str = '';
                                                    $str .= '<tr id="row1" class="tableRow">';
                                                    $str .= '<td>';
                                                    $str .= '<select id="ddlproductOrder' . $cnt . '" name="ddlproductOrder[]" data-row="' . $cnt . '" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า" data-live-search="true">';
                                                    $str .= $ddl;
                                                    $str .= '</select>';
                                                    $str .= '</td>';
                                                    $str .= '<td class="text-right">'
                                                            . '<select name="pricetype[]" id="pricetype' . $cnt . '" class="select-select2" style="width: 100%;" data-row="1"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true">'
                                                            . $appm->GetDropdownPriceType('')
                                                            . '</select>'
                                                            . '</td>';
                                                    $str .= '<td class="text-right"><input id="sales_price' . $cnt . '" class="form-control disabledText" value="' . $order->PRODUCT_RETAIL_PRICE . '" disabled/></td>';
                                                    $str .= '<td class="text-right"><input id="sales_discount' . $cnt . '" class="form-control disabledText" value="' . number_format($order->PRODUCT_RETAIL_DISCOUNT * 100, 2) . '" disabled/></td>';
                                                    $str .= '<td class="text-right"><input id="txtPrice' . $cnt . '" name="txtPrice[]" class="form-control disabledText" value="' . number_format($order->PRICE, 2) . '" readonly="true"/></td>';
                                                    $str .= '<td class="text-center"><input id="txtQty' . $cnt . '" name="txtQty[]" type="text" class="form-control" value="' . $order->AMOUNT . '" onkeyup="calculatePrice(this, ' . $cnt . ')"/></td>';
                                                    $str .= '<td class="text-right"><input id="txtTotal' . $cnt . '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td>';
                                                    $str .= '<input type="hidden" id="promotion' . $cnt . '" name="promotionid[]" value = "' . $order->REF_PROMOTION . '">';
                                                    $str .= '<td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>';
                                                    $str .= '</tr>';

                                                    echo $str;
                                                    $cnt++;
                                                }
                                                $rowcnt = $cnt - 1;
                                                echo '<input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="' . $rowcnt . '">';
                                            } else {
                                                ?>
                                                <tr id="row1" class="tableRow">
                                                    <td>
                                                        <select id="ddlproductOrder1" name="ddlproductOrder[]" data-row="1" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า" data-live-search="true">
                                                            <option ></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            <?= $appm->GetProductDropdownlist(); ?>
                                                        </select>
                                                                                        <!--<input type="text" id="example-typeahead" name="example-typeahead" class="form-control input-typeahead" autocomplete="off" placeholder="ค้นหาสินค้า">-->
                                                    </td>
                                                    <td class="text-right"><select name="pricetype[]" id="pricetype1" class="select-select2" style="width: 100%;" data-row="1"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true"><?= $appm->GetDropdownPriceType(0); ?></select></td>
                                                    <td class="text-right"><input id="sales_price1" class="form-control disabledText" value="" disabled/></td>
                                                    <td class="text-right"><input id="sales_discount1" class="form-control disabledText" value="" disabled/></td>
                                                    <td class="text-right"><input id="txtPrice1" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td>
                                                    <td class="text-center"><input id="txtQty1" name="txtQty[]" type="text" class="form-control" onkeyup="calculatePrice(this, 1)"/></td>
                                                    <td class="text-right"><input id="txtTotal1" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td>
                                            <input type="hidden" id="promotion1" name="promotionid[]">
                                            <td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>
                                            </tr>
                                            <input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="1">
                                        <?php }
                                        ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"><button type="button" class="btn btn-xs btn-primary" onClick="addField()"><i class="gi gi-circle_plus"></i> เพิ่มสินค้า</button></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div align="right">ส่วนลดพิเศษ<span id="custgrp">ของ <?= $customer->CUSTOMER_GRPDESC; ?> </span> 
                                    <input class="form-control disabledText inlineDiv" id="custgrp_discount" style="width:130px;" value="" disabled/> บาท 
                                    <input type="hidden" id="special_discount" value="<?= $customer->CUSTOMER_GRPDISCOUNT; ?>">
                                    <input type="hidden" name="sd_cal" id="sd_cal" value=""></div>
                                <div align="right">ยอดขายรวมทั้งหมด <input class="form-control disabledText inlineDiv" id="txtOrderSubtotal" name="txtOrderSubtotal" style="width:130px;" disabled/> บาท <input type="hidden" name="txtSubtotal" id="txtSubtotal" value = ""></div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="btn-right" style="position: relative;"><button type="submit" id="btnsubmit" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึกผลการเข้าพบลูกค้า" onclick="lightBlock();" aria-describedby="tooltip441840"><i class="gi gi-floppy_disk"></i> บันทึก</button>
                </div>
            </div>
            <!-- END Simple Stats Widgets -->
        </div>
        <!-- END First Row -->
    </div>
    <!-- END Page Content -->

    <!-- Add Entry Form-->
    <div id="lightBlock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    </div>
    <!-- END Add Entry Form -->

    <script>
        var content;
        $(document).ready(function () {
            $('#lightBlock').on('hidden.bs.modal', function () {
                //window.location.reload();
            });

        });

        /*function ReRenderPage(pageobject) {
         $('#contentpanel').empty().append(pageobject);
         }*/

        function lightBlock() {
            //alert(sortby);
            //
            //window.location.hash = '#lightBlock';


            $('#lightBlock').modal({
                backdrop: 'static',
                keyboard: false
            });
            //content = $('#contentpanel').html();
            //$('#cnf').html(content);
            $.ajax({
                type: "POST",
                url: "sale_planner_block.php",
                //data: {date: '<?= REQ_DEACTIVE_PLAN; ?>', planid: plan},
                data: $('#tform1').serialize(),
                success: function (data) {
                    document.getElementById("lightBlock").innerHTML = data;
                    //$('#lightBlock').empty().append(data);
                }
            });
        }

        function InitialPrice() {
            var totalrow = $("#rowOrdercnt").val();
            for (var i = 1; i <= totalrow; i++) {
                var qty = $("#txtQty" + i).val();
                var sellprice = $('#txtPrice' + i).val();
                $('#txtTotal' + i).val(parseFloat(qty * sellprice).toFixed(2));
            }
            calculateSubtotal();
        }

        function renderprice(data) {
            //var selectedText = $(data).find("option:selected").text();
            /*var productid = data.value;
             var rowid = $(data).data('row');
             var retail_price = $(data).find("option:selected").data('rprice');
             var retail_discount = $(data).find("option:selected").data('rdiscount');
             var retail_discount_price = $(data).find("option:selected").data('rdprice');
             $('#retail_price' + rowid).val(retail_price);
             $('#retail_discount' + rowid).val(retail_discount * 100);
             $('#txtPrice' + rowid).val(retail_discount_price);
             $('#txtQty' + rowid).val('');
             $('#promotion' + rowid).val($(data).find("option:selected").data('promotion'));
             $('#txtQty' + rowid).attr('readOnly', false);*/
            var productid = data.value;
            var rowid = $(data).data('row');
            var selective = 'r';
            if ($('#pricetype' + rowid + ' option:selected').val() == '1') {
                selective = 'w';
            }
            drawprice(data, rowid, selective);
        }

        var drawprice = function (data, rowid, selective) {

            var sales_price = $(data).find("option:selected").data(selective + 'price');
            var sales_discount = $(data).find("option:selected").data(selective + 'discount').toFixed(2);
            var sales_discount_price = $(data).find("option:selected").data(selective + 'dprice').toFixed(2);
            $('#sales_price' + rowid).val(sales_price);
            $('#sales_discount' + rowid).val(sales_discount * 100);
            $('#txtPrice' + rowid).val(sales_discount_price);
            $('#txtQty' + rowid).val('');
            $('#custgrp_discount').val('');
            $('#sd_cal').val('');
            $("#ordertotal").val('');
            $("#txtOrderSubtotal").val('');
            $('#txtTotal' + rowid).val('');
            $('#promotion' + rowid).val($(data).find("option:selected").data('promotion'));
            $('#txtQty' + rowid).attr('readOnly', false);
        };

        function calculatePrice(data, rowid) {
            var qty = data.value;
            var sellprice = $('#txtPrice' + rowid).val();
            $('#txtTotal' + rowid).val((qty * sellprice).toFixed(2));
            calculateSubtotal();
        }

        function calculateSubtotal() {
            var totalrow = $("#rowOrdercnt").val();
            var subt = 0;
            for (var i = 1; i <= totalrow; i++) {
                if ($('#txtTotal' + i).length) {
                    var total = parseInt($('#txtTotal' + i).val());
                    subt = subt + total;
                }
            }
            var customer_grp_discount = $('#special_discount').val() * subt;
            subt = subt - customer_grp_discount;
            $('#custgrp_discount').val(customer_grp_discount.toFixed(2));
            $('#sd_cal').val(customer_grp_discount.toFixed(2));
            $("#txtOrderSubtotal").val(subt.toFixed(2));
            $("#txtSubtotal").val(subt.toFixed(2));
            $("#ordertotal").val(subt.toFixed(2));
            if (subt != '0' && !isNaN(subt)) {
                $('#btnSubmit').removeClass('disabled');
                $('#btnSubmit').attr('disabled', false);
            } else {
                $('#btnSubmit').addClass('disabled');
                $('#btnSubmit').attr('disabled', true);
            }
        }

        function changeprice(data) {
            var selective = 'r';
            var rowid = $(data).data('row');
            if ($(data).find("option:selected").val() == '1') {
                //pricetype = 1;
                selective = 'w';
            }
            var productdata = $('#ddlproductOrder' + rowid);
            drawprice(productdata, rowid, selective);
        }

        function CreateNewOrderRefPlan() {
            bootbox.confirm({
                size: 'small',
                title: "ยืนยันการนัดหมาย",
                message: "คุณต้องการยืนยันการนัดหมายใช่หรือไม่",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> ยกเลิก',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> ยืนยันการทำรายการ',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    var loading = "";
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "AppHttpRequest.php",
                            data: $("#recordform").serialize(),
                            dataType: "json",
                            beforeSend: function (xhr) {
                                loading = bootbox.dialog({
                                    size: 'small',
                                    message: '<p class="text-center">เรากำลังบันทึกข้อมูลการนัดหมายของท่าน กรุณารอสักครู่...</p>',
                                    closeButton: false
                                });
                            },
                            error: function (transport, status, errorThrown) {
                                console.log(transport.responseText);
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: transport.responseText,
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            },
                            success: function (data) {
                                loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                if (data.MSGID === 100) {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "บันทึกข้อมูลการนัดหมายสำเร็จ",
                                            title: "การแจ้งเตือน",
                                            callback: function () {
                                                window.location = "Planner/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>";
                                            }
                                        });

                                    }, 3000);
                                } else {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        bootbox.alert({
                                            size: 'small',
                                            message: "บันทึกข้อมูลการนัดหมายไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                            title: "การแจ้งเตือน"
                                        });
                                    }, 3000);
                                }
                            }
                        });

                    }

                }
            });
        }
    </script>
    <?php
    if ($orderdetail->MSGMESSAGE2 > 0 && $orderdetail->MSGID == SERV_COMPLETE) {
        echo '<script>InitialPrice();</script>';
    }
    ?>
    <?php include 'inc/page_footer.php'; ?>
    <?php include 'inc/template_scripts.php'; ?>
    <script src="plugins/bootstrapselect/bootstrap-select.min.js"></script>
    <!-- Load and execute javascript code used only in this page -->
    <script src="js/pages/uiTables.js"></script>
    <script>$(function () {
            UiTables.init();
        });</script>

    <script type="text/javascript" >
        function addField() {
            var cnt = $("#rowOrdercnt").val();
            cnt++;
            $("#rowOrdercnt").val(cnt);
            var opt = "<?= $appm->GetProductDropdownlist(); ?>";
            var priceopt = "<?= $appm->GetDropdownPriceType(0); ?>";
            /*$('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlproductOrder' + cnt + '" name="ddlproductOrder[]" data-row="' + cnt + '"  onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td>\n\
             <td class="text-right"><select name="pricetype[]" id="pricetype' + cnt + '" class="select-select2" style="width: 100%;" data-row="' + cnt + '"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true">' + priceopt + '</select></td>\n\
             <td class="text-right"><input id="sale_price' + cnt + '" class="form-control disabledText" value="" disabled/></td><td class="text-right">\n\
             <input id="sale_discount' + cnt + '" class="form-control disabledText" value="" disabled/></td></td><td align="right"><input id="txtPrice' + cnt + '" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td><td><input type="text" id="txtQty' + cnt + '" name="txtQty[]" class="form-control" onkeyup="calculatePrice(this,' + cnt + ')" /></td><td align="right"><input id="txtTotal' + cnt + '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td><input type="hidden" id="promotion' + cnt + '" name="promotionid[]"><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#orderProduct'));*/
            $('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlproductOrder' + cnt + '" name="ddlproductOrder[]" data-row="' + cnt + '"  onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td>\n\
   <td class="text-right"><select name="pricetype[]" id="pricetype' + cnt + '" class="select-select2" style="width: 100%;" data-row="' + cnt + '"  onchange="changeprice(this)" data-placeholder="ค้นหาสินค้า" data-live-search="true">' + priceopt + '</select></td> \n\
<td class="text-right"><input id="sales_price' + cnt + '" class="form-control disabledText" value="" disabled/></td>\n\
<td class="text-right"><input id="sales_discount' + cnt + '" class="form-control disabledText" value="" disabled/></td>\n\
</td><td align="right"><input id="txtPrice' + cnt + '" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td>\n\
<td><input type="text" id="txtQty' + cnt + '" name="txtQty[]" class="form-control" onkeyup="calculatePrice(this,' + cnt + ')" readonly/></td>\n\
<td align="right"><input id="txtTotal' + cnt + '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td>\n\
<input type="hidden" id="promotion' + cnt + '" name="promotionid[]"><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#orderProduct'));

            $.getScript('js/app.js');
        }
        $(document).on('click', '.remove-btn', function (events) {
            $(this).parents('tr').remove();
            if ($('#orderProduct').children('tr').length <= 0) {
                $("#rowOrdercnt").val(0);
            }
            calculateSubtotal();
        });
    </script>

    <?php include 'inc/template_end.php'; ?>