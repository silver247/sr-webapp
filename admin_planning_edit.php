<?php
$pagename = "Selling";
$subpagename = "Planning";
?>
<?php
include 'inc/config_admin.php';
include './bundle.php';

//Nam edit
$req = REQ_EDIT_PLAN;
$percentage_type = "P";
$circulation_type = "I";
?>
<?php
$template['header_link'] = 'WELCOME';
$appmng = new AppManager();
$output = new response();
$output = $appmng->queryYearPlan();
//service::printr($output);
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    แก้ไขยอดขายทั้งหมด
                    <a href="Admin/Planning/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้ายอดขายทั้งหมด</div></a>
                </div>
                <form name="tform1" method="post" id="tform1" onsubmit="CreatePlan();return false;">
                    <input type="hidden" name="req" value="<?= $req ?>"/>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="col-xs-12 col-md-12" style="text-align: center">
                                <div class="form-group">                                    
                                    <div class="col-xs-6 col-md-6" style="float: left">
                                        <input type="radio" name="inputtype" value="<?= $circulation_type ?>" onclick="changeType(this)" checked="checked" class="checktype"/>&nbsp;มูลค่าการขาย
                                    </div>
                                    <div class="col-xs-6 col-md-6"  style="float: left">
                                        <input type="radio" name="inputtype" value="<?= $percentage_type ?>" onclick="changeType(this)" class="checktype"/>&nbsp;สัดส่วนการขาย
                                    </div>
                                </div>
                            </div>
                            <!-- nam
                            <?php
                            service::printr($output);
                            ?>
                            -->
                            <div class="form-group">
                                <label>ยอดขายทั้งหมดในปี 2560</label>
                                <div class="input-group text">
                                    <?php
                                    $value = $output->MSGDATA2;
                                    if (isset($value)) {
                                        $value = number_format($value, 0);
                                    } else {
                                        $value = number_format(0, 0);
                                    }
                                    ?>
                                    <input type="text" id="regionAll" name="regionAll" class="form-control money" readonly="readonly" onkeypress="checknumber()" value="<?= $value ?>"onkeyup="dokeyup(this);
                                            sumRegion();"/>
                                    <span class="input-group-addon">บาท</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 input2block">
                                    <div class="form-group">
                                        <label class="region_label">ภาคเหนือ</label>
                                        <div class="input-group text">
                                            <?php
                                            $temp = $output->MSGDATA1;
                                            $value = $percent = 0;
                                            $region = REGION_N;
                                            if (isset($temp[$region]["target_value"]) && isset($temp[$region]["target_percent"])) {
                                                $value = $temp[$region]["target_value"];
                                                $value = number_format($value, 0);
                                                $percent = $temp[$region]["target_percent"];
                                                $percent = number_format($percent, 2);
                                            } else {
                                                $value = number_format(0, 0);
                                                $percent = number_format(0, 2);
                                            }
                                            ?>
                                            <input type="text" id="region<?= $circulation_type . REGION_N ?>" name="region_price[]" class="form-control money region price" value="<?= $value ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="16"  maxlength="16"/>
                                            <input type="hidden" name="region[]" value="<?= REGION_N ?>"/>
                                            <span class="input-group-addon circulation">บาท</span>
                                            <input type="text" id="region<?= $percentage_type . REGION_N ?>" name="region_percent[]" class="form-control money region percent" value="<?= $percent ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="6" maxlength="5" readonly="readonly">
                                            <span class="input-group-addon percentage">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 input2block">
                                    <div class="form-group">
                                        <label class="region_label">ภาคใต้</label>
                                        <div class="input-group text">
                                            <?php
                                            $temp = $output->MSGDATA1;
                                            $value = $percent = 0;
                                            $region = REGION_S;
                                            if (isset($temp[$region]["target_value"]) && isset($temp[$region]["target_percent"])) {
                                                $value = $temp[$region]["target_value"];
                                                $value = number_format($value, 0);
                                                $percent = $temp[$region]["target_percent"];
                                                $percent = number_format($percent, 2);
                                            } else {
                                                $value = number_format(0, 0);
                                                $percent = number_format(0, 2);
                                            }
                                            ?>
                                            <input type="text" id="region<?= $circulation_type . REGION_S ?>" name="region_price[]" class="form-control money region price" value="<?= $value ?>"  onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="16" maxlength="16"/>
                                            <input type="hidden" name="region[]" value="<?= REGION_S ?>"/>
                                            <span class="input-group-addon circulation">บาท</span>
                                            <input type="text" id="region<?= $percentage_type . REGION_S ?>" name="region_percent[]" class="form-control money region percent" value="<?= $percent ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="6" maxlength="5" readonly="readonly">
                                            <span class="input-group-addon percentage">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 input2block">
                                    <div class="form-group">
                                        <label class="region_label">ภาคตะวันออก</label>
                                        <div class="input-group text">
                                            <?php
                                            $temp = $output->MSGDATA1;
                                            $value = $percent = 0;
                                            $region = REGION_E;
                                            if (isset($temp[$region]["target_value"]) && isset($temp[$region]["target_percent"])) {
                                                $value = $temp[$region]["target_value"];
                                                $value = number_format($value, 0);
                                                $percent = $temp[$region]["target_percent"];
                                                $percent = number_format($percent, 2);
                                            } else {
                                                $value = number_format(0, 0);
                                                $percent = number_format(0, 2);
                                            }
                                            ?>
                                            <input type="text" id="region<?= $circulation_type . REGION_E ?>" name="region_price[]" class="form-control money region price" value="<?= $value ?>"  onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="16" maxlength="16"/>
                                            <input type="hidden" name="region[]" value="<?= REGION_E ?>"/>
                                            <span class="input-group-addon circulation">บาท</span>
                                            <input type="text" id="region<?= $percentage_type . REGION_E ?>" name="region_percent[]" class="form-control money region percent" value="<?= $percent ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="6" maxlength="5" readonly="readonly">
                                            <span class="input-group-addon percentage">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 input2block">
                                    <div class="form-group">
                                        <label class="region_label">ภาคตะวันตก</label>
                                        <div class="input-group text">
                                            <?php
                                            $temp = $output->MSGDATA1;
                                            $value = $percent = 0;
                                            $region = REGION_W;
                                            if (isset($temp[$region]["target_value"]) && isset($temp[$region]["target_percent"])) {
                                                $value = $temp[$region]["target_value"];
                                                $value = number_format($value, 0);
                                                $percent = $temp[$region]["target_percent"];
                                                $percent = number_format($percent, 2);
                                            } else {
                                                $value = number_format(0, 0);
                                                $percent = number_format(0, 2);
                                            }
                                            ?>
                                            <input type="text" id="region<?= $circulation_type . REGION_W ?>" name="region_price[]" class="form-control money region price" value="<?= $value ?>"  onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="16" maxlength="16"/>
                                            <input type="hidden" name="region[]" value="<?= REGION_W ?>"/>
                                            <span class="input-group-addon circulation">บาท</span>
                                            <input type="text" id="region<?= $percentage_type . REGION_W ?>" name="region_percent[]" class="form-control money region percent" value="<?= $percent ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="6" maxlength="5" readonly="readonly">
                                            <span class="input-group-addon percentage">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 input2block">
                                    <div class="form-group">
                                        <label class="region_label">ภาคตะวันออกเฉียงเหนือ
                                        </label>
                                        <div class="input-group text">
                                            <?php
                                            $temp = $output->MSGDATA1;
                                            $value = $percent = 0;
                                            $region = REGION_NE;
                                            if (isset($temp[$region]["target_value"]) && isset($temp[$region]["target_percent"])) {
                                                $value = $temp[$region]["target_value"];
                                                $value = number_format($value, 0);
                                                $percent = $temp[$region]["target_percent"];
                                                $percent = number_format($percent, 2);
                                            } else {
                                                $value = number_format(0, 0);
                                                $percent = number_format(0, 2);
                                            }
                                            ?>
                                            <input type="text" id="region<?= $circulation_type . REGION_NE ?>" name="region_price[]" class="form-control money region price" value="<?= $value ?>"  onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="16" maxlength="16"/>
                                            <input type="hidden" name="region[]" value="<?= REGION_NE ?>"/>
                                            <span class="input-group-addon circulation">บาท</span>
                                            <input type="text" id="region<?= $percentage_type . REGION_NE ?>" name="region_percent[]" class="form-control money region percent" value="<?= $percent ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="6" maxlength="5" readonly="readonly">
                                            <span class="input-group-addon percentage">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 input2block">
                                    <div class="form-group">
                                        <label class="region_label">ภาคกลาง</label>
                                        <div class="input-group text">
                                            <?php
                                            $temp = $output->MSGDATA1;
                                            $value = $percent = 0;
                                            $region = REGION_M;
                                            if (isset($temp[$region]["target_value"]) && isset($temp[$region]["target_percent"])) {
                                                $value = $temp[$region]["target_value"];
                                                $value = number_format($value, 0);
                                                $percent = $temp[$region]["target_percent"];
                                                $percent = number_format($percent, 2);
                                            } else {
                                                $value = number_format(0, 0);
                                                $percent = number_format(0, 2);
                                            }
                                            ?>
                                            <input type="text" id="region<?= $circulation_type . REGION_M ?>" name="region_price[]" class="form-control money region price" value="<?= $value ?>"  onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="16" maxlength="16">
                                            <input type="hidden" name="region[]" value="<?= REGION_M ?>"/>
                                            <span class="input-group-addon circulation">บาท</span>
                                            <input type="text" id="region<?= $percentage_type . REGION_M ?>" name="region_percent[]" class="form-control money region percent" value="<?= $percent ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);
                                                    sumRegion();" onchange="validateInput()" size="6" maxlength="5" readonly="readonly">
                                            <span class="input-group-addon percentage">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="btn-right" style="position: relative;" id="submit_btn"><button type="submit" id="submit_btn" name="submit_btn" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึก" aria-describedby="tooltip441840"><i class="gi gi-floppy_disk"></i> บันทึก</button></div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>

<script>
    $(document).ready(function () {
        //sumRegion();
        $("#submit_btn").click(function () {
            $("#tform1").submit();
        });
    });
    function sumRegion() {
        if ($(".checktype:checked").val() == "<?= $circulation_type ?>") {
            var region<?= REGION_N ?> = eval($('#region<?= $circulation_type . REGION_N ?>').val().replace(/,/g, ''));
            if (region<?= REGION_N ?> < 0 || isNaN(region<?= REGION_N ?>)) {
                region<?= REGION_N ?> = 0;
                $('#region<?= $circulation_type . REGION_N ?>').val("0.00");
            }
            var region<?= REGION_S ?> = eval($('#region<?= $circulation_type . REGION_S ?>').val().replace(/,/g, ''));
            if (region<?= REGION_S ?> < 0 || isNaN(region<?= REGION_S ?>)) {
                region<?= REGION_S ?> = 0;
                $('#region<?= $circulation_type . REGION_S ?>').val("0.00");
            }
            var region<?= REGION_E ?> = eval($('#region<?= $circulation_type . REGION_E ?>').val().replace(/,/g, ''));
            if (region<?= REGION_E ?> < 0 || isNaN(region<?= REGION_E ?>)) {
                region<?= REGION_E ?> = 0;
                $('#region<?= $circulation_type . REGION_E ?>').val("0.00");
            }
            var region<?= REGION_W ?> = eval($('#region<?= $circulation_type . REGION_W ?>').val().replace(/,/g, ''));
            if (region<?= REGION_W ?> < 0 || isNaN(region<?= REGION_W ?>)) {
                region<?= REGION_W ?> = 0;
                $('#region<?= $circulation_type . REGION_W ?>').val("0.00");
            }
            var region<?= REGION_NE ?> = eval($('#region<?= $circulation_type . REGION_NE ?>').val().replace(/,/g, ''));
            if (region<?= REGION_NE ?> < 0 || isNaN(region<?= REGION_NE ?>)) {
                region<?= REGION_NE ?> = 0;
                $('#region<?= $circulation_type . REGION_NE ?>').val("0.00");
            }
            var region<?= REGION_M ?> = eval($('#region<?= $circulation_type . REGION_M ?>').val().replace(/,/g, ''));
            if (region<?= REGION_M ?> < 0 || isNaN(region<?= REGION_M ?>)) {
                region<?= REGION_M ?> = 0;
                $('#region<?= $circulation_type . REGION_M ?>').val("0.00");
            }
            var regionAll = region<?= REGION_N ?> + region<?= REGION_S ?> + region<?= REGION_E ?> + region<?= REGION_W ?> + region<?= REGION_NE ?> + region<?= REGION_M ?>;
            if (regionAll > 0) {
                if (region<?= REGION_N ?> >= 0) {
                    var temppercent = ((region<?= REGION_N ?> / regionAll) * 100).toFixed(2);
                    $('#region<?= $percentage_type . REGION_N ?>').val(temppercent);
                }
                if (region<?= REGION_S ?> >= 0) {
                    var temppercent = ((region<?= REGION_S ?> / regionAll) * 100).toFixed(2);
                    $('#region<?= $percentage_type . REGION_S ?>').val(temppercent);
                }
                if (region<?= REGION_E ?> >= 0) {
                    var temppercent = ((region<?= REGION_E ?> / regionAll) * 100).toFixed(2);
                    $('#region<?= $percentage_type . REGION_E ?>').val(temppercent);
                }
                if (region<?= REGION_W ?> >= 0) {
                    var temppercent = ((region<?= REGION_W ?> / regionAll) * 100).toFixed(2);
                    $('#region<?= $percentage_type . REGION_W ?>').val(temppercent);
                }
                if (region<?= REGION_NE ?> >= 0) {
                    var temppercent = ((region<?= REGION_NE ?> / regionAll) * 100).toFixed(2);
                    $('#region<?= $percentage_type . REGION_NE ?>').val(temppercent);
                }
                if (region<?= REGION_M ?> >= 0) {
                    var temppercent = ((region<?= REGION_M ?> / regionAll) * 100).toFixed(2);
                    $('#region<?= $percentage_type . REGION_M ?>').val(temppercent);
                }
            } else {
                $(".percent[type=text]").val("0.00");
            }

            var sumStr = regionAll.toString();
            if (isNaN(regionAll) && result < 0) {
                return;
            }
            var n = 0;
            var result = "";
            var c = "";
            for (a = sumStr.length - 1; a >= 0; a--) {
                c = sumStr.charAt(a);
                if (c != ',') {
                    n++;
                    if (n == 4) {
                        result = "," + result;
                        n = 1;
                    }

                    result = c + result;
                }

            }

            $('#regionAll').val(result);
        } else if ($(".checktype:checked").val() == "<?= $percentage_type ?>") {
            var regionAll = $('#regionAll').val();
            regionAll = regionAll.replace(/,/g, '');
            if (!isNaN(regionAll) && regionAll !== "") {
                $(".percent[type=text]").each(function (index, obj) {
                    var parent = $(obj).parent();
                    if (!isNaN($(obj).val()) && $(obj).val() !== "") {
                        try {
                            var temppercent = eval($(obj).val());
                            if (temppercent > 0 && temppercent < 100) {
                                var val = ((regionAll * temppercent) / 100).toFixed(0);
                                if (!isNaN(val)) {
                                    $(".price[type=text]", $(parent)).val(val);
                                }
                            }
                        } catch (ex) {
                        }
                    } else {
                        $(obj).val("");
                        $(".price[type=text]", $(parent)).val("0");
                    }
                });
            }
        }
    }
    function validateInput() {
        var validate = true;
        $(".region").each(function () {
            if (validate) {
                var region = parseInt($(this).val().replace(/,/g, ''));
                var obj = $(this);
                if (region < 0) {
                    bootbox.alert({
                        size: 'small',
                        message: "กรุณาระบุยอดขายให้มีค่ามากกว่าหรือเท่ากับ 0 เท่านั้น",
                        title: "การแจ้งเตือน",
                        callback: function () {
                            $(obj).val("");
                            $(obj).focus();
                        }
                    });
                    validate = false;
                }
            }
        });
        return true;
    }
    function validateForm() {
        if (!validateInput()) {
            return false;
        }
        return true;
    }
    function CreatePlan() {
        if (!validateInput()) {
            return false;
        }
        bootbox.confirm({
            size: 'small',
            title: "ยืนยันยอดขาย",
            message: "คุณยืนยันยอดขายนี้ใช่หรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ยกเลิก',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ยืนยันยอดขาย',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                var loading = "";
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "AppHttpRequest.php",
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            loading = bootbox.dialog({
                                size: 'small',
                                message: '<p class="text-center">เรากำลังบันทึกข้อมูลยอดขายขายของท่าน กรุณารอสักครู่...</p>',
                                closeButton: false
                            });
                        },
                        error: function (transport, status, errorThrown) {
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: "บันทึกข้อมูลแผนการขายไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                    title: "การแจ้งเตือน"
                                });
                            }, 3000);
                            console.log(transport.responseText);
                        },
                        success: function (data) {
                            console.log(data);
                            //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                            if (data.MSGID === 100) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลยอดขายขายสำเร็จ",
                                        title: "การแจ้งเตือน",
                                        callback: function () {
                                            window.location = "Admin/Planning/";
                                        }
                                    });
                                }, 3000);
                            } else {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลยอดขายขายไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            }
                        }
                    });
                }

            }
        });
    }
    function changeType(obj) {
        $(".price[type=text]").val("0.00");
        $(".percent[type=text]").val("0.00");
        $("#regionAll").val("0.00");
        if ($(obj).val() == "<?= $percentage_type ?>") {
            $("#regionAll").removeAttr("readonly");
            $(".percent[type=text]").each(function (index, obj) {
                var parent = $(obj).parent();
                $(parent).append($(".percent[type=text]", $(parent)));
                $(".percent[type=text]", $(parent)).removeAttr("readonly");
                $(parent).append($(".percentage", $(parent)));
                $(parent).append($(".price[type=text]", $(parent)));
                $(".price[type=text]", $(parent)).attr("readonly", "readonly");
                $(parent).append($(".circulation", $(parent)));
            });
        } else if ($(obj).val() == "<?= $circulation_type ?>") {
            $(".price[type=text]").each(function (index, obj) {
                $("#regionAll").attr("readonly", "readonly");
                var parent = $(obj).parent();
                $(parent).append($(".price[type=text]", $(parent)));
                $(".price[type=text]", $(parent)).removeAttr("readonly");
                $(parent).append($(".circulation", $(parent)));
                $(parent).append($(".percent[type=text]", $(parent)));
                $(".percent[type=text]", $(parent)).attr("readonly", "readonly");
                $(parent).append($(".percentage", $(parent)));
            });
        }
    }
</script>

<?php include 'inc/template_scripts.php'; ?>
<?php include 'inc/template_end.php'; ?>