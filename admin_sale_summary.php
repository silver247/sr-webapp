<?php $pagename = "Sale" ?>
<?php
include 'inc/config_admin.php';
include_once './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
$userid = filter_input(INPUT_GET, 'id');
$user = new user();
$user = $appm->GetUserDataList($userid)->MSGDATA1[0];
$date = date('Y-m-d');
$month = date('n');
$year = date('Y');
$dailysalesobject = $appm->GetSalesSummary($userid, $date, $month, $year);
$monthlysalesobject = $appm->GetSalesSummary($userid, '', $month, $year);
$yearsalesobject = $appm->GetSalesSummary($userid, '', '', $year);
$previousyearobject = $appm->GetSalesSummary($userid, '', '', $year - 1);
$previous2yearobject = $appm->GetSalesSummary($userid, '', '', $year - 2);
$dailysales[$month] = 0;
$dailypercent = 0;
$monthlysales[$month] = 0;
$monthlypercent = 0;
$totalsales = 0;
$yearsalespercent = 0;
$userplan = $appm->queryUserPlan($userid, $month, $year);
//service::printr($dailysalesobject);
if (!empty($dailysalesobject->MSGDATA1)) {
    $dailysales = $dailysalesobject->MSGDATA1[0];
}
if (!empty($monthlysalesobject->MSGDATA1)) {
    $monthlysales = $monthlysalesobject->MSGDATA1[0];
}
if (!empty($yearsalesobject->MSGDATA1)) {
    $yearsales = $yearsalesobject->MSGDATA1[0];
    foreach ($yearsales as $yearsale) {
        $totalsales += $yearsale;
    }
    $yearsalespercent = ($totalsales * 100) / 10000;
}

if (!empty($previousyearobject->MSGDATA1)) {
    $previousyear = $previousyearobject->MSGDATA1[0];
}
if (!empty($previous2yearobject->MSGDATA1)) {
    $previous2year = $previous2yearobject->MSGDATA1[0];
}
$datalist = array();
$datalistp = array();
$datalistp2 = array();

for ($i = 1; $i <= 12; $i++) {
    $datalist[$i] = 0;
    if (isset($yearsales[$i])) {
        $datalist[$i] = $yearsales[$i];
    }
    $datalistp[$i] = 0;
    if (isset($previousyear[$i])) {
        $datalistp[$i] = $previousyear[$i];
    }
    $datalist2p[$i] = 0;
    if (isset($previous2year[$i])) {
        $datalist2p[$i] = $previous2year[$i];
    }
}
$datalist = join(", ", $datalist);
$datalistp = join(", ", $datalistp);
$datalist2p = join(", ", $datalist2p);
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>
<?php
$monthlist = array();
for ($i = 1; $i <= 12; $i++) {
    $monthlist[] = "'" . $thai_month_arr[$i] . "'";
}
$monthlist = join(", ", $monthlist);
$curdate = new DateTime();
$startdate = new DateTime($user->STARTDATE);
$interval = $curdate->diff($startdate);
//service::printr($userplan);
//service::printr($dailysales);
?>
<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    ข้อมูลสรุปยอดขาย
                    <a href="Admin/Sale/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าพนักงานขาย</div></a>
                </div>

                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:25px;">
                        <table>
                            <tr>
                                <th>ชื่อ-นามสกุล</th>
                                <td colspan="3"><?= $user->TNAME; ?> <?= $user->TLNAME; ?></td>
                            </tr>
                            <tr>
                                <th width="120px">ตำแหน่ง</th>
                                <td width="250px">-</td>
                                <th width="130px">ภูมิภาคที่ดูแล</th>
                                <td><?=
                                    $user->REGIONNAME;
                                    ;
                                    ?></td>
                            </tr>
                            <tr>
                                <th>วันที่เริ่มงาน</th>
                                <td><?= thaidate_withouttime($user->STARTDATE); ?></td>
                                <th>อายุงาน</th>
                                <td><?= $interval->format('%y'); ?> ปี <?= $interval->format('%m'); ?> เดือน <?= $interval->format('%d'); ?> วัน</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="block full">
                    <div class="row">
                        <div class="col-md-6">
                            <strong>สรุปยอดขายรายวัน (<?= $nowDMYTHSH ?>)</strong>
                            <?php
                            $tragetvalue = 0;
                            if (isset($userplan->MSGDATA1["target_sales_value"])) {
                                $tragetvalue = $userplan->MSGDATA1["target_sales_value"];
                            }
                            $value = floatval($tragetvalue);
                            $year = date("Y");
                            $month = date("n");
                            $date = date('Y-m-d');
                            $day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                            $viewvalue = number_format($value / 12 / $day + 1, 2);
                            if ($value == 0) {
                                $viewvalue = number_format(0, 2);
                                $dailypercent = 0;
                            } else {
                                $dailypercent = ($dailysales[$month] * 100) / ($value / 12 / $day + 1);
                            }
                            ?>
                            <div class="progress progressWithBT">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= $dailypercent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $dailypercent > 100 ? 100 : $dailypercent ?>%"><?= number_format($dailypercent,2); ?>%</div>
                            </div>
                            <div class="progressBT"><?= number_format($dailysales[$month], 2); ?>/<?= $viewvalue ?> บาท</div>
                        </div>
                        <div class="col-md-6">
                            <strong>สรุปยอดขายรายเดือน (<?= $nowMTH ?>)</strong>
                            <?php
                            $viewvalue = number_format($value / 12, 2);
                            if ($value == 0) {
                                $viewvalue = number_format(0, 2);
                                $monthlypercent = 0;
                            } else {
                                $monthlypercent = ($monthlysales[$month] * 100) / ($value / 12);
                            }
                            ?>
                            <div class="progress progressWithBT">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?= $monthlypercent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $monthlypercent > 100 ? 100 : $monthlypercent ?>%"><?= number_format($monthlypercent,2); ?>%</div>
                            </div>
                            <div class="progressBT"><?= number_format($monthlysales[$month], 2); ?>/<?= $viewvalue ?> บาท</div>
                        </div>
                        <div class="col-md-offset-3 col-md-6">
                            <strong>สรุปยอดขายรายปี (<?= $nowYTH ?>)</strong>
                            <?php
                            $viewvalue = number_format($value, 2);
                            if ($value == 0) {
                                $viewvalue = number_format(0, 2);
                                $yearsalespercent = 0;
                            } else {
                                $yearsalespercent = ($totalsales * 100) / $value;
                            }
                            ?>
                            <div class="progress progressWithBT">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $yearsalespercent; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $yearsalespercent > 100 ? 100 : $yearsalespercent ?>%"><?= number_format($yearsalespercent,2); ?>%</div>
                            </div>
                            <div class="progressBT"><?= number_format($totalsales, 2); ?>/<?= $viewvalue ?> บาท</div>
                        </div>
                    </div>
                </div>
                <div class="block full" style="border-top:0px;">
                    <canvas  id="chart-bars" style="height: 380px;"></canvas>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<!--<script src="js/pages/compCharts.js"></script>-->
<script>
    $(document).ready(function () {

    });
    //var month = 
    var ctx = $('#chart-bars');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: [<?= $monthlist; ?>],
            datasets: [
                {
                    label: "<?= $year; ?>",
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [<?= $datalist; ?>],
                },
                {
                    label: "<?= $year - 1; ?>",
                    backgroundColor: 'rgba(255, 159, 64, 0.2)',
                    borderColor: 'rgb(255, 159, 64)',
                    data: [<?= $datalistp; ?>],
                },
                {
                    label: "<?= $year - 2; ?>",
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgb(75, 192, 192)',
                    data: [<?= $datalist2p; ?>],
                }
            ]
        },

        // Configuration options go here
        options: {
            maintainAspectRatio: false,
            beginAtZero: true
        }
    });

</script>

<?php include 'inc/template_end.php'; ?>