<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
include './bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
$res = new response();
$date = "";
if (isset($_GET['date'])) {
    $res = $appm->GetMyScheduleByDate($date);
    $date = filter_input(INPUT_GET, 'date');
    //echo "test";
} else {
    $date = date('Y-m-d');
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <form name="tform1" method="post" id="tform1" onsubmit="CreateNewPlan(); return false;">
                <input type="hidden" name="req" id="hidDo" value="<?= REQ_ADD_PLAN; ?>">
                <div class="block full">

                    <div class="block-title">
                        เพิ่มตารางนัดหมาย (ลูกค้าเก่า) 
                        <a href="Planner/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปตารางนัดหมาย</div></a>
                    </div>
                    <div class="row">

                        <div class="col-md-offset-2 col-md-8">
                            <div class="form-group">
                                <label>ชื่อร้าน</label>
                                <select id="shop" name="shop" class="select-select2" style="width: 100%;" data-placeholder="ร้านค้า" data-live-search="true">
                                    <option ></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                    <?= $appm->GetCustomerDropdownlist(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>การโทรนัดหมาย</label>
                                <div class="orderTable">
                                    <div class="row" style="padding: 5px 10px;">
                                        <label class="csscheckbox csscheckbox-primary"><input type="radio" name="rdappoint" id="appoint-y" value="1" checked><span></span> ได้โทรนัดวัน และเวลาที่จะเข้าพบ</label>
                                        <br/>
                                        <div class="tapCheckbox" id="appoint">
                                            <div class="form-group">
                                                <label>วันที่นัดหมาย</label>
                                                <input type="text" id="plandate" name="plandate" readonly="true" class="form-control input-datepicker" value="" placeholder="วันที่นัดหมาย" />
                                            </div>
                                            <div class="form-group">
                                                <label>เวลานัดหมาย</label>
                                                <div class="input-group bootstrap-timepicker">
                                                    <div class="bootstrap-timepicker-widget dropdown-menu">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td>
                                                                    <td class="separator">&nbsp;</td>
                                                                    <td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td>
                                                                    <td class="separator">&nbsp;</td>
                                                                    <td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td>
                                                                    <td class="separator">:</td>
                                                                    <td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td>
                                                                    <td class="separator">:</td>
                                                                    <td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td>
                                                                    <td class="separator"></td>
                                                                    <td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td>
                                                                    <td class="separator">&nbsp;</td>
                                                                    <td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <input type="text" id="plantime" name="plantime" readonly="true" class="form-control input-timepicker24">
                                                    <span class="input-group-btn">
                                                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 5.375px;"></span><i class="fa fa-clock-o"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <label class="csscheckbox csscheckbox-primary"><input type="radio" name="rdappoint" id="appoint-n"  value="0"><span></span> ยังไม่ได้มีการโทรนัดหมายใดๆ</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>การสั่งซื้อสินค้า</label>
                                <div class="orderTable">
                                    <div class="row" style="padding: 5px 10px;">
                                        <label class="csscheckbox csscheckbox-primary"><input type="radio" name="rdorderProduct" value="1" id="orderProduct-y" checked><span></span> ลูกค้ามีการสั่งซื้อสินค้าเพิ่ม</label>
                                        <br/>
                                        <div class="tapCheckbox" id="orderProduct-d">
                                            <div class="orderTable">
                                                <table class="table table-vcenter table-condensed table-striped table-borderless">
                                                    <thead>
                                                        <tr>
                                                            <th>รุ่นสินค้า</th>
                                                            <th width="100px" class="text-right">ราคาขาย</th>
                                                            <th width="100px" class="text-right">% ส่วนลด</th>
                                                            <th width="100px" class="text-right">ราคาสุทธิ</th>
                                                            <th width="80px" class="text-center">จำนวน</th>
                                                            <th width="100px" class="text-right">ยอดขายรวม</th>
                                                            <th width="30px"></th>
                                                        </tr>										
                                                    </thead>
                                                    <tbody id="orderProduct">
                                                    <input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="1">
                                                    <tr id="row1" class="tableRow">
                                                        <td>
                                                            <select id="ddlproductOrder1" name="ddlproductOrder[]" data-row="1" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า" data-live-search="true">
                                                                <option ></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                <?= $appm->GetProductDropdownlist(); ?>
                                                            </select>
                                                                                            <!--<input type="text" id="example-typeahead" name="example-typeahead" class="form-control input-typeahead" autocomplete="off" placeholder="ค้นหาสินค้า">-->
                                                        </td>
                                                        <td class="text-right"><input id="retail_price1" class="form-control disabledText" value="" disabled/></td>
                                                        <td class="text-right"><input id="retail_discount1" class="form-control disabledText" value="" disabled/></td>
                                                        <td class="text-right"><input id="txtPrice1" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td>
                                                        <td class="text-center"><input id="txtQty1" name="txtQty[]" type="text" class="form-control" onkeyup="calculatePrice(this, 1)"/></td>
                                                        <td class="text-right"><input id="txtTotal1" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td>
                                                    <input type="hidden" id="promotion1" name="promotionid[]">
                                                    <td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="5"><button type="button" class="btn btn-xs btn-primary" onClick="addField()"><i class="gi gi-circle_plus"></i> เพิ่มสินค้า</button></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div align="right">ยอดขายรวมทั้งหมด <input class="form-control disabledText inlineDiv" id="txtOrderSubtotal" name="txtOrderSubtotal" style="width:130px;" disabled/> บาท</div>
                                        </div>
                                        <label class="csscheckbox csscheckbox-primary"><input type="radio" name="rdorderProduct" value="0" id="orderProduct-n"><span></span> ลูกค้าไม่มีการสั่งซื้อสินค้าเพิ่ม</label>
                                        <div class="tapCheckbox" id="orderProduct-b">
                                            <div class="form-group">
                                                <label>เหตุผลทำไมไม่สั่งสินค้า</label>
                                                <textarea class="form-control" name="txtnobuy" placeholder="เหตุผลทำไมไม่สั่งสินค้า"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>ได้นำเสนอสินค้ารุ่นอื่นให้ลูกค้าหรือไม่ รุ่นใดบ้าง เพราะเหตุใด</label>
                                                <div class="orderTable">
                                                    <table class="table table-vcenter table-condensed table-striped table-borderless">
                                                        <thead>
                                                            <tr>
                                                                <th width="150px">รุ่นสินค้า</th>
                                                                <th>เหตุผลที่แนะนำ</th>
                                                                <th width="30px"></th>
                                                            </tr>										
                                                        </thead>
                                                        <tbody id="adProduct">
                                                        <input type="hidden" name="rowSuggest" id="rowSuggest" value="1">
                                                        <tr id="rows1" class="tableRow">
                                                            <td>
                                                                <select id="ddlproductSuggest1" name="ddlproductSuggest[]" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า">
                                                                    <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                    <?= $appm->GetProductDropdownlist(); ?>
                                                                </select>
                                                            </td>
                                                            <td><input id="txtproductSuggest1" name="txtproductSuggest[]" class="form-control" /></td>
                                                            <td><button type="button" class="btn btn-xs btn-danger remove-sbtn"><i class="gi gi-bin"></i></button></td>
                                                        </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5"><button type="button" class="btn btn-xs btn-primary" onClick="addFieldAD()"><i class="gi gi-circle_plus"></i> เพิ่มข้อมูล</button></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>ผลตอบรับจากลูกค้าเป็นอย่างไร อธิบาย</label>
                                                <textarea class="form-control" name="txtq2r1" placeholder="ผลตอบรับจากลูกค้า"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>หมายเหตุ</label>
                                                <textarea class="form-control" name="txtq2r2" placeholder="หมายเหตุ"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;">
                        <button type="submit"  id="btnSubmit" data-toggle="tooltip" title="ยืนยันการนัดหมาย" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="ยืนยันการนัดหมาย" aria-describedby="tooltip441840" ><i class="gi gi-circle_plus"></i> ยืนยันการนัดหมาย</button>
                        <a href="Planner/<?= isset($_GET['date']) ? $_GET['date'] . '/' : '' ?>" data-toggle="tooltip" title="ยกเลิก" class="btn btn-effect-ripple btn-xs btn-danger" data-original-title="ยกเลิก" aria-describedby="tooltip441840"><i class="gi gi-circle_remove"></i> ยกเลิก</a>
                    </div>
                </div>
            </form>

        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script>
    function renderprice(data) {
        //var selectedText = $(data).find("option:selected").text();
        var productid = data.value;
        var rowid = $(data).data('row');
        var retail_price = $(data).find("option:selected").data('rprice');
        var retail_discount = $(data).find("option:selected").data('rdiscount');
        var retail_discount_price = $(data).find("option:selected").data('rdprice');
        $('#retail_price' + rowid).val(retail_price);
        $('#retail_discount' + rowid).val(retail_discount * 100);
        $('#txtPrice' + rowid).val(parseFloat(retail_discount_price).toFixed(2));
        $('#txtQty' + rowid).val('');
        $('#promotion' + rowid).val($(data).find("option:selected").data('promotion'));
        $('#txtQty' + rowid).attr('readOnly', false);
    }

    function calculatePrice(data, rowid) {
        var qty = data.value;
        var sellprice = $('#txtPrice' + rowid).val();
        $('#txtTotal' + rowid).val((qty * sellprice).toFixed(2));
        calculateSubtotal();
    }

    function calculateSubtotal() {
        var totalrow = $("#rowOrdercnt").val();
        var subt = 0;
        for (var i = 1; i <= totalrow; i++) {
            if ($('#txtTotal' + i).length) {
                var total = parseInt($('#txtTotal' + i).val());
                subt = subt + total;
            }
        }
        $("#txtOrderSubtotal").val(subt.toFixed(2));
        $("#ordertotal").val(subt.toFixed(2));
    }
    /*function renderprice(data) {
     //var selectedText = $(data).find("option:selected").text();
     var productid = data.value;
     var rowid = $(data).data('row');
     var icost = $(data).find("option:selected").data('icost');
     var iprofit = $(data).find("option:selected").data('iprofit');
     var sellprice = icost + (icost * iprofit);
     $('#txtPrice' + rowid).val(sellprice);
     $('#txtQty' + rowid).val('');
     $('#txtQty' + rowid).attr('readOnly', false);
     }
     
     function calculatePrice(data, rowid) {
     var qty = data.value;
     var sellprice = $('#txtPrice' + rowid).val();
     $('#txtTotal' + rowid).val(qty * sellprice);
     calculateSubtotal();
     }
     
     function calculateSubtotal() {
     var totalrow = $("#rowOrdercnt").val();
     var subt = 0;
     for (var i = 1; i <= totalrow; i++) {
     if ($('#txtTotal' + i).length) {
     var total = parseInt($('#txtTotal' + i).val());
     subt = subt + total;
     }
     }
     $("#txtOrderSubtotal").val(subt);
     }*/
</script>
<script>
    $(document).ready(function () {
        $('#appoint-y').click(function () {
            $('#appoint').show();
        });
        $('#appoint-n').click(function () {
            $('#appoint').hide();
        });

        $('#orderProduct-b').hide();
        $('#orderProduct-y').click(function () {
            $('#orderProduct-d').show();
            $('#orderProduct-b').hide();
        });
        $('#orderProduct-n').click(function () {
            $('#orderProduct-b').show();
            $('#orderProduct-d').hide();
        });
        moment.locale();
        $('#plandate').datepicker('update', new Date(moment('<?= $date; ?>').add(1, 'days')));
    });
</script>

<script>

    $('#plandate').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        language: "th-th"
    });

</script>

<script type="text/javascript" >
    function addField() {
        var cnt = $("#rowOrdercnt").val();
        cnt++;
        $("#rowOrdercnt").val(cnt);
        var opt = "<?= $appm->GetProductDropdownlist(); ?>";
        $('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlproductOrder' + cnt + '" name="ddlproductOrder[]" data-row="' + cnt + '"  onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td>\n\
    <td class="text-right"><input id="retail_price' + cnt + '" class="form-control disabledText" value="" disabled/></td><td class="text-right"><input id="retail_discount' + cnt + '" class="form-control disabledText" value="" disabled/></td></td><td align="right"><input id="txtPrice' + cnt + '" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td><td><input type="text" id="txtQty' + cnt + '" name="txtQty[]" class="form-control" onkeyup="calculatePrice(this,' + cnt + ')" /></td><td align="right"><input id="txtTotal' + cnt + '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td><input type="hidden" id="promotion' + cnt + '" name="promotionid[]"><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#orderProduct'));

        $.getScript('js/app.js');
    }
    $(document).on('click', '.remove-btn', function (events) {
        $(this).parents('tr').remove();
        if ($('#orderProduct').children('tr').length <= 0) {
            $("#rowOrdercnt").val(0);
        }
        calculateSubtotal();
    });

    function addFieldAD() {
        var cnt = $("#rowSuggest").val();
        cnt++;
        $("#rowSuggest").val(cnt);
        var opt = "<?= $appm->GetProductDropdownlist(); ?>";
        //var total_text = document.getElementsByClassName("tableRow");
        //total_text = total_text.length + 1;
        $('<tr id="rows' + cnt + '" class="tableRow"><td><select id="ddlproductSuggest' + cnt + '" name="ddlproductSuggest[]" data-row="' + cnt + '"  class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td><td><input id="txtproductSuggest' + cnt + '" name="txtproductSuggest[]" class="form-control" /></td><td><button class="btn btn-xs btn-danger remove-sbtn"><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#adProduct'));
        $.getScript('js/app.js');
    }

    $(document).on('click', '.remove-sbtn', function (events) {
        $(this).parents('tr').remove();
        if ($('#adProduct').children('tr').length <= 0) {
            $("#rowSuggest").val(0);
        }
        calculateSubtotal();
    });

    $(document).on('click', '#orderProduct-y', function (events) {
        destroyChoiceSuggest($('#adProduct'));
    });
    $(document).on('click', '#orderProduct-n', function (events) {
        destroyChoiceOrder($('#orderProduct'));
    });

    function destroyChoiceSuggest(root) {
        $(root).children('tr').remove();
        var cnt = $("#rowSuggest").val();
        cnt = 1;
        $("#rowSuggest").val(cnt);
        var opt = "<?= $appm->GetProductDropdownlist(); ?>";
        $('<tr id="rows' + cnt + '" class="tableRow"><td><select id="ddlproductSuggest' + cnt + '" name="ddlproductSuggest[]" data-row="' + cnt + '"   class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td><td><input id="txtproductSuggest' + cnt + '" name="txtproductSuggest[]" class="form-control" /></td><td><button class="btn btn-xs btn-danger remove-sbtn"><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#adProduct'));
        $.getScript('js/app.js');
    }

    function destroyChoiceOrder(root) {
        $(root).children('tr').remove();
        var cnt = $("#orderProduct").val();
        cnt = 1;
        $("#orderProduct").val(cnt);
        $('#txtOrderSubtotal').val('');
        var opt = "<?= $appm->GetProductDropdownlist(); ?>";
        $('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlproductOrder' + cnt + '" name="ddlproductOrder[]" data-row="' + cnt + '"  onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td>\n\
    <td class="text-right"><input id="retail_price' + cnt + '" class="form-control disabledText" value="" disabled/></td><td class="text-right"><input id="retail_discount' + cnt + '" class="form-control disabledText" value="" disabled/></td></td><td align="right"><input id="txtPrice' + cnt + '" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td><td><input type="text" id="txtQty' + cnt + '" name="txtQty[]" class="form-control" onkeyup="calculatePrice(this,' + cnt + ')" /></td><td align="right"><input id="txtTotal' + cnt + '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td><input type="hidden" id="promotion' + cnt + '" name="promotionid[]"><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#orderProduct'));

        $.getScript('js/app.js');
    }

    function CreateNewPlan() {
        var ddlshop = $('#shop option:selected').val();
        if (ddlshop.trim().length <= 0) {
            bootbox.alert({
                size: 'small',
                message: "กรุณาเลือกร้าน"
            });
            return;
        }
        if ($("#orderProduct-y").is(":checked")) {
            var txtOrderSubtotal = $('#txtOrderSubtotal').val();
            if (txtOrderSubtotal.trim().length <= 0 || isNaN(parseFloat(txtOrderSubtotal))) {
                bootbox.alert({
                    size: 'small',
                    message: "กรุณากรอกข้อมูลให้ครบถ้วน"
                });
                return;
            }
        }
        if ($("#orderProduct-n").is(":checked")) {
            var cnt = $("#rowSuggest").val();
            for (var i = 1; i <= cnt; i++) {
                if ($('#txtproductSuggest' + i).val().trim().length <= 0) {
                    bootbox.alert({
                        size: 'small',
                        message: "กรุณากรอกข้อมูลให้ครบถ้วน"
                    });
                    return;
                }
            }
        }
        bootbox.confirm({
            size: 'small',
            title: "ยืนยันการนัดหมาย",
            message: "คุณต้องการยืนยันการนัดหมายใช่หรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ยกเลิก',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ยืนยันการนัดหมาย',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                var loading = "";
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "AppHttpRequest.php",
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            loading = bootbox.dialog({
                                size: 'small',
                                message: '<p class="text-center">เรากำลังบันทึกข้อมูลการนัดหมายของท่าน กรุณารอสักครู่...</p>',
                                closeButton: false
                            });
                        },
                        error: function (transport, status, errorThrown) {
                        console.log(transport.responseText);
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: transport.responseText,
                                    title: "การแจ้งเตือน"
                                });
                            }, 3000);
                        },
                        success: function (data) {
                            console.log(data);
                            //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                            if (data.MSGID === 100) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลการนัดหมายสำเร็จ",
                                        title: "การแจ้งเตือน",
                                        callback: function () {
                                            window.location = "Planner/<?= $date; ?>/";
                                        }
                                    });
                                }, 3000);
                            } else {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลการนัดหมายไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            }
                        }
                    });

                }

            }
        });
    }
</script>

<?php include 'inc/template_end.php'; ?>