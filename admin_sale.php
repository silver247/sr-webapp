<?php $pagename = "Sale" ?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    พนักงานขาย
                </div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:35px;">ค้นหา</label>
                    <div class="inlineDiv">
                        <input class="form-control" type="text" id="search" name="search" style="width:200px" placeholder="ค้นหาข้อมูล" />
                    </div>
                </div>

                <div class="block full">
                    <div class="btn-right"><a href="Admin/Sale/Add/" data-toggle="tooltip" title="เพิ่มข้อมูลพนักงานขาย" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มข้อมูลพนักงานขาย</a></div>
                    <div class="table-responsive" id="tableSale"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
    $(document).ready(function () {
        tableSale($('#search').val());
    });
</script>
<script>
    function tableSale(search) {
        //alert(shop+month+year);

        $.ajax({
            type: "GET",
            url: "admin_sale_table.php",
            //data: {search: search},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="saleTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 50px;">ลำดับที่</th>'
                show += '<th style="width: 100px;">รหัสพนักงาน</th>'
                show += '<th>ชื่อ-นามสกุล</th>'
                show += '<th style="width: 120px;">ตำแหน่ง</th>'
                show += '<th style="width: 100px;">วันที่เริ่มงาน</th>'
                //show += '<th style="width: 80px;">ภูมิภาค</th>'
                show += '<th class="no-sort" style="width: 350px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'

                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        show += '<tr>'
                        show += '<td class="text-center">' + i + '</td>'
                        show += '<td>' + value.USERID + '</td>'
                        show += '<td>' + value.TNAME + ' ' + value.TLNAME + '</td>'
                        show += '<td>' + value.ROLENAME + '</td>'
                        show += '<td>' + value.STARTDATE + '</td>'
                        //show += '<td>'+value.REGIONNAME+'</td>'
                        show += '<td class="text-right">'
                        show += '<a href="Admin/Sale/Sum/' + value.USERID + '/" data-toggle="tooltip" title="ข้อมูลสรุปยอดขาย" class="btn btn-effect-ripple btn-xs btn-primary"><i class="gi gi-eye_open"></i> สรุปยอดขาย</a>'
                        show += '<a href="Admin/Sale/Detail/' + value.USERID + '/" data-toggle="tooltip" title="ข้อมูลการขาย" class="btn btn-effect-ripple btn-xs btn-info"><i class="hi hi-list-alt"></i> ข้อมูลการขาย</a>'
                        show += '<a href="Admin/Sale/Edit/' + value.USERID + '/" data-toggle="tooltip" title="แก้ไขข้อมูลพนักงานขาย" class="btn btn-effect-ripple btn-xs btn-warning"><i class="gi gi-pencil"></i> แก้ไข</a>'
                        show += '</td>'
                        show += '</tr>'
                        i++;
                    });
                }
                show += '</tbody>'
                show += '</table>'

                $('#tableSale').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () {
                        UiTables.init();
                    });
                });

                $('#saleTable').dataTable({
                    ordering: true,
                    info: false,
                    searching: true,
                    lengthChange: false,
                    columnDefs: [
                        {targets: 6, orderable: false}
                    ]
                })
            }
        });
    }
</script>

<?php include 'inc/template_end.php'; ?>