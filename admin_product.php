<?php 
$pagename = "Product";
$subpagename = "Product";
?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    รายการสินค้า
                </div>
                <div class="block-option">
                    <label class="control-label inlineDiv" style="width:35px;"></label>
                    <div class="inlineDiv">
                    	<!--<input class="form-control" type="text" id="search" name="search" style="width:200px" placeholder="ค้นหาข้อมูล" />-->
					</div>
                </div>
                
                <div class="block full">
	                <div class="btn-right"><a href="Admin/Product/Add/" data-toggle="tooltip" title="เพิ่มข้อมูลสินค้า" class="btn btn-effect-ripple btn-xs btn-success"><i class="gi gi-circle_plus"></i> เพิ่มข้อมูลสินค้า</a></div>
					<div class="table-responsive" id="tableProduct"></div>
                </div>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>
$(document).ready(function () {
	tableProduct($('#search').val());
        
        $(document).on('click', '#btnCancel', function (e) {
            e.preventDefault();
            var product = $(this).data('key').toString();
            bootbox.confirm({
                size: 'small',
                title: "ยืนยันลบรายการสินค้า",
                message: "คุณต้องการยืนยันการลบสินค้านี้ใช่หรือไม่",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> ยกเลิก',
                        className: 'btn-danger'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> ยืนยันการลบสินค้า',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    var loading = "";
                    if (result) {
                        $.ajax({
                            method: "POST",
                            url: "AppHttpRequest.php",
                            data: {req: '<?=REQ_DEL_PRODUCT;?>',productid: product},
                            dataType: "json",
                            beforeSend: function (xhr) {
                                loading = bootbox.dialog({
                                    size: 'small',
                                    message: '<p class="text-center">เรากำลังบันทึกข้อมูลร้านค้าของท่าน กรุณารอสักครู่...</p>',
                                    closeButton: false
                                });
                            },
                            error: function (transport, status, errorThrown) {
                                setTimeout(function () {
                                    loading.find('.bootbox-body').html("error : " + errorThrown + "detail : " + transport.responseText);
                                }, 3000);
                                //loading.modal('hide');
                                //bootbox.alert("error : " + errorThrown + "detail : " + transport.responseText);
                                //$this.attr('disabled', false);
                            },
                            success: function (data) {
                                loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                                if (data.MSGID === 100) {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                        tableProduct($('#search').val());
                                    }, 3000);
                                } else {
                                    setTimeout(function () {
                                        loading.modal('hide');
                                    }, 3000);
                                }
                            }
                        });

                    }

                }
            });
        });
});
</script>
<script>
    function tableProduct(search) {
        //alert(shop+month+year);

        $.ajax({
            type: "GET",
            url: "admin_product_table.php",
            //data: {search: search},
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log(transport.responseText);
            },
            success: function (data) {
                console.log(data);

                var show = "";

                show += '<table id="productTable" class="table table-vcenter table-condensed table-striped table-borderless">'
                show += '<thead>'
                show += '<tr>'
                show += '<th class="text-center" style="width: 100px;">รหัสสินค้า</th>'
                show += '<th>รุ่นสินค้า</th>'
                show += '<th class="text-center" style="width: 120px;">ราคาทุนขายปลึก</th>'
                show += '<th class="text-center" style="width: 120px;">ราคาทุนขายส่ง</th>'
                show += '<th class="text-center" style="width: 120px;">ราคาขายปลีก</th>'
                show += '<th class="text-center" style="width: 120px;">ราคาขายส่ง</th>'
                show += '<th class="no-sort" style="width: 150px;"></th>'
                show += '</tr>'
                show += '</thead>'
                show += '<tbody>'

                if (data.MSGID == '<?= SERV_COMPLETE; ?>') {
                    var i = 1;
                    data.MSGDATA1.forEach(function (value) {
                        var retail_price = parseFloat(value.PRODUCT_RETAIL_COST) + (parseFloat(value.PRODUCT_RETAIL_COST) * parseFloat(value.PRODUCT_RETAIL_PROFIT));
                        var wholesale_price = parseFloat(value.PRODUCT_WHOLE_COST) + (parseFloat(value.PRODUCT_WHOLE_COST) * parseFloat(value.PRODUCT_WHOLE_PROFIT));
                    show += '<tr>'
                    show += '<td class="text-center">'+value.PRODUCT_ID+'</td>'
                    show += '<td>'+value.PRODUCT_NAME+'</td>'
                    show += '<td class="text-right">'+value.PRODUCT_RETAIL_COST+'</td>'
                    show += '<td class="text-right">'+value.PRODUCT_WHOLE_COST+'</td>'
                    show += '<td class="text-right">'+retail_price+'</td>'
                    show += '<td class="text-right">'+wholesale_price+'</td>'
                    show += '<td class="text-right">'
                    var disabled = '';
                    if(value.PRODUCT_STATUS != '<?=PRODUCT_STATUS_ACTIVE;?>'){
                        disabled = 'disabled';
                    }
                    show += '<a href="Admin/Product/Edit/'+value.PRODUCT_ID+'/" data-toggle="tooltip" title="แก้ไขข้อมูลสินค้า" class="btn btn-effect-ripple btn-xs btn-warning '+disabled+'"><i class="gi gi-pencil"></i> แก้ไข</a>'
                    show += '<a href="javascript:void(0)" data-toggle="tooltip" title="ลบข้อมูลสินค้า" id="btnCancel" data-key="' + value.PRODUCT_ID + '" class="btn btn-effect-ripple btn-xs btn-danger '+disabled+'"><i class="gi gi-bin"></i> ลบ</a>'
                    show += '</td>'
                    show += '</tr>'
                    i++;
                });
                }
                show += '</tbody>'
                show += '</table>'

                $('#tableProduct').html(show);

                $.getScript('js/pages/uiTables.js', function () {
                    $(function () { UiTables.init(); });
                });

                $('#productTable').dataTable({
					ordering: true,
					info: false,
					searching: true,
					lengthChange: false,
					columnDefs: [
						{targets: 5, orderable: false}
					]
				})
            }
        });
    }
</script>

<?php include 'inc/template_end.php'; ?>