<?php
$pagename = "Product";
$subpagename = "Product";
?>
<?php
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
include './bundle.php';
$action = filter_input(INPUT_GET, 'action');
$product = new product();
$appm = new AppManager();
$req = REQ_ADD_PRODUCT;
$productid = "";
if ($action == ACTION_EDIT) {
    $productid = filter_input(INPUT_GET, 'id');
    $product = $appm->GetProductDetail($productid);
    $req = REQ_EDIT_PRODUCT;
    $readonly = " readonly";
    //service::printr($orderdetaillist);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <form name="tform1" id="tform1" method="POST" onsubmit="CreateProduct();return false;">
                <input type="hidden" name="req" value="<?= $req ?>"/>
                <input type="hidden" name="productid" value="<?= $productid; ?>">
                <div class="block full">
                    <div class="block-title">
                        <?= $_GET["action"] == "add" ? "เพิ่มข้อมูลสินค้า" : "แก้ไขข้อมูลสินค้า" ?> 
                        <a href="Admin/Product/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าสินค้าทั้งหมด</div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="form-group">
                                <label>ประเภทสินค้า <span style="color:red;">*</span> </label>
                                <select id="ddlprdtype" name="ddlprdtype" class="select-select2" style="width: 100%;" data-placeholder="ประเภทสินค้า" required="true">
                                    <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                    <?= $appm->GetProductTypeDropdownlist($product->PRODUCTTYPE_ID); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>ชื่อสินค้า <span style="color:red;">*</span> </label>
                                <input name="productname" id="productname" class="form-control" value="<?= $product->PRODUCT_NAME; ?>" placeholder="ชื่อสินค้า" required="true"/>
                            </div>
                            <div class="form-group">
                                <label>ชื่อรุ่น <span style="color:red;">*</span> </label>
                                <input name="modelid" id="modelid" class="form-control" value="<?= $product->MODELID; ?>" placeholder="ชื่อรุ่น" required="true"/>
                            </div>
                            <div class="form-group">
                                <label>ราคาขายปลีก (ต้นทุน) </label>
                                <div class="input-group text">
                                    <input type="text" id="retail_cost" name="retail_cost" class="form-control money" value="<?= $product->PRODUCT_RETAIL_COST; ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);" required="true">
                                    <span class="input-group-addon">บาท</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ราคาขายปลีก (% กำไร) </label>
                                <div class="input-group text">
                                    <input type="text" id="retail_profit" name="retail_profit" class="form-control money" value="<?= $product->PRODUCT_RETAIL_PROFIT * 100; ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);" required="true">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ราคาขายส่ง (ต้นทุน)</label>
                                <div class="input-group text">
                                    <input type="text" id="whole_cost" name="whole_cost" class="form-control money" value="<?= $product->PRODUCT_WHOLE_COST; ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);" required="true">
                                    <span class="input-group-addon">บาท</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ราคาขายส่ง (% กำไร)</label>
                                <div class="input-group text">
                                    <input type="text" id="whole_profit" name="whole_profit" class="form-control money" value="<?= $product->PRODUCT_WHOLE_PROFIT * 100; ?>" onkeypress="checknumber()" onkeyup="dokeyup(this);" required="true">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;">
                        <button type="submit"  id="btnSubmit"  data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึกรายการสั่งซื้อ" aria-describedby="tooltip441840"><i class="gi gi-floppy_disk"></i> บันทึก</button></div>
                </div>
            </form>

        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script>
    function CreateProduct() {
        if (!validateform.validate('tform1')) {
            bootbox.alert({
                size: 'small',
                message: "กรุณากรอกข้อมูลให้ครบถ้วน",
                title: "การแจ้งเตือน"
            });
            return false;
        }
        bootbox.confirm({
            size: 'small',
            title: "ยืนยันยอดขาย",
            message: "คุณยืนยันการเพิ่ม/แก้ไขสินค้านี้ใช่หรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ยกเลิก',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ยืนยันการเพิ่ม/แก้ไขสินค้า',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                var loading = "";
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "AppHttpRequest.php",
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            loading = bootbox.dialog({
                                size: 'small',
                                message: '<p class="text-center">เรากำลังบันทึกข้อมูลสินค้าของท่าน กรุณารอสักครู่...</p>',
                                closeButton: false
                            });
                        },
                        error: function (transport, status, errorThrown) {
                            setTimeout(function () {
                                loading.modal('hide');
                                bootbox.alert({
                                    size: 'small',
                                    message: "บันทึกข้อมูลสินค้าไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                    title: "การแจ้งเตือน"
                                });
                            }, 3000);
                            console.log(transport.responseText);
                        },
                        success: function (data) {
                            //loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                            if (data.MSGID === 100) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลสินค้าสำเร็จ",
                                        title: "การแจ้งเตือน",
                                        callback: function () {
                                            window.location = "Admin/Product/";
                                        }
                                    });
                                }, 3000);
                            } else {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลสินค้าไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            }
                        }
                    });
                }

            }
        });
    }
</script>
<?php include 'inc/template_end.php'; ?>