<?php
$pagename = "Product";
$subpagename = "Promotion";
?>
<?php
include './bundle.php';
include 'inc/config_admin.php';
$template['header_link'] = 'WELCOME';
$action = filter_input(INPUT_GET, 'action');
$appm = new AppManager();
$req = REQ_ADD_PROMOTION;
$sdate = date('Y-m-d');
$edate = date('Y-m-d');
$order_date = date('Y-m-d');
$promotionid = "";
$promotion = new promotion();
$promotionheader = new promotion();
if ($action == ACTION_EDIT) {
    $promotionid = filter_input(INPUT_GET, 'id');
    $promotionobject = $appm->GetPromotionDetail($promotionid);
    $promotionheader = $appm->GetPromotionHeader($promotionid, '', '')->MSGDATA1[0];
    $promotion = $promotionobject->MSGDATA1[0];
    $sdate = $promotionheader->PROMOTION_STARTDATE;
    $edate = $promotionheader->PROMOTION_ENDDATE;
    $req = REQ_EDIT_PROMOTION;
    //service::printr($orderdetaillist);
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <form name="tform1" method="post" id="tform1" onsubmit="lightBlock(); return false;">
                <div class="block full">
                    <div class="block-title">
                        <?= $_GET["action"] == "add" ? "เพิ่มโปรโมชั่น" : "แก้ไขโปรโมชั่น" ?> 
                        <a href="Admin/Promotion/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปหน้าสินค้าโปรโมชั่น</div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
                            <input type="hidden" name="req" value="<?= $req; ?>">
                            <input type="hidden" name="promotionid" value="<?= $promotionid; ?>">
                            <div class="form-group">
                                <label>ชื่อโปรโมชั่น</label>
                                <input type="text" id="name" name="name" class="form-control" value="<?= $promotionheader->PROMOTION_NAME; ?>" placeholder="ชื่อโปรโมชั่น">
                            </div>
                            <div class="form-group">
                                <label>รายละเอียด</label>
                                <textarea class="form-control" placeholder="รายละเอียดโปรโมชั่น" name="promotion_desc"><?= $promotionheader->PROMOTION_DESC; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>วันที่เริ่มโปรโมชั่น</label>
                                <input type="text" id="startdate" name="startdate" class="form-control input-datepicker" readonly="true" placeholder="เลือกวันที่เริ่มโปรโมชั่น">
                            </div>
                            <div class="form-group">
                                <label>วันที่สิ้นสุดโปรโมชั่น</label>
                                <input type="text" id="enddate" name="enddate" class="form-control input-datepicker" readonly="true" placeholder="เลือกวันที่สิ้นสุดโปรโมชั่น">
                            </div>
                            <div class="form-group">
                                <label>สินค้า</label>
                                <div class="orderTable">
                                    <table class="table table-vcenter table-condensed table-striped table-borderless">
                                        <thead>
                                            <tr>
                                                <th>รุ่นสินค้า</th>
                                                <th width="100px" class="text-center">ราคาขายปลีก</th>
                                                <th width="100px" class="text-center">% ส่วนลดปลีก</th>
                                                <th width="100px" class="text-center">ราคาปลีกสุทธิ</th>
                                                <th width="90px" class="text-center">ราคาส่ง</th>
                                                <th width="100px" class="text-center">% ส่วนลดส่ง</th>
                                                <th width="100px" class="text-center">ราคาส่งสุทธิ</th>
                                                <th width="25px"></th>
                                            </tr>										
                                        </thead>
                                        <tbody id="orderProduct">
                                            <?php
                                            if ($action == ACTION_EDIT) {
                                                $rowcnt = 0;
                                                $str = '';
                                                foreach ($promotion->PROMOTION_PRODUCT as $productdata) {
                                                    $product = new product();
                                                    $product = $productdata;
                                                    $str = '';
                                                    $rowcnt++;
                                                    $retail_discount = $product->PRODUCT_RETAIL_DISCOUNT * 100;
                                                    $whole_discount = $product->PRODUCT_WHOLE_DISCOUNT * 100;
                                                    //service::printr($orderdetail);
                                                    //$str .= '<input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="' . $rowcnt . '">';
                                                    $str .= '<tr id="row' . $rowcnt . '" class="tableRow">';
                                                    $str .= '<td>';
                                                    $str .= '<select id="product' . $rowcnt . '" name="product[]" data-row="' . $rowcnt . '" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า" data-live-search="true">';
                                                    $str .= $appm->GetProductDropdownlistForPromotion($product->PRODUCT_ID);
                                                    $str .= '</select>';
                                                    $str .= '</td>';
                                                    $str .= '<td align="right"><input id="retail_price' . $rowcnt . '" value="' . $product->PRODUCT_RETAIL_PRICE . '" class="form-control disabledText" disabled/></td>';
                                                    $str .= '<td><input class="form-control money" id="retail_discount' . $rowcnt . '" name="retail_discount[]" value="' . $retail_discount . '" onkeypress="checknumber()" onkeyup="dokeyup(this,"' . $rowcnt . '");" placeholder="% ลด"/></td>';
                                                    $str .= '<td align="right"><input id="retail_price_discount' . $rowcnt . '" value="' . $product->PRODUCT_RETAIL_DISCOUNT_PRICE. '" class="form-control disabledText" disabled/></td>';
                                                    $str .= '<td align="right"><input id="whole_price' . $rowcnt . '" value="' . $product->PRODUCT_WHOLE_PRICE . '" class="form-control disabledText" disabled/></td>';
                                                    $str .= '<td><input class="form-control money" id="whole_discount' . $rowcnt . '" name="whole_discount[]" value="' . $whole_discount . '" onkeypress="checknumber()" onkeyup="dokeyup(this,"' . $rowcnt . '");" placeholder="% ลด"/></td>';
                                                    $str .= '<td align="right"><input id="whole_price_discount' . $rowcnt . '" value="' . $product->PRODUCT_WHOLE_DISCOUNT_PRICE . '" class="form-control disabledText" disabled/></td>';
                                                    $str .= '<td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>';
                                                    $str .= '</tr>';

                                                    echo $str;
                                                }
                                                echo '<input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="' . $rowcnt . '">';
                                            } else {
                                                ?>
                                            <input type="hidden" name="rowOrdercnt" id="rowOrdercnt" value="1">
                                                <tr id="row1" class="tableRow">
                                                    <td>
                                                        <select id="product1" name="product[]"  data-row="1" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า">
                                                            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            <?= $appm->GetProductDropdownlistForPromotion(''); ?>
                                                        </select>
                                                    </td>
                                                    <td align="right"><input id="retail_price1" class="form-control disabledText" disabled/></td>
                                                    <td><input class="form-control money" id="retail_discount1" name="retail_discount[]" value="0" onkeypress="checknumber()" onkeyup="dokeyup(this);calculateRetail(this, 1);" placeholder="% ลด" readonly="true"/></td>
                                                    <td align="right"><input id="retail_price_discount1" class="form-control disabledText" disabled/></td>
                                                    <td align="right"><input id="whole_price1" class="form-control disabledText" disabled/></td>
                                                    <td><input class="form-control money" id="whole_discount1" name="whole_discount[]" value="0" onkeypress="checknumber()" onkeyup="dokeyup(this);calculateWhole(this, 1);" placeholder="% ลด" readonly="true"/></td>
                                                    <td align="right"><input id="whole_price_discount1" class="form-control disabledText" disabled/></td>
                                                    <td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td>
                                                </tr>
                                            <?php }
                                            ?>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5"><button type="button" class="btn btn-xs btn-primary" onClick="addField()"><i class="gi gi-circle_plus"></i> เพิ่มสินค้า</button></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;"><button type="submit"  id="btnSubmit"  data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึกรายการสั่งซื้อ" aria-describedby="tooltip441840"><i class="gi gi-floppy_disk"></i> บันทึก</button></div>
                </div>
            </form>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/uiTables.js"></script>
<script>$(function () {
                                                        UiTables.init();
                                                    });</script>

<script>
    $('#startdate').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        language: "th-th"
    });
    $('#enddate').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        language: "th-th"
    });
    moment.locale();
    $('#startdate').datepicker('update', new Date(moment('<?= $sdate; ?>').add(1, 'days')));
    $('#enddate').datepicker('update', new Date(moment('<?= $edate; ?>').add(1, 'days')));
</script>
<!-- Add Entry Form-->
<div id="lightBlock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-alert">
        <div class="modal-content">
            <form class="push">
                <div class="modal-header">
                    <div class="block-title">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <i class="gi gi-ok_2"></i> ยืนยันการสร้างรายการส่งเสริมการขาย
                    </div>
                </div>
                <div class="block modal-body">
                    <div class="form-group">
                        คุณต้องการยืนยันการสร้างรายการส่งเสริมการขายใช่หรือไม่
                    </div>
                    <div class="text-center hidden" id="loading_apr">
                        <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>
                    </div>
                    <span class="text-center hidden" id="ApproveResult"></span>
                </div>
                <div class="block modal-footer">
                    <div class="text-right btn-force">
                        <a href="javascript:void(0)" data-toggle="tooltip" title="ไม่อนุมัติการสั่งซื้อ" id="btnApprove" class="btn btn-effect-ripple btn-xs btn-success">ยืนยัน</a>
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-xs btn-danger" id="btnCancelApprove" data-dismiss="modal">ยกเลิก</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" >
    $(document).ready(function () {
        $('#btnApprove').on('click', function (e) {
            e.preventDefault();
            $('#loading_apr').removeClass('hidden');
            $('#btnApprove').addClass('disabled');
            //var data = {req: $('#req').val(), orderid: $('#orderid').val(), command: '<?= ORDER_STATUS_DONE; ?>'};
            SendData();
        });

        $('#lightBlock').on('hidden.bs.modal', function () {
            //window.location = "Admin/Promotion/";
        });
        
    });
    function renderprice(data) {
        //var selectedText = $(data).find("option:selected").text();
        var productid = data.value;
        var rowid = $(data).data('row');
        var retail_price = $(data).find("option:selected").data('rprice');
        var retail_discount = $(data).find("option:selected").data('rdiscount');
        var whole_price = $(data).find("option:selected").data('wprice');
        var whole_discount = $(data).find("option:selected").data('wdiscount');
        $('#retail_price' + rowid).val(retail_price);
        $('#whole_price' + rowid).val(whole_price);
        $('#retail_discount' + rowid).attr('readonly', false);
        $('#whole_discount' + rowid).attr('readonly', false);
    }

    function calculateRetail(data, rowid) {
        var discount = data.value;
        if (discount.length == 0) {
            data.value = 0;
            discount = 0;
        }
        discount = discount / 100;
        var price = $('#retail_price' + rowid).val();
        price = price - (discount * price);
        $('#retail_price_discount' + rowid).val(price.toFixed(2));
    }
    function calculateWhole(data, rowid) {
        var discount = data.value;
        if (discount.length == 0) {
            data.value = 0;
            discount = 0;
        }
        discount = discount / 100;
        var price = $('#whole_price' + rowid).val();
        price = price - (discount * price);
        $('#whole_price_discount' + rowid).val(price.toFixed(2));
    }



    function addField() {
        var cnt = $("#rowOrdercnt").val();
        cnt++;
        $("#rowOrdercnt").val(cnt);
        var opt = "<?= $appm->GetProductDropdownlistForPromotion(''); ?>";
        var appendrow = '<tr id="row' + cnt + '" class="tableRow">\n\
    <td><select id="product' + cnt + '" name="product[]"  data-row="' + cnt + '" onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า">\n\
<option></option>' + opt + '</select></td>\n\
<td align="right"><input id="retail_price' + cnt + '" class="form-control disabledText" disabled/></td>\n\
<td><input class="form-control money" id="retail_discount' + cnt + '" name="retail_discount[]" onkeypress="checknumber()" value="0" onkeyup="dokeyup(this);calculateRetail(this,' + cnt + ');" placeholder="% ลด" readonly="true"/></td>\n\
<td align="right"><input id="retail_price_discount' + cnt + '" class="form-control disabledText" disabled/></td>\n\
<td align="right"><input id="whole_price' + cnt + '" class="form-control disabledText" disabled/></td>\n\
<td><input class="form-control money" id="whole_discount' + cnt + '" name="whole_discount[]" onkeypress="checknumber()" value="0" onkeyup="dokeyup(this);calculateWhole(this,' + cnt + ');" placeholder="% ลด" readonly="true"/></td>\n\
<td align="right"><input id="whole_price_discount' + cnt + '" class="form-control disabledText" disabled/></td>\n\
<td><button type="button" class="btn btn-xs btn-danger remove-btn"><i class="gi gi-bin"></i></button></td></tr>';
        //$('<tr id="row' + cnt + '" class="tableRow"><td><select id="ddlproductOrder' + cnt + '" name="ddlproductOrder[]" data-row="' + cnt + '"  onchange="renderprice(this)" class="select-select2" style="width: 100%;" data-placeholder="ค้นหาสินค้า"  data-live-search="true"><option></option>' + opt + '</select></td><td align="right"><input id="txtPrice' + cnt + '" name="txtPrice[]" class="form-control disabledText" readonly="true"/></td><td><input type="text" id="txtQty' + cnt + '" name="txtQty[]" class="form-control" onkeyup="calculatePrice(this,' + cnt + ')" /></td><td align="right"><input id="txtTotal' + cnt + '" name="txtTotal[]" class="form-control disabledText" readonly="true"/></td><td><button class="btn btn-xs btn-danger remove-btn" ><i class="gi gi-bin"></i></button></td></tr>').appendTo($('#orderProduct'));
        $(appendrow).appendTo($('#orderProduct'));
        $.getScript('js/app.js');
    }
    $(document).on('click', '.remove-btn', function (events) {
        $(this).parents('tr').remove();
        if ($('#orderProduct').children('tr').length <= 0) {
            $("#rowOrdercnt").val(0);
        }
        //calculateSubtotal();
    });
    
    function RenderLightBlockModal(trg) {
        if (trg == 1) {
            $('#btnCancelApprove').text('ปิดหน้าต่างนี้');
            $('#loading_apr').addClass('hidden');
            $('#ApproveResult').removeClass('hidden');
        } else {
            $('#btnApprove').removeClass('disabled');
            $('#loading_apr').addClass('hidden');
            $('#ApproveResult').addClass('hidden');
            $('#btnCancelApprove').text('ยกเลิก');
        }
    }
    
    function lightBlock() {
        RenderLightBlockModal(0);
        window.location.hash = '#';
        $('#lightBlock').modal();
    }

    function SendData() {

        $.ajax({
            method: "POST",
            url: "AppHttpRequest.php",
            data: $('#tform1').serialize(),
            dataType: 'json',
            error: function (transport, status, errorThrown) {
                console.log("error : " + errorThrown + "detail : " + transport.responseText);
                setTimeout(function () {
                    RenderLightBlockModal(1);
                    $('#ApproveResult').empty().html(errorThrown);
                }, 3000);
            },
            success: function (data) {
                if (data.MSGID === 100) {
                    setTimeout(function () {
                        RenderLightBlockModal(1);
                        $('#ApproveResult').empty().html(data.MSGMESSAGE1);
                    }, 3000);
                } else {
                    setTimeout(function () {
                        RenderLightBlockModal(1);
                        $('#ApproveResult').empty().html(data.MSGMESSAGE1);
                    }, 3000);
                }
            }
        });

    }

</script>

<?php include 'inc/template_end.php'; ?>