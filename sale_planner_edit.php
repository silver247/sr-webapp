<?php $pagename = "Planner" ?>
<?php
include 'inc/config.php';
include_once 'bundle.php';
$template['header_link'] = 'WELCOME';
$appm = new AppManager();
$planid = filter_input(INPUT_GET, 'id');
$res = new response();
if (isset($_GET['date'])) {
    $res = $appm->GetMyPlanHeaderByPlanID($planid);
    //echo "test";
} else {
    header("Location: ../");
}
if ($res->MSGID == SERV_COMPLETE) {
    $plan = new plan();
    $plan = $res->MSGDATA1[0];
}
?>
<?php include 'inc/template_start.php'; ?>
<?php include 'inc/page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- First Row -->
    <div class="row">
        <!-- Simple Stats Widgets -->
        <div class="col-xs-12">
            <div class="block full">
                <div class="block-title">
                    แก้ไขการนัดหมาย 
                    <a href="Planner/<?= $_GET['date'] ?>/"><div class="block-title-right"><i class="fa fa-angle-double-left"></i> กลับไปตารางนัดหมาย</div></a>
                </div>
                <form name="tform1" method="post" id="tform1" onsubmit="EditMyPlan(); return false;">
                    <input type="hidden" name="req" id="hidDo" value="<?= REQ_EDIT_PLANHEADER; ?>">
                    <input type="hidden" name="planid" id="planid" value="<?= $planid; ?>">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="form-group">
                                <label>ชื่อร้าน</label>
                                <select id="shop" name="shop" class="select-select2" style="width: 100%;" data-placeholder="ร้านค้า">
                                    <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                    <?= $appm->GetCustomerDropdownlistWithValue($plan->CUSTOMER_ID); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>วันที่นัดหมาย</label>
                                <input type="text" id="plandate" name="plandate" class="form-control input-datepicker" value="" placeholder="วันที่นัดหมาย" readonly="true"/>
                            </div>
                            <div class="form-group">
                                <label>เวลานัดหมาย</label>
                                <div class="input-group bootstrap-timepicker">
                                    <div class="bootstrap-timepicker-widget dropdown-menu">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td><a href="#" data-action="incrementHour"><i class="fa fa-chevron-up"></i></a></td>
                                                    <td class="separator">&nbsp;</td>
                                                    <td><a href="#" data-action="incrementMinute"><i class="fa fa-chevron-up"></i></a></td>
                                                    <td class="separator">&nbsp;</td>
                                                    <td><a href="#" data-action="incrementSecond"><i class="fa fa-chevron-up"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" class="form-control bootstrap-timepicker-hour" maxlength="2"></td>
                                                    <td class="separator">:</td>
                                                    <td><input type="text" class="form-control bootstrap-timepicker-minute" maxlength="2"></td>
                                                    <td class="separator">:</td>
                                                    <td><input type="text" class="form-control bootstrap-timepicker-second" maxlength="2"></td>
                                                </tr>
                                                <tr>
                                                    <td><a href="#" data-action="decrementHour"><i class="fa fa-chevron-down"></i></a></td>
                                                    <td class="separator"></td>
                                                    <td><a href="#" data-action="decrementMinute"><i class="fa fa-chevron-down"></i></a></td>
                                                    <td class="separator">&nbsp;</td>
                                                    <td><a href="#" data-action="decrementSecond"><i class="fa fa-chevron-down"></i></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="text" id="plantime" name="plantime" readonly="true" value="<?= $plan->PLANTIME; ?>" class="form-control input-timepicker24">
                                    <span class="input-group-btn">
                                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary" style="overflow: hidden; position: relative;"><span class="btn-ripple animate" style="height: 38px; width: 38px; top: 3px; left: 5.375px;"></span><i class="fa fa-clock-o"></i></a>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>หมายเหตุ</label>
                                <textarea class="form-control" name="txtq3r2" placeholder="หมายเหตุ"><?= $plan->Q3_R2; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="btn-right" style="position: relative;"><button type="submit"  id="btnSubmit" data-toggle="tooltip" title="บันทึกการแก้ไข" class="btn btn-effect-ripple btn-xs btn-success" data-original-title="บันทึกการแก้ไข" aria-describedby="tooltip441840" ><i class="gi gi-circle_plus"></i> บันทึกการแก้ไข</button></div>
                </form>
            </div>
        </div>
        <!-- END Simple Stats Widgets -->
    </div>
    <!-- END First Row -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script>
    $('#plandate').datepicker({
        format: "yyyy-mm-dd",
        weekStart: 0,
        language: "th-th"
    });
    moment.locale();
    $('#plandate').datepicker('update', new Date(moment('<?= $plan->PLANDATE; ?>').add(1, 'days')));

    function EditMyPlan() {

        bootbox.confirm({
            size: 'small',
            title: "ยืนยันแก้ไขการนัดหมาย",
            message: "คุณต้องการยืนยันแก้ไขการนัดหมายใช่หรือไม่",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ยกเลิก',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ยืนยันการแก้ไข',
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                var loading = "";
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "AppHttpRequest.php",
                        data: $("#tform1").serialize(),
                        dataType: "json",
                        beforeSend: function (xhr) {
                            loading = bootbox.dialog({
                                size: 'small',
                                message: '<p class="text-center">เรากำลังบันทึกข้อมูลการนัดหมายของท่าน กรุณารอสักครู่...</p>',
                                closeButton: false
                            });
                        },
                        error: function (transport, status, errorThrown) {
                            console.log("error : " + errorThrown + " detail : " + transport.responseText);
                            setTimeout(function () {
                                loading.find('.bootbox-body').html("error : " + errorThrown + "detail : " + transport.responseText);

                            }, 3000);
                            setTimeout(function () {
                                loading.modal('hide');
                            }, 3000);
                            //loading.modal('hide');
                            //bootbox.alert("error : " + errorThrown + "detail : " + transport.responseText);
                            //$this.attr('disabled', false);
                        },
                        success: function (data) {
                            loading.find('.bootbox-body').html(data.MSGMESSAGE1);
                            if (data.MSGID === 100) {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลการนัดหมายสำเร็จ",
                                        title: "การแจ้งเตือน",
                                        callback: function () {
                                            window.location = "Planner/<?=$date;?>/";
                                        }
                                    });
                                }, 3000);
                            } else {
                                setTimeout(function () {
                                    loading.modal('hide');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "บันทึกข้อมูลการนัดหมายไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ",
                                        title: "การแจ้งเตือน"
                                    });
                                }, 3000);
                            }
                        }
                    });

                }

            }
        });
    }
</script>

<?php include 'inc/template_end.php'; ?>