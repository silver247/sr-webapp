<?php require_once('functions/convertdate.php'); ?>
<?php require_once('functions/format.php'); ?>

<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $template['title'] ?></title>

        <base href="http://<?= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] ?>/<?=$template['site_root'];?>" />
        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
        <meta name="robots" content="<?php echo $template['robots'] ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Icons -->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Theme bootstrap-table-->
        <link rel='stylesheet prefetch' href='css/bootstrap-table.css'>
        <link rel='stylesheet prefetch' href='css/bootstrap-editable.css'>
        <!-- END bootstrap-table -->

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- The stylesheet of this template edit by pongja -->
        <link rel="stylesheet" href="css/app.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->
        <?php if ($template['theme']) { ?><link rel="stylesheet" href="css/themes/<?php echo $template['theme']; ?>.css" id="theme-link"><?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-3.3.1.min.js"></script>

        <script src="js/vendor/jquery-3.2.1.min.js"></script>

        <?php
        $monthNameTH = array("01" => "มกราคม", "02" => "กุมภาพันธ์", "03" => "มีนาคม", "04" => "เมษายน", "05" => "พฤษภาคม", "06" => "มิถุนายน", "07" => "กรกฎาคม", "08" => "สิงหาคม", "09" => "กันยายน", "10" => "ตุลาคม", "11" => "พฤศจิกายน", "12" => "ธันวาคม");
        $monthNameTHSH = array("01" => "ม.ค.", "02" => "ก.พ.", "03" => "มี.ค.", "04" => "เม.ย.", "05" => "พ.ค.", "06" => "มิ.ย.", "07" => "ก.ค.", "08" => "ส.ค.", "09" => "ก.ย.", "10" => "ต.ค.", "11" => "พ.ย.", "12" => "ธ.ค.");

        $nowDMY = date('d m Y');
        $nowDMYex = explode(" ", $nowDMY);
        $nowD = $nowDMYex[0];
        $nowMTH = $monthNameTH[$nowDMYex[1]];
        $nowMTHSH = $monthNameTHSH[$nowDMYex[1]];
        $nowYTH = $nowDMYex[2] + 543;
        $nowDMYTHSH = $nowD . " " . $nowMTHSH . " " . $nowYTH;
        $last1MY = date('m Y', strtotime('-1 months'));
        $last1MYex = explode(" ", $last1MY);
        $last1M = $monthNameTH[$last1MYex[0]];
        $last1Y = $last1MYex[1] + 543;
        $last2MY = date('m Y', strtotime('-2 months'));
        $last2MYex = explode(" ", $last2MY);
        $last2M = $monthNameTH[$last2MYex[0]];
        $last2Y = $last2MYex[1] + 543;

        $nowMYTH = $nowMTH . "<br/>" . $nowYTH;
        $last1MYTH = $last1M . "<br/>" . $last1Y;
        $last2MYTH = $last2M . "<br/>" . $last2Y;
        ?>

    </head>
    <body>