<!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
<script>
    var req_success = "<?=SERV_COMPLETE;?>";
</script>
<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/bootbox.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>
<script src="js/app_custom.js"></script>
<script src="js/vendor/moment.js"></script>

<!-- jQuery, Bootstrap Date -->
<script src="datepicker/js/bootstrap-datepicker.js"></script>
<script src="datepicker/js/bootstrap-datepicker-thai.js"></script>
<script src="datepicker/js/locales/bootstrap-datepicker.th.js"></script>
<script src="plugins/chartjs/Chart.bundle.min.js"></script>