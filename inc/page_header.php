<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<header class="navbar<?php
if ($template['header_navbar']) {
    echo ' ' . $template['header_navbar'];
}
?><?php
if ($template['header']) {
    echo ' ' . $template['header'];
}
?>">
    <!-- Left Header Navigation -->
    <?php /* ?>
      <ul class="nav navbar-nav-custom">
      <!-- Main Sidebar Toggle Button -->
      <li>
      <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
      <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
      <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
      </a>
      </li>
      <!-- END Main Sidebar Toggle Button -->
      </ul>
      <?php */ ?>    
    <!-- END Left Header Navigation -->

    <!-- Right Header Navigation -->
    <ul class="nav navbar-nav-custom pull-right">
        <!-- User Dropdown -->
        <li class="header-username">
            <strong>คุณ<?=$_SESSION['thname'];?></strong><br/>
            <?=$_SESSION['roledesc'];?>
        </li>
        <li class="header-user">
            <img src="img/icon_menu/user.png" alt="user">
        </li>
        <li class="header-logout">
            <a href="logout.php" style="padding: 0 0 0 0;line-height: 20px;">
                <img src="img/icon_menu/logout.png" alt="logout">
                <div style="font-size:16px">ออกจากระบบ</div>
            </a>

        </li>
        <!-- END User Dropdown -->
    </ul>
    <!-- END Right Header Navigation -->
</header>
<!-- END Header -->
