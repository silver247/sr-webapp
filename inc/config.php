<?php
$mainpage = 'Sales';
//echo $_SERVER['SCRIPT_NAME'];

if (strpos($_SERVER['SCRIPT_NAME'], 'index.php') === false && strpos($_SERVER['SCRIPT_NAME'], 'change_password.php') === false){
    include './modules/pagecontrol.php';
    Init($mainpage);
}
/* Template variables */
$template = array(
    'name' => 'AppUI',
    'version' => '1.0',
    'author' => '',
    'robots' => 'noindex, nofollow',
    'title' => '',
    'description' => '',
    // true                         enable page preloader
    // false                        disable page preloader
    'page_preloader' => false,
    // 'navbar-default'             for a light header
    // 'navbar-inverse'             for a dark header
    'header_navbar' => 'navbar-inverse',
    // ''                           empty for a static header/main sidebar
    // 'navbar-fixed-top'           for a top fixed header/sidebars
    // 'navbar-fixed-bottom'        for a bottom fixed header/sidebars
    'header' => 'navbar-fixed-top',
    // ''                           empty for the default full width layout
    // 'fixed-width'                for a fixed width layout (can only be used with a static header/main sidebar)
    'layout' => '',
    // 'sidebar-visible-lg-mini'    main sidebar condensed - Mini Navigation (> 991px)
    // 'sidebar-visible-lg-full'    main sidebar full - Full Navigation (> 991px)
    // 'sidebar-alt-visible-lg'     alternative sidebar visible by default (> 991px) (You can add it along with another class - leaving a space between)
    // 'sidebar-light'              for a light main sidebar (You can add it along with another class - leaving a space between)
    'sidebar' => 'sidebar-visible-lg-mini',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies' => '',
    // '', 'classy', 'social', 'flat', 'amethyst', 'creme', 'passion'
    'theme' => '',
    // Used as the text for the header link - You can set a value in each page if you like to enable it in the header
    'header_link' => '',
    // The name of the files in the inc/ folder to be included in page_head.php - Can be changed per page if you
    // would like to have a different file included (eg a different alternative sidebar)
    'inc_sidebar' => 'page_sidebar',
    'inc_sidebar_alt' => 'page_sidebar_alt',
    'inc_header' => 'page_header',
    // The following variable is used for setting the active link in the sidebar menu
    'active_page' => basename($_SERVER['PHP_SELF']),
    'site_root' => 'srwebapp/'
    // Config Application Root Path
);

/* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */
$primary_nav = array(
    array(
        'name' => '<img src="img/icon_menu/home.png"/><br/>หน้าแรก',
        'url' => 'Home/',
        'pagename' => 'Home'
    ),
    array(
        'name' => '<img src="img/icon_menu/plan.png"/><br/>ตารางนัดหมาย',
        'url' => 'Planner/',
        'pagename' => 'Planner'
    ),
    array(
        'name' => '<img src="img/icon_menu/order.png"/><br/>รายการสั่งซื้อ',
        'url' => 'Order/',
        'pagename' => 'Order'
    ),
    array(
        'name' => '<img src="img/icon_menu/sumary.png"/><br/>สรุปยอดขาย',
        'url' => 'Summary/',
        'pagename' => 'Summary'
    ),
    array(
        'name' => '<img src="img/icon_menu/customer.png"/><br/>ลูกค้า',
        'url' => 'Customer/',
        'pagename' => 'Customer'
    )
);
?>